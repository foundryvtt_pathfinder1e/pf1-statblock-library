import { extractPack, compilePack } from "@foundryvtt/foundryvtt-cli";
// import { promises as fs } from "fs";
// import path from "path";
// import PackGroup from "./lib/packgroup.js";
import url from "node:url";
import yargs from "yargs";

import yaml from "js-yaml";
import fs from "fs-extra";
import path from "node:path";

import * as utils from "./utils.mjs";
import { getActionDefaultData, getChangeDefaultData } from "./pack-default-data.mjs";

const __dirname = url.fileURLToPath(new URL(".", import.meta.url));
const __filename = url.fileURLToPath(import.meta.url);
const templateData = loadDocumentTemplates();
const manifest = loadManifest();

/**
 * Loads the system manifest file.
 *
 * @returns {object} The system manifest file as an object.
 */
function loadManifest() {
  return fs.readJsonSync(path.resolve(__dirname, "../module.json"));
}

// Only handle commands if this script was executed directly
console.log("argv is ", process.argv);
if (process.argv[1] === __filename || process.argv[2] === "pack") {
    yargs(process.argv.slice(2))
      .demandCommand(1, 1)
      .command({
        command: "pack",
        describe: `Compile all packs (or a pack) into ldb file(s)`,
        handler: async (argv) => {
          await compilePacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      .command({
        command: "unpack",
        describe: `Extract all packs (or a pack) into source JSONs`,
        handler: async (argv) => {
          await extractPacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      .command({
        command: "prune",
        describe: `Prune all packs (or a pack)`,
        handler: async (argv) => {
          await prunePacks(argv.folder ?? process.argv[3] ?? null);
        },
      })
      // Option to overwrite the default `reset` option
      .option("folder", { describe: "Work with a specific pack", type: "string" })
      .parse();
  }

async function extractPacks(pack) {
    const packs = pack ? [pack] : await fs.readdir(path.join("release", "packs"));
    for (const pack of packs) {
        if (pack === ".gitattributes") continue;
        console.log("Unpacking " + pack);
        const dest = path.join("src", pack);
        const src = path.join("release", "packs", pack);

        // Find associated manifest pack data
        const packData = manifest.packs.find((p) => {
          // console.log("p is ", p, "base name is ", path.basename(p.path));
          if (p.path) return path.basename(p.path) === `${pack}.db`;
          else return p.name === pack;
        });
        if (!packData) console.warn(`No data found for package ${pack} within the module manifest.`);

        await extractPack(src, dest, {
          yaml: true,
          log: true,
          // clean: true, 
          transformEntry : (entry) => sanitizePackEntry(entry, packData?.type),
        });
    }
}

async function compilePacks(pack) {
    const packs = pack ? [pack] : await fs.readdir(path.join("src"));
    for (const pack of packs) {
        if (pack === ".gitattributes") continue;
        console.log("Packing " + pack);
        const src = path.join("src", pack);
        const dest = path.join("release", "packs", pack);
        await compilePack(src, dest, { yaml: true, log: true});
    }
}

async function prunePacks(pack) {
    const folders = pack ? [pack] : await fs.readdir(path.join("src"));

    const start = performance.now();
    let lastReport = start;

    folders.forEach(async (folder) => {
      // Find associated manifest pack data
      const packData = manifest.packs.find((p) => {
        // console.log("p is ", p, "base name is ", path.basename(p.path));
        if (p.path) return path.basename(p.path) === `${folder}.db`;
        else return p.name === folder;
      });
      if (!packData) console.warn(`No data found for package ${folder} within the module manifest.`);

      console.log("Pruning folder:", folder);
      const files = await fs.readdir(path.join("src", folder));
      
      files.forEach((file) => {
        // Read in and load the file as YAML data
        const data =  fs.readFileSync(path.join("src", folder, file), 'utf8');
        let yamlData = yaml.load(data);

        // Sanitize the data and dump it back to YAML data
        yamlData = yaml.dump(sanitizePackEntry(yamlData, packData?.type));

        // Write the file back to disk
        fs.writeFileSync(path.join("src", folder, file), yamlData, 'utf8');

        // Report processing state every few seconds
        const now = performance.now();
        if ((now - lastReport) > 15_000) {
            lastReport = now;
            const currentDocTime = now - startCurrent;
            console.log(i+1, "/", files.length, ":", yamlData.name, `[${Math.round(currentDocTime)}ms]`);
        }
      });
    });

    const end = performance.now();
    console.log("All packs processed in", Math.round(end - start), "ms");
}

/**
 * Arrays of dot paths exempt from data trimming; `system.` is implied, as only system data is trimmed.
 * This should include paths to any objects that can contain arbitrary (i.e. not in template) properties.
 */
const TEMPLATE_EXCEPTION_PATHS = {
  Actor: [],
  Item: [
    "classSkills",
    "uses.autoDeductChargesCost",
    "uses.rechargeFormula",
    "links.supplements",
    "flags",
    "casting",
    "learnedAt",
    "properties",
    "source",
    "items",
    "ammo",
  ],
  Component: [],
};

/**
 * Loads the document templates file.
 *
 * @returns {object} The document templates object, merged with their respective templates.
 */
function loadDocumentTemplates() {
  const templates = fs.readJsonSync(path.resolve(__dirname, "./template.json"));

  for (const doc of Object.values(templates)) {
    if (doc.types) delete doc.types;

    for (const [k, v] of Object.entries(doc)) {
      if (k === "templates") continue;

      if (v.templates instanceof Array) {
        for (const templateKey of v.templates) {
          doc[k] = utils.mergeObject(v, doc.templates?.[templateKey] ?? {});
        }
        delete v.templates;
      }
    }

    if (doc.templates) delete doc.templates;
  }

  return templates;
}

/**
 * Santize pack entry.
 *
 * This resets an entry's permissions to default and removes all non-pf1 flags.
 *
 * @param {object} entry Loaded compendium content.
 * @param {string} [documentType] The document type of the entry, determining which data is scrubbed.
 * @returns {object} The sanitized content.
 */
function sanitizePackEntry(entry, documentType = "") {
  // Delete unwanted fields
  delete entry.ownership;
  delete entry._stats;
  if ("effects" in entry && entry.effects.length === 0) delete entry.effects;

  // Ignore folders; not present on inventoryItems
  if (entry._key?.startsWith("!folders")) return entry;

  // Remove non-system/non-core flags
  for (const key of Object.keys(entry.flags ?? {})) {
    if (utils.isEmpty(entry.flags[key])) delete entry.flags[key];
    else if (!["pf1", "core"].includes(key)) delete entry.flags[key];
  }
  if (utils.isEmpty(entry.flags)) delete entry.flags;

  // Remove top-level keys not part of Foundry's core data model
  // For usual documents, this is enforced by Foundry. For inventoy items, it is not.
  // const allowedCoreFields = ["name", "type", "img", "data", "flags", "items", "system", "_id", "_key", "folder"];
  // if (["Actor", "Item"].includes(documentType)) {
  //   for (const key of Object.keys(entry)) {
  //     if (!allowedCoreFields.includes(key)) delete entry[key];
  //   }
  // }

  // Remove folders anyway if null
  if (entry.folder === null) delete entry.folder;

  // Adhere to template data
  // if (templateData) {
  //   const systemData = entry.system ?? entry.data;
  //   const template = templateData[documentType]?.[entry.type];
  //   if (systemData && template) {
  //     entry.system = enforceTemplate(systemData, template, {
  //       documentName: documentType,
  //       type: entry.type,
  //     });
  //   }
  //   if (documentType === "Actor" && entry.items?.length > 0) {
  //     // Treat embedded items like normal items for sanitization
  //     entry.items = entry.items.map((i) => sanitizePackEntry(i, "Item"));
  //   }
  //   if (documentType === "Item" && entry.system.items && Object.keys(entry.system.items).length > 0) {
  //     // Treat embedded items like normal items for sanitization
  //     for (const [itemId, itemData] of Object.entries(entry.system.items)) {
  //       entry.system.items[itemId] = sanitizePackEntry(itemData, "Item");
  //     }
  //   }
  // }
  return entry;
}

/**
 * Enforce a template on an object.
 *
 * @param {object} object - The data object to be trimmed
 * @param {object} template - The template to enforce
 * @param {object} [options={}] - Additional options to augment the behavior.
 * @param {"Actor" | "Item" | "Component"} [options.documentName] - The document(-like) name to which this template belongs.
 * @param {"Action" | "Change"} [options.componentName] - The component name to which this template belongs.
 * @param {string} [options.type] - The document type of the object, if it is not already present.
 * @returns {object} A data object which has been trimmed to match the template
 */
function enforceTemplate(object, template, options = {}) {
  // Do not enforce templates on documents which do not have them
  if (!object || !template || !["Actor", "Item", "Component"].includes(options.documentName)) return object;

  // Create a diff of the object and template to remove all default values
  const diff = utils.diffObject(template, object);
  const flattened = utils.flattenObject(diff);
  for (const path of Object.keys(flattened)) {
    // Delete additional properties unless in template or in the exception list
    // ... but remove exceptions anyway if they're null or empty string.
    const inTemplate = utils.hasProperty(template, path);
    const isExempt =
      options.documentName &&
      TEMPLATE_EXCEPTION_PATHS[options.documentName].some((exceptionPath) => path.startsWith(exceptionPath));

    const value = flattened[path];
    if (!inTemplate && (!isExempt || (isExempt && (value === "" || value === null)))) {
      delete flattened[path];
    }

    // Delete null values if template has empty string
    const currentValue = utils.getProperty(object, path);
    const templateValue = utils.getProperty(template, path);
    if (templateValue === "" && currentValue === null) delete flattened[path];

    const templateHasArray = Array.isArray(utils.getProperty(template, path));
    const isEmptyArray = flattened[path] instanceof Array && flattened[path].length === 0;
    if (templateHasArray && isEmptyArray) {
      delete flattened[path];
    }
  }

  /* -------------------------------------------- */
  /*  Handling special cases/cleanup              */
  /* -------------------------------------------- */
  for (const path of Object.keys(flattened)) {
    // Delete erroneous keys containing paths to delete
    if (path.includes(".-=")) {
      delete flattened[path];
    }

    // Item cleanup
    if (options.documentName === "Item") {
      // Delete abundant when set to false
      if (flattened["flags.pf1.abundant"] === false) {
        delete flattened["flags.pf1.abundant"];
      }

      // Delete ammo type when empty
      if (!flattened["system.ammo.type"]) {
        delete flattened["system.ammo.type"];
      }

      // Delete non-set class skills
      if (path.startsWith("classSkills.") && flattened[path] === false) {
        delete flattened[path];
      }

      // Delete non-set properties in weapons
      if (options.type === "weapon" && path.startsWith("properties.") && flattened[path] === false) {
        delete flattened[path];
      }
    }
  }

  /* -------------------------------------------- */
  /*  Handling components                         */
  /* -------------------------------------------- */
  if ("actions" in flattened && Array.isArray(flattened.actions)) {
    const defaultData = getActionDefaultData();
    flattened.actions = flattened.actions.map((action) =>
      enforceTemplate(action, defaultData, { documentName: "Component", componentName: "Action" })
    );
  }
  if ("changes" in flattened && Array.isArray(flattened.changes)) {
    const defaultData = getChangeDefaultData();
    flattened.changes = flattened.changes.map((change) =>
      enforceTemplate(change, defaultData, { documentName: "Component", componentName: "Change" })
    );
  }

  return utils.expandObject(flattened);
}