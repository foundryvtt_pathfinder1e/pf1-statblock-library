name: Mythic Ice Devil (CR 16)
_id: 72vpRagPPIUB7kVD
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: 4j7uiJvlX6yfmp7V
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!72vpRagPPIUB7kVD.4j7uiJvlX6yfmp7V'
  - name: Mythic Ice Devil (CR 16)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Mythic Adventures</p><section
        id="MythicIceDevil"><div class="heading"><p
        class="alignright"><strong>CR</strong> 16/MR 6</p></div><div><p><b>XP
        </b>76,800</p><p>LE Large outsider (devil, evil, extraplanar, lawful,
        mythic)</p><p><b>Init </b>+15M; <b>Senses </b>all-around vision,
        darkvision 60 ft., see in darkness; Perception +27</p><p><b>Aura
        </b>fear (10 ft., DC 24)</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>38, touch 14,
        flat-footed 33 (+5 Dex, +24 natural, -1 size)</p><p><b>hp </b>221
        (14d10+144); regeneration 5 (good spells, good weapons)</p><p><b>Fort
        </b>+15, <b>Ref </b>+14, <b>Will </b>+12</p><p><b>Defensive Abilities
        </b>cold logic; <b>DR </b>10/epic and good; <b>Immune </b>cold, fire,
        poison; <b>Resist </b>acid 10; <b>SR </b>27</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>40 ft., fly
        60 ft. (good), ice burrowing 30 ft.</p><p><b>Melee </b><i><i>+2 frost
        spear</i></i> +23/+18/+13 (2d6+12/x3 plus 1d6 cold plus ice shards plus
        <i>slow</i>), bite +15 (2d6+3), tail slap +15 (1d8+3 plus
        <i>slow</i>)</p><p><b>Space </b>10 ft.; <b>Reach </b>10
        ft.</p><p><b>Special Attacks </b>entrap (DC 23, 1d10 rounds, hardness 5,
        hp 10), favored enemy +6, mythic power (6/day, surge +1d8),
        pounce</p><p><b>Spell-Like Abilities</b> (CL 13th; concentration +20) 
        <br />Constant—<i>fly</i> <br />At Will—<i>cone of cold</i> (DC 22),
        <i>ice storm</i>, <i>greater teleport</i> (self plus 50 lbs. of objects
        only), <i>persistent image</i> (DC 22), <i><i>wall of</i> ice</i> (DC
        21) <br />1/day—summon (level 4, 2 bone devils 50%)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>25, <b>Dex
        </b>21, <b>Con </b>22, <b>Int </b> 25, <b>Wis </b>22, <b>Cha
        </b>24</p><p><b>Base Atk </b>+14; <b>CMB </b>+22; <b>CMD
        </b>37</p><p><b>Feats </b>Alertness, Cleave, Combat ReflexesM, Improved
        InitiativeM, Iron WillM, Power Attack, Weapon Focus
        (spear)</p><p><b>Skills </b>Acrobatics +22, Bluff +24, Diplomacy +24,
        Fly +13, Intimidate +21, Knowledge (planes) +24, Knowledge (any three
        others) +21, Perception +27, Sense Motive +27, Spellcraft +21, Stealth
        +18, Survival +23</p><p><b>Languages </b>Celestial, Common, Draconic,
        Infernal; telepathy 100 ft.</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        (Hell)</p><p><b>Organization </b>solitary, team (2-3), council (4-10),
        or contingent (1-3 ice devils, 2-6 horned devils, and 1-4 bone
        devils)</p><p><b>Treasure </b>standard (<i>+2 frost spear</i>, other
        treasure)</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Cold Logic (Su)</b> When a mythic ice devil's spell
        resistance protects it from a mind-affecting effect, it can expend one
        use of mythic power as an immediate action to turn that effect upon its
        source, as if using <i>spell turning</i>.  </p><p><b>Favored Enemy
        (Ex)</b> A mythic ice devil can expend one use of mythic power to gain a
        +6 favored enemy bonus against one type of creature for 1 hour, as if it
        were a 14th-level ranger.  </p><p><b>Ice Burrowing (Ex)</b> This ability
        works like the burrow ability, but only through ice and snow (including
        magical ice such as a <i><i>wall of</i> ice</i>).  </p><p><b>Ice Shards
        (Su)</b> A mythic ice devil's ice-tipped spear embeds jagged shards of
        supernaturally cold ice in its target. This functions as bleed (1d6),
        except the damage is cold damage. Dealing 5 or more points of fire
        damage to the target removes all ice shards. Creatures with the fire
        subtype are immune to this ability.  </p><p><b>Slow (Su)</b> A hit from
        a mythic ice devil's tail or spear induces numbing cold. The opponent
        must succeed at a DC 23 Fortitude save or be affected as though by a
        <i>slow</i> spell for 1d6 rounds. This effect comes from the devil, not
        its weapon; it is not a quality possessed by the spear itself. The save
        DC is Constitution-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A mythic ice
        devil is a master of strategy, able to adapt to the nature of its
        opponents and turn its enemy's attacks back at them. Its lean form and
        upright posture sets it apart from the stockier non-mythic ice
        devils.</p></h4></div></section>
    _id: fEazHA6EdXf6lkON
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!72vpRagPPIUB7kVD.fEazHA6EdXf6lkON'
sort: 0
_key: '!journal!72vpRagPPIUB7kVD'

