name: Evaluator Robot
_id: 2wUOP44xt4pT6yDe
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Iron_Gods.jpg
    title:
      show: false
      level: 1
    _id: 8peg3gwTQJfCkIzE
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!2wUOP44xt4pT6yDe.8peg3gwTQJfCkIzE'
  - name: Evaluator Robot
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 90</p><section id="EvaluatorRobot"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        12</p></div><div><p><strong>XP </strong>19,200</p><p>N Medium construct
        (robot)</p><p><strong>Init </strong>+9; <strong>Senses
        </strong>darkvision 60 ft., low-light vision, superior optics;
        Perception +18</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>27, touch 15, flat-footed 22 (+5 Dex, +12
        natural)</p><p><strong>hp </strong>158 (16d10+20 plus 50 hp force
        field)</p><p><strong>Fort </strong>+5, <strong>Ref </strong>+10,
        <strong>Will </strong>+10</p><p><strong>Defensive Abilities
        </strong>hardness 10; <strong>Immune </strong>construct
        traits</p><p><strong>Weaknesses </strong>vulnerable to critical hits and
        electricity</p></div><hr /><div><p><strong>OFFENSE</strong></p></div><hr
        /><div><p><strong>Spd </strong>50 ft., fly 120 ft.
        (perfect)</p><p><strong>Melee </strong>bastard sword +22/+17/+12/+7
        (1d10+6/19-20 plus stun)  or 2 slams +22 (1d4+6 plus
        stun)</p><p><strong>Ranged </strong>integrated laser rifle +21 ranged
        touch (4d6 fire)</p><p><strong>Space </strong>5 ft.; <strong>Reach
        </strong>5 ft.</p><p><strong>Special Attacks </strong>memory wipe, stun
        (DC 19, 1d4 rounds)</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>22, <strong>Dex </strong>21, <strong>Con </strong>-,
        <strong>Int </strong> 12, <strong>Wis </strong>17, <strong>Cha
        </strong>1</p><p><strong>Base Atk </strong>+16; <strong>CMB
        </strong>+22; <strong>CMD </strong>37</p><p><strong>Feats
        </strong>Blind-Fight, Cleave, Flyby Attack, Improved Initiative, Iron
        Will, Power Attack, Skill Focus (Sense Motive), Vital
        Strike</p><p><strong>Skills </strong>Fly +17, Knowledge (local) +12,
        Perception +18, Sense Motive +25, Stealth +10</p><p><strong>Languages
        </strong>Androffan, Common; process languages</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        (Numeria)</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Force Field (Ex)</strong> An evaluator robot is
        sheathed in a thin layer of shimmering energy that grants it 50 bonus
        hit points. All damage dealt to an evaluator robot with an active force
        field is deducted from these hit points first. As long as the force
        field is active, the evaluator robot is immune to critical hits. An
        evaluator robot's force field has fast healing 10, but once these hit
        points are reduced to 0, the force field shuts down and does not
        reactivate for 24 hours.  </p><p><strong>Integrated Laser Rifle
        (Ex)</strong> An evaluator robot has a built-in laser rifle. This weapon
        has a range of 150 feet and deals 4d6 points of fire damage on a hit.
        The weapon can fire once per round as a ranged touch attack. A laser
        attack can pass through force fields and force effects, such as a
        <i>wall of force</i>, to strike a foe beyond without damaging that
        field. Objects like glass or other transparent barriers don't provide
        cover from lasers, but unlike force barriers, a transparent physical
        barrier still takes damage when a laser passes through it. Invisible
        creatures and objects are immune to damage from lasers. Fog, smoke, and
        other  clouds provide cover in addition to concealment from laser
        attacks. Darkness (magical or otherwise) has no effect on lasers other
        than providing concealment.  </p><p><strong>Memory Wipe (Ex)</strong> As
        a standard action, an evaluator robot can make a touch attack that, if
        it hits, injects nanites into a target and erases the last 12 hours of
        its memories. A successful DC 19 Will save negates this effect. This is
        a mind-affecting effect, and the save DC is Intelligence-based. 
        </p><p><strong>Process Languages (Ex)</strong> Exceptional processing
        and data stores allows an evaluator robot to parse language in a way
        that lets it permanently speak and understand any spoken or written
        language it observes for at least 1 minute.  </p><p><strong>Stun
        (Ex)</strong> An evaluator robot's melee attacks deliver a nonlethal
        jolt of electricity with each strike. If the robot strikes a creature
        twice in one round with its bastard sword or slam attack, that target
        must succeed at a DC 19 Fortitude save or be stunned for 1d4 rounds. The
        save DC is Intelligence-based.  </p><p><strong>Superior Optics
        (Ex)</strong> An evaluator robot can see invisible creatures or objects
        as if they were visible.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Androffans,
        the race responsible for the presence of robots on Golarion, were a
        spacefaring people who visited dozens of worlds. Masters of engineering,
        they created robots that could perform a wide array of tasks, even
        trusting their creations to perform complex surgeries on the Androffans'
        own organic forms. In the course of their interplanetary explorations,
        the Androffans quickly learned that not every world was ready to
        comprehend the awesome experience of leaving one's home planet to visit
        others. Not wanting to risk valuable crew members, the Androffans
        created evaluator robots as an alternative to sending a shuttle mission
        to alien planets and interacting in person. When sensors aboard
        Androffan ships orbiting foreign worlds discovered other humanoid
        species, evaluator robots would be dispatched to assess the planets'
        alien cultures.  Taking forms designed to be recognizable to a planet's
        general populace, evaluator robots would drop from orbiting surveillance
        ships onto alien worlds to collect data so that their masters could
        determine the readiness of the planets' inhabitants to accept the
        existence of creatures from other worlds. Androffans also used evaluator
        robots to determine how violent or superstitious the indigenous
        populations were so that they could carefully plan direct contact.
        Evaluator robots were fashioned into pleasing and majestic forms to
        command respect and admiration from the humanoids they interacted with,
        and were usually made slightly taller than the planet's primary race so
        as to seem properly impressive. These robots' advanced construction
        utilizes sophisticated lightweight materials, making their durable
        frames weigh in under 400 pounds. The model presented here is the most
        common design-a radiant humanoid angel with gleaming feathers of brushed
        metal.    <br /><strong>Design</strong><br />  The engineers who
        designed evaluator robots created each one to serve specific purposes
        tailored to the world it was intended to observe. These thematic designs
        were tested through trial and error by the evaluators themselves, who
        paid close attention to the religions encountered on each planet. They
        discovered that reverence for angelic beings was common on many worlds
        populated by sentient humanoids, so the engineers built the majority of
        evaluator robots in this form.  Although some evaluators were designed
        for use on worlds that used advanced technology, evaluators were rarely
        used to observe cultures that had already reached space on their own or
        that were otherwise accustomed to dealing with beings from other worlds.
        In some cases, Androffan engineers designed evaluator robots to mimic
        the form of beings iconic to the populace, gathering information about
        these individuals from intercepted communication signals. This was often
        judged too risky, however, as it was found that cultures rarely
        responded well to the eventual revelation that their resurrected heroes
        and messiahs were actually alien robots.  Even though redesigning an
        evaluator robot's outward form was simple, their complex programming
        required a vast amount of collected data. If the robot couldn't reply in
        a convincing manner, or if its memory-modifying nanites took no hold on
        the alien humanoids it encountered, then the evaluator's mission was
        quickly compromised. The robot's creators then had to hope that the
        interaction would be interpreted as a fluke supernatural experience, or
        that the witness would be disbelieved and derided by its community. 
        Evaluator robots are universally curious, their programming filling them
        with an endless need to learn about living creatures. These robots take
        every opportunity to ask questions of intelligent organic life forms,
        especially humanoids. An evaluator robot compares these answers to
        profiles of existing cultures, either installed when it was created or
        assembled from previous interactions. Complex algorithms capable of
        parsing the nuances of myriad humanoid societies allow the robot to
        quickly evaluate and categorize the nature of a particular society using
        these hundreds or thousands of profiles. Once an evaluator has
        thoroughly assessed a planet's cultures, its Androffan masters use this
        information to plan a landing expedition to make direct contact. 
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        As robots, evaluators have no society outside of their programming, and
        their habitat is wherever they happen to be deployed. In the Inner Sea
        region, nearly all evaluator robots encountered to date have been found
        in Silver Mount.  Since they were designed to interact with sentient
        organic beings, evaluator robots don't regularly associate with  other
        robots unless tasked with a project requiring such interaction, and
        instead keep the company of humanoids (though their cold robotic
        presence isn't always comforting).  Crafts that split from
        <i>Divinity</i> during its crash and fell to other parts of Numeria
        contained two other known evaluator robots. A tribe of Ghost Wolves
        destroyed one of these evaluators-designed to resemble a horned,
        red-skinned fiend-when it appeared before the Kellid barbarians in the
        Sellen Hills. The robot fought with wicked claws, and after it was
        destroyed, it exploded in a blast that killed dozens of the barbarians.
        The other evaluator, reportedly appearing as a six-armed, three-faced
        woman with bronze skin, was spotted brief ly by griffon riders at Castle
        Urion before it headed south into the River Kingdoms, and hasn't been
        seen since.</p></h4></div></section>
    _id: GTC4EDJNyoi81CKk
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!2wUOP44xt4pT6yDe.GTC4EDJNyoi81CKk'
sort: 0
_key: '!journal!2wUOP44xt4pT6yDe'

