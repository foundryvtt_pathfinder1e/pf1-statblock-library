name: Kevoth-Kul
_id: myABtChyhE1M49ar
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Iron_Gods.jpg
    title:
      show: false
      level: 1
    _id: 1fAmiCvtJF4cQ0sH
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!myABtChyhE1M49ar.1fAmiCvtJF4cQ0sH'
  - name: Kevoth-Kul
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 89</p><section id="KevothKul"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        14</p></div><div><p><strong>XP </strong>38,400</p><p>Male human
        barbarian 15</p><p>CN Medium humanoid (human)</p><p><strong>Init
        </strong>+9; <strong>Senses </strong>Perception +12</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>20, touch 14, flat-footed 15 (+6 armor, +1 deflection, +4 Dex,
        +1 dodge, -2 rage)</p><p><strong>hp </strong>253
        (15d12+150)</p><p><strong>Fort </strong>+19, <strong>Ref </strong>+12,
        <strong>Will </strong>+9; +4 vs. enchantments when raging, -2 against
        mind-affecting effects</p><p><strong>Defensive Abilities
        </strong>improved uncanny dodge, indomitable will, trap sense +5;
        <strong>DR </strong>4/-</p><p><strong>Weaknesses
        </strong>addicted</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong><i><i>+3 adamantine furious
        greatsword</i></i> +24/+19/+14 (2d6+12/17-20)</p><p><strong>Ranged
        </strong><i><i>+1 returning throwing axe</i></i> +21
        (1d6+7)</p><p><strong>Space </strong>5 ft.; <strong>Reach </strong>5
        ft.</p><p><strong>Special Attacks </strong>greater rage (37 rounds/day),
        rage powers (increased damage reduction +1, intimidating glare,
        knockback, powerful blow +4, rolling dodge +3)</p></div><hr
        /><div><p><strong>TACTICS</strong></p></div><hr /><div><p><strong>Before
        Combat </strong>Kevoth-Kul ingests a dose of <i>Numeria</i>n fluids
        before combat (see Addicted).</p><p><strong>During Combat
        </strong>Kevoth-Kul uses his magical throwing axe to fight at range, but
        vastly prefers to fight in melee with his greatsword-a devastating
        weapon that grows more powerful when he rages. He uses Vital Strike on
        rounds when he must close with a target, but when he makes a full
        attack, he uses Power Attack to gain a +12 bonus on damage at the cost
        of a -4 penalty on attack rolls. His first attack in a round is always a
        sunder attempt, as he knows that a foe deprived of a weapon is easier to
        defeat.</p><p><strong>Morale </strong>While raging, Kevoth-Kul fights to
        the death. The Black Sovereign is a proud and arrogant man, but he has
        also grown tired of his role as puppet leader of Starfall. If he is
        captured alive (or reduced to 15 or fewer hit points while not raging),
        he throws down his weapons and demands in an uncharacteristically weary
        voice for his victors to finish him off. He is not interested in
        becoming a prisoner of yet another group, and if he gets the impression
        that the PCs want to keep him alive, he'll attack again, hoping to earn
        a final death in combat. If cured of his addiction to <i>Numeria</i>n  
        fluids, though, his reaction is much different-see the Campaign Role
        section on the next page for more details.</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>22, <strong>Dex </strong>20, <strong>Con </strong>26,
        <strong>Int </strong> 11, <strong>Wis </strong>9, <strong>Cha
        </strong>14</p><p><strong>Base Atk </strong>+15; <strong>CMB
        </strong>+21 (+25 sunder); <strong>CMD </strong>36 (38 vs.
        sunder)</p><p><strong>Feats </strong>Dodge, Greater Sunder, Improved
        Critical (greatsword), Improved Initiative, Improved Sunder, Improved
        Vital Strike, Power Attack, Toughness, Vital Strike</p><p><strong>Skills
        </strong>Climb +17, Intimidate +20, Perception +12, Ride +16, Sense
        Motive -6, Survival +17, Swim +17</p><p><strong>Languages
        </strong>Common, Hallit</p><p><strong>SQ </strong>fast movement, side
        effects</p><p><strong>Combat Gear</strong> <i>Numeria</i>n fluidsNLFS
        (7); <strong>Other Gear </strong><i>+2 hide armor</i>, <i>+1 adamantine
        furious greatsword</i>, <i>belt of incredible dexterity +4</i>, <i>cloak
        of resistance +2</i>, <i>ring of protection +1</i>, key to area
        C43</p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Addicted (Ex)</strong>
        Kevoth-Kul has been an addict of the strange <i>Numeria</i>n fluids
        provided to him by the Technic League for many years. The Black
        Sovereign's long addiction to these fluids has resulted in a tolerance
        to their effects, but he's constantly in a state of mild delirium and
        takes a -5 penalty on Perception checks and Sense Motive checks, as well
        as a -2 penalty on saving throws against mind-affecting effects. Curing
        his addiction is a difficult task because of the length of time he's
        spent addicted to the fluids. His current addiction is severe, with a
        save DC of 40 due to his continued exposure to the drugs. <i>Remove
        disease</i> can cure his addiction with a successful DC 40 caster level
        check. <i>Heal</i> automatically cures his addiction. If cured of his
        addiction, the Black Sovereign loses the penalties detailed above, along
        with the current side effects he's under-he doesn't lose his bonuses to
        Strength, Dexterity, or Constitution or his immortality, though, as
        these are permanent effects (see Side Effects, below). If cured of his
        addiction, the Black Sovereign may become a potent ally of the PCs
        against the Technic League- or he might immediately attack them-see
        Campaign Role below to determine his reaction to being cured. 
        </p><p><strong>Side Effects (Ex)</strong> Over time, the Black
        Sovereign's exposure to <i>Numeria</i>n fluids has resulted in a
        permanent +2 increase to his Strength, Dexterity, and Constitution
        scores. Perhaps more impressively, the fluids have granted him
        immortality: Kevoth-Kul takes no penalties from aging effects and won't
        die from old age. Each time the PCs encounter the Black  Sovereign, roll
        randomly on the <i>Numeria</i>n Fluids table on page 29 of <i>Pathfinder
        Campaign Setting</i>: <i>Numeria</i>, <i>Land of Fallen Stars</i> to
        determine what additional effect the man is experiencing at that
        particular time, re-rolling results of 01 or 100. If you aren't using
        that book, instead roll d%. On a result of 01-75, Kevoth-Kul is
        sickened; on a result of 76-100, he gains fast healing
        5.</p></div></section>
    _id: WOENQy65QozA1eOW
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!myABtChyhE1M49ar.WOENQy65QozA1eOW'
sort: 0
_key: '!journal!myABtChyhE1M49ar'

