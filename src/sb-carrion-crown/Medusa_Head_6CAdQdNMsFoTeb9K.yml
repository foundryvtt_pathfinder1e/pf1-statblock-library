name: Medusa Head
_id: 6CAdQdNMsFoTeb9K
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Carrion_Crown.jpg
    title:
      show: false
      level: 1
    _id: Z9LzheiK1tbnLpFw
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!6CAdQdNMsFoTeb9K.Z9LzheiK1tbnLpFw'
  - name: Medusa Head
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 43</p><section id="MedusaHead"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        3</p></div><div><p><b>XP </b>800</p><p>NE Small undead </p><p><b>Init
        </b>+1; <b>Senses </b>darkvision 60 ft.; Perception +0</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>15, touch 12,
        flat-footed 14 (+1 Dex, +3 natural, +1 size)</p><p><b>hp </b>30
        (4d8+12)</p><p><b>Fort </b>+3, <b>Ref </b>+2, <b>Will
        </b>+4</p><p><b>Immune </b>undead traits</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>fly 40 ft.
        (perfect)</p><p><b>Melee </b>bite +6 (1d4+2 plus petrifying
        bite)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>14, <b>Dex
        </b>12, <b>Con </b>-, <b>Int </b> -, <b>Wis </b>11, <b>Cha
        </b>15</p><p><b>Base Atk </b>+3; <b>CMB </b>+4; <b>CMD </b>15 (can't be
        tripped)</p><p><b>Feats </b>Flyby AttackB</p><p><b>Languages
        </b>Common</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary,
        pair, or trio</p><p><b>Treasure </b>none</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Petrifying
        Bite (Su)</b> Creatures bitten by a medusa head must make a DC 14
        Fortitude save or turn to stone for 1d4 rounds. Targets immune to poison
        are immune to this effect. The save DC is Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Beheaded are
        floating skulls or severed heads whose bodies have long since abandoned
        them, either in the moment of death or long after. Reanimated via dark
        magic, these horrors are usually created as mindless sentinels for
        dungeons or lairs. </p><p>Beheaded silently hover at about eye-level,
        often making them the last thing a casual wanderer or careless villager
        in an infested area ever sees.</p><p><b>Creating Beheaded
        </b>Spellcasters might create and employ beheaded in multiple
        ways.</p><p><b>Animating Beheaded:</b> A magic-user can create any form
        of beheaded from a severed head of the appropriate size or creature. The
        spellcaster must cast the spell <i>animate dead</i> using an onyx gem
        worth at least 100 gp, followed by the spell <i>fly</i> on the head to
        be animated. The creator can only create a number of Hit Dice of
        beheaded equal to the amount allowed by <i>animate dead</i>.
        </p><p>Beheaded with the variant abilities below can also be created,
        though for the purposes of how many can be created, they are treated as
        if they had one additional Hit Die for each additional ability.
        </p><p>Beheaded count against the number of Hit Dice of skeletons or
        zombies that can be created using <i>animate dead</i> and vise
        versa.</p><p><b>Beheaded Familiars:</b> Using the Improved Familiar
        feat, a spellcaster can gain an obedient severed head as a
        familiar.</p><p>The character must be at least caster level 3rd to
        acquire the severed head. This creature is always neutral evil. Although
        any type of magic-user with the proper requisites can take a severed
        head as a familiar, necromancers and undead wizards employ them most
        frequently.</p><p><b>Variant Beheaded </b><br />The following are
        variant abilities that a beheaded might possess. These traits can be
        mixed and matched in any way and applied to any of the beheaded listed
        above. Each ability increases the CR of the beheaded by the listed
        amount.</p><p><b>Belching </b>(+1 CR): A belching beheaded gains the
        ability to spew raw energy from its mouth, giving it a ranged touch
        attack that does 1d6 damage of a specific type (acid, cold, electric, or
        fire) chosen at the time of the beheaded's animation.</p><p><b>Burning
        </b>(+1 CR): This beheaded is similar to the flaming skull; however, the
        fire not only surrounds the skull, but can pass on to those it attacks.
        The fire that consumes the head can be any color the animator chooses,
        though blue is one of the most common. A burning skull gains the burn
        (1d6) special ability when using its slam attack, where the Reflex save
        DC is 10 + 1/2 the burning skull's racial HD + the burning skull's Cha
        modifier.</p><p><b>Grabbing </b>(+0 CR): This type of beheaded has long
        tendrils of ragged hair. It gains the grab special ability when using
        its slam attack, and can attempt to grapple any creature of Medium size
        or smaller in this way </p><p><b>Screaming </b>(+0 CR): Once every 1d4
        rounds, a screaming beheaded can open its jaw and emit a bone-chilling
        scream. All creatures within 30 feet must make a Will save or be shaken
        for 1d4 rounds. The save DC is equal to 10 + 1/2 the screaming skull's
        racial HD + the screaming skull's Cha modifier. This is a sonic
        mind-affecting fear effect. Whether or not the save is successful, an
        affected creature is immune to the same beheaded's scream for 24 hours.
        </p></h4></div></section>
    _id: MsQGipiLGvsQGOzD
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!6CAdQdNMsFoTeb9K.MsQGipiLGvsQGOzD'
sort: 0
_key: '!journal!6CAdQdNMsFoTeb9K'

