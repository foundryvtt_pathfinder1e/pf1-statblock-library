name: Cauldron Bloom
_id: rrkDRlVQu1soorkh
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Tyrants_Grasp.jpg
    title:
      show: false
      level: 1
    _id: 13GXLSRatdaU6zCt
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!rrkDRlVQu1soorkh.13GXLSRatdaU6zCt'
  - name: Cauldron Bloom
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 142</p><section id="CauldronBloom"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        15</p></div><div><p><strong>XP </strong>51,200</p><p>N Colossal plant
        </p><p><strong>Init </strong>+6; <strong>Senses </strong>blindsight 120
        ft., low-light vision, tremorsense 1 mi., <i>true seeing</i>; Perception
        +33</p></div><hr /><div><p><strong>DEFENSE</strong></p></div><hr
        /><div><p><strong>AC </strong>28, touch 4, flat-footed 26 (+2 Dex, +24
        natural, -8 size)</p><p><strong>hp </strong>231
        (22d8+132)</p><p><strong>Fort </strong>+21, <strong>Ref </strong>+11,
        <strong>Will </strong>+11</p><p><strong>Immune </strong>plant traits;
        <strong>SR </strong>26</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>10 ft., burrow 10 ft.</p><p><strong>Melee </strong>3 slams +24
        (2d8+16)</p><p><strong>Space </strong>30 ft.; <strong>Reach </strong>30
        ft.</p><p><strong>Special Attacks </strong>sanguine centrifuge (2d8+16
        plus 3d6 bleed, AC 22, hp 27; DC 23), warping field (DC
        23)</p><p><strong>Spell-Like Abilities</strong> (CL 22nd; concentration
        +24) <br />1/day—<i>greater teleport</i> (self plus stomach contents
        only; see text)</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>42, <strong>Dex </strong>15, <strong>Con </strong>27,
        <strong>Int </strong> 15, <strong>Wis </strong>14, <strong>Cha
        </strong>14</p><p><strong>Base Atk </strong>+16; <strong>CMB
        </strong>+40 (+44 bull rush); <strong>CMD </strong>52 (54 vs. bull
        rush)</p><p><strong>Feats </strong>Awesome Blow, Cleave, Combat
        Expertise, Great Cleave, Greater Bull Rush, Improved Bull Rush, Improved
        Initiative, Iron Will, Lightning Reflexes, Power Attack, Skill Focus
        (Perception)</p><p><strong>Skills </strong>Handle Animal +13, Perception
        +33, Perform (percussion) +13, Stealth +11 (+31 while underground),
        Survival +24; <strong>Racial Modifiers </strong>+20 Stealth while
        underground</p><p><strong>Languages </strong>Sylvan, Terran (can't
        speak)</p><p><strong>SQ </strong>terraforming teleport</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any cold
        land</p><p><strong>Organization </strong>solitary</p><p><strong>Treasure
        </strong>standard</p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Sanguine Centrifuge
        (Su)</strong> Creatures that enter, or begin their turn within, the
        cauldron bloom's stomach take 2d8+16 points of bludgeoning damage plus
        3d6 bleed as spinning arcane currents force the blood from their bodies.
        This bleed cannot be stopped while the creature is within the cauldron
        bloom's stomach. Corpses of creatures slain by this damage are torn
        apart, as <i>disintegrate</i>. Furthermore, creatures within the stomach
        must succeed at a DC 23 Reflex save to maintain control while caught in
        the currents or be nauseated for  1 round; creatures with freedom of
        movement are still affected but gain a +4 bonus on the saving throw. The
        save DC is Charisma-based. This otherwise functions as the swallow whole
        ability, with the following changes: a cauldron bloom can simultaneously
        hold up to one Gargantuan creature, two Huge creatures, four Large
        creatures, and so forth; swallowed creatures can use any of their
        available attacks to cut themselves free; and after a swallowed creature
        has cut itself free, the cauldron bloom can continue to teleport
        creatures into its stomach with its warping field ability, but it
        receives a cumulative -2 penalty to its ability DCs for each time a
        creature has cut its way out.  </p><p><strong>Terraforming Teleport
        (Sp)</strong> A cauldron bloom's <i>greater teleport</i> spell-like
        ability has a range of 1 mile, and the destination must be within enough
        stone, soil, sand, or similar material to surround the cauldron bloom's
        body. The material the cauldron bloom will occupy is in turn teleported
        to backfill the cavity left by the creature's departure. If a cauldron
        bloom attempts to teleport to an invalid location, the ability fails
        with no effect.  </p><p><strong>Warping Field (Su)</strong> A cauldron
        bloom draws prey close with invisible arcane strands, which it projects
        in one of two forms: diffuse or targeted. Projecting diffuse strands is
        a swift action, and forces each creature within 120 feet of the cauldron
        bloom to attempt a DC 23 Reflex save. A creature that fails the save is
        teleported 60 feet toward the cauldron bloom; if this would bring a
        creature or object within 30 feet of the cauldron bloom, it is instead
        teleported into the cauldron bloom's stomach. Projecting targeted
        strands is a standard action and forces one creature within 360 feet to
        attempt a DC 23 Fortitude save. If the creature fails, it is teleported
        into the cauldron bloom's stomach. Targets must be at least one size
        category smaller than the cauldron bloom, and this ability attempts to
        teleport the closest creature first. A creature cannot be teleported
        into the cauldron bloom if it would not fit, as noted above. Creatures
        affected by effects that block extradimensional travel, such as
        <i>dimensional lock</i>, are immune to this ability. Both save DCs are
        Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Cauldron
        blooms are immense, slow-moving subterranean plants that maintain their
        arcane metabolisms by consuming massive quantities of blood. They dwell
        in cold environs, typically along the migration routes of herd animals,
        and slowly teleport to new locations in search of food. Feeding cauldron
        blooms generally erupt from the ground in the midst of such herds. The
        animals' panicked cries are cut short when the terrified individuals
        wink out of existence, reappearing a moment later inside the cauldron
        bloom's otherworldly guts. Arcane winds within the cauldron bloom's
        stomach rapidly draw the blood from ingested prey, shred their bodies,
        and leave nothing behind. A particularly hungry cauldron bloom can
        systematically consume entire herds of animals, or  sequester one
        individual-such as an unlucky giant or megafauna-inside its stomach. The
        prey's blood fuels the plant's magical physiology, casting flickering
        arcane light as it distributes throughout the cauldron bloom's body. 
        Typically, cauldron blooms live for hundreds of years. Their bodies are
        around 15 feet in diameter and weigh upward of 80 tons. 
        <strong></strong></p><p><strong>Ecology</strong></p><p> Cauldron blooms
        do not feed in a traditional sense. Instead, they wrap potential prey in
        invisible threads of magical energy. Snared creatures are teleported
        directly into the blooms' stomachs, where interior winds swirl in a
        roaring arcane centrifuge that forcibly extracts prey's blood and
        quickly renders any mundane organic material to a fine powder. Equipment
        and magical oddities are left behind, however, meaning clouds of
        manufactured shrapnel circulate within most older cauldron blooms. 
        Cauldron blooms most often dwell in cold regions. A consistent diet of
        blood is necessary for them to maintain a healthy and elevated internal
        temperature. They use their innate teleportation abilities to jump
        between subterranean points in pursuit of herding prey. Cauldron blooms
        give little sign of their presence while beneath the ground, and their
        senses are highly attuned to surface vibrations. 
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        Curiously, despite their warm-blooded nature and massive size, cauldron
        blooms don't exude heat. This allows them to go largely undetected
        between feedings. Their petals are also dexterous and quiet enough that
        their slow burrowing doesn't alert surface-dwellers. Intelligent,
        meticulous planners, most of these giant plants use their stealth to
        actively cultivate herds of prey animals. Cauldron blooms may divert
        water, manage predator populations, and engage in similar protective
        activities to ensure a long-term food supply.  The plants' unique method
        of locomotion means their habitats are pocked with  incongruous
        subsurface materials. A marsh, for instance, may have a stand-alone
        deposit of dense stone, and a few settlements are even founded on such
        locations. A dead cauldron bloom, however, may be a hazard to the local
        environment, as its decay slowly turns the area into a giant sinkhole; a
        hole above a particularly ancient cauldron bloom eventually reveals a
        treasure trove of collected objects within the dead creature's gut. 
        Their heightened intellects mean that cauldron blooms are generally
        quite knowledgeable about local terrain, fauna, and even historical
        events, though such information is presented through the unique filter
        of a subterranean carnivorous plant. One historical record even
        describes a cauldron bloom with heightened telekinetic abilities working
        alongside smaller beings to build and protect an entire city; the
        report's veracity is the subject of ongoing and lively academic
        debate.</p></h4></div></section>
    _id: wGhK1pYrebTwz1UE
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!rrkDRlVQu1soorkh.wGhK1pYrebTwz1UE'
sort: 0
_key: '!journal!rrkDRlVQu1soorkh'

