name: Chelarac
_id: FduRumWJqGkM4ERj
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Tyrants_Grasp.jpg
    title:
      show: false
      level: 1
    _id: FPgy98sXH7SQThZP
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FduRumWJqGkM4ERj.FPgy98sXH7SQThZP'
  - name: Chelarac
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 144</p><section id="Chelarac"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        8</p></div><div><p><strong>XP </strong>4,800</p><p>NE Large aberration
        </p><p><strong>Init </strong>+8; <strong>Senses </strong>darkvision 60
        ft.; Perception +22</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>22, touch 14, flat-footed 17 (+4 Dex, +1 dodge, +8 natural, -1
        size)</p><p><strong>hp </strong>114 (12d8+60)</p><p><strong>Fort
        </strong>+9, <strong>Ref </strong>+8, <strong>Will
        </strong>+11</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft., climb 30 ft.</p><p><strong>Melee </strong>bite +15
        (1d10+10 plus poison and siphon knowledge)</p><p><strong>Space
        </strong>10 ft.; <strong>Reach </strong>5 ft.</p><p><strong>Special
        Attacks </strong>release brood</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>25, <strong>Dex </strong>18, <strong>Con </strong>20,
        <strong>Int </strong> 19, <strong>Wis </strong>17, <strong>Cha
        </strong>16</p><p><strong>Base Atk </strong>+9; <strong>CMB
        </strong>+17; <strong>CMD </strong>32</p><p><strong>Feats
        </strong>Alertness, Combat Expertise, Dodge, Improved Feint, Improved
        Initiative, Mobility</p><p><strong>Skills </strong>Bluff +12, Climb +15,
        Diplomacy +12, Knowledge (arcana) +10, Knowledge (dungeoneering) +10,
        Knowledge (history) +10, Knowledge (local) +13, Knowledge (nature) +10,
        Knowledge (planes) +10, Knowledge (religion) +10, Perception +22, Sense
        Motive +19, Stealth +15</p><p><strong>Languages </strong>Aklo, Common,
        Undercommon; broodspeak</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        underground</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure
        </strong>incidental</p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Broodspeak
        (Su)</strong> A chelarac can communicate telepathically with its
        broodlings at a range of 1,000 feet.  </p><p><strong>Poison
        (Ex)</strong> Bite-injury; <i>save</i> Fort DC 21; <i>frequency</i>
        1/round for 6 rounds; <i>effect</i> 1d6 damage and 1 Int damage;
        <i>cure</i> 1 <i>save</i>.  </p><p><strong>Release Brood (Ex)</strong>
        If a chelarac takes more than 15 points of damage from an attack that
        deals piercing or slashing damage, one of the blisters on its abdomen
        pops, releasing a chelarac broodling. Or as a full-round action, a
        chelarac can intentionally burst one of the blisters and release a
        broodling. A chelarac can release only up to 1d6 broodlings each day. 
        </p><p><strong>Siphon Knowledge (Su)</strong> A chelarac can drain
        knowledge from its victims. Any intelligent creature that takes damage
        from a chelarac's bite must succeed at a DC 19 Will save or have 1d6
        hours of its memory absorbed by the chelarac. The target creature still
        retains its memories, but it might recall some of the details
        incorrectly. The save DC is Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Chelaracs
        appear as enormous spiders that have a human head with spiderlike
        mouthparts. They are usually a greenish-yellow hue, and their abdomen is
        covered in dozens of fluid-filled sacs in which their young gestate. A
        typical chelarac stands 7 feet tall, has a leg span of 10 feet, and
        weighs over 800 pounds.  Chelaracs are dangerous monsters that become
        even more dangerous as the fight goes on. Thankfully, these reclusive
        creatures live in isolated lairs underground or within ruins far from
        regular foot traffic. Some adventurers initially mistake chelaracs for
        araneas, often underestimating their danger.  These creatures can siphon
        memories from their victims, and they use this stolen knowledge to gain
        a better understanding of the outside world. Since they are typically
        encountered by explorers and adventurers, chelaracs obtain exciting and
        esoteric lore from their victims. Chelaracs  are also long-lived, their
        life spans sometimes lasting for thousands of years. Taken together,
        this means that chelaracs can become living libraries of obscure lore
        and valuable information that would otherwise be lost to the march of
        time. Some creatures seek out chelaracs in hopes of learning from them;
        few chelaracs welcome strangers into their lairs, so interested parties
        must bargain with the monsters for their secrets. These bargains usually
        involve the stranger voluntarily subjecting herself to the chelarac's
        bite so that the monster can siphon her memories in exchange for the
        sought-after lore.  Perhaps the most disturbing thing about chelaracs is
        the blister-like sacs that protrude from their abdomens, which hold
        their wriggling young. Chelaracs reproduce through parthenogenesis and
        thus do not require another chelarac to mate. Creepy enough by
        themselves, chelarac broodlings are only more terrifying in that their
        humanlike faces match the faces of victims from whom their parent has
        siphoned knowledge. The memories siphoned from victims are passed down
        to the young chelaracs as well. This effect can happen immediately,
        resulting in disturbing encounters with a chelarac. A victim of the
        chelarac's siphoning might be surprised to see a young chelarac emerge
        with his own face or that of someone the victim knows. In addition, when
        young chelaracs emerge they babble stolen memories, making for an
        uncomfortable and potentially embarrassing encounter with the brood.  A
        chelarac can choose when to release a broodling and can keep one in its
        brood sac for an indefinite amount of time. A broodling grows to its
        young stage and then remains in stasis until the chelarac hatches it or
        the sac is ruptured. Young chelaracs rarely spend more than a day with
        their parent before scuttling off into the darkness to find their own
        lair. Broodlings grow at a surprising rate, reaching full adulthood in a
        matter of months. Newly hatched broodlings are less reticent to share
        their stored memories, and strangers can more easily ask these young
        monsters for information.  An obscure text written by a Darklands
        alchemist includes a formula to make a substance that can be combined
        with the blood of a chelarac. The text claims that if the alchemist
        mixes this substance with the chelarac's blood and drinks the resulting
        concoction, the drinker is able to access a wealth of memories that were
        siphoned and stored in the monster's mind.</p></h4></div></section>
    _id: avj4wqkZkPLhYN26
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!FduRumWJqGkM4ERj.avj4wqkZkPLhYN26'
sort: 0
_key: '!journal!FduRumWJqGkM4ERj'

