name: Graveknight (fighter 10)
_id: mWvMyjZbVzCqy5Dw
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/undead.png
    title:
      show: false
      level: 1
    _id: 7VoKkMeBjZe6q9aw
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!mWvMyjZbVzCqy5Dw.7VoKkMeBjZe6q9aw'
  - name: Graveknight (fighter 10)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 3</p><section
        id="Graveknight"><div class="heading"><p
        class="alignright"><strong>CR</strong> 11</p></div><div><p><b>XP
        </b>12,800</p><p>Human graveknight fighter 10</p><p>LE Medium undead
        (augmented humanoid)</p><p><b>Init </b>+5; <b>Senses </b>darkvision 60
        ft.; Perception +19</p><p><b>Aura </b>sacrilegious aura (30 ft., DC
        19)</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC
        </b>25, touch 11, flat-footed 24 (+10 armor, +1 Dex, +4
        natural)</p><p><b>hp </b>139 (10d10+80)</p><p><b>Fort </b>+13, <b>Ref
        </b>+6, <b>Will </b>+6; +3 vs. fear</p><p><b>Defensive Abilities
        </b>bravery +3, channel resistance +4, rejuvenation; <b>DR </b>10/magic;
        <b>Immune </b>acid, cold, electricity, undead traits; <b>SR
        </b>22</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>30 ft.</p><p><b>Melee </b><i><i>+1
        greatsword</i></i> +25/+20 (2d6+19 plus 2d6 acid)</p><p><b>Ranged
        </b>composite longbow +14/+9 (1d8+11/x3)</p><p><b>Space </b>5 ft.;
        <b>Reach </b>5 ft.</p><p><b>Special Attacks </b>channel destruction,
        devastating blast (6d6 acid, DC 19), undead mastery (DC 19), weapon
        training (heavy blades +2, bows +1)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>27, <b>Dex
        </b>12, <b>Con </b>10, <b>Int </b> 15, <b>Wis </b>12, <b>Cha
        </b>18</p><p><b>Base Atk </b>+10; <b>CMB </b>+20; <b>CMD
        </b>29</p><p><b>Feats </b>Cleave, Critical Focus, Dazzling Display,
        Greater Weapon Focus (greatsword), Improved Initiative, Mounted Combat,
        Power Attack, Ride-By Attack, Shatter Defenses, Spirited Charge,
        Toughness, Trample, Unseat, Vital Strike, Weapon Focus (greatsword),
        Weapon Specialization (greatsword)</p><p><b>Skills </b>Climb +13,
        Intimidate +25, Knowledge (nobility) +12, Perception +19, Ride +19, Swim
        +13; <b>Racial Modifiers </b>+8 Intimidate, +8 Perception, +8
        Ride</p><p><b>Languages </b>Common, Dwarven, Infernal</p><p><b>SQ
        </b>armor training 2, phantom mount, ruinous revivification</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        land</p><p><b>Organization </b>solitary or troop (graveknight plus 12-24
        skeletal champions)</p><p><b>Treasure </b>NPC gear (<i>+1 full
        plate</i>, <i>+1 greatsword</i>, composite longbow [+8 Str] with 20
        arrows, <i>belt of giant strength +2</i>, other treasure)</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Undying
        tyrants and eternal champions of the undead, graveknights arise from the
        corpses of the most nefarious warlords and disgraced heroes-villains too
        merciless to submit to the shackles of death. They bear the same weapons
        and regalia they did in life, though warped or empowered by their
        profane resurrection. The legions they once held also flock to them in
        death, ready to serve their wicked ambitions once more. A graveknight's
        essence is fundamentally tied to its armor, the bloodstained trappings
        of its battle lust. This armor becomes an icon of its perverse natures,
        transforming into a monstrous second skin over the husk of desiccated
        flesh and scarred bone locked within.  <br /><b>CREATING A GRAVEKNIGHT
        </b><br /> "Graveknight" is an acquired template that can be added to
        any living creature with 5 or more Hit Dice (referred to hereafter as
        the base creature). Most graveknights were once humanoids. A graveknight
        uses the base creature's statistics and abilities except as noted here. 
        <br /><b>CR:</b> Same as base creature +2.  <br /><b>Alignment:</b> Any
        evil.  Type: The graveknight's type changes to undead (augmented). Do
        not recalculate class Hit Dice, BAB, or saves.  <br /><b>Senses:</b> A
        graveknight gains darkvision 60 ft.  <br /><b>Aura:</b> A graveknight
        emanates the following aura.  <br /><b>Sacrilegious Aura (Su):</b> A
        graveknight constantly exudes an aura of intense evil and negative
        energy in a 30-foot radius. This aura functions as the spell
        <i>desecrate</i> and uses the graveknight's armor as an altar of sorts
        to double the effects granted. The graveknight constantly gains the
        benefits of this effect (including the bonus hit points, as this aura is
        part of the graveknight's creation). In addition, this miasma of fell
        energies hinders the channeling of positive energy. Any creature that
        attempts to summon positive energy in this area-such as through a
        cleric's channel energy ability, a paladin's lay on hands, or any spell
        with the healing subtype-must make a concentration check with a DC equal
        to 10 + 1/2 the graveknight's Hit Dice + the graveknight's Charisma
        modifier. If the character fails, the effect is expended but does not
        function.  <br /><b>Armor Class:</b> Natural armor improves by +4.  <br
        /><b>Hit Dice:</b> Change all racial Hit Dice to d8s. Class Hit Dice are
        unaffected. As an undead, a graveknight uses its Charisma modifier to
        determine bonus hit points.  <br /><b>Defensive Abilities:</b> A
        graveknight gains channel resistance +4; DR 10/magic; and immunity to
        cold, electricity, and any additional energy type noted by its ruinous
        revivification special quality. A graveknight also gains spell
        resistance equal to its augmented CR + 11.  The graveknight also gains
        the following ability.  <br /><i>Rejuvenation (Su)</i>: One day after a
        graveknight is destroyed, its armor begins to rebuild the undead
        horror's body. This process takes 1d10 days-if the body is destroyed
        before that time passes, the armor merely starts the process anew. After
        this time has elapsed, the graveknight wakens fully healed.  Attacks: A
        graveknight gains a slam attack if the base creature didn't have one.
        Damage for the slam depends on the graveknight's size (see
        <i>Bestiary</i> 302). </p>

        <p><strong>Special Attack</strong>s: A graveknight gains the following
        special attacks. Save DCs are equal to 10 + 1/2 the graveknight's HD +
        the graveknight's Charisma modifier unless otherwise noted.  <br
        /><i>Channel Destruction (Su)</i>: Any weapon a graveknight wields
        seethes with energy, and deals an additional 1d6 points of damage for
        every 4 Hit Dice the graveknight has. This additional damage is of the
        energy type determined by the ruinous revivification special quality. 
        <br /><i>Devastating Blast (Su)</i>: Three times per day, the
        graveknight may unleash a 30-foot cone of energy as a standard action.
        This blast deals 2d6 points of damage for every 3 Hit Dicea graveknight
        has (Reflex for half ). This damage is of the energy type determined by
        the graveknight's ruinous revivification special quality.  <br
        /><i>Undead Mastery (Su)</i>: As a standard action, a graveknight can
        attempt to bend any undead creature within 50 feet to its will. The
        targeted undead must succeed at a Will save or fall under the
        graveknight's control. This control is permanent for unintelligent
        undead; an undead with an Intelligence score is allowed an additional
        save every day to break free from the graveknight's control. A creature
        that successfully saves cannot be affected again by the same
        graveknight's undead mastery for 24 hours.  A graveknight can control 5
        Hit Dice of undead creatures for every Hit Die it has. If the
        graveknight exceeds this number, the excess from earlier uses of the
        ability becomes uncontrolled, as per <i>animate dead</i>.  <br
        /><b>Special Qualities:</b> A graveknight gains the following.  <br
        /><i>Phantom Mount (Su)</i>: Once per hour, a graveknight can summon a
        skeletal horse similar to a <i>phantom steed</i>. This mount is more
        real than a typical <i>phantom steed</i>, and can carry one additional
        rider. The mount's powers are based on the graveknight's total Hit Dice
        rather than caster level. A graveknight's mount looks distinctive and
        always appears the same each time it is summoned. If the mount is
        destroyed, it can be summoned again with full hit points 1 hour later. 
        <br /><i>Ruinous Revivification (Su)</i>: At the time of its creation,
        the graveknight chooses one of the following energy types: acid, cold,
        electricity, or fire. This energy type should be relevant to the
        graveknight's life or death, defaulting to fire if none are especially
        appropriate. This energy type inf luences the effects of several of a
        graveknight's special abilities.  <br /><b>Ability Scores:</b> Str +6,
        Int +2, Wis +4, Cha +4. As an undead creature, a graveknight has no
        Constitution score.  <br /><b>Skills:</b> Graveknights gain a +8 racial
        bonus on Intimidate, Perception, and Ride checks.  <br /><b>Feats:</b>
        Graveknights gain Improved Initiative, Mounted Combat, Ride-By Attack,
        and Toughness as bonus feats.  <br /><b>GRAVEKNIGHT ARMOR </b><br /> In
        death, the graveknight's life force lingers on in its armor, not its
        corpse, in much the same way that a lich's essence is bound within a
        phylactery. Unless every part of a graveknight's armor is ruined along
        with its body, a graveknight can rejuvenate after it is destroyed. A
        typical suit of full plate graveknight armor has hardness 10 and 45 hit
        points, though armor with enhancements or made of special materials
        proves more difficult to destroy. Merely breaking a graveknight's armor
        does not destroy it; it must be ruined, such as by being disintegrated,
        taken to the Positive Energy Plane, or thrown into the heart of a
        volcano.</p></h4></div></section>
    _id: rh85KDiU2l7NxyyA
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!mWvMyjZbVzCqy5Dw.rh85KDiU2l7NxyyA'
sort: 0
_key: '!journal!mWvMyjZbVzCqy5Dw'

