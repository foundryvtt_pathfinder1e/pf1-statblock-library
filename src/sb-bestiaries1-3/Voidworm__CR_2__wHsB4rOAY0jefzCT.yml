name: Voidworm (CR 2)
_id: wHsB4rOAY0jefzCT
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: beG63q3Dbzu4Lohx
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!wHsB4rOAY0jefzCT.beG63q3Dbzu4Lohx'
  - name: Voidworm (CR 2)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 2</p><section
        id="Voidworm"><div class="heading"><p
        class="alignright"><strong>CR</strong> 2</p></div><div><p><b>XP
        </b>600</p><p>CN Tiny outsider (chaotic, extraplanar, protean,
        shapechanger)</p><p><b>Init </b>+3; <b>Senses </b>blindsense 30 ft.,
        darkvision 30 ft., <i>detect law</i>; Perception +8</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>15, touch 15,
        flat-footed 12 (+3 Dex, +2 size)</p><p><b>hp </b>16 (3d10); fast healing
        2</p><p><b>Fort </b>+1, <b>Ref </b>+6, <b>Will </b>+2</p><p><b>Defensive
        Abilities </b>amorphous anatomy, freedom of movement; <b>Immune
        </b>acid; <b>Resist </b>electricity 10, sonic 10</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>20 ft., fly
        50 ft. (perfect)</p><p><b>Melee </b>bite +8 (1d3-2), tail slap +3 (1d3-2
        plus confusion)</p><p><b>Space </b>2-1/2 ft.; <b>Reach </b>0
        ft.</p><p><b>Spell-Like Abilities</b> (CL 6th; concentration +7)  <br
        />Constant—<i>detect law</i> <br />At Will—<i>dancing lights</i>,
        <i>ghost sound</i> (DC 11), <i>prestidigitation</i> <br
        />3/day—<i>blur</i> (self only), <i>obscuring mist</i> <br
        />1/week—<i>commune</i> (CL 12th, 6 questions)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>7, <b>Dex
        </b>17, <b>Con </b>10, <b>Int </b> 8, <b>Wis </b>8, <b>Cha
        </b>13</p><p><b>Base Atk </b>+3; <b>CMB </b>+4; <b>CMD </b>12 (can't be
        tripped)</p><p><b>Feats </b>Skill Focus (Perception), Weapon
        Finesse</p><p><b>Skills </b>Acrobatics +9 (+5 jump), Bluff +7, Escape
        Artist +7, Fly +19, Knowledge (arcana) +5, Perception +8, Stealth
        +15</p><p><b>Languages </b>Common, Protean</p><p><b>SQ </b>change shape
        (2 forms, both of which must be Tiny animals; <i>beast shape</i>
        II)</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any (Limbo)</p><p><b>Organization
        </b>solitary, pair, or school (3-18)</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Confusion (Su)</b> A creature struck by a voidworm's tail
        slap must make a DC 12 Will save or become confused for 1 round. This is
        a mind-affecting effect. The save DC is Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Debate rages
        as to whether or not the strange and capricious creatures called
        voidworms are actually proteans at all. To the wizards and sorcerers who
        summon them as familiars, the answer seems obvious- these tiny dwellers
        of Limbo have all the requisite racial traits of proteans, down to their
        serpentine shapes. Yet the established protean castes find such claims
        outright insulting, claiming instead that it is such acts of conjuration
        that call voidworms forth from the raw stuff of Limbo, giving them shape
        and life according to the spellcasters' expectations, and that these
        lesser beings are but pale ref lections of their formidable kin.
        Voidworms themselves have little to say on the matter-creatures of the
        moment, and sparing little thought for the constantly mutable concept of
        "reality," voidworms only barely grasp cause and effect, and the past
        has no more substance or signif icance for them than a dream. In order
        to gain a voidworm as a familiar, a spellcaster must be chaotic neutral,
        be caster level 7th, and have the Improved Familiar feat.  Regardless of
        their actual origins, voidworms maintain a thriving ecology in the chaos
        of Limbo, forming together into darting, flashing schools that are often
        hunted for sport by naunets and other predators of chaos. Mortal
        wizards, however, most commonly encounter voidworms as summoned
        familiars. These tiny, serpentine creatures are particularly valued by
        illusionists, evokers, and other magical practitioners who deal with
        distorting or molding reality, though the familiars' bizarre logic and
        miniscule attention spans sometimes make them more trouble than they're
        worth. Still, their confusing attack and remarkable hardiness have saved
        more than one wizard on the battlef ield, and their strange thought
        processes can sometimes offer unique insights in the laboratory. When
        traveling in more mundane lands, wizards often order voidworm familiars
        to use their change shape ability to disguise themselves as ordinary
        pets or animal familiars, though these disguises tend to slip when the
        voidworm grows curious or playful.  A voidworm is only 2 feet long and
        weighs a mere 2 pounds. No two voidworms are exactly alike in their
        coloration or markings. Their two feathery wings generally take on
        brighter colors than the rest of their bodies, and in the case of
        voidworms conjured as familiars, these "wings" are the same color as
        their masters' eyes.</p></h4></div></section>
    _id: 8EFB4QP78j2oyo6y
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!wHsB4rOAY0jefzCT.8EFB4QP78j2oyo6y'
sort: 0
_key: '!journal!wHsB4rOAY0jefzCT'

