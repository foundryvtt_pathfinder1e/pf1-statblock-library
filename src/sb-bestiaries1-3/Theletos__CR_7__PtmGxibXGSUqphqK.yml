name: Theletos (CR 7)
_id: PtmGxibXGSUqphqK
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: BSk6zC0l7450XHqh
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!PtmGxibXGSUqphqK.BSk6zC0l7450XHqh'
  - name: Theletos (CR 7)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 2</p><section
        id="Theletos"><div class="heading"><p
        class="alignright"><strong>CR</strong> 7</p></div><div><p><b>XP
        </b>3,200</p><p>N Medium outsider (aeon, extraplanar)</p><p><b>Init
        </b>+6; <b>Senses </b>darkvision 60 ft.; Perception +16</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>20, touch 15,
        flat-footed 17 (+2 deflection, +2 Dex, +1 dodge, +5 natural)</p><p><b>hp
        </b>76 (9d10+27); fast healing 5</p><p><b>Fort </b>+9, <b>Ref </b>+5,
        <b>Will </b>+12</p><p><b>Immune </b>cold, critical hits, poison;
        <b>Resist </b>electricity 10, fire 10; <b>SR </b>18</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30 ft., fly
        30 ft. (poor)</p><p><b>Melee </b>2 slams +13 (1d6+4), 2 tentacles +8
        (1d4+2 plus fate drain)</p><p><b>Space </b>5 ft.; <b>Reach </b>5
        ft.</p><p><b>Special Attacks </b>wreath of fate</p><p><b>Spell-Like
        Abilities</b> (CL 9th; concentration +10)<br />At will—<i>augury</i>,
        <i>command</i> (DC 12), <i>doom</i> (DC 12), <i>sanctuary</i> (DC 12)<br
        />3/day— <i>bestow curse</i> (DC 14), <i>enthrall</i> (DC 13), <i>touch
        of idiocy</i> (DC 13), <i>dispel magic</i>, <i>remove curse</i>,
        <i>suggestion</i> (DC 14)<br />1/day—<i>charm monster</i> (DC 15),
        <i>lesser geas</i> (DC 15)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>18, <b>Dex
        </b>14, <b>Con </b>17, <b>Int </b> 11, <b>Wis </b>19,  <b>Cha
        </b>12</p><p><b>Base Atk </b>+9; <b>CMB </b>+13; <b>CMD </b>28 (can't be
        tripped)</p><p><b>Feats </b>Dodge, Hover, Improved Initiative, Improved
        Iron Will, Iron Will</p><p><b>Skills </b>Fly -2, Intimidate +13,
        Knowledge (planes) +16, Perception +16, Sense Motive +16, Spellcraft
        +12, Stealth +14</p><p><b>Languages </b>envisaging</p><p><b>SQ
        </b>extension of all, void form</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        (Outer Planes)</p><p><b>Organization </b>solitary, pair, or collective
        (3-12)</p><p><b>Treasure </b>none</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Fate Drain (Su)</b> A theletos
        possesses a pair of flexible crystalline tentacles with which it can
        drain a creature's sense of fate and destiny. Whenever it strikes a foe
        with these tentacles, the creature struck must make a DC 17 Will save or
        take 1d4 points of Charisma damage.  Until a creature's Charisma damage
        from this ability is healed, the victim takes a -2 penalty on all saving
        throws (regardless of the actual total amount of Charisma damage it
        takes). The save DC is Constitution-based.  </p><p><b>Wreath of Fate
        (Su)</b> As a full-round-action every 1d4 rounds, a theletos can release
        a 60-foot cone of energy from its chest. Any intelligent creature struck
        by this cone must make a DC 15 Will save or become nearly overwhelmed
        with the knowledge of various fates that destiny has in store for
        him-there is no way to make sense of these myriad <i>doom</i>s and
        boons, and as a result, the victim is staggered. As long as this
        condition persists, the victim may choose to make two rolls when
        attempting an attack roll, a saving throw, or a skill check-he must
        accept the worse of the two rolls, but in so doing the wreath of fate
        passes from his soul and he is no longer staggered by this ability.
        Wreath of fate is a curse effect, and as such can be affected by
        <i>remove curse</i> or <i>break enchantment</i>-the effective caster
        level of this curse is equal to the theletos's HD (CL 9th in most
        cases). The save DC is Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The strange
        theletos is the guardian of the duality between freedom and fate.
        Slavery is no more of an issue to a theletos than is true freedom, but
        without one, the other cannot exist. In areas where slavery is rife, a
        theletos might aid in freeing some slaves, while in regions where
        slavery has been abolished, this strange being works to subjugate many
        creatures with its own mind-controlling spell-like abilities-often
        encouraging them to further undertake acts of slavery themselves. The
        theletos is also a guardian of fate and prophecy, and while for some
        creatures it might allow glimpses of futures, others who peer into the
        future almost seem to cause the aeon physical pain. The theletos cannot
        explain why one seer might be allowed to divine futures while another
        should not-it knows only that some prophets should be denied this
        pursuit.</p><p>A theletos is 5 feet tall and weighs 100
        pounds.</p></h4></div></section>
    _id: NshxxjgVgT97acff
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!PtmGxibXGSUqphqK.NshxxjgVgT97acff'
sort: 0
_key: '!journal!PtmGxibXGSUqphqK'

