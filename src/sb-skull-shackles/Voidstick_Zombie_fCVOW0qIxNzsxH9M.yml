name: Voidstick Zombie
_id: fCVOW0qIxNzsxH9M
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Skull_and_Shackles.jpg
    title:
      show: false
      level: 1
    _id: LLg4ATl8AM08upww
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!fCVOW0qIxNzsxH9M.LLg4ATl8AM08upww'
  - name: Voidstick Zombie
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 57</p><section id="VoidstickZombie"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        12</p></div><div><p><strong>XP </strong>19,200</p><p>CE Medium undead
        </p><p><strong>Init </strong>+4; <strong>Senses </strong>darkvision 60
        ft., lifesense 60 ft; Perception +10</p><p><strong>Aura
        </strong>sacrilegious aura</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>26, touch 14, flat-footed 26 (+12 natural, +4
        profane)</p><p><strong>hp </strong>157 (15d8+90); fast healing
        3</p><p><strong>Fort </strong>+9, <strong>Ref </strong>+7, <strong>Will
        </strong>+9</p><p><strong>Defensive Abilities </strong>channel
        resistance +4; <strong>Immune </strong>undead traits</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong>2 melee touch +13 (1d8
        negative energy)</p><p><strong>Space </strong>5 ft.; <strong>Reach
        </strong>5 ft.</p><p><strong>Special Attacks </strong>channel negative
        energy 5/day (DC 22, 8d6)</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>12, <strong>Dex </strong>11, <strong>Con </strong>-,
        <strong>Int </strong> 7, <strong>Wis </strong>6, <strong>Cha
        </strong>15</p><p><strong>Base Atk </strong>+11; <strong>CMB
        </strong>+12; <strong>CMD </strong>26</p><p><strong>Feats
        </strong>Channel Smite, Great Fortitude, Improved Channel, Improved
        Initiative, Iron Will, Lightning Reflexes, Skill Focus (Stealth),
        Toughness</p><p><strong>Skills </strong>Climb +5, Perception +10,
        Stealth +11</p><p><strong>Languages </strong>Common</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any</p><p><strong>Organization
        </strong>solitary, pair, or plague (3-9)</p><p><strong>Treasure
        </strong>none</p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Channel Energy
        (Su)</strong> The <i>voidstick</i> zombie can channel negative energy as
        a 15th-level cleric.  </p><p><strong>Sacrilegious Aura (Sp)</strong> The
        overwhelming entropic energies and the sheer number of
        <i><i>voidstick</i>s</i> animating the <i>voidstick</i> zombie warp and
        augment negative and positive energy around the creature. As with all
        <i><i>voidstick</i>s</i>, an aura of intense negative energy extends in
        a 30-foot radius from the zombie functioning as the spell
        <i>desecrate</i>. Undead within this aura receive a +1 profane bonus on
        attack and damage rolls and the DC to resist channeled negative energy
        increases by +3. The <i>voidstick</i> zombie constantly gains the
        benefits of this effect (the attack and damage bonuses are already
        incorporated into its statistics).  In addition, this miasma of void
        energies also interferes with wielding positive energy. Any creature
        attempting to use positive energy in this area-such as through a
        cleric's channel energy ability, a paladin's lay on hands, or any spell
        with the healing descriptor-must make a DC 25 concentration check. If
        this check fails, the effect is blocked, consuming one use of the
        ability, or the spell is lost.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The
        <i>voidstick</i> zombie is the hateful creation of wicked shamans and
        necromancers, who use <i><i>voidstick</i>s</i> to animate the dead. One
        <i>voidstick</i> is required for every Hit Die possessed by the base
        creature. These vile devices, each of which is 6 to 10 inches in length,
        are pierced through the living or dead body of a creature, pumping the
        dark energy of the void into its form. Packed with the bitter entropy of
        negative energy, the creature rises, seeking out the life force of
        others with endless sadness and insane determination. Its single goal is
        to extinguish life and smother any source of positive energy. Multiple
        <i><i>voidstick</i>s</i> in a creature's form feed one another,
        augmenting their standard abilities and imbuing the touch of the
        <i>voidstick</i> zombie with the power to drain life from anything it
        touches. The zombie becomes almost a sliver of the void itself, existing
        in a state of such negative power that its very proximity can tamper
        with other creatures' abilities to summon the powers of the Positive
        Energy Plane, and with the perpetual sadness that numbs its own
        intellect it gains the power to enhance and inf luence other creatures
        that depend on negative energy.  These sad creatures, wracked with
        soulless negative energy, constantly hunger for destruction, delighted
        at every chance they get to snuff the spark of life from the living.
        They hunt the islands throughout the Shackles, especially the Cannibal
        Isles, where their creation began long before its current inhabitants
        made their home there.  As the brutal and savage kuru people of the
        Cannibal Isles explored the ruins of Ghol-Gan, they discovered the foul
        magic involved with the <i><i>voidstick</i>s</i>, and as they turned to
        barbarism and cannibalism, their shamans carved these devices and
        created the first <i>voidstick</i> zombie seen on Golarion in thousands
        of years. The kuru use <i>voidstick</i> zombies for war against each
        other and to serve as ruthless hunters, culling humanoids who wander too
        close to their blood-drenched islands. Kuru shamans create the vile
        <i><i>voidstick</i>s</i> in order to enhance their own necromantic
        strength as well as to animate <i>voidstick</i> zombies. In some tribes
        it is seen as a great honor to be transformed into one of these powerful
        creatures, and some aged shamans on the eve of their death elect to
        undergo this transformation instead of becoming a ritual meal for their
        tribes.  Since the rediscovery of <i><i>voidstick</i>s</i>, those
        interested in necromancy and the creation of new undead have sent
        emissaries to the Shackles eager to buy them. These enterprising
        necromancers experiment with the devices and unleash <i>voidstick</i>
        zombies into Avistan and Garund to fulfill their murderous intent.  <br
        /><strong>Voidsticks</strong><br /> In remote parts of the world, it is
        not always practical for the faithful to visit holy places with any
        regularity. In particular, the weak, the sick, and the dying may be in
        no position to trek across plains or risk dangerous water crossings to
        see their spiritual leaders. In response to this, the <i>godstick</i>
        was born-a crafted rod that serves as a portable shrine, carried by
        shamans and witchdoctors and pushed into the ground to focus devotion at
        any location. Ingenuity is not just the way of the benign, however, and
        just as often it is the way of evil. Before long, shamans who worshiped
        more hateful deities corrupted this notion; they created the
        <i><i>voidstick</i>s</i> and drove them into bodies, living or dead,
        charging those bodies with the power of the endless void and creating
        rattling, hungry abominations.  </p><p><strong>Voidstick </strong>
        </p><p><strong>Aura </strong>faint necromancy [evil]; CL 5 
        </p><p><strong>Slot </strong>special; </p><p><strong>Price
        </strong>2,500 gp, </p><p><strong>Weighs </strong>1 lb. 
        </p><p><strong>DESCRIPTION </strong> Each <i>voidstick</i> is 6 t0 10
        inches long, with a diameter of about an inch. Carved from single pieces
        of polished darkwood, they often display markings or symbols sacred to
        the deity of their creator, but can just as easily be plain and smooth.
        Planting a <i>voidstick</i> into the ground with an appropriate prayer
        is a full-round action that provokes attacks of opportunity. The device
        floods the area with negative energy, producing an effect identical to
        the <i>desecrate</i> spell in a 20- foot radius. In addition, any evil
        divine caster within 20 feet of the <i>voidstick</i> may cast her spells
        without the need for any material component with a value of 10 gp or
        less, or any focus item with a value of 50 gp or less. This ability
        functions only for the individual planting the <i>voidstick</i> and
        persists until the stick is uprooted.  While this is a useful tool for
        shamans in locations poorly serviced by trade in magical goods, it pales
        in comparison to the <i>voidstick</i>'s most potent function: the
        creation of undead. Creating a <i>voidstick</i> zombie requires an
        hour-long ritual during which foul symbols are drawn across a corpse's
        flesh. At the end of the hour, the creator must make a DC 25 Knowledge
        (religion) check before driving the first <i>voidstick</i> into the
        victim's heart. If this check succeeds, the victim is transformed into a
        <i>voidstick</i> zombie. To fully animate the creature, one
        <i>voidstick</i> must be used for each Hit Die the base creature has. In
        12 rounds, the creature rises under its own power, eager to spread its
        negative energy and snuff out life nearby. Undead created using
        <i><i>voidstick</i>s</i> are not under the control of their creator but
        can be commanded using channeled negative energy, spells, or similar
        effects.  <i>Voidsticks</i> can also be driven into a living body to
        slay the creature and transition it to undeath. If a living body is
        used, the creature must be pinned or otherwise helpless for the duration
        of the entire ritual. When the ritual is complete and the first
        <i>voidstick</i> is inserted, the creature must make a DC 18 Fortitude
        save. If this save succeeds, the creature is reduced to 0 hit points and
        is dying, but the magic of the <i>voidstick</i> and the ritual are
        wasted and the ritual must be performed again, using another
        <i>voidstick</i>. If the save fails, the creature dies and the ritual is
        successful, transforming the base creature into a <i>voidstick</i>
        zombie.  Undead that contain such an item benefit from the +2 bonus hit
        points per Hit Die for having been created in the area of an enhanced
        <i>desecrate</i> spell.  </p><p><strong>CONSTRUCTION </strong>
        </p><p><strong>Requirements </strong>Craft Wondrous Item, <i>animate
        dead</i>, <i>desecrate</i>; Creator must have 5 ranks in Knowledge
        (religion). </p><p><strong>Cost </strong>1,750
        gp</p></h4></div></section>
    _id: bXJqMezLFxjfPBj6
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!fCVOW0qIxNzsxH9M.bXJqMezLFxjfPBj6'
sort: 0
_key: '!journal!fCVOW0qIxNzsxH9M'

