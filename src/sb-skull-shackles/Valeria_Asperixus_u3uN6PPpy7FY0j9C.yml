name: Valeria Asperixus
_id: u3uN6PPpy7FY0j9C
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Skull_and_Shackles.jpg
    title:
      show: false
      level: 1
    _id: 21SZmMC2Ell1uWjy
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!u3uN6PPpy7FY0j9C.21SZmMC2Ell1uWjy'
  - name: Valeria Asperixus
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 60</p><section id="ValeriaAsperixus"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        11</p></div><div><p><strong>XP </strong>12,800</p><p>Female human
        fighter (polearm master) 6/Hellknight 6 (<i>Advanced Player's Guide</i>
        106, <i>Inner Sea World Guide</i> 278)</p><p>LE Medium humanoid
        (human)</p><p><strong>Init </strong>+3; <strong>Senses
        </strong>low-light vision; Perception +9</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>26, touch 16, flat-footed 23 (+10 armor, +3 deflection, +3
        Dex)</p><p><strong>hp </strong>121 (12 HD;
        6d10+6d10+54)</p><p><strong>Fort </strong>+14, <strong>Ref </strong>+10,
        <strong>Will </strong>+9; +4 vs. compulsions, +2 vs.
        fear</p><p><strong>Defensive Abilities </strong>force of will (+2,
        +4)</p></div><hr /><div><p><strong>OFFENSE</strong></p></div><hr
        /><div><p><strong>Spd </strong>30 ft.</p><p><strong>Melee
        </strong><i><i>+1 <i>axiomatic bardiche</i></i>*</i> +19/+14/+9
        (1d10+10/19-20)</p><p><strong>Ranged </strong>mwk composite longbow
        +16/+11/+6 (1d8+4/3)</p><p><strong>Space </strong>5 ft.; <strong>Reach
        </strong>5 ft. (10 ft. with bardiche)</p><p><strong>Special Attacks
        </strong>polearm training +1, pole fighting, smite chaos 2/day (+2
        attack and AC, +6 damage), steadfast pike</p><p><strong>Spell-Like
        Abilities</strong> (CL 12th; concentration +14) <br />At Will—<i>detect
        chaos</i> <br />5/day—<i>discern lies</i> (DC 15)</p></div><hr
        /><div><p><strong>TACTICS</strong></p></div><hr /><div><p><strong>Before
        Combat </strong>Once the <i>Abrogail's Fury</i> comes under attack,
        Valeria drinks her <i>potion of cat's grace</i> and uses her tracker
        discipline to summon a leopard to help defend Admiral Druvalia Thrune.
        The effects of Druvalia's <i>ward the faithful</i> spell are already
        included in Valeria's stat block.</p><p><strong>During Combat
        </strong>Valeria stays close to her mistress, Druvalia Thrune,
        throughout any combat, using her Bodyguard and In Harm's Way feats to
        intercept attacks meant for the admiral. If forcibly separated from her
        charge, she does everything in her power to get back to Druvalia's side.
        Valeria uses the reach of her bardiche to keep foes at a distance while
        the Chelish marines deal with them. She casts <i>detect chaos</i> as a
        move action to determine if any enemies are chaotic, then focuses her
        attacks on those foes, using her smite chaos ability and Improved Vital
        Strike with her <i>axiomatic bardiche</i>, using her pole fighting
        ability, if necessary, to shorten her grip on her weapon to attack
        adjacent foes. If knocked overboard, Valeria drinks her <i>potion of
        fly</i> to return to Druvalia's side.</p><p><strong>Morale
        </strong>Valeria is enraged if Druvalia is killed, and fights to the
        death to avenge her.</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>19, <strong>Dex </strong>16, <strong>Con </strong>16,
        <strong>Int </strong> 8, <strong>Wis </strong>10, <strong>Cha
        </strong>14</p><p><strong>Base Atk </strong>+12; <strong>CMB
        </strong>+16; <strong>CMD </strong>32 (34 vs.
        sunder)</p><p><strong>Feats </strong>Bodyguard*, Combat Reflexes,
        Improved Vital Strike, In Harm's Way*, Iron Will, Power Attack, Stand
        Still, Toughness, Vital Strike, Weapon Focus (bardiche*), Weapon
        Specialization (bardiche*)</p><p><strong>Skills </strong>Handle Animal
        +6, Intimidate +15, Knowledge (engineering) +4, Knowledge (planes) +2,
        Perception +9, Ride +4, Swim +5</p><p><strong>Languages
        </strong>Common</p><p><strong>SQ </strong>disciplines (tracker 2/day,
        vigilance 2/day), Hellknight armor 2, Order of the
        Scourge</p><p><strong>Combat Gear</strong> <i>elixir of swimming</i>,
        <i>potion of cat's grace</i>, <i>potion of cure serious wounds</i>,
        <i>potion of fly</i>; <strong>Other Gear </strong><i>+1 Hellknight
        plate</i>, <i>+1 <i>axiomatic bardiche</i></i>*, masterwork composite
        longbow (+4 Str) with 20 arrows, <i>belt of giant strength
        +2</i></p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Detect Chaos
        (Sp)</strong> This ability functions like a paladin's detect evil
        ability, save that it detects chaos.  </p><p><strong>Disciplines (Sp and
        Su</strong>) Valeria has access to the Hellknight disciplines of tracker
        and vigilance. Twice per day, she can summon an eagle, riding dog, wolf,
        or leopard as if using a <i>summon monster</i> spell, but the duration
        is 1 hour. In addition, Valeria gains low-light vision, and as a
        full-round action twice per day, she can see through up to 5 feet of
        wood or stone for as long as she concentrates (maximum 6 rounds). Metal
        or denser barriers block this effect.  </p><p><strong>Force of Will
        (Ex)</strong> Valeria gains a +4 bonus on Will saves against spells with
        the compulsion descriptor and a +2 bonus on Will saves against spells
        with the fear descriptor.  </p><p><strong>Hellknight Armor (Ex)</strong>
        Valeria has earned the right to wear a special type of masterwork full
        plate called Hellknight plate armor (<i>Inner Sea World Guide</i> 290).
        While wearing Hellknight plate, Valeria reduces the armor check penalty
        by 2, increases the maximum Dexterity bonus allowed by 1, and moves at
        full speed.  </p><p><strong>Order</strong> Valeria is a member of the
        Order of the Scourge.  </p><p><strong>Smite Chaos (Su)</strong> This
        ability functions as the paladin's smite evil ability, but against
        chaotic-aligned creatures. This ability is twice as effective against
        outsiders with the chaotic subtype, chaotic-aligned aberrations, and
        fey.</p><p>* See the <i>Advanced Player's Guide</i>.</p></div></section>
    _id: 0LfVFQ3qZGoWYPhg
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!u3uN6PPpy7FY0j9C.0LfVFQ3qZGoWYPhg'
sort: 0
_key: '!journal!u3uN6PPpy7FY0j9C'

