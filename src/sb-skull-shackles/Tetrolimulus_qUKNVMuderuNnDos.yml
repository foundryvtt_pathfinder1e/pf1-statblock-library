name: Tetrolimulus
_id: qUKNVMuderuNnDos
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Skull_and_Shackles.jpg
    title:
      show: false
      level: 1
    _id: JYlisKmITZsDLnjJ
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!qUKNVMuderuNnDos.JYlisKmITZsDLnjJ'
  - name: Tetrolimulus
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 58</p><section id="Tetrolimulus"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        11</p></div><div><p><strong>XP </strong>12,800</p><p>NE Large magical
        beast (aquatic)</p><p><strong>Init </strong>+8; <strong>Senses
        </strong>darkvision 60 ft., low-light vision; Perception +7</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>27, touch 13, flat-footed 23 (+4 Dex, +14 natural, -1
        size)</p><p><strong>hp </strong>147 (14d10+70)</p><p><strong>Fort
        </strong>+16, <strong>Ref </strong>+13, <strong>Will
        </strong>+7</p><p><strong>Resist </strong>cold 10</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>50 ft., swim 50 ft.</p><p><strong>Melee </strong>2 claws +19
        (2d6+6/x4), sting +17 (1d6+3 plus poison)</p><p><strong>Space
        </strong>10 ft.; <strong>Reach </strong>10 ft.</p><p><strong>Special
        Attacks </strong>poison, pounce</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>22, <strong>Dex </strong>18, <strong>Con </strong>21,
        <strong>Int </strong> 3, <strong>Wis </strong>12, <strong>Cha
        </strong>9</p><p><strong>Base Atk </strong>+14; <strong>CMB
        </strong>+21; <strong>CMD </strong>35</p><p><strong>Feats
        </strong>Endurance, Great Fortitude, Improved Initiative, Iron Will,
        Lunge, Multiattack, Run</p><p><strong>Skills </strong>Climb +10,
        Perception +7, Survival +14</p><p><strong>SQ </strong>amphibious,
        shoreline mastery</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> temperate
        coasts</p><p><strong>Organization </strong>solitary,
        pair</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Poison (Ex)</strong> <i>Tetrodotoxin</i>:
        Sting-injury; <i>save</i> Fort DC 22; <i>frequency</i> once; initial
        <i>effect</i> staggered for 1 round, secondary <i>effect</i> paralysis
        for 1d4 rounds; <i>cure</i> 2 consecutive <i>save</i>s. 
        </p><p><strong>Shoreline Mastery (Ex)</strong> The multi-limbed nature
        of the crab half of the tetrolimulus allows it to ignore the effects of
        uneven or difficult terrain. This does not apply to terrain magically
        manipulated to impede movement.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A terrifying
        mix of prehistoric arthropod and heavily armored mantis, the
        tetrolimulus is the stuff of nightmares for shipwrecked and abandoned
        mariners. Plated with a spiny crustacean exoskeleton, the upper body of
        the tetrolimulus is reminiscent of a very robust kind of mantis. Its
        raptorial forelimbs, folded as if in prayer, flash forward with
        frightening speed and precision to brutally slice opponents before they
        have had a chance to act. Captains and mutineers alike are quick to
        remind their enemies of these deadly claws and the creature's other
        name, the "beach guillotine," for the brutal justice it exacts on those
        put ashore for choosing the wrong side in a mutiny.  With somewhat less
        panache, the tetrolimulus is often described as the "sea-mantis" because
        it resembles a crab's strong legs and shell merged with a mantis'
        powerful arms. Trailing behind is a scorpion's deadly stinger. The
        creature's durable, spiked shell covers five pairs of blade-like legs
        that work together to produce remarkable speeds even through challenging
        terrain. Truly a master of the beaches, the tetrolimulus has caught many
        mariners off guard with a blazing charge over varied terrain,
        perforating a noiseless trail through wet sand, then clattering over
        rocks with the sound of dice thrown across a table.  Its final and most
        dangerous terror, held upright and waved like a regal scepter in combat,
        is the tetrolimulus's tail stinger. The stinger is razor sharp along its
        outer edge, but its neat incisions are nowhere near as dangerous as the
        poison that coats its blade. Those who succumb to a dose of
        poison-called tetrodotoxin-are soon to be a meal for the sea-mantis.
        Muscle spasms and cramps accompany a gradual slowing of movement,
        hinting at oncoming paralysis and the agony of a neat butchering while
        still alive for easy consumption. One of nature's cruelest poisons,
        tetrodotoxin is a popular tool of the Red Mantis assassins, who
        appreciate both the poison's painful efficacy and the mantis-form of its
        progenitor.  <strong></strong></p><p><strong>Ecology</strong></p><p> 
        Out of the water, the tetrolimulus adopts an unusual swaying movement of
        the upper body. Although its purpose is not entirely clear, it is
        thought that, much like the land-dwelling mantis, the movement enhances
        the creature's primitive vision and makes picking out prey by its own
        relative movement easier. It has been suggested that remaining
        completely still when confronted by a tetrolimulus may prevent
        detection, but none have been able to confirm the success of this
        tactic, and there are none who are confident enough of the theory to
        test it in the field.  Female tetrolimuluses, the hunters of the
        species, are by far the more aggressive. Rarely seen, males live in
        deeper waters as bottom feeders, emerging only in the mating season in
        early spring. At this time for a few days each year, both sexes make
        great journeys, sometimes of hundreds of miles, to return to ancient
        coastal breeding grounds. Here dominant and aggressive females meet and
        mate with the strongest of the smaller and more delicate males. Only a
        small number of these males get a chance to breed, and an even smaller
        number survive to return to the oceans. The energy and effort of their
        travels exhaust the females, and once they've been impregnated, the
        easiest and closest source of food is the weaker males surrounding them.
        A fertilized female may even continue to exhibit signs of availability
        to encourage more males to approach her- not for reproduction, but to
        satisfy her more immediate hunger.  Young are born at sea, and perhaps
        as payment for their strength and power later in life, they spend their
        first few months at the bottom of the food chain. Without the thick
        shells of maturity, they are easy prey, which contributes a great deal
        to population control of their species. As their shells thicken and
        harden, they start to enjoy a less harried existence, and by 6 months
        old they start to fight back. They reach maturity in 12 to 18 months and
        can live for up to 50 years.  Tetrolimuluses' behavior is largely
        instinct-driven, but during the breeding season the normally nomadic
        creatures will fight viciously to protect the shores of their ancestral
        breeding grounds. Even male sea-mantises rise to combat, though at sea
        they more commonly flee than risk confrontation.  Females can be found
        in the area of the Abendego Gulf, from Mediogalti Island all the way
        into the Shackles and as far north as the Sodden Lands. Even Rahadoum
        has seen the occasional tetrolimulus washed ashore to the south, where
        the creatures are feared as the twisted manifestations of Achaekek,
        conjured for worship by the Red Mantis and a stark reminder of the folly
        of religious devotion. There are descriptions of creatures similar to
        the tetrolimulus to the north, even into the Inner Sea, but these
        stories are as old and leaky as the ships of the pirates and traders who
        tell them and no reports of tetrolimuluses along the cost of Cheliax,
        for example, are younger than two generations.  Much of tetrolimuluses'
        bulk is armor, and despite their size they can survive on relatively
        small quantities of food. In perhaps the only mark of intelligence in
        their species, they try to avoid overfishing, instead roaming over
        several miles of coastline to balance their Ecology. The majority of
        their diet is fish and cephalopods, but they are competent trackers and
        follow hints of habitation on their beaches to devour any coast-dwelling
        mammals foolish enough to find themselves on the beaches, including
        humanoids. Like most animals, tetrolimuluses tend to avoid large
        settlements or areas frequently visited by humans. Remote or
        inaccessible beaches are the females' preferred habitat, but
        occasionally a powerful storm rolling out of the Eye of Abendego can
        toss them on more popular or even populous coastlines. Surprisingly high
        concentrations of the creatures are found around Mediogalti Island and
        its outlying cluster. Some suggest this is due to deliberate cultivation
        by the Red Mantis, thanks to the tetrolimulus's favorable form, but it
        may simply be because the treacherous waters and hidden coves are
        perfect for their reclusive lifestyle, and the frequent mutinies,
        shipwrecks, and foolish adventurers provide a varied and ample diet for
        the brutal predators.  <br /><strong>Tetrodotoxin</strong><br />  While
        none are stupid enough to actively farm sea-mantises, occasionally the
        corpse of one is washed up into the more accessible bays on the coasts
        of the Shackles or Mediogalti Island. Some of the more enterprising
        residents of Ilizmagorti have developed a method of harvesting the cruel
        tetrodotoxin poison, from which the tetrolimulus gets its name, for sale
        to the assassins of the Red Mantis.  A single dose of tetrodotoxin sells
        for 1,300 gp. Its rarity and potency make it a valuable product, and
        prices outside the Shackles or Mediogalti Island can be 50-100% higher. 
        One dose of poison can be harvested from the corpse of a tetrolimulus,
        provided the lower half of the creature is intact. This requires a DC 25
        Survival check, and even those who usually find themselves competent at
        skinning or gutting creatures struggle with the intricacies of the sharp
        tail stinger. In harvesting the tetrodotoxin poison, those without the
        poison use ability are subject to the standard 5% chance of
        self-poisoning.</p></h4></div></section>
    _id: xCgqsTVDQhhJ2ZHZ
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!qUKNVMuderuNnDos.xCgqsTVDQhhJ2ZHZ'
sort: 0
_key: '!journal!qUKNVMuderuNnDos'

