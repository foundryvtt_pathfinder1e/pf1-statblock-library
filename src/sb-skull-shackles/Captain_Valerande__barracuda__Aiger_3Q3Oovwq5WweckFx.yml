name: Captain Valerande "barracuda" Aiger
_id: 3Q3Oovwq5WweckFx
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Skull_and_Shackles.jpg
    title:
      show: false
      level: 1
    _id: lNappKdiYjGd0SJg
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!3Q3Oovwq5WweckFx.lNappKdiYjGd0SJg'
  - name: Captain Valerande "barracuda" Aiger
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 59</p><section
        id="CaptainValerandebarracudaAiger"><div class="heading"><p
        class="alignright"><strong>CR</strong> 12</p></div><div><p><strong>XP
        </strong>19,200</p><p>Male half-elf rogue (pirate) 5/Inner Sea Pirate 8
        (<i>Ultimate Combat</i> 72, <i>Pirates of the Inner Sea</i> 24)</p><p>CE
        Medium humanoid (elf, human)</p><p><strong>Init </strong>+5;
        <strong>Senses </strong>low-light vision; Perception +17</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>26, touch 17, flat-footed 20 (+7 armor, +1 deflection, +5 Dex,
        +1 dodge, +1 natural, +1 shield)</p><p><strong>hp </strong>76
        (13d8+18)</p><p><strong>Fort </strong>+5, <strong>Ref </strong>+13,
        <strong>Will </strong>+3; +2 vs. enchantments, +1 vs. fear and
        mind-affecting</p><p><strong>Defensive Abilities </strong>evasion,
        uncanny dodge, unflinching*</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong><i><i>+2 rapier</i></i>
        +15/+10 (1d6+4/18-20), <i>+1 boarding axe</i> +13
        (1d6+2/x3)</p><p><strong>Space </strong>5 ft.; <strong>Reach </strong>5
        ft.</p><p><strong>Special Attacks </strong>sneak attack +6d6 plus
        trip</p></div><hr /><div><p><strong>TACTICS</strong></p></div><hr
        /><div><p><strong>During Combat </strong>Barracuda uses his officers to
        set up flanking opportunities, barking orders to move them around the
        combat as needed. He uses his swinging reposition ability to try to bull
        rush players over the side of the ship, then moves closer to allies so
        he can make full attacks as soon as possible. He uses Combat Expertise
        if faced by particularly hard-hitting foes.</p><p><strong>Morale
        </strong>If reduced to fewer than 15 hit points, Barracuda drops his
        weapons and surrenders, hoping to appeal to the PCs' sense of pity at
        this point to claim his birthright.</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>14, <strong>Dex </strong>20, <strong>Con </strong>12,
        <strong>Int </strong> 13, <strong>Wis </strong>8, <strong>Cha
        </strong>10</p><p><strong>Base Atk </strong>+9; <strong>CMB
        </strong>+14; <strong>CMD </strong>28</p><p><strong>Feats </strong>Agile
        Maneuvers, Combat Expertise, Dodge, Improved Feint, Sea Legs*, Skill
        Focus (Bluff), Two-Weapon Defense, Two-Weapon Fighting, Weapon
        Finesse</p><p><strong>Skills </strong>Acrobatics +23, Appraise +14,
        Bluff +21, Climb +4, Intimidate +15, Perception +17, Profession (sailor)
        +15, Sense Motive +15, Swim +20</p><p><strong>Languages </strong>Common,
        Elven, Osiriani</p><p><strong>SQ </strong>elf blood, pirate tricks
        (classic duelist, deep breath, defensive climber, foot sweep, rigging
        monkey), rogue talents (bleeding attack +6), swinging
        reposition*</p><p><strong>Combat Gear</strong> <i>+4 studded
        leather</i>, <i>+2 rapier</i>, <i>+1 boarding axe</i>, <i>amulet of
        natural armor +1</i>, <i>ring of protection +1</i></p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Pirate Tricks (Ex)</strong> Barracuda gains a +1
        competence bonus on attack rolls with a cutlass, rapier, or short sword.
        He can hold his breath for 36 rounds before risking drowning. He does
        not lose his Dex bonus to AC when climbing. If he damages a foe with
        sneak attack, he can try to trip that foe as an immediate action. He
        gains a +2 bonus on Climb checks when using ropes, and when he does so,
        climbs twice as quickly as normal.</p><p>* See <i>Ultimate
        Combat</i>.</p></div></section>
    _id: KNVySg6tg27UT80S
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!3Q3Oovwq5WweckFx.KNVySg6tg27UT80S'
sort: 0
_key: '!journal!3Q3Oovwq5WweckFx'

