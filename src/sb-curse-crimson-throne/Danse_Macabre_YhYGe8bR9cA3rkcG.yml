name: Danse Macabre
_id: YhYGe8bR9cA3rkcG
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Curse_of_the_Crimson_Throne.jpg
    title:
      show: false
      level: 1
    _id: j4hYPXuIirZIrwDN
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!YhYGe8bR9cA3rkcG.j4hYPXuIirZIrwDN'
  - name: Danse Macabre
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Curse Of The Crimson Throne Chapter
        Appendix</p><section id="DanseMacabre"><div class="heading"><p
        class="alignright"><strong>CR</strong> 14</p></div><div><p><strong>XP
        </strong>38,400</p><p>NE Large undead (incorporeal)</p><p><strong>Init
        </strong>+13; <strong>Senses </strong>darkvision 60 ft., lifesense 60
        ft.; Perception +29</p><p><strong>Aura </strong>dance of death (40 ft.,
        DC 27)</p></div><hr /><div><p><strong>DEFENSE</strong></p></div><hr
        /><div><p><strong>AC </strong>29, touch 29, flat-footed 19 (+10
        deflection, +9 Dex, +1 dodge, -1 size)</p><p><strong>hp </strong>203
        (14d8+140)</p><p><strong>Fort </strong>+14, <strong>Ref </strong>+13,
        <strong>Will </strong>+17</p><p><strong>Defensive Abilities
        </strong>incorporeal, deathless channel resistance +4; <strong>Immune
        </strong>cold, undead traits; <strong>SR </strong>25</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>fly 40 ft. (perfect)</p><p><strong>Melee </strong>incorporeal
        scythe +18/+13 touch (2d6/x4 plus 1d6 Constitution
        drain)</p><p><strong>Space </strong>10 ft.; <strong>Reach </strong>10
        ft.</p></div><hr /><div><p><strong>STATISTICS</strong></p></div><hr
        /><div><p><strong>Str </strong>-, <strong>Dex </strong>28, <strong>Con
        </strong>-, <strong>Int </strong> 8, <strong>Wis </strong>22,
        <strong>Cha </strong>30</p><p><strong>Base Atk </strong>+10; <strong>CMB
        </strong>+20; <strong>CMD </strong>41</p><p><strong>Feats
        </strong>Combat Reflexes, Dodge, Improved Initiative, Iron Will,
        Mobility, Skill Focus (Perception), Spring Attack</p><p><strong>Skills
        </strong>Fly +32, Perception +29, Sense Motive
        +23</p><p><strong>Languages </strong>Common, Necril</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure
        </strong>incidental</p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Dance of Death
        (Su)</strong> A danse macabre is constantly surrounded by a 40-foot aura
        known as the dance of death, an endless gala of dancing spectral
        figures. Any living creature that enters the area of the dance of death
        must attempt a DC 27 Will save. On a failed save, the victim joins the
        ghostly dancers, takes 1d4 points of Constitution drain, and is affected
        as per <i>irresistible dance</i> (CL 14th). A creature can attempt a new
        DC 27 Will save at the end of its turn to escape from the effects of a
        dance of death; otherwise, these effects persist for as long as the
        victim remains within the aura. The Constitution drain effect only
        occurs, however, after the initial failed saving throw. As victims
        cannot willingly move from the square in which they dance, the dance's
        effects end when the danse macabre moves to a point where the victim is
        no longer within its aura, the danse macabre is destroyed, or if the
        victim is physically  removed from the area. A victim who succeeds at a
        save (whether from the original exposure or at a later point to
        successfully escape the effect) is immune to the dance of death of the
        same danse macabre for 24 hours. Dance of death is a visual, sonic,
        mind-affecting compulsion effect, and blindness or deafness can provide
        protection from the effect. The save DC is Charisma-based. 
        </p><p><strong>Deathless (Su)</strong> As a manifestation of death
        incarnate, a danse macabre is not itself subject to permanent
        destruction. If reduced to 0 hit points, it vanishes, only to rejuvenate
        at full hit points in 1d4 days. Only by destroying the creature and then
        casting <i>hallow</i> to consecrate the site it manifested upon prevents
        the undead monster's reappearance.  </p><p><strong>Incorporeal Scythe
        (Su)</strong> A danse macabre wields an incorporeal scythe in combat
        that leaves deep and horrific wounds on any creature it strikes. Attacks
        made with this scythe are touch attacks and deal slashing damage, but
        are not modified by any of the monster's ability scores. A creature hit
        by a danse macabre's incorporeal scythe attack must succeed at a DC 23
        Fortitude save or take 1d6 points of Constitution drain. Each time a
        danse macabre drains Constitution from a target, the danse macabre
        regains 5 hit points, regardless of the total number of Constitution
        points drained by the attack. The save DC is Charisma-based. 
        </p><p><strong>Lifesense (Su)</strong> A danse macabre notices and
        locates living creatures within 60 feet, just as if it had the
        blindsight ability. It also senses the strength of life forces
        automatically, as per <i>deathwatch</i>.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Danse
        macabres are the embodiment of the inevitability of death. They
        represent the ultimate equalizer of station, revealing in their dance of
        death the futility of all life. It has been said that, in the end, all
        mortal beings must face the fateful piper and dance to its tune. If such
        claims are true, then surely the danse macabre is the personification of
        such a dreadful doom.  Danse macabres typically manifest as looming,
        black-cloaked skeletons, although they may appear in other sinister
        forms depending on personifications of death unique to the cultures near
        where they manifest, such as a fiery pillar, a pale child, or a man in a
        white mask. The crowd of dancing souls that surround these 10-foot-tall
        specters typically do little to dispel their ominous aura. While these
        ghostly images are entirely insubstantial and harmless, they also
        emanate faint, haunting music, as if a violin-led orchestra is playing
        along. When a victim succumbs to the creature's dance of death, the
        music becomes nearly deafening and the spectral figures appear almost
        real. The dancers and music cannot be interacted with, but visibly and
        audibly mark the boundaries of the danse macabre's dance of death aura. 
        <strong></strong></p><p><strong>Ecology</strong></p><p>  As undead
        creatures, danse macabres require nothing from their environments and
        contribute nothing in return. Their very presence typically suggests the
        murder of large numbers of creatures and, upon their horrifying
        manifestations, the deaths of others that happen too near. Yet a danse
        macabre will not simply form on the ruins of any battlefield or on the
        site of any mass murder. Instead, these malevolent spirits appear in
        areas where those who were slaughtered did not expect their deaths
        until, perhaps, the very last moment. At other times, a danse macabre
        (such as the one encountered in the ruins of Scarwall) manifests at a
        much later date in an area where revelry and sadism commingled,
        triggered into being long after those who died by a singularly potent
        pulse of necromantic energy or the vengeful thrashings of a bitter and
        angry god.  <strong></strong></p><p><strong>Habitat &amp;
        Society</strong></p><p>  A danse macabre is a solitary creature that
        seeks nothing other than to call others to join its eternal celebration
        of the inevitable. It manifests only in locations tainted by untimely
        deaths-the sites of countless violent executions, estates overrun by
        deadly plagues, or battlefields where mass slaughters took place. In all
        of these locations, hundreds, if not thousands, of victims met their
        fates, often in rapid succession. None claim to know what terrible death
        count or measurement of psychic trauma is required to spawn a danse
        macabre, and indeed, some of the most gore-soaked sites on Golarion have
        never led to one of these beings' manifestations. An element of grim
        irony or communal revenge tends to inspire such hauntings, making it
        impossible to predict what catastrophe or massacre might cause one to
        appear.  Some scholars of the undead suggest that danse macabres harbor
        no hatred for the living, but have a natural drive to bring mortals to
        their final state on an accelerated schedule. Witnesses of danse
        macabres, however, tend to disagree, and the appearance of a danse
        macabre can quickly depopulate a location via both its depredations and
        the flight of any survivors. Fortunately, these morbid shades rarely
        move far from the sites of their initial manifestations, leading to
        numerous tales of haunted halls where the dead endlessly dance in their
        eternal revel.</p></h4></div></section>
    _id: b6om6TCxVHjCuYFh
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!YhYGe8bR9cA3rkcG.b6om6TCxVHjCuYFh'
sort: 0
_key: '!journal!YhYGe8bR9cA3rkcG'

