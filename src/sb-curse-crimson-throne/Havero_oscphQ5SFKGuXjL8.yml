name: Havero
_id: oscphQ5SFKGuXjL8
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Curse_of_the_Crimson_Throne.jpg
    title:
      show: false
      level: 1
    _id: gJfR3TawFdo6lrtS
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!oscphQ5SFKGuXjL8.gJfR3TawFdo6lrtS'
  - name: Havero
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Curse Of The Crimson Throne Chapter
        Appendix</p><section id="Havero"><div class="heading"><p
        class="alignright"><strong>CR</strong> 24</p></div><div><p><strong>XP
        </strong>1,228,800</p><p>NE Colossal aberration </p><p><strong>Init
        </strong>+5; <strong>Senses </strong>all-around vision, blindsense 120
        ft., see in darkness; Perception +47</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>43, touch 3, flat-footed 42 (+1 Dex, +40 natural, -8
        size)</p><p><strong>hp </strong>527 (34d8+374)</p><p><strong>Fort
        </strong>+24, <strong>Ref </strong>+14, <strong>Will
        </strong>+26</p><p><strong>Defensive Abilities </strong>alien mind;
        <strong>DR </strong>20/-; <strong>Immune </strong>cold, mind-affecting
        effects; <strong>Resist </strong>acid 30, electricity 30, fire 30;
        <strong>SR </strong>35</p><p><strong>Weaknesses </strong>light
        sensitivity</p></div><hr /><div><p><strong>OFFENSE</strong></p></div><hr
        /><div><p><strong>Spd </strong>20 ft., fly 60 ft.
        (clumsy)</p><p><strong>Melee </strong>6 tentacles +34 (2d6+17/19-20 plus
        special)</p><p><strong>Space </strong>60 ft.; <strong>Reach </strong>120
        ft.</p><p><strong>Special Attacks </strong>appendages</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>45, <strong>Dex </strong>12, <strong>Con </strong>32,
        <strong>Int </strong> 5, <strong>Wis </strong>21, <strong>Cha
        </strong>30</p><p><strong>Base Atk </strong>+25; <strong>CMB
        </strong>+50 (+52 bull rush, +54 sunder); <strong>CMD </strong>61 (63
        vs. bull rush, 63 vs. sunder)</p><p><strong>Feats </strong>Awesome Blow,
        Critical Focus, Great Fortitude, Greater Sunder, Hover, Improved Bull
        Rush, Improved Critical (tentacle), Improved Great Fortitude, Improved
        Initiative, Improved Lightning Reflexes, Improved Sunder, Iron Will,
        Lightning Reflexes, Power Attack, Skill Focus (Fly), Skill Focus
        (Perception), Staggering Critical</p><p><strong>Skills </strong>Fly +11,
        Perception +47; <strong>Racial Modifiers </strong>+16
        Perception</p><p><strong>Languages </strong>telepathic
        savant</p><p><strong>SQ </strong>no breath</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any (outer
        space)</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure
        </strong>incidental</p></div><hr /><div><p><strong>SPECIAL
        ABILITIES</strong></p></div><hr /><div><p><strong>Alien Mind
        (Ex)</strong> Anyone who attempts to link minds with a havero (such as
        via <i>detect thoughts</i> or telepathy) risks the trauma associated
        with tapping into its raw alien consciousness. A character who attempts
        this must succeed at a DC 37 Will save or suffer the effects of
        <i>feeblemind</i> (CL 20th). The save DC is Charisma-based. 
        </p><p><strong>Appendages (Su)</strong> As a swift action, a havero can
        shape any or all of its 14 squirming tentacles into a variety of
        specialized appendages for various purposes. The options for tentacles a
        havero has at its disposal are listed below (at your discretion, some
        haveros may have developed even more unique or specialized tentacle
        options beyond these). An option's cost in transformation points (TP) is
        listed in parentheses. The total cost of a havero's  tentacles can't
        exceed 20 TP (note that an ocular tentacle costs 0 TP). Damage listed
        for each tentacle is the base damage before the havero's Strength
        modifier is applied. A havero's appendages always function as primary
        attacks. The havero detailed above has devoted 18 TP to six constrictor
        tentacles, leaving the remaining eight to function as ocular tentacles.
        The save DCs for specialized appendage attacks are Constitution-based. 
        <i>Acid Spewer (5 TP)</i>: The tentacle is tipped with a fluid-seeping
        pucker that cannot make melee attacks, but does grant the havero the
        ability to spray acid in a 180-foot line (8d6 acid damage, Reflex DC 38
        half).  <i>Armor Plated (3 TP)</i>: The tentacle is plated in thick
        scales and wraps around the havero's body, increasing its natural armor
        bonus by 2 per armor-plated tentacle. An armor-plated tentacle cannot
        attack.  <i>Constrictor (3 TP)</i>: The tentacle is thick and covered
        with suckers; it deals 2d6 points of bludgeoning damage and has the grab
        and constrict (2d6) abilities.  <i>Ghost (8 TP)</i>: The tentacle
        becomes insubstantial. It resolves attacks as touch attacks, and deals
        6d6 points of negative energy damage on a hit; this damage is modified
        by the havero's Charisma modifier (+10 for a typical havero) but not by
        its Strength modifier. A creature damaged by a ghost tentacle must
        succeed at a DC 38 Fortitude save or take 1d6 points of Charisma drain. 
        <i>Ocular (0 TP)</i>: The tentacle is studded with bulging eyes; it
        cannot attack, but grants the havero all-around vision and a +2 bonus on
        all Perception checks. The bonuses on Perception checks stack with those
        granted by other ocular tentacles. A blind havero that grows an ocular
        tentacle is no longer blind (although this doesn't grant immunity to
        additional blindness effects).  <i>Poison Stinger (5 TP)</i>: The
        tentacle is tipped with a poisonous stinger that deals 4d6 points of
        piercing damage and injects venom on a successful hit. Poison
        stinger-injury; save Fort DC 38; frequency 1/round for 6 rounds; effect
        1d6 Wisdom drain; cure 2 consecutive saves.  <i>Slasher (3 TP)</i>: The
        tentacle has sharp talons; it deals 2d6 points of piercing and slashing
        damage and 2d6 bleed.  <i>Vorpal (12 TP)</i>: The tentacle ends in a
        supernaturally sharp pincer that deals 6d6 points of slashing damage on
        a successful hit, has a 18-20 critical threat range, and has the
        <i>vorpal</i> weapon special ability.  </p><p><strong>Telepathic Savant
        (Su)</strong> A havero can transmit vague impressions of its thoughts
        across limitless distances to any creature it is aware of, including
        those that interact with its tentacles on distant worlds. A havero
        doesn't communicate using language and its thoughts are limited and
        often unintelligible to mortal minds, but it can certainly impart
        powerful basic emotions and primal urges. A havero contacting a creature
        using this ability does not subject the target to its alien
        mind.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>The word "havero" has its roots in ancient Thassilonian,
        roughly translating into "smothering arms." The existence of haveros was
        first confirmed, accidentally, through divination magic used by
        stargazers of ancient Thassilon. Seeking larger, more potent beasts to
        conjure and command, Thassilonian sages among the order known as the
        Thrallkeepers gradually uncovered a massive creature capable of spawning
        unlimited clawed horrors that dwelled far beyond the range of most
        remote viewing devices. Intrigued by a new potential source of power
        buried somewhere in the night sky, the Thrallkeepers engaged in a
        foolish race to be the first to secure a havero.  Horrifically, haveros
        are not the mere imaginings of those sages who chronicle the heavens.
        They are entities of pure darkness, and on terrible occasions a lone
        havero has been drawn to Golarion, putting all the races of the world
        into reach of its ruinous arms. 
        <strong></strong></p><p><strong>Ecology</strong></p><p>  Although their
        thought processes are too alien to permit interpretation, haveros are
        decidedly malevolent. A havero's telepathy is theoretically limitless in
        range, although when sending its mind across galaxies, its mental
        projections require considerable time to travel. The Thrallkeepers
        theorized that the accidental interception of haveros' stray thoughts
        might be a cause for some forms of madness and inexplicable genius. Some
        scholars have even hypothesized that the ancient Thrallkeepers were able
        to transport haveros to Golarion not because of their own discoveries,
        but because the haveros quietly sent them the necessary ideas for how
        they might accomplish the feat.  <strong></strong></p><p><strong>Habitat
        &amp; Society</strong></p><p>  An ancient tome of starry observations
        and occult lore titled <i>On Verified Madness</i> refers to the haveros'
        home as the farthest corner of the Dark Tapestry. Haveros have no
        natural niche on Golarion and they exist there only due to the
        machinations of those who once had both the power and the recklessness
        to transport them to this world.  <br /><strong>Havero
        Tentacle</strong><br />  A single havero has the potential to overwhelm
        even experienced PCs. GMs who wish to model the threat of a havero
        without killing the entire adventuring party can use the statistics
        below for a lone havero tentacle, perhaps poking through an
        interdimensional portal or snaking up from a deep canyon. However,
        destroying a single one-or even a handful-of the havero's tentacles
        doesn't kill the horror, but only buys the PCs enough time to escape and
        live to see another day.</p></h4></div></section>
    _id: mgtxgS9OBQxyeoi6
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!oscphQ5SFKGuXjL8.mgtxgS9OBQxyeoi6'
sort: 0
_key: '!journal!oscphQ5SFKGuXjL8'

