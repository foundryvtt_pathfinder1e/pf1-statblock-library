name: Sunbaked Zombie
_id: rEzn8DWFdYkJY5Pg
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Mummys_Mask.jpg
    title:
      show: false
      level: 1
    _id: EiOcC06T3nOKixKb
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!rEzn8DWFdYkJY5Pg.EiOcC06T3nOKixKb'
  - name: Sunbaked Zombie
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 80</p><section id="SunbakedZombie"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        1</p></div><div><p><strong>XP </strong>400</p><p>NE Medium undead
        </p><p><strong>Init </strong>+1; <strong>Senses </strong>darkvision 60
        ft.; Perception +0</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>13, touch 11, flat-footed 12 (+1 Dex, +2
        natural)</p><p><strong>hp </strong>12 (2d8+3)</p><p><strong>Fort
        </strong>+0, <strong>Ref </strong>+1, <strong>Will
        </strong>+3</p><p><strong>DR </strong>5/slashing; <strong>Immune
        </strong>undead traits; <strong>Resist </strong>fire 10</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong>slam +4
        (1d6+4)</p><p><strong>Space </strong>5 ft.; <strong>Reach </strong>5
        ft.</p><p><strong>Special Attacks </strong>death throes (DC 11), fiery
        gaze (1d6 fire, DC 11)</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>17, <strong>Dex </strong>12, <strong>Con </strong>-,
        <strong>Int </strong> -, <strong>Wis </strong>10, <strong>Cha
        </strong>10</p><p><strong>Base Atk </strong>+1; <strong>CMB </strong>+4;
        <strong>CMD </strong>15</p><p><strong>Feats
        </strong>ToughnessB</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        warm</p><p><strong>Organization </strong>solitary, pair, or infestation
        (3-12)</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Most
        creatures that die in the desert are consumed by roaming scavengers or
        buried forever by sand, yet those who avoid such a fate bake under the
        merciless sun. Such desiccated corpses near pyramids or other areas with
        strong necromantic magic sometimes rise as sunbaked zombies. With their
        corporeal bodies warped by the sun, and their innards but a dry
        facsimile of living organs, sunbaked zombies use the gifts of the sun to
        lash those who cross their paths. A sunbaked zombie is the same size as
        its original form, but typically weighs less than half its original
        weight.  <strong></strong></p><p><strong>Ecology</strong></p><p>  Other
        than the dry, leathery skin clinging to the creature's bones, the most
        striking physical difference between a sunbaked zombie and a normal
        zombie is the sunbaked zombie's flame-filled eye sockets. Though it
        lacks functioning eyes-those having long since shriveled to dust-the
        sunbaked zombie can see as well as any other zombie, and the flames in
        its dried sockets can set enemies afire. With its dried, taut skin and
        taut and insides reduced to dust, it retains a normal zombie's
        resistance to damage. When its animating spark is extinguished, a
        sunbaked zombie's corpse bursts into a cloud of poisonous gas, choking
        those around it.  So long as the sun shines, sunbaked zombies move about
        as if with purpose. At night, however, they wander in circles until the
        sun blazes across the morning sky  once more. The faint light given off
        by their eye sockets, combined with their stumbling movement, has led
        some desert nomads to claim they are desert will-o-wisps. 
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        Sun-baked zombies most often rise near pyramids and other burial sites
        in hot deserts, where latent necromantic energy lingers from countless
        arcane rituals and restless spirits. As such, sunbaked zombies are
        primarily found among the dunes of Osirion and the other nations that
        make up northern Garund. Typically animated in isolation, sunbaked
        zombies rarely form hordes like normal zombies, but when entire caravans
        fall to thirst and the desert sun, all of its members might rise as
        these terrible undead.  When one intentionally raises a sunbaked zombie
        using <i>animate dead</i>, the body to be raised must be left out in the
        sun's rays for a full 12 hours and must be salted every hour during this
        time to hasten its desiccation. Spell effects that produce light work
        for this purpose only if they count as actual sunlight, and even then
        they must be combined with <i>desecrate</i>. Casting the animating spell
        at night always fails; the sun must be out and directly beating down on
        the corpse. Without the intense magical focus of a spell, it takes many
        days for the corpse to absorb enough sun and necromantic energy to rise
        spontaneously.  <br /><strong>Creating a Sunbaked Zombie</strong><br />
        "Sunbaked zombie" is an acquired template that can be added to any
        corporeal creature (other than undead), referred to hereafter as the
        base creature.  <br /><strong>CR:</strong> This depends on the
        creature's new total number of Hit Dice, as follows.  
        </p><table><tbody><tr><th>HD</th><th>CR</th><th>XP</th></tr><tr><td>1</td><td>1/2</td><td>200</td></tr><tr><td>2</td><td>1</td><td>400</td></tr><tr><td>3-4</td><td>2</td><td>600</td></tr><tr><td>5-6</td><td>3</td><td>800</td></tr><tr><td>7-8</td><td>4</td><td>1,200</td></tr><tr><td>9-10</td><td>5</td><td>1,600</td></tr><tr><td>11-12</td><td>6</td><td>2,400</td></tr><tr><td>13-16</td><td>7</td><td>3,200</td></tr><tr><td>17-20</td><td>8</td><td>4,800</td></tr><tr><td>21-24</td><td>9</td><td>6,400</td></tr></tbody></table>   
        <br /><strong>Alignment:</strong> Always neutral evil.  <br
        /><strong>Type:</strong> The creature's type changes to undead. It
        retains any subtypes except for alignment subtypes (such as good) and
        subtypes that indicate kind (such as giant). It does not gain the
        augmented subtype.  <br /><strong>Armor Class:</strong> The natural
        armor bonus is based on the creature's size.  
        <table><tbody><tr><th>Sunbaked Zombie Size</th><th>Natural Armor
        Bonus</th></tr><tr><td>Tiny or
        smaller</td><td>+0</td></tr><tr><td>Small</td><td>+1</td></tr><tr><td>Medium</td><td>+2</td></tr><tr><td>Large</td><td>+3</td></tr><tr><td>Huge</td><td>+4</td></tr><tr><td>Gargantuan</td><td>+7</td></tr><tr><td>Colossal</td><td>+11</td></tr></tbody></table>    
        <br /><strong>Hit Dice:</strong> Drop Hit Dice gained from class levels
        (to a minimum of 1) and change racial HD to d8s. Sunbaked zombies gain
        additional HD as noted on the following table. Sunbaked zombies use
        their Charisma modifiers to determine bonus hit points (instead of
        Constitution).    <table><tbody><tr><th>Sunbaked Zombie Size
        Bonus</th><th>Hit Dice</th></tr><tr><td>Tiny or
        smaller</td><td>-</td></tr><tr><td>Small or Medium</td><td>+1
        HD</td></tr><tr><td>Large</td><td>+2 HD</td></tr><tr><td>Huge</td><td>+4
        HD</td></tr><tr><td>Gargantuan</td><td>+6
        HD</td></tr><tr><td>Colossal</td><td>+10
        HD</td></tr></tbody></table>     <br /><strong>Saves:</strong> A
        sunbaked zombie's base save bonuses are Fort +1/3 HD, Ref +1/3 HD, and
        Will +1/2 HD + 2.  <br /><strong>Defensive Abilities:</strong> A
        sunbaked zombie loses the base creature's defensive abilities and gains
        DR 5/slashing and resist fire 10 (or immunity to fire if it has 11 HD or
        more), as well as all of the standard immunities and traits granted by
        the undead type.  <br /><strong>Speed:</strong> Winged sunbaked zombies
        can still fly, but their maneuverability drops to clumsy. If the base
        creature flew magically, so can the sunbaked zombie. Retain all other
        movement types.  <br /><strong>Attacks:</strong> A sunbaked zombie
        retains all natural weapons, manufactured weapon attacks, and weapon
        proficiencies of the base creature. It also gains a slam attack that
        deals damage based on the sunbaked zombie's size, but as if it were one
        size category larger than its actual size (<i>Pathfinder RPG
        Bestiary</i> 301-302).  <br /><strong>Special Attacks:</strong> A
        sunbaked zombie retains none of the base creature's special attacks, but
        gains the following.  <br /><i>Death Throes (Su)</i>: When a sunbaked
        zombie is destroyed, its body explodes in a burst of stale dust.
        Adjacent creatures must succeed at a Fortitude save or be staggered for
        1d4+1 rounds. The DC is equal to 10 + 1/2 the sunbaked zombie's Hit Dice
        + the sunbaked zombie's Cha modifier. Creatures that don't breathe are
        immune to this effect.  <br /><i>Fiery Gaze (Su)</i>: A sunbaked
        zombie's eye sockets flicker with a small flame that gives light
        equivalent to that of a candle. As a standard action, a sunbaked zombie
        can direct its gaze against a single creature within 30 feet of it. A
        creature targeted must succeed at a Fortitude save  or take 1d6 points
        of fire damage. If the sunbaked zombie has 5 or more Hit Dice, its fiery
        gaze deals 2d6 points of fire damage, and this damage increases by an
        additional 1d6 points of fire damage for every 4 additional Hit Dice the
        sunbaked zombie possesses. A creature damaged by this effect must
        succeed at a Reflex save or catch fire. Each round, a burning creature
        can attempt a Reflex save to quench the flames; failure results in
        another 1d6 points of fire damage. Flammable items worn by a creature
        must also save or take the same damage as the creature. If a creature is
        already on fire, it suffers no additional effects from a fiery gaze. The
        save DC is Charisma-based.  <br /><strong>Abilities:</strong> Str +2. A
        sunbaked zombie has no Con or Int score, and its Wis and Cha become 10. 
        <br /><strong>BAB:</strong> A sunbaked zombie's base attack bonus is
        equal to 3/4 of its Hit Dice.  <br /><strong>Skills:</strong> A sunbaked
        zombie has no skill ranks.  <br /><strong>Feats:</strong> A sunbaked
        zombie loses all feats possessed by the base creature and gains
        Toughness as a bonus feat.  <br /><strong>Special Qualities:</strong> A
        sunbaked zombie loses most special qualities of the base creature. It
        retains any extraordinary special qualities that improve its melee or
        ranged attacks.</h4></div></section>
    _id: 13BFdpeqhihlZ67i
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!rEzn8DWFdYkJY5Pg.13BFdpeqhihlZ67i'
sort: 0
_key: '!journal!rEzn8DWFdYkJY5Pg'

