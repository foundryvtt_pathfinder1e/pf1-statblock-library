name: Gishvit
_id: MAOJC91qvi2pS3Pd
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/War_for_the_Crown.jpg
    title:
      show: false
      level: 1
    _id: JI0OdWSuzqR34FuQ
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!MAOJC91qvi2pS3Pd.JI0OdWSuzqR34FuQ'
  - name: Gishvit
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 127</p><section id="Gishvit"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        1/2</p></div><div><p><strong>XP </strong>200</p><p>LN Tiny outsider
        (extraplanar, lawful)</p><p><strong>Init </strong>+6; <strong>Senses
        </strong>darkvision 60 ft.; Perception +4</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>14, touch 14, flat-footed 12 (+2 Dex, +2 size)</p><p><strong>hp
        </strong>4 (1d10-1)</p><p><strong>Fort </strong>-1, <strong>Ref
        </strong>+4, <strong>Will </strong>+2</p><p><strong>Weaknesses
        </strong>vulnerability to erase</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>20 ft., climb 20 ft.</p><p><strong>Melee </strong>bite +1
        (1d4-2), ribbon +3 touch (grab)</p><p><strong>Space </strong>2-1/2 ft.;
        <strong>Reach </strong>0 ft. (10 ft. with ribbon)</p><p><strong>Special
        Attacks </strong>overwhelm, ribbon</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>6, <strong>Dex </strong>15, <strong>Con </strong>9, <strong>Int
        </strong> 11, <strong>Wis </strong>10, <strong>Cha
        </strong>14</p><p><strong>Base Atk </strong>+1; <strong>CMB </strong>+1
        (+3 grapple, +11 when maintaining grapple with ribbon); <strong>CMD
        </strong>9 (11 vs. grapple, 17 vs. trip)</p><p><strong>Feats
        </strong>Improved GrappleB, Improved Initiative</p><p><strong>Skills
        </strong>Climb +10, Knowledge (arcana) +4, Knowledge (history) +4,
        Knowledge (planes) +4, Perception +4, Sense Motive
        +4</p><p><strong>Languages </strong>truespeech (can't
        speak)</p><p><strong>SQ </strong>transcription</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        (Axis)</p><p><strong>Organization </strong>solitary, pair, or curation
        (3-12)</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Overwhelm (Su)</strong> A gishvit can transfer the
        information within its pages to a single target through its grasping
        ribbon. When a gishvit has successfully grappled a foe, it can force
        this information directly into the grabbed creature's mind, overwhelming
        it with an onslaught of facts and recorded information. The grappled
        creature must succeed at a DC 12 Will save or be staggered for 1d4
        rounds. The save DC is Charisma-based.  </p><p><strong>Ribbon
        (Ex)</strong> A gishvit's tongue is a primary attack with reach of 10
        feet. A gishvit applies its Dexterity modifier as a bonus on attack
        rolls with its bookmark ribbon. A gishvit's ribbon deals no damage on a
        hit, but it can be used to grab the target. A gishvit has a +8 racial
        bonus to maintain its grapple on a foe grabbed by its ribbon. A gishvit
        does not gain the grappled condition while using its ribbon in this
        manner.  </p><p><strong>Transcription (Su)</strong> A gishvit finds
        pleasure in being a repository of information. It can immediately
        transcribe information dictated to it and material it overhears.
        Alternately, a gishvit can spend an hour with a willing  subject,
        wherein it collects memories and experiences from the subject. This
        transcription process is not dangerous to either the gishvit or the
        target.  Information recorded and stored in this manner appears as
        filled pages within the gishvit's booklike body, and it remains there
        for up to 24 hours after the gishvit is slain (unless the gishvit was
        killed by an <i>erase</i> spell, see below). Another creature can
        research the pages of a willing or deceased gishvit. The text inside a
        gishvit is comprehensible to any creature that can read and knows at
        least one written language. Researching the information within a gishvit
        for 1 hour grants the reader a +4 insight bonus on any single Knowledge
        check in the next 24 hours, and the reader can attempt such checks even
        if not trained in that Knowledge skill.  </p><p><strong>Vulnerability to
        Erase (Su)</strong> An <i>erase</i> spell deals 2d4 points of damage to
        a gishvit and has a 50% chance of removing all written text within its
        pages (no save). A gishvit reduced to 0 hit points or below in this
        manner is slain, becoming a blank book.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Sometimes
        referred to as "lore roaches" by scholars familiar with the Outer
        Planes, gishvits are natural inhabitants of the lawful plane of Axis.
        Often associated with the native axiomites and scrivenites, gishvits
        occupy a mundane role on Axis, maintaining vast stores of knowledge
        within their booklike bodies. Transcribing knowledge from living
        creatures, a gishvit stores information in its body in the form of
        written text, preserving the acquired knowledge within itself. Because
        of this unique method of information gathering, gishvits are treasured
        by information brokers and spellcasters on the Material Plane.  A
        gishvit varies between 1 to 2 feet in length, often weighing no more
        than 8 pounds.  <strong></strong></p><p><strong>Ecology</strong></p><p>
        The booklike body of a gishvit is indicative of its ultimate purpose:
        the consumption and storage of knowledge. For as long as the ordered
        realm of Axis has existed, gishvits have clambered throughout its
        seemingly endless streets and meandered within its many archives of
        knowledge. Some scholars believe that these creatures first emerged from
        the quintessence of Axis as companions to axiomites and scrivenites.
        While axiomites tend to ignore the pesky little creatures, scrivenites
        associate with the booklike outsiders, sometimes even treating them like
        pets more than pests.  A gishvit has a set of razor-sharp teeth that
        emerge from the edges of its cover page and back page, though it lacks
        the strength to use them with great effectiveness. These teeth are
        visible only when the outsider opens its mouth. A set of similarly sharp
        insectile limbs emerge from within the gishvit's body, acting as legs
        that can be retracted at a whim to act as embellishments for the 
        gishvit's spine. A whipping, silken bookmark tongue that issues from
        between the pages of its body completes the gishvit's odd anatomy.  A
        gishvit absorbs knowledge from its surroundings and from individuals who
        wish to store their memories and experiences within the creature's
        pages. Once the small outsider has consumed a sufficient amount of
        knowledge, it darts off to digest the information. This digestion takes
        place over several hours, during which the gishvit's interior pages fill
        with the stored lore.  Gishvits understand all spoken languages thanks
        to their truespeech ability, though they lack vocal cords and the
        ability to speak. Only creatures capable of telepathic communication can
        engage a gishvit in conversation, and even then, such conversations are
        generally dominated by the gishvit's ravenous desire for new information
        or its hope to hibernate and mull over the stored information after
        filling its pages with stores of memories and experiences. Despite
        having considerable insight into numerous topics, even without ingesting
        any knowledge from other creatures, a gishvit finds little joy in
        relating such information through conversation. Negotiators find
        conversations with gishvits incredibly difficult, and though gishvits
        rarely have the chance to converse with other beings, they still find
        most conversations a detraction from their endless urge to transcribe
        knowledge.  In addition to being able to collect a wealth of information
        into their small forms, gishvits are known for their knack for
        transcribing accurate information. Since they interact directly with
        subjects' experiences and memories, they have the ability to cut through
        bias and misremembered experiences. This makes the information stored
        within a gishvit valuable to court proceedings and other instances where
        the truth is more important than a potentially flawed statement.  <br
        /><strong>HABITAT &amp; SOCIETY</strong><br />  Most inhabitants of Axis
        are powerful enough to pay gishvits little more heed than a creature
        from the Material Plane would associate with most common vermin. Despite
        the creatures' lowly status, several factions on the lawful plane employ
        gishvits as negotiation tools or unintentional spies. In these roles,
        the little gishvits excel, as they are able to extract knowledge from
        negotiators and put it to paper or collect valuable and accurate
        information from stationed guards. Still, many creatures on Axis have
        far more reliable means of extracting information, so they  instead
        relegate gishvits to a pitiable status as planar vermin. It is not
        uncommon for an eager gishvit to pester a creature enjoying a meal
        inside one of Axis's many restaurants to spend time with it in hopes of
        adding lore to its pages, or for a patron of the plane's many archives
        to find herself accosted by a whipping bookmark tongue when she picks it
        up by accident while the creature was resting on a shelf.  On both Axis
        and the Material Plane, a satiated gishvit quickly enters a state of
        gradual torpor, followed by a period of hibernation. Such gishvits often
        seek to rest in large archives or repositories, hiding among hundreds or
        thousands of similar-looking mundane books. They then wait for centuries
        at a time, content to rest with the knowledge they have digested. After
        several centuries, a gishvit willingly discards its digested knowledge.
        It is then instinctually compelled to repeat the process of acquiring
        and digesting new information. Sadly for the gishvit, it often finds
        itself trapped in a now-long forgotten archive, unable to escape. In
        these unfortunate circumstances, the next creatures to enter the
        gishvit's sealed-off home are immediately assaulted by the
        knowledge-ravenous creatures.</p></h4></div></section>
    _id: isfnBNyB6gb5KOsl
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!MAOJC91qvi2pS3Pd.isfnBNyB6gb5KOsl'
sort: 0
_key: '!journal!MAOJC91qvi2pS3Pd'

