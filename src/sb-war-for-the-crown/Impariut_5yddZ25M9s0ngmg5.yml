name: Impariut
_id: 5yddZ25M9s0ngmg5
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/War_for_the_Crown.jpg
    title:
      show: false
      level: 1
    _id: XEqs2SCp0vhZdgGP
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!5yddZ25M9s0ngmg5.XEqs2SCp0vhZdgGP'
  - name: Impariut
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 131</p><section id="Impariut"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        10</p></div><div><p><strong>XP </strong>9,600</p><p>LN Medium outsider
        (extraplanar, inevitable, lawful)</p><p><strong>Init </strong>+2;
        <strong>Senses </strong>darkvision 60 ft., low-light vision; Perception
        +23</p></div><hr /><div><p><strong>DEFENSE</strong></p></div><hr
        /><div><p><strong>AC </strong>24, touch 12, flat-footed 22 (+2 Dex, +12
        natural)</p><p><strong>hp </strong>134 (12d10+68); regeneration 5
        (chaotic)</p><p><strong>Fort </strong>+12, <strong>Ref </strong>+8,
        <strong>Will </strong>+12</p><p><strong>Defensive Abilities
        </strong>constructed; <strong>DR </strong>10/chaotic; <strong>SR
        </strong>21</p></div><hr /><div><p><strong>OFFENSE</strong></p></div><hr
        /><div><p><strong>Spd </strong>30 ft.</p><p><strong>Melee
        </strong><i><i>+1 mithral longsword</i></i> +18/+13/+8 (1d8+6/19-20),
        slam +12 (1d8+2) or 2 slams +17 (1d8+5)</p><p><strong>Space </strong>5
        ft.; <strong>Reach </strong>5 ft.</p><p><strong>Special Attacks
        </strong>dethrone, kingslayer</p><p><strong>Spell-Like
        Abilities</strong> (CL 12th; concentration +17) <br />At Will—<i>arcane
        mark</i>, <i>discern lies</i> (DC 19), <i>dispel magic</i> <br
        />3/day—<i>dimension door</i>, <i>hold monster</i> (DC 20),
        <i>invisibility</i>, <i>magic missile</i>, <i>mark of justice</i> <br
        />1/day—<i>plane shift</i> (willing targets only), <i>true
        seeing</i></p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>20, <strong>Dex </strong>15, <strong>Con </strong>18,
        <strong>Int </strong> 15, <strong>Wis </strong>18, <strong>Cha
        </strong>21</p><p><strong>Base Atk </strong>+12; <strong>CMB
        </strong>+17 (+19 reposition, steal); <strong>CMD </strong>29 (31 vs.
        reposition or steal)</p><p><strong>Feats </strong>Alertness, Combat
        Expertise, Improved Reposition, Improved Steal, Lightning Reflexes,
        Power Attack</p><p><strong>Skills </strong>Diplomacy +20, Disguise +17,
        Intimidate +20, Knowledge (history, nobility, planes) +14, Perception
        +23, Sense Motive +23, Stealth +8</p><p><strong>Languages
        </strong>truespeech</p><p><strong>SQ </strong>change shape (Small or
        Medium humanoid; <i>alter</i> self), royal gift</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        urban</p><p><strong>Organization </strong>solitary, pair, or
        intervention (3-5)</p><p><strong>Treasure </strong>standard (<i>+1
        mithral longsword</i>, other treasure)</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Dethrone (Ex)</strong> An impariut can attempt a
        reposition or steal combat maneuver in place of a slam attack. 
        </p><p><strong>Kingslayer (Su)</strong> Against any creature with a
        title denoting leadership or nobility (such as a baroness, chief, or
        lord), an impariut's attacks gain a +2 bonus on attack rolls and deal an
        additional 1d8 damage. Against any creature recognized as an autonomous
        region's head of state (such as an empress or king), an impariut's
        attacks instead gain a +4 bonus on attack rolls, deal an additional 2d8
        damage, and ignore any damage reduction the target has. 
        </p><p><strong>Royal Gift (Su)</strong> Once per day as a full-round
        action, an impariut can grant its blessing to a willing humanoid
        creature by touching it for 1 full round. The target gains a  +1 sacred
        bonus on Will saving throws; a +2 sacred bonus on saving throws against
        disease and poison; and a +2 sacred bonus on Knowledge (history),
        Knowledge (nobility), and Sense Motive checks for 1 year. A single
        creature can have no more than one royal gift from an impariut at a
        time. As long as the royal gift lasts, the impariut can sense the
        target's position and general condition, as per <i>status</i>. The
        impariut can remove a royal gift as a free action, and a royal gift can
        be removed by <i>dispel law</i>. An impariut can only maintain one royal
        gift for every 4 Hit Dice it has (3 royal gifts for a standard
        impariut).</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>Monarchs, dictators, governors, and mayors may rule under
        the pretense of sustaining lawful civilization, yet those who abuse
        their power or harm their people undermine the foundation of government.
        To the forces of Axis, these bad actors represent a threat to cosmic
        order and a means by which the chaos of uprisings and revolution can
        take hold. The mortal agents of lawful deities are responsible for
        encouraging responsible governance; where they fail, an impariut must
        intervene.  Each impariut is a stern counselor sent to reform a powerful
        leader, guiding that figure toward sustainable policies that maintain
        order. Unlike many inevitables that care only that the universal order
        is upheld, impariuts exhibit slightly more patience and flexibility,
        knowing that an iron-fisted response is as likely to trigger riots as a
        negligent regime. As a result, impariuts are capable diplomats and
        teachers, equally proficient with incentives as with unflinching
        punishments.  An impariut stands precisely 7 feet tall and weighs
        exactly 350 pounds. 
        <strong></strong></p><p><strong>Ecology</strong></p><p> Impariuts are
        armored advisors built from the petitioners of mortal rulers in the
        Adamantine Crucible of Axis. Whereas a petitioner's memories are all but
        destroyed when creating most inevitables, impariuts maintain a
        fundamental sense of their accomplishments and failings during their
        mortal lives-all the better to recognize these deeds in others. Each
        impariut also bears a slightly different appearance that reflects the
        designs and fashion of its former existence, such as wearing the colors
        or regalia associated with its former kingdom. That said, most of every
        impariut's appearance, knowledge, and morality is standardized, infused
        into its body upon its creation. This knowledge includes an encyclopedic
        expertise on mortal history, law, methods of civic administration, and
        social welfare.  <strong></strong></p><p><strong>Habitat &amp;
        Society</strong></p><p>  Impariuts engage in extended assignments beyond
        Axis, often infiltrating a royal court for several weeks to learn about
        a disreputable ruler. After assembling a proper profile of the target,
        the inevitable typically  approaches the wayward ruler in private to
        reveal its true identity, enumerate the ruler's foibles, detail the
        likely consequences of continued negligence, and provide an assessment
        of how best to correct the situation. In a typical case, the impariut
        offers the ruler its ongoing assistance as an advisor to help steer the
        leader back to a proper path. However, if at any point the inevitable
        ascertains that the ruler is beyond redemption, it is empowered to
        execute the head of state and shepherd the best suitable replacement.
        This replacement is usually the legal heir, but if that individual is
        also deemed to be dishonest, an impariut will balance public sentiment
        with legal entitlement to identify the best candidate.  When a ruler's
        deposition seems immediately necessary, an impariut might forego private
        counseling and cast off its disguise to condemn a wayward ruler before a
        crowd, snatch his crown, and hurl him from his seat of power. In rare
        cases in which corruption is widespread, an impariut might gather a
        local council of lesser leaders to establish a new government in as
        peaceful a way as possible. The only taboo for impariuts is that each
        avoids directly intervening in any nations it ruled during its mortal
        life. This does not prevent an impariut from answering a distant
        descendant's question if contacted directly, but impariuts view anything
        more than this reactive assistance as a conflict of interest.  As
        inevitables, impariuts are physically tireless and capable of working
        for centuries at a time. However, extended exposure to irresponsible
        governance sometimes erodes an impariut's judgment or causes it to take
        especially drastic measures when a lighter hand would suffice. These
        rogue impariuts are very rare, and each one rebels in its own way; one
        might become a serial killer of monarchs, whereas another might
        establish itself as the leader of a nation. Other inevitables or lawful
        outsiders usually neutralize these malfunctioning impariuts swiftly. To
        minimize these transgressions, senior inevitables encourage impariuts to
        return to the Eternal City between assignments. There these inevitables
        can bask in the perfect orderliness, grounding themselves and mentally
        preparing to guide another generation of imperfect nobles. Impariuts
        between assignments share information freely, evaluating and circulating
        the most effective leadership techniques, disciplinary tactics, and
        methods of avoiding any unintended consequences of their intervention. 
        Even though impariuts most often answer orders from Axis, a mortal
        spellcaster can petition an impariut to serve as a counselor for a
        careless king or tutor for an adolescent heir. So long as the
        inevitable's  proposed pupil seems able to learn and rule, an impariut
        might charge as little as 500 gp per month of nonhazardous instruction. 
        Those spellcasters who would instead force an impariut to obey can
        placate it by offering rare history volumes and noteworthy biographies,
        gaining a +1 bonus on the binder's Charisma check for every 100 gp worth
        of literature provided (maximum +4). At the same time, though, an
        impariut judges the conjurer, assessing whether the spellcaster might
        ultimately undermine the region's rule of law-and whether to destroy the
        conjurer at a later date.</p></h4></div></section>
    _id: I9jHSUUCuo4EVQiS
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!5yddZ25M9s0ngmg5.I9jHSUUCuo4EVQiS'
sort: 0
_key: '!journal!5yddZ25M9s0ngmg5'

