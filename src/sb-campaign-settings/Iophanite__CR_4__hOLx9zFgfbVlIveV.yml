name: Iophanite (CR 4)
_id: hOLx9zFgfbVlIveV
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: 9WH6xtFwIDnbZsdq
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!hOLx9zFgfbVlIveV.9WH6xtFwIDnbZsdq'
  - name: Iophanite (CR 4)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Inner Sea Gods</p><section
        id="Iophanite"><div class="heading"><p
        class="alignright"><strong>CR</strong> 4</p></div><div><p><b>XP
        </b>1,200</p><p>LG Medium outsider (angel, extraplanar, fire, good,
        lawful)</p><p><b>Init </b>+3; <b>Senses </b>darkvision 60 ft., low-light
        vision; Perception +12</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>18, touch 14,
        flat-footed 14 (+3 Dex, +1 dodge, +4 natural; +2 deflection vs.
        evil)</p><p><b>hp </b>37 (5d10+10)</p><p><b>Fort </b>+6, <b>Ref </b>+7,
        <b>Will </b>+1; +4 vs. poison, +4 resistance vs. evil</p><p><b>DR
        </b>5/magic; <b>Immune </b>fire, petrification; <b>Resist </b>acid 5,
        cold 5,  electricity 5; <b>SR </b>15</p><p><b>Weaknesses </b>vulnerable
        to cold</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>fly 40 ft. (perfect)</p><p><b>Melee </b>2 blades +8
        (1d8+1 plus burn)</p><p><b>Space </b>5 ft.; <b>Reach </b>5
        ft.</p><p><b>Special Attacks </b>burn (1d6 fire, DC 14),
        radiance</p><p><b>Spell-Like Abilities</b> (CL 6th; concentration +7)<br
        />Constant—<i>protection from evil</i><br />At Will—<i>know
        direction</i>, <i>mage hand</i><br />3/day—<i>burning hands</i> (DC 12),
        <i>expeditious retreat</i><br />1/day—<i>scorching ray</i></p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>12, <b>Dex
        </b>17, <b>Con </b>14, <b>Int </b> 10, <b>Wis </b>11, <b>Cha
        </b>13</p><p><b>Base Atk </b>+5; <b>CMB </b>+6; <b>CMD
        </b>20</p><p><b>Feats </b>Dodge, Mobility, Weapon
        Finesse</p><p><b>Skills </b>Acrobatics +1, Fly +7, Intimidate +9,
        Knowledge (geography) +8, Knowledge (planes) +8, Knowledge (religion)
        +5, Perception +12; <b>Racial Modifiers </b>+4
        Perception</p><p><b>Languages </b>Celestial, Infernal; 
        truespeech</p><p><b>SQ </b>shield form</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        (Heaven)</p><p><b>Organization </b>solitary, pair, or squad
        (3-8)</p><p><b>Treasure </b>standard</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Radiance (Sp)</b> An iophanite
        usually glows with a golden light equivalent to that of a candle. In
        battle, its glow increases, filling the area within 5 feet.  An
        iophanite can suppress or resume this glow as a free action.
        Additionally, as a standard action at will an iophanite can intensify
        its glow to reproduce the effects of the spell <i>flare</i>.  A creature
        can resist this effect with a DC 14 Fortitude save, though evil
        creatures take a -4 penalty on their saves. The save DC is
        Charisma-based.  </p><p><b>Shield Form (Su)</b> Once per day, an
        iophanite can transform into a <i>+1 spiked light steel shield</i> sized
        for a Small or Medium creature. An iophanite cannot communicate or use
        any of its other abilities while in this form. Once it transforms, it
        cannot change back for 24 hours, though the spell <i>break
        enchantment</i> can end the transformation early. An iophanite regains
        its full hit points when it shifts back into its normal form. If the
        shield is destroyed, the iophanite is killed.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Iophanites
        comprise a unique classification of messenger angel in service to
        Iomedae. Energetic, enthusiastic, impulsive, and outgoing, it constantly
        looks for ways to aid the forces of righteousness in the wars against
        fiends and villainy. As a being of pure righteous spirit in a burning
        physical form, an iophanite is often confused for an exotic form of
        harbinger or lantern archon. Its form is slightly mutable, and it can
        deform itself into an oval curved like a shield, or manifest simple
        tendrils to manipulate objects or lash out at opponents. When
        interacting with mortals, an iophanite usually manifests ripples of
        flames or pulses of light that thrum in time with its speaking. Most
        creatures have a difficult time telling iophanites apart, but these
        angels can always recognize each other.</p><p>Iophanites are talkative
        and have excellent memories, making them naturally inclined to carry
        news, battle orders, and other critical information. Their bodies are
        hot to the touch, but do not ignite combustibles unless the iophanite
        wishes it, allowing it to carry scrolls or other flammable goods without
        risk of destroying them. Of the lawful angels, they tend to be the most
        creative in terms of working around rules and the most forgiving of
        others' failures. They are also inclined to serve as the guardians of
        mortals, being mindful of virtuous and selfsacrificing heroes who might
        be destined for greatness in much the same way Iomedae herself
        was.</p><p>Iophanites measure exactly 5 feet in diameter and weigh 200
        pounds.</p></h4></div></section>
    _id: yDGmAOPMqRXs3nLS
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!hOLx9zFgfbVlIveV.yDGmAOPMqRXs3nLS'
sort: 0
_key: '!journal!hOLx9zFgfbVlIveV'

