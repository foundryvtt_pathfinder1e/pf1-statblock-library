name: Burleev (CR 4)
_id: xpJsjfL8kVarCGNE
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: HuqWcLISBqgI1X1t
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!xpJsjfL8kVarCGNE.HuqWcLISBqgI1X1t'
  - name: Burleev (CR 4)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Inner Sea Gods</p><section id="Burleev"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        4</p></div><div><p><b>XP </b>1,200</p><p>N Medium outsider (cold or
        fire, extraplanar)</p><p><b>Init </b>+4; <b>Senses </b>darkvision 60
        ft., <i>detect magic</i>; Perception +9</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>16, touch 10,
        flat-footed 16 (+6 natural)</p><p><b>hp </b>32 (5d10+5)</p><p><b>Fort
        </b>+5, <b>Ref </b>+3, <b>Will </b>+5</p><p><b>Defensive Abilities
        </b>frostfire spirit,; <b>DR </b>5/magic; <b>Immune </b>cold or fire;
        <b>SR </b>15</p><p><b>Weaknesses </b>vulnerable to cold or
        fire</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd
        </b>30 ft.</p><p><b>Melee </b>2 slams +5 (1d6 plus 1d6 cold or
        fire)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Spell-Like
        Abilities</b> (CL 5th; concentration +8)<br />At will—<i>detect
        magic</i>, <i>read magic</i><br />3/day—<i>cure light wounds</i><br
        />1/day—<i>invisibility</i></p><p><b>Sorcerer Spells Known</b> (CL 5th;
        concentration +8)<br />2nd (5)—<i>acid arrow</i>, <i><i>daze</i>
        monster</i> (DC 15)<br />1st (7)—<i>chill touch</i> (DC 14), <i>color
        spray</i> (DC 14), <i>magic missile</i>, <i>sleep</i> (DC 14)<br />0 (at
        will)—<i>dancing lights</i>, <i>daze</i> (DC 13), <i>disrupt undead</i>,
        <i>ghost sound</i> (DC 13), <i>mage hand</i>,
        <i>prestidigitation</i></p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>11, <b>Dex
        </b>10, <b>Con </b>13, <b>Int </b> 10, <b>Wis </b>13, <b>Cha
        </b>16</p><p><b>Base Atk </b>+5; <b>CMB </b>+5; <b>CMD
        </b>15</p><p><b>Feats </b>Combat Casting, Improved Initiative, Lightning
        Reflexes</p><p><b>Skills </b>Intimidate +11, Knowledge (arcana) +8,
        Knowledge (planes) +8, Perception +9, Spellcraft +8, Stealth +4, Use
        Magic Device +11; <b>Racial Modifiers </b>+4 Stealth</p><p><b>Languages
        </b>Abyssal, Celestial, Draconic,  Protean; <i>read
        magic</i></p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any (Maelstrom)</p><p><b>Organization
        </b>solitary, pair, or cabal (3-5)</p><p><b>Treasure
        </b>standard</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Frostfire Spirit (Su)</b> A
        burleev is surrounded by either cold or fire energy. The burleev can
        change the energy type as a swift action. When surrounded by fire, the
        burleev has the fire subtype, it adds fire damage to its attacks, and
        creatures striking it with melee weapons, natural attacks, or unarmed
        strikes take 1d6 points of fire damage; when surrounded by cold, it
        instead gains the cold subtype and deals cold damage rather than fire
        damage. It can also completely dampen its aura for 1d6 rounds, but
        cannot reactivate it until this time has passed.  </p><p><b>Spells</b> A
        burleev cast spells as a 5th-level sorcerer.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A burleev is
        a planar explorer created by the power of Nethys. Some are his mortal
        petitioners assigned to this role after their death by a deliberate act
        of the god, whereas others are hapless visitors who were transformed by
        proximity to Nethys or certain parts of his realm. They serve as his
        eyes and ears on many planes, using their power to adapt to hostile
        environments and report their discoveries to his greater servitors. Each
        has a unique allotment of spells suited for its current task, and a
        burleev that has completed its service in one inhospitable location
        might be destroyed and recreated with a different array of arcane
        talents that suit it better for its next duty.</p><p>The spells shown
        above represent those of a typical burleev.</p><p>A burleev's
        supernatural nimbus burns brightly with cold or heat, making it
        painfully cold or hot to the touch. As a burleev discovers information
        useful to the god of magic, the color of its aura increases in
        intensity. The eldest of these creatures, or those that have travelled
        far from Nethys's realm in the Maelstrom for the longest, often burn
        like living pyres. These burleevs sometimes take sorcerer class levels
        as their magical power grows to match their ever-increasing knowledge.
        Should it later be crushed and reformed as part of its continuing
        duties, such a burleev retains much of its brightness and arcane
        might.</p><p>A spellcaster whose research interests mirror those of a
        burleev can keep such an outsider's attention for days, weeks, or longer
        so long as the mortal continues to make new and exciting
        discoveries.</p><p>Burleevs stand around 6 feet tall and weigh roughly
        80 pounds.</p></h4></div></section>
    _id: BZRJHhU29lw7501I
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!xpJsjfL8kVarCGNE.BZRJHhU29lw7501I'
sort: 0
_key: '!journal!xpJsjfL8kVarCGNE'

