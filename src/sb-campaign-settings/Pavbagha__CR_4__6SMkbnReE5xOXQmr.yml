name: Pavbagha (CR 4)
_id: 6SMkbnReE5xOXQmr
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: 1XV78M5ftiqhJ6bW
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!6SMkbnReE5xOXQmr.1XV78M5ftiqhJ6bW'
  - name: Pavbagha (CR 4)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Inner Sea Gods</p><section id="Pavbagha"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        4</p></div><div><p><b>XP </b>1,200</p><p>LN Large outsider (extraplanar,
        lawful)</p><p><b>Init </b>+5; <b>Senses </b>darkvision 60 ft., low-light
        vision, scent; Perception +11</p><p><b>Aura </b>courage (10
        ft.)</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC
        </b>16, touch 10, flat-footed 15 (+1 Dex, +6 natural, -1
        size)</p><p><b>hp </b>37 (5d10+10)</p><p><b>Fort </b>+6, <b>Ref </b>+4,
        <b>Will </b>+7</p><p><b>DR </b>5/chaotic; <b>Immune </b>fear; <b>SR
        </b>15</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>40 ft.</p><p><b>Melee </b>bite +6 (1d8+2 plus
        grab), 2 claws +7 (1d6+2 plus grab)</p><p><b>Space </b>10 ft.; <b>Reach
        </b>10 ft.</p><p><b>Special Attacks </b>pounce, rake (2 claws +7,
        1d6+2), stunning claw (4/day, DC 15)</p><p><b>Spell-Like Abilities</b>
        (CL 6th; concentration +6)<br />At will—<i>feather fall</i> (self only),
        <i>guidance</i>, <i>light</i><br />3/day—<i>channel vigor</i>*, cure
        <i>light</i> wounds, <i>true strike</i><br />1/day—<i>bull's
        strength</i>, <i>dimension door</i> (self plus 50 lbs. of objects
        only)</p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>15, <b>Dex </b>13, <b>Con </b>14, <b>Int </b> 10,
        <b>Wis </b>17, <b>Cha </b>10</p><p><b>Base Atk </b>+5; <b>CMB </b>+8
        (+12 grapple); <b>CMD </b>19 (23 vs. trip)</p><p><b>Feats </b>Improved
        Initiative, Lightning Reflexes, Weapon Focus (claw)</p><p><b>Skills
        </b>Acrobatics +13 (+17 when jumping), Knowledge (history, religion) +8,
        Perception +11, Stealth +9 (+13 in tall grass), Swim +10; <b>Racial
        Modifiers </b>+4 Acrobatics (+8 when jumping), +4 Stealth (+8 in tall
        grass)</p><p><b>Languages </b>Celestial, Common, Draconic</p><p><b>SQ
        </b>fade</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any (Axis)</p><p><b>Organization
        </b>solitary, pair, or pride (3-5)</p><p><b>Treasure
        </b>standard</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Aura of Courage (Su)</b> A
        pavbagha is immune to fear, magical or otherwise. Each ally within 10
        feet of it gains a +4 morale bonus on saving throws against fear
        effects. This ability functions only while the pavbagha is conscious,
        not if it's unconscious or dead.  </p><p><b>Fade (Su)</b> As a standard
        action, a pavbagha can fade from sight, as <i>invisibility</i>, for up
        to 10 rounds per day. These rounds need not be consecutive. 
        </p><p><b>Stunning Claw (Ex)</b> This ability functions like the
        Stunning Fist feat, except the pavbagha uses a claw attack instead of an
        unarmed strike. The servitor can use this ability five times per day. A
        successful DC 15 Fortitude saving throw negates this effect. The save DC
        is Wisdom-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A pavbagha is
        the reincarnated soul of an en<i>light</i>ened mortal worshiper of Irori
        transformed into the shape of a white tiger. Having lived one full
        mortal lifetime (if not more), it is patient, calm, and wise. It prefers
        to draw on its experience to guide and instruct mortals on ways to
        better themselves. Many enemies mistake a pavbagha's inner peace for
        weakness or pacifism, but the servitor was a warrior and a fierce
        predator in previous lives, and it quickly leaps into battle to defend
        its students or confront those who would dare destroy
        knowledge.</p><p>Pavbaghas patrol the borders of Irori's realm, alert
        for disturbances in the Serene Circle or forbidden natives of Axis who
        venture too close to the god's territory. Fulfilling the roles of
        guardians in the mortal world pleases pavbaghas, whether they're looking
        after a special person or watching over a sacred site. Although they
        don't need to eat, they enjoy the challenge and exercise of hunting and
        stalking prey. Rather than killing its catch, a pavbagha usually lays a
        single paw upon its target before allowing the creature to run away,
        secure in its triumph.</p><p>Some pavbaghas serve in temples and
        monasteries dedicated to Irori, where they help in training students in
        physical combat, particularly in how to deal with monsters and other
        dangerous beasts. Others guide students in meditation, helping them
        unravel those quandaries they might have on the path to perfection.
        Still other pavbaghas that make their homes in monasteries on the
        Material Plane focus their efforts on attending to those who visit
        Iroran shrines and temples looking for divine assistance.</p><p>A
        pavbagha measures about 10 to 12 feet long and weighs between 750 and
        900 pounds.</p></h4></div></section>
    _id: Sk623X2UNzmBE0qG
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!6SMkbnReE5xOXQmr.Sk623X2UNzmBE0qG'
sort: 0
_key: '!journal!6SMkbnReE5xOXQmr'

