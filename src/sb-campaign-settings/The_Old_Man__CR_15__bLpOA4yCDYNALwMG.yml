name: The Old Man (CR 15)
_id: bLpOA4yCDYNALwMG
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: KIGqBlAWAkbm1o6z
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!bLpOA4yCDYNALwMG.KIGqBlAWAkbm1o6z'
  - name: The Old Man (CR 15)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Inner Sea Gods</p><section
        id="TheOldMan"><div class="heading"><p
        class="alignright"><strong>CR</strong> 15</p></div><div><p><b>XP
        </b>51,200</p><p>LN Medium outsider (extraplanar, herald,
        lawful)</p><p><b>Init </b>+10; <b>Senses </b>blindsight 30 ft.,
        darkvision 60 ft., low-light vision; Perception +30</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>37, touch 35,
        flat-footed 26 (+10 Dex, +1 dodge, +4 monk, +10 Wis, +2
        natural)</p><p><b>hp </b>178 (17d10+85); regeneration 15
        (chaotic)</p><p><b>Fort </b>+10, <b>Ref </b>+20, <b>Will
        </b>+20</p><p><b>Defensive Abilities </b>improved evasion; <b>DR
        </b>10/chaotic; <b>Immune </b>disease, poison; <b>Resist </b>acid 30,
        cold 30, electricity  30, fire 30, sonic 30; <b>SR </b>26</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>80 ft., climb
        30 ft., swim 30 ft.</p><p><b>Melee </b>unarmed strike +27/+22/+17/+12
        (2d8+5) or <br />flurry of blows  +20/+20/+15/+15/+10/+10/+5 
        (2d8+5)</p><p><b>Ranged </b>pebble +27/+22/+17/+12 
        (1d3+5)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>stunning fist (16/day, DC 28)</p><p><b>Spell-Like
        Abilities</b> (CL 17th; concentration +23)<br />7/day—<i>air walk</i>,
        <i>augury</i>, <i>cure light wounds</i>, <i>dimension door</i>,
        <i>invisibility</i>, <i>true strike</i>, <i>water walk</i><br
        />3/day—<i>commune</i>, <i>haste</i>, <i>heal</i>, <i>legend
        lore</i></p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>20, <b>Dex </b>31, <b>Con </b>20, <b>Int </b> 21,
        <b>Wis </b>31, <b>Cha </b>22</p><p><b>Base Atk </b>+17; <b>CMB </b>+27
        (+29 disarm, +31 grapple, +29 trip); <b>CMD </b>57 (59 vs. disarm, 59
        vs. grapple, 59 vs. trip)</p><p><b>Feats </b>Agile Maneuvers, Combat
        Expertise, Dodge, Greater Grapple, Improved Disarm, Improved Grapple,
        Improved Trip, Improved Unarmed StrikeB, Mobility, Stunning FistB,
        Weapon FinesseB, Wind Stance</p><p><b>Skills </b>Acrobatics +30 (+66
        when jumping), Climb +13, Diplomacy +23, Handle Animal +14, Heal +22,
        Knowledge (history) +22, Knowledge (religion) +13, Perception +30,
        Perform (string) +23, Ride +27, Sense Motive +30, Sleight of Hand +30,
        Stealth +30, Swim +25</p><p><b>Languages </b>Common, Tien, Vudrani;
        telepathy 100 ft.</p><p><b>SQ </b>monk abilities</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        land (Axis)</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>standard</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Monk Abilities (Ex or Su</b>)
        The Old Man has the following abilities of a 16th-level monk: AC bonus
        (+4), fast movement (50 ft.), flurry of blows, high jump, ki pool (18
        points, adamantine, cold iron, lawful, magic, silver), quivering palm
        (DC 28), slow fall (80 ft.), stunning fist (blinded, deafened, fatigued,
        staggered, or stunned), unarmed strike</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The Old Man
        is the herald of Irori, a teacher, mentor, guide, and trainer in the
        service of the god of knowledge, history, and self-perfection. He can be
        patient or irate, confusing or enlightening, lenient or strict,
        depending on what his students need. He often wanders Golarion as a
        mortal man, bereft of his supernatural powers, instructing others and
        leading by example. If attacked when in this limited shape, he usually
        allows himself to be beaten or killed, especially if his "death" would
        provide a powerful lesson to an important student. If slain as a mortal,
        he simply reincarnates in his celestial form, unharmed, and never bears
        a grudge about it.</p><p>The Old Man's true form is that of an elderly
        human man, perhaps of Tien or Vudrani ancestry but never clearly
        identifiable as such. He is typically bald and wiry, and usually wears a
        long white moustache or beard. Though he appears frail and sometimes
        supports himself with a cane, crutch, or staff, he is incredibly strong
        and can move with an alien grace when he so chooses. He may dress in a
        simple robe, an elaborate ceremonial garment, or a simple
        loincloth.</p><p>He is often accompanied by one of five animals: a
        turtle, monkey, ox, rooster, or pig.</p><p>When not acting on Irori's
        behalf, the Herald of Irori keeps busy with tending to animals, planting
        gardens, meditating, practicing martial arts forms, swimming, and
        quipping with students.</p><p>The Herald of Irori stands just over 5
        feet tall and weighs about 120 pounds.</p></h4></div></section>
    _id: DDG95fR5oBJEil0E
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!bLpOA4yCDYNALwMG.DDG95fR5oBJEil0E'
sort: 0
_key: '!journal!bLpOA4yCDYNALwMG'

