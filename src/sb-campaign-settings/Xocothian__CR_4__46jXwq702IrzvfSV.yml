name: Xocothian (CR 4)
_id: 46jXwq702IrzvfSV
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: eYGbWGop1zCpzGE0
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!46jXwq702IrzvfSV.eYGbWGop1zCpzGE0'
  - name: Xocothian (CR 4)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Inner Sea Gods</p><section
        id="Xocothian"><div class="heading"><p
        class="alignright"><strong>CR</strong> 4</p></div><div><p><b>XP
        </b>1,200</p><p>N Large outsider (air, extraplanar, water)</p><p><b>Init
        </b>+1; <b>Senses </b>darkvision 60 ft., low-light vision; Perception
        +9</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC
        </b>17, touch 10, flat-footed 16 (+1 Dex, +7 natural, -1
        size)</p><p><b>hp </b>33 (6d10)</p><p><b>Fort </b>+5, <b>Ref </b>+5,
        <b>Will </b>+5</p><p><b>DR </b>5/cold iron; <b>SR </b>15</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30 ft., fly
        90 ft. (average), swim 90 ft., speed burst 200 ft.</p><p><b>Melee
        </b>bite +7 (1d8+2), 2 wings +2 (1d6+1)</p><p><b>Space </b>10 ft.;
        <b>Reach </b>5 ft.</p><p><b>Spell-Like Abilities</b> (CL 6th;
        concentration +6)<br />At will—<i>alter winds</i>, <i>know
        direction</i>, <i>shocking grasp</i><br />3/day—<i>chill metal</i> (DC
        13), <i>cure light wounds</i>, <i>faerie fire</i>, <i>obscuring
        mist</i><br />1/day—<i>dimension door</i> (self plus 50 lbs. of objects
        only), <i>hydraulic torrent</i> (DC 13)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>15, <b>Dex
        </b>12, <b>Con </b>11, <b>Int </b> 10, <b>Wis </b>11, <b>Cha
        </b>10</p><p><b>Base Atk </b>+6; <b>CMB </b>+9; <b>CMD </b>20 (can't be
        tripped)</p><p><b>Feats </b>Combat Reflexes, Lightning Reflexes, Power
        Attack</p><p><b>Skills </b>Fly +8, Handle Animal +9, Knowledge (nature)
        +9, Knowledge (religion) +9, Perception +9, Stealth +6, Swim
        +10</p><p><b>Languages </b>Druidic, Sylvan; <i>speak with
        animals</i></p><p><b>SQ </b>form of sea and sky</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        (Plane of Air or Plane of Water)</p><p><b>Organization </b>solitary,
        pair, or school (3-5)</p><p><b>Treasure </b>standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Form of Sea
        and Sky (Su)</b> Once per hour, a xocothian can transform itself into
        two Small elementals (one air and one water) for up to 7 minutes. These
        elementals appear in adjacent squares. Each have half of the xocothian's
        current hit points and share the same mind. They can reform into the
        xocothian as a standard action if they are adjacent to each other-the
        reformed xocothian's hit points are equal to the total of the two
        elementals' hit points. If either elemental is slain, the xocothian must
        use its standard action on its next turn to reform (treat a slain
        elemental's hit points as 0 when determining the reformed xocothian's
        total hit points).  </p><p><b>Speed Burst (Ex)</b> A xocothian can fly
        or swim up to 200 feet as a full-round action. When using this ability,
        it must move in a straight line. This does not provoke attacks of
        opportunity.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>A xocothian is a physical manifestation of Gozreh's dual
        nature and destructive power. As a creature that has a form mingling
        both fish and fowl, it's as at home in the water as it is in the air. A
        xocothian amuses itself by manipulating clouds-it can fly in a way that
        whips off portions of clouds, allowing the creature to sculpt the cloud
        into fantastic creations. When on the sea, the creature dives in and out
        of the waves in a way that looks almost like a massive skipping stone
        dipping below the waves with each strike and then erupting into the air
        in a spray of sea water.</p><p>Blunt and impatient, xocothians aren't
        fond of nuanced manipulation or clever gambits when dealing with others.
        They prefer straightforward approaches to obstacles and problems, and
        always takes such a route unless impeded in some way.</p><p>When on the
        Material Plane and not called into service by mortal worshipers of
        Gozreh, xocothians enjoy exploring the natural wonders of the world.
        They splash through the seas, and soar through the skies, keeping away
        from civilization on these travels. Sometimes when encountering mortals,
        the creatures hide themselves in <i>obscuring mist</i>, hoping to be
        mistaken as a cloud.</p><p>When at sea, they sometimes surge past ships
        underwater or in the air to create confusion and panic. When feeling
        sociable, they chat with local animals and discuss matters of weather
        and migrations with members of Gozreh's faith and respectful druids of
        other religions. They grow outraged with mortals who poison or pollute
        water and even those who befoul the air with bad smells (such as by
        burning garbage, casting <i>stinking cloud</i>, or running a tannery).
        Although they don't need to eat, they sometimes choose to do so for
        pleasure, enjoying the sort of food that a carnivorous fish or bird
        would consume.</p><p>As enigmatic as its creator, a xocothian may refer
        to itself as "I," "we," "she," "he," "it," or "they," whether in one
        body or two.</p><p>Xocothians are about 8 feet long and weigh around 650
        pounds.</p></h4></div></section>
    _id: RjfWTdnWhUka6tol
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!46jXwq702IrzvfSV.RjfWTdnWhUka6tol'
sort: 0
_key: '!journal!46jXwq702IrzvfSV'

