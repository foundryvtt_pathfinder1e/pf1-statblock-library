name: First Blade
_id: CMJW39sJU4yZXNBj
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Kingmaker.jpg
    title:
      show: false
      level: 1
    _id: GtTE2NXwx8FRwKy1
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!CMJW39sJU4yZXNBj.GtTE2NXwx8FRwKy1'
  - name: First Blade
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 35</p><section id="FirstBlade"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        15</p></div><div><p><strong>XP </strong>51,200</p><p>First Blade
        </p><p>CN Large outsider (chaotic, extraplanar)</p><p><strong>Init
        </strong>+5; <strong>Senses </strong>darkvision 60 ft., low-light
        vision, ironsense; Perception +23</p><p><strong>Aura
        </strong><i>rage</i> 100 ft.</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>30, touch 10, flat-footed 29 (+20 armor, +1 Dex, -1
        size)</p><p><strong>hp </strong>261 (18d10+162); regeneration 5
        (adamantine)</p><p><strong>Fort </strong>+20, <strong>Ref </strong>+12,
        <strong>Will </strong>+8</p><p><strong>DR </strong>15/adamantine and
        law; <strong>Immune </strong>magic, poison; <strong>Resist </strong>cold
        10, sonic 10; <i>heavy fortification</i></p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong>2 slams +29 (2d10+16 plus
        bleed)</p><p><strong>Space </strong>10 ft.; <strong>Reach </strong>10
        ft.</p><p><strong>Special Attacks </strong>blade slam, bleed (1d10),
        powerful blows</p><p><strong>Spell-Like Abilities</strong> (CL 18th;
        concentration +19)<br />3/day—<i>chill metal</i>, <i>heat metal</i>,
        <i>repel metal or stone</i>, <i>wall of iron</i><br />1/day—<i>blade
        barrier</i></p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>32, <strong>Dex </strong>13, <strong>Con </strong>28,
        <strong>Int </strong> 10, <strong>Wis </strong>15, <strong>Cha
        </strong>13</p><p><strong>Base Atk </strong>+18; <strong>CMB
        </strong>+30; <strong>CMD </strong>41</p><p><strong>Feats
        </strong>Blind-Fight, Cleave, Combat Reflexes, Great Cleave, Improved
        Bull Rush, Improved Initiative, Improved Overrun, Power Attack, Weapon
        Focus (slam)</p><p><strong>Skills </strong>Acrobatics +22, Climb +32,
        Intimidate +22, Knowledge (history) +21, Perception +23, Sense Motive
        +23, Stealth +0</p><p><strong>Languages </strong>Abyssal, Celestial,
        Common, Infernal, Protean</p><p><strong>SQ </strong>breathless, ever
        armed, lord of battle, swarm form</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        battle</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure </strong>standard</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Blade Slam (Ex)</strong> The First Blade's slam
        attacks deal bludgeoning and slashing damage. Its slams count as natural
        weapons or manufactured weapons (whichever is most beneficial to it) for
        the purpose of spells that enhance attacks. Its attacks count as
        adamantine, chaos, and magic for the purpose of overcoming damage
        reduction. </p><p><strong>Breathless (Ex)</strong> Unlike most
        outsiders, the First Blade does not need to breathe. </p><p><strong>Ever
        Armed (Su)</strong> As a swift action, at will, the First Blade can
        cause a mundane melee weapon of any type or size to appear in its hands.
        Although it typically battles unarmed, it sometimes makes use of weapons
        as needed. These weapons come from Gorum's divine realm, and this
        ability cannot be used to claim a specific weapon. </p><p><strong>Heavy
        Fortification (Ex)</strong> The First Blade has a 75% chance to treat a
        critical hit or sneak attack as a normal attack, as if it were wearing
        <i>heavy fortification</i> armor. </p><p><strong>Immunity to Magic
        (Ex)</strong> The First Blade is immune to spells or spell-like
        abilities that allow spell resistance. The First Blade can lower this
        resistance for 1 round as a standard action (similar to a creature with
        spell resistance lowering its spell resistance) to allow other creatures
        to cast spells on it; it can use its spell-like abilities on itself
        without difficulty. Certain spells and effects function differently
        against it, as noted below. • A magical attack that deals electricity
        damage <i>slow</i>s the First Blade (as the <i>slow</i> spell) for 1
        round, with no saving throw. • A magical attack that deals fire damage
        breaks any <i>slow</i> effect on the First Blade and heals 1 point of
        damage for each 3 points of damage the attack would otherwise deal. If
        the amount of healing would cause the First Blade to exceed its full
        normal hit points, it gains any excess as temporary hit points. The
        First Blade gets no saving throw against fire effects. • The First Blade
        is affected normally by rust attacks, such as those of a rust monster or
        a <i>rusting grasp</i> spell. </p><p><strong>Ironsense (Ex)</strong> The
        First Blade automatically detects iron objects (including steel) within
        60 feet, just as if it possessed the blindsight ability.
        </p><p><strong>Lord of Battle (Ex)</strong> The First Blade is treated
        as an 18th-level barbarian and fighter for any game rule (such as a feat
        prerequisite) that requires levels in barbarian or fighter. It is
        proficient in all weapons. </p><p><strong>Powerful Blows (Ex)</strong>
        The First Blade inflicts one and a half times its Strength modifier and
        threatens a critical hit on a 19-20 with its slam attacks.
        </p><p><strong>Rage Aura (Su)</strong> Willing creatures within 100 feet
        of the First Blade gain the effects of a <i>rage</i> spell
        automatically, whether they are allies or enemies of the Herald. Those
        who choose not to be affected are immune to the aura until they leave
        the area and return (at which point they may again accept or refuse the
        aura's effect). </p><p><strong>Swarm Form (Su)</strong> The First Blade
        can shift between its manlike body and a floating swarm of Diminutive
        and Tiny sharp metal fragments as a standard action. In swarm form it
        has the swarm subtype; it cannot use its slam attacks but instead can
        make a swarm attack (4d6 and distraction). Though its individual
        components may float up to 10 feet above the ground as they move, the
        Blade's swarm form cannot fly. The First Blade remains in one form until
        it chooses to assume its other form. A change in form cannot be
        dispelled, nor does the First Blade revert to any particular form when
        killed (both shapes are its true form). A <i>true seeing</i> spell
        reveals both forms simultaneously.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Said to have
        been formed from an unthinking sliver of steel dashed from Gorum's blade
        during one of his first clashes with a long-felled god-beast of the
        Outer Sphere, the First Blade is a living tool of war. Inspired by
        Gorum's divine bloodlust, this shard of the god of battle's blade
        continues to obey and do battle in the service of its divine master.
        Having been reforged through the eons into a manifestation of Gorum's
        perfect warrior, the First Blade serves as the war god's herald,
        traveling where its master desires. Yet while the heralds of most
        deities go forth to bear the word of their divine patrons or answer the
        most desperate summons of their gods' most pious servants, the First
        Blade endlessly marches to battle. Never interested in diplomacy or
        subtlety, Gorum has little need for a messenger other than one capable
        of communicating in the language of the battlefield. The god of battle
        answers the calls only of those who please him in battle and request his
        heralds' aid in epic clashes dedicated to his honor- weakling priests
        seeking salvation or cowardly revenge never have their entreaties for
        the herald's presence answered. The appearance of the First Blade
        changes to match a style of armor impressive to those against whom it
        will be doing battle, though typically notched as if it has been
        employed in numerous battles. Rarely seen in the same form twice, its
        plate mail form might vary from the elegant mail of angelic hosts to
        slabs of spiked iron over thick bestial hides more common to orc
        warlords. Whatever the shape, the interior of the armor is never
        visible. Those who have come close enough to the First Blade and
        survived claim that only more layers of armor lie beneath its plates,
        though the hint of something glowing within sometimes spills forth
        should the herald suffer a rare but occasional wound. Regardless of the
        specifics of its warlike form, the herald of Gorum typically stands
        about 15 feet tall and weighs nearly 2
        tons.<strong></strong></p><p><strong>Ecology</strong></p><p> Although a
        living creature, the First Blade is little more than a weapon of Gorum,
        knowing little beyond its lord's command and going only where he wills.
        Like a golem in many respects, the herald leaves but scant traces upon
        the lands it passes through between battles, eating little and having no
        need even to breathe. In war, however, its presence and passage are
        obvious, marked by rent bodies and blood-soaked earth. Although the
        First Blade can speak a variety of languages, few of those encountering
        the herald have heard it do so. Like its master, the First Blade prefers
        actions-especially violent, purposeful ones-to words. Typically its
        words are brief refutations of those who have summoned it for an unfit
        purpose, though several legends tell of the divine messenger offering
        its respect or the pleasure of its master before striking the final blow
        upon an opponent who has put up a particularly capable fight or proven
        her prowess on the field of
        battle.<strong></strong></p><p><strong>Habitat &amp;
        Society</strong></p><p> While the herald of Gorum answers the call of
        servants of the lord of battle who seek their lord's intervention in
        appropriately glorious battle, no accounts exist of the First Blade
        responding to any summons involving a task other than combat. Most
        accounts of the herald tell of the gory swaths it cuts through
        battlefields of heroes, in clashes between titanic armies, or in
        weeks-long battles between history's greatest warlords. Rumors among
        Gorum's clergy also suggest that sometimes the herald appears when
        summoned to a lesser battle, but upon finding a mere skirmish or clash
        among weaklings, it slaughters all involved. Whether such an end is an
        honor or a disgrace for those killed remains a subject of some debate
        among theologians.</p></h4></div></section>
    _id: oncRo7cLhy2mbuHG
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!CMJW39sJU4yZXNBj.oncRo7cLhy2mbuHG'
sort: 0
_key: '!journal!CMJW39sJU4yZXNBj'

