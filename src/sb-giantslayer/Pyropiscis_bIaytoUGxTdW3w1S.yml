name: Pyropiscis
_id: bIaytoUGxTdW3w1S
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Giantslayer.jpg
    title:
      show: false
      level: 1
    _id: BdM9pFSfSP3d4WqS
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!bIaytoUGxTdW3w1S.BdM9pFSfSP3d4WqS'
  - name: Pyropiscis
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 95</p><section id="Pyropiscis"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        8</p></div><div><p><strong>XP </strong>4,800</p><p>N Large magical beast
        (fire)</p><p><strong>Init </strong>+6; <strong>Senses
        </strong>darkvision 60 ft., low-light vision, tremorsense 60 ft.;
        Perception +10</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>22, touch 11, flat-footed 20 (+11 armor, +2 Dex, -1
        size)</p><p><strong>hp </strong>105 (10d10+50)</p><p><strong>Fort
        </strong>+11, <strong>Ref </strong>+9, <strong>Will
        </strong>+5</p><p><strong>DR </strong>5/adamantine; <strong>Immune
        </strong>fire</p><p><strong>Weaknesses </strong>lava dependency,
        vulnerable to cold</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>10 ft., burrow 60 ft. (through lava or magma only); 
        sprint</p><p><strong>Melee </strong>bite +17 (2d8+10/19-20 plus burn and
        grab)</p><p><strong>Ranged </strong>lava bomb +11 (3d6 plus 2d6
        fire)</p><p><strong>Space </strong>10 ft.; <strong>Reach </strong>10
        ft.</p><p><strong>Special Attacks </strong>burn (1d6, DC 19), searing
        bite</p></div><hr /><div><p><strong>STATISTICS</strong></p></div><hr
        /><div><p><strong>Str </strong>24, <strong>Dex </strong>14, <strong>Con
        </strong>18, <strong>Int </strong> 2, <strong>Wis </strong>15,
        <strong>Cha </strong>6</p><p><strong>Base Atk </strong>+10; <strong>CMB
        </strong>+18 (+22 grapple); <strong>CMD </strong>30</p><p><strong>Feats
        </strong>Improved Critical (bite), Improved Initiative, Lunge,
        Toughness, Weapon Focus (bite)</p><p><strong>Skills </strong>Perception
        +10, Stealth +6 (+14 in lava); <strong>Racial Modifiers </strong>+8
        Stealth in lava</p><p><strong>SQ </strong>hibernation</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> warm mountains or
        underground</p><p><strong>Organization </strong>solitary, pack (3-6), or
        school (12-20)</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Hibernation (Ex)</strong> A pyropiscis can enter a
        state of hibernation for an indefinite period of time in order to
        survive longer periods away from a source of lava. Entering a state of
        hibernation takes 1 hour, during which the pyropiscis encases itself in
        a thick layer of igneous stone. While hibernating, a pyropiscis doesn't
        need to breathe, drink, or eat. The stone casing has hardness 8 and 90
        hit points. As long as the casing remains intact, the pyropiscis within
        remains unharmed. The pyropiscis remains in a state of hibernation until
        it senses lava (or another source of extreme heat) nearby, at which
        point it breaks out of its case over the course of 1d4 minutes. 
        </p><p><strong>Lava Bomb (Ex)</strong> Like an active volcano, a
        pyropiscis can spit a lava bomb-a blob of molten rock-as a ranged attack
        (range increment 30 feet). If a lava bomb hits, it deals 3d6 points of
        bludgeoning damage and 2d6 points of fire damage to its target. 
        </p><p><strong>Lava Dependency (Ex)</strong> A pyropiscis can breathe
        indefinitely while submerged in lava. It can survive out of lava for 1
        hour per point of Constitution. Beyond this limit, the pyropiscis runs
        the risk of suffocation, as if it were drowning.  </p><p><strong>Searing
        Bite (Ex)</strong> A pyropiscis's searing-hot jaws are designed to bind
        readily to flesh, giving it a firm grasp on its prey. This functions as
        the constrict ability, except that a pyropiscis deals 2d6 points of fire
        damage when it makes a successful grapple check, rather than dealing
        bludgeoning damage.  </p><p><strong>Sprint (Ex)</strong> Once per
        minute, a pyropiscis may sprint, increasing its land speed to 40 feet
        for 1 round.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>Few environments are more inhospitable to life than the
        depths of a volcano, where magma surges through the rock like blood
        through veins. Of the creatures that do live in this hellish landscape,
        few are better adapted than the pyropiscis. Pyropiscises depend on this
        deadly environment of extreme temperatures and choking gases for their
        very lives. While they have a fishlike appearance, pyropiscises do not
        swim-their bodies are far too dense to float in water or similar
        liquids. Instead, pyropiscises rely on their powerful muscles and sharp
        scales to burrow through molten rock.  A typical pyropiscis measures
        over 12 feet long, and weighs almost 4,000 pounds. Brilliant red scales
        glow and pulse with terrible heat, protecting those portions of their
        bodies not covered in blackened iron plates. 
        <strong></strong></p><p><strong>Ecology</strong></p><p>  The unique
        physiology of pyropiscises allows them to derive sustenance directly
        from molten rock, leaching necessary minerals and volatiles from lava as
        it passes through their gills. A pyropiscis draws energy directly from
        the lava in which it dwells, using the intense heat to power its
        metabolic processes. While pyropiscises don't need to consume the flesh
        of other creatures to survive, they require nutrients from flesh to grow
        and to reproduce. A well-fed pyropiscis continues to grow throughout its
        lifetime and can reach an immense size.  Pyropiscises typically lurk
        close to the surface of active volcanoes, and ride eruptions like
        flowing tides to find prey. They burst forth from rivers of lava to
        snatch fleeing creatures with their jaws, and consume the charred
        corpses of those who can't outrun the volcano's fury. Once they have
        finished feeding, they swim back against the current of lava and into
        the volcano. Those pyropiscises that linger to gorge themselves, and
        those too weak to fight the downhill flow, become stranded on land. They
        then enter hibernation, awaiting the next eruption. A pyropiscis can
        hibernate for centuries at a time. Eventually, however, a stranded
        pyropiscis begins to starve, and will break out of its hibernation in
        search of food.  Pyropiscises reproduce by laying clutches of four to
        six eggs. They surround their eggs in fiercely guarded nests constructed
        of iron drawn from the surrounding stone. Newly hatched pyropiscises
        look like worms made of lava. Their scales are soft, and they lack the
        protective plating of their adult counterparts.  Sometimes, pyropiscises
        venture deep into the hearts of volcanoes that host portals to other
        planes, and wander through in search of sustenance. The Elemental Plane
        of Fire is their most common extraplanar hunting ground, but
        occasionally pyropiscises are found hunting in Hell. Lengthy sojourns
        into infernal realms sometimes grant pyropiscises fiendish traits.  
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        Pyropiscises are found across Golarion, almost exclusively in areas of
        intense volcanic activity. They can reach many of the world's active
        volcanoes by navigating the interconnected series of magma chambers that
        worm their way through the depths of the earth, ranging from arctic hot
        springs to volcanic tropical islands.  These lava-dwelling fish rarely
        venture far onto the surface, preferring to remain belowground where
        rock remains molten for hundreds or thousands of years. The natural
        instability of their homes sometimes turns against pyropiscises,
        however, and particularly violent eruptions send angry pyropiscises
        raining down on nearby surface settlements, where they gorge themselves
        and cause even greater destruction.  Pyropiscises are social creatures.
        They live in schools of a dozen or more individuals, typically hunting
        alone or in smaller packs. They reproduce infrequently, and only ever
        after a successful hunt. Pyropiscises are intensely protective of all
        the young in their school regardless of parentage, and take turns
        guarding them. When a new crop of young reaches maturity, they form a
        school of their own, venturing to a different part of the same volcano.
        Even if a pyropiscis's hunts take it to other planes, it always strives
        to return to the volcano of its birth to spawn. Pyropiscis schools
        readily adopt stranded wanderers, but while welcoming to their own kind,
        these fiery beasts are hostile to all other creatures.  Some fire giants
        domesticate pyropiscises, overcoming their surly, willful nature through
        stubbornness. The giants' size and immunity to fire allow groups of them
        to handle an unruly pyropiscis throughout its training. Still, fire
        giants must continually placate their pets with charred flesh to prevent
        them from lashing out. This steady stream of food grows the pyropiscis
        to an enormous size, and some fire giants have been known to ride giant
        pyropiscises as mounts into battle. Pyropiscises also serve fire giants
        as fortress guardians, swimming in moats of magma at the entrances to
        their tunnels.  Pyropiscises are a recurring bane to miners and
        excavators. The rumble and activity of mining efforts can stir
        hibernating pyropiscises from their slumber, and the newly wakened
        pyropiscises spare few lives in their search for food and safety. The
        most unfortunate miners may even break directly into a pyropiscis's
        cocoon, as a hibernating pyropiscis encased in solid rock is
        indistinguishable from the surrounding stone. Those with the skill or
        unfortunate experience to recognize a sleeping pyropiscis for what it is
        can make a decent profit selling the monster to individuals in search of
        a guardian or owners of particularly opulent menageries, provided they
        can chisel the cocoon free and transport it to market without rousing
        the creature inside.</p></h4></div></section>
    _id: 6TIOiY8z6H9HcFDw
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!bIaytoUGxTdW3w1S.6TIOiY8z6H9HcFDw'
sort: 0
_key: '!journal!bIaytoUGxTdW3w1S'

