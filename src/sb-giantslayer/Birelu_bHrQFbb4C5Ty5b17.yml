name: Birelu
_id: bHrQFbb4C5Ty5b17
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Giantslayer.jpg
    title:
      show: false
      level: 1
    _id: LTq3OFuaJEZye1Rk
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!bHrQFbb4C5Ty5b17.LTq3OFuaJEZye1Rk'
  - name: Birelu
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 94</p><section id="Birelu"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        10</p></div><div><p><strong>XP </strong>9,600</p><p>CN Medium outsider
        (extraplanar, incorporeal)</p><p><strong>Init </strong>+10;
        <strong>Senses </strong>darkvision 60 ft., low-light vision, scent;
        Perception +17</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>21, touch 21, flat-footed 14 (+4 deflection, +6 Dex, +1
        dodge)</p><p><strong>hp </strong>110 (13d10+39)</p><p><strong>Fort
        </strong>+11, <strong>Ref </strong>+14, <strong>Will
        </strong>+7</p><p><strong>Defensive Abilities </strong>incorporeal;
        <strong>DR </strong>10/cold iron; <strong>SR </strong>21</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>fly 50 ft. (perfect)</p><p><strong>Melee </strong>gore +19 (1d8
        plus 2d6 force), 2 claws +19 (1d6 plus 2d6 force)</p><p><strong>Space
        </strong>5 ft.; <strong>Reach </strong>5 ft.</p><p><strong>Special
        Attacks </strong>force of nature, powerful charge (gore, 2d8 plus 4d6
        force), spirit walk</p><p><strong>Spell-Like Abilities</strong> (CL
        13th; concentration +17)  <br />Constant—<i>speak with animals</i> <br
        />At Will—<i>call animal</i>, <i>charm animal</i> (DC 15) <br
        />3/day—<i>dominate animal</i> (DC 17), <i>moonstruck</i> (DC 18) <br
        />1/day—<i>baleful polymorph</i> (DC 19), <i>commune with
        nature</i></p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>-, <strong>Dex </strong>23, <strong>Con </strong>16,
        <strong>Int </strong> 10, <strong>Wis </strong>13, <strong>Cha
        </strong>19</p><p><strong>Base Atk </strong>+13; <strong>CMB
        </strong>+19; <strong>CMD </strong>34</p><p><strong>Feats
        </strong>Blind-Fight, Combat Reflexes, Dodge, Flyby Attack, Improved
        Initiative, Iron Will, Mobility</p><p><strong>Skills </strong>Fly +20,
        Handle Animal +13, Knowledge (geography) +16, Knowledge (nature) +16,
        Perception +17, Sense Motive +14, Stealth +22, Survival
        +8</p><p><strong>Languages </strong>Sylvan, <i>speak with
        animals</i></p></div><hr /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any forest, mountain, or
        plains</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Force of Nature (Su)</strong> A birelu channels the
        raw power of nature into its attacks. Its natural attacks are considered
        to have the ghost touch special ability, and each of its natural attacks
        deals an additional 2d6 points of force damage. This damage is doubled
        when a birelu makes a powerful charge.  </p><p><strong>Spirit Walk
        (Su)</strong> Once per round as a standard action, a birelu can merge
        itself with a single humanoid or animal. This ability is similar to the
        <i>magic jar</i> spell (caster level 13th), except it doesn't require a
        receptacle. To use this ability, the birelu must be adjacent to the
        target. The target can resist the attack with a successful DC 20 Will
        save. A creature that successfully saves is immune to the  same birelu's
        spirit walk ability for 24 hours. The save DC is Charisma-based. 
        Additionally, a creature affected by this ability undergoes a physical
        transformation, growing either more beastlike or more humanlike,
        depending on its original form. A humanoid affected by this ability is
        affected as by <i>greater animal aspect</i>UC. The birelu chooses the
        aspect gained from this effect. This aspect can't be changed, though the
        birelu can select a new aspect if it merges with the same creature again
        at a later time. An animal affected by the spirit walk ability is
        affected as by <i>anthropomorphic animal</i>UM. Either effect lasts for
        as long as the creature remains possessed by the birelu, ending once the
        birelu leaves the creature's body.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>In ages past,
        humans didn't live apart from nature as they do now. They had no cities,
        no farms, and no metal tools. In this primitive time, humans lived as
        beasts, hunting and foraging in the savage wild, living and dying by the
        cruel whims of nature. To these early humans, a successful hunt
        sometimes meant the difference between the life of the tribe and utter
        extinction. For this reason, the land and the animals who lived on it
        commanded great respect among the tribes. The environment represented
        more than just food or clothing or territory; it was an ally, an enemy,
        or even a god unto itself.  There was need in those days for people who
        could pass from the physical world and into the world of the spirits,
        where they could intercede with the spirits of the land and the animals
        to ensure the tribe's survival. From this need came the birelus,
        manifestations of humankind's desire to bridge the boundary between
        humans and nature. These ancient beings served as guardians and
        travelers of the paths between worlds.  A birelu seems to both stand
        upright and walk upon all fours at the same time, its form shifting and
        flowing incomprehensibly between the two poses. Even while partially
        hunched in this manner, a birelu stands over 7 feet tall when in its
        natural state.  <strong></strong></p><p><strong>Ecology</strong></p><p> 
        Birelus are ancient beings, having existed since time immemorial. It may
        be that they were born at the very dawn of time, champions of a unity
        between humankind and nature that would not exist on the Material Plane
        for eons hence, and simply waited to intrude upon the world when it was
        ready for them. More likely, they owe their existence to humankind
        itself, springing to life as the manifestation of ideas held sacred by
        the early mortal races. Whatever the case, birelus came to Golarion when
        the mortal races were still young. In those primordial times, they acted
        as guardians and guides, teaching the first mortal shamans how to cross
        the boundaries  between the physical world and the spirit world, and
        bringing terrible destruction upon those who violated that sacred
        boundary. These early birelus sought to guide humanity, to groom its
        growth in harmony with nature as a way to restrict the destructive
        tendencies of civilization. Despite these efforts, humanity marched
        forward through time to establish cities, discover arcane magic, and
        usher in the destruction that the early birelus had attempted to
        hinder.  As outsiders, birelus don't need to eat or drink to survive,
        though they take great pleasure in hunting and feeding on wild prey.
        Most often they hunt while possessing the body of an animal, usually the
        apex predator of the local environment. Occasionally, a birelu stalks
        its prey in the possessed body of a humanoid, though typically this is
        done to honor the humanoid host. As much as they delight in such
        behavior, birelus never over-hunt an area, and they seem to have an
        innate sense about the state of the local Ecology. 
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        Birelus have no set habitat, nor do they hold territory, preferring
        instead to wander the wild places of the world. Birelus don't often
        intrude upon inhabited areas, though when they do, the results are often
        disastrous. They find the trappings of civilized life distasteful, even
        blasphemous, and focus their attentions on destroying the objects of
        their ire. Often they accomplish this by possessing people and turning
        them against their own homes and their fellow citizens. Birelus can
        sometimes be reasoned with and convinced to depart in peace, but this is
        rare, as their attacks are often confused for the actions of
        lycanthropes, and are met with violence rather than diplomacy by mortal
        humanoids.  For the most part, birelus are solitary creatures. They
        don't scorn the company of their own kind, but neither do they delight
        in it or seek it out. Birelus who meet in passing are more likely to
        ignore each other than to interact in any meaningful way. Some have
        speculated that there may in fact be only one birelu, and that those
        specimens encountered on Golarion emanate from it or serve as its
        avatars.  A birelu's attentions are drawn to animals more often than to
        any other kind of creature, earning them the title "spirits of the
        beasts" in some cultures. Birelus seem to delight in possessing animals
        and using their bodies to explore and interact with the world. Upon
        entering new territory, a birelu uses its <i>call animal</i> spell-like
        ability to draw in  potential hosts, choosing the strongest and fastest
        of the native creatures to inhabit. Birelus are very protective of their
        wild animal hosts, and they abandon a creature's body if the animal is
        at grave risk rather than fighting to the host's death. Domesticated
        animals-or those serving as animal companions to the birelu's enemies-
        don't receive the same consideration.  Birelus' interactions with
        humanoids are more complicated. Civilized peoples hold no interest to
        them, and in fact often earn their scorn. Birelus react with hostility
        toward those who openly bear the signs of civilization, such as worked
        metal or agricultural tools. A birelu's attitude is much softer toward
        less civilized humanoids. Primitive tribes of hunter-gatherers and those
        who shun the cities of the world and live in communion with nature are
        often able to make peaceful contact with a birelu. In return, the birelu
        might draw in prey animals to feed the tribe, or guide them to more
        fertile lands in times of scarcity. It may even join the tribe's
        warriors in combat against their enemies, leaping from warrior to
        warrior in order to grant them the benefits of <i>greater animal
        aspect</i>.</p></h4></div></section>
    _id: kpKjG3tCCaWvIyg0
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!bHrQFbb4C5Ty5b17.kpKjG3tCCaWvIyg0'
sort: 0
_key: '!journal!bHrQFbb4C5Ty5b17'

