name: Sunflower Leshy
_id: fW0mIHzGEpX5CqEf
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Ironfang_Invasion.jpg
    title:
      show: false
      level: 1
    _id: 6BBvvUhoQbdsGJWW
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!fW0mIHzGEpX5CqEf.6BBvvUhoQbdsGJWW'
  - name: Sunflower Leshy
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 115</p><section id="SunflowerLeshy"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        1</p></div><div><p><strong>XP </strong>400</p><p>N Small plant
        (leshy)</p><p><strong>Init </strong>+3; <strong>Senses
        </strong>darkvision 60 ft., low-light vision; Perception
        +0</p><p><strong>Aura </strong>heliotrope (20 ft., DC 13)</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>14, touch 14, flat-footed 11 (+3 Dex, +1 size)</p><p><strong>hp
        </strong>11 (2d8+2)</p><p><strong>Fort </strong>+4, <strong>Ref
        </strong>+3, <strong>Will </strong>+0</p><p><strong>Immune
        </strong>electricity, plant traits, sonic</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>20 ft.</p><p><strong>Melee </strong>head butt +0
        (1d4-2)</p><p><strong>Space </strong>5 ft.; <strong>Reach </strong>5
        ft.</p><p><strong>Special Attacks </strong>seed spray (1d4, 15-ft. cone,
        DC 16)</p><p><strong>Spell-Like Abilities</strong> (CL 4; concentration
        +6)  <br />Constant—<i>pass without trace</i></p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>7, <strong>Dex </strong>16, <strong>Con </strong>12,
        <strong>Int </strong> 5, <strong>Wis </strong>11, <strong>Cha
        </strong>15</p><p><strong>Base Atk </strong>+1; <strong>CMB </strong>-2;
        <strong>CMD </strong>11</p><p><strong>Feats </strong>Voice of the
        SibylUM</p><p><strong>Skills </strong>Acrobatics +3 (-1 to jump),
        Diplomacy +3, Sense Motive +1, Stealth +7 (+11 in plains and hills),
        Survival +0 (+4 in plains and hills); <strong>Racial Modifiers
        </strong>+4 Stealth and Survival in plains and
        hills</p><p><strong>Languages </strong>Druidic, Sylvan; plantspeech
        (flowers)</p><p><strong>SQ </strong>change shape (Small flower; <i>tree
        shape)</i>, verdant burst</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any warm plains or
        hills</p><p><strong>Organization </strong>solitary, pair, bunch (3-8),
        or field (9+)</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Heliotrope (Ex)</strong> Sunflower leshys reflect the
        sun and other sources of bright light from their faces onto all who come
        too close. Any creature within 20 feet of a sunflower leshy must succeed
        at a DC 13 Will saving throw or be dazzled for 1d4+1 rounds. A creature
        dazzled once cannot be dazzled again by the same leshy for 24 hours. The
        save DC is Charisma-based.  </p><p><strong>Seed Spray (Ex)</strong>
        Three times per day, a sunflower leshy can spray  seeds in a 15-foot
        cone as a standard action. The spray deals 1d4 points of piercing damage
        to all targets in its area of effect. Against dazzled targets, this
        attack instead deals 1d6 points of piercing damage. A successful DC 14
        Reflex saving throw halves the damage from this attack. The save DC is
        Dexterity-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A circle of
        petals radiates from a sunflower leshy's head, ranging in color from
        soft butter yellow to bright orange to plum with white tips. Serrated
        green leaves sprout like a ruff beneath its head. A sunflower leshy
        stands 4 feet tall and weighs 75 pounds.  Among the most social of
        leshys, sunflower leshys serve as the ambassadors and diplomats of
        leshykind. They enjoy socializing and interacting in groups of leshys or
        with other creatures. Solitary sunflower leshys are rare, as their need
        for companionship compels them to find others with whom to interact.
        Leshys who lack companionship for 1 week lose their petals and
        heliotrope aura as a result of being depressed. Being welcomed back into
        a group restores the aura in a day and petals after 1 week.  Sunflower
        leshys are peacemakers and shun martial activities and conflict unless
        forced into confrontation. To avoid altercations with unfriendly
        creatures, they band together and trust that the presence of their
        heliotrope aura protects them. Other leshys look to them to serve as
        mediators and help resolve differences.  <br /><strong>Creating A
        Sunflower Leshy</strong><br />  Sunflower leshys grow in open fields
        under bright sunlight. To grow a sunflower leshy, the maker plants a
        seed from an extant sunflower leshy and waters it daily. The maker must
        talk to the seedling daily to encourage the leshy's growth. The
        conversation must be upbeat and positive; otherwise, the leshy emerges
        with withered petals and a permanent frown.  <br /></p><div
        class="heading"><p class="alignleft">Sunflower Leshy</p> 
        <strong>CL</strong> 5th; <strong>Price</strong> 1,000 gp  <br /><hr
        /><strong>Ritual</strong><br /><hr />  <strong>Requirements</strong>
        Knowledge (nature) 5 ranks, light, <i>plant growth</i>, <i>summon
        nature's ally I</i>; <strong>Skill</strong> Knowledge (nature) DC 12;
        <strong>Cost</strong> 500 gp</div></h4></div></section>
    _id: WRZd9yvww6a30WD6
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!fW0mIHzGEpX5CqEf.WRZd9yvww6a30WD6'
sort: 0
_key: '!journal!fW0mIHzGEpX5CqEf'

