name: Anhana
_id: 9tMNhZSvNTsdZi9y
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Ironfang_Invasion.jpg
    title:
      show: false
      level: 1
    _id: JbdVBAFK3odlsKHA
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!9tMNhZSvNTsdZi9y.JbdVBAFK3odlsKHA'
  - name: Anhana
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 116</p><section id="Anhana"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        3</p></div><div><p><strong>XP </strong>800</p><p>CG Small fey
        </p><p><strong>Init </strong>+3; <strong>Senses </strong>low-light
        vision; Perception +12</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>15, touch 15, flat-footed 11 (+3 Dex, +1 dodge, +1
        size)</p><p><strong>hp </strong>27 (5d6+10)</p><p><strong>Fort
        </strong>+3, <strong>Ref </strong>+7, <strong>Will
        </strong>+6</p><p><strong>DR </strong>5/cold iron</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong>shears +7
        (1d8+1/19-20)</p><p><strong>Space </strong>5 ft.; <strong>Reach
        </strong>5 ft.</p><p><strong>Spell-Like Abilities</strong> (CL 5th;
        concentration +9)  <br />Constant—<i>speak with animals</i> <br />At
        Will—<i>ghost sound</i> <br />3/day—<i>deep slumber</i> (DC 17),
        <i>entangle</i> (DC 15), <i>hideous laughter</i> (DC 16) <br
        />1/day—<i>thorn body</i>APG</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>10, <strong>Dex </strong>17, <strong>Con </strong>14,
        <strong>Int </strong> 13, <strong>Wis </strong>14, <strong>Cha
        </strong>19</p><p><strong>Base Atk </strong>+2; <strong>CMB </strong>+1;
        <strong>CMD </strong>15</p><p><strong>Feats </strong>Alertness, Dodge,
        Weapon Finesse</p><p><strong>Skills </strong>Diplomacy +12, Fly +13,
        Perception +12, Perform (sing) +12, Sense Motive +12, Stealth +15, Swim
        +8</p><p><strong>Languages </strong>Common, Sylvan; <i>speak with
        animals</i></p><p><strong>SQ </strong>speak with water</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any
        forest</p><p><strong>Organization </strong>solitary, pair, or sisterhood
        (3-15)</p><p><strong>Treasure </strong>standard</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Shears (Su)</strong> An anhana carries a pair of
        gardening shears about 10 inches long. In the hands of an anhana, these
        shears deal damage as per a <i>+1 short sword</i> two sizes larger than
        the anhana. An anhana can use its Weapon Finesse feat with its shears.
        If an anhana's shears are lost or destroyed, it can make another pair
        with a week of work and a few scraps of metal.  </p><p><strong>Speak
        with Water (Sp)</strong> Once per day, four anhanas who are working
        together can speak with any body of standing or flowing fresh water at
        least as large as an anhana. This ability functions like <i>stone
        tell</i>. Flowing water can generally speak about events happening
        upstream (even distantly upstream), but not events happening
        downstream.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>Peaceful and kindly woodland creatures, anhanas tend to
        forest trees and befriend animals and foresters. Although an anhana has
        the general appearance of a young halfling woman, her unnatural eyes and
        erratic demeanor mark  her as a fey creature. Anhanas are wise, clever,
        and helpful. They use their magical shears to tend to the forest,
        encouraging its growth and development so that nature and civilization
        can exist in harmony. Anhanas return kindness with generosity, but they
        use their otherworldly abilities to confound and delay enemies of the
        forest.  An anhana stands between 2 and 3 feet tall and weighs about 30
        pounds.  <strong></strong></p><p><strong>Ecology</strong></p><p> 
        Anhanas are never children; they step fully formed from mature oak or
        maple trees in deep forests. Although an anhana forms at her full-if
        small-size, she is unclothed and her skin is covered with wedges of bark
        matching the tree that produced her. A newly sprouted anhana has the Hit
        Dice and abilities presented above, but cannot speak and has only an
        unformed impulse to aid others and protect the natural world. The
        anhana's tree produces a low, celebratory thrumming for a few days after
        the anhana sprouts. This noise carries far, but is audible only to fey
        and plant creatures. Mature anhanas recognize the thrumming and seek out
        the new arrival to welcome her into their sisterhood. Anhanas share
        clothing and other gifts with their new sister and teach her music,
        kindness, and woodcraft. Most anhanas absorb their bark-like coverings
        within a few weeks, but all anhanas can temporarily extend sharp wedges
        of bark from their skin to deter foes.  A tree that produces an anhana
        is usually one with a benevolent history or divine connection, such as
        having shaded a weary saint or growing where a nature goddess once
        stepped. Hamadryads (<i>Pathfinder RPG Bestiary 4</i> 148) hint that
        they have the ability to tend a tree with magic so that it sprouts an
        anhana, but they keep the precise process secret.  All anhanas are
        friends of the natural world. They speak freely and softly with animals,
        attempting to befriend even predators. They prune flowers and trees to
        encourage growth rather than harm or cull the plants. Anhanas in a
        sisterhood can amplify their understanding of the natural world and
        communicate with streams and lakes.  Although benevolent in their
        stewardship of the forest, anhanas understand that nature preys upon
        itself. An anhana does not mourn a fat rabbit captured by an owl or a
        creeping vine that kills a host tree. Similarly, an anhana does not
        begrudge a woodcutter his livelihood, but instructs the woodcutter how
        to harvest wood with the least impact on the forest.  Anhanas subsist on
        a diet of nuts and berries. They prefer not to spend all their time
        gathering sustenance, so they carefully prune and tend berry bushes and
        nut trees to maximize their yield. Lacking any particular connection to
        these particular bushes and trees, however, anhanas are likely to wander
        to some other part of the forest and leave the nuts and berries for
        another to find and enjoy.  Anhanas can live to be several centuries
        old. As their physical appearance does not change as they age, it is
        nearly impossible to distinguish an old anhana and from a young one. If
        an anhana does not die of disease or misfortune, she eventually feels a
        calling to return to nature. The anhana-aided by her sisters if
        possible- journeys to a deep stream or lake, sinks below the surface of
        the water, and permanently diffuses her essence into the water itself. 
        Anhanas can sprout in any climate, but they are most common in temperate
        forests. In boreal forests, anhanas sometimes retain their woody skin
        (these anhanas have a +2 natural armor bonus, but lack the <i>thorn
        body</i> spell-like ability). Jungle anhanas are skilled climbers and
        often live their lives among the canopies of great trees instead of on
        the ground (these anhanas have a climb speed of 30 feet, but they lack
        the speak with water spell-like ability). 
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        Anhanas are uncommon, but several groups inhabit the Verduran Forest and
        the Duroth Wood of Iobaria. Those few anhanas that live in the Fangwood
        are susceptible to the Blight, and most anhanas who sprout there have
        the blighted fey template (<i>Pathfinder Campaign Setting</i>: <i>Inner
        Sea Bestiary</i> 6).  Although these kindly fey originate in deep
        forests, they enjoy helping civilized folk who have good hearts and
        intentions. Anhanas often loiter near well-traveled paths or on the
        outskirts of settlements that abut an old forest. They spend their days
        tending to the forest so that it exists in harmony with people, and can
        be seen pruning back overgrown thornbushes, explaining directions to
        lost travelers, and entertaining local foresters with their cheerful
        songs. Though generally peaceful, they use their abilities to confound
        and oppose those who would destroy or irreparably harm forests.  Unlike
        many fey, anhanas are not shy. They eagerly approach strangers on forest
        paths to  say hello and offer assistance. Believing that it is impolite
        to startle others, they often announce their presence by rustling bushes
        or using <i>ghost sound</i> to simulate the sound of a tinkling bell or
        babbling river. Anhanas don't seem to understand that this "courtesy" is
        sometimes alarming to others.  Anhanas prefer to congregate in groups of
        their own kind called sisterhoods. Sisterhoods sometimes occupy a single
        grove or glen for decades, earning a reputation as kindly forest
        spirits, while other sisterhoods wander throughout their forests.
        Sisterhoods dissolve when individual members disagree about whether or
        where to travel, although the members separate peaceably.  Anhanas have
        a sixth sense for the emotions of others. They hide from angry or
        hateful people but are attracted to happy travelers. Anhanas are also
        drawn to the scared or desperate, such as lost children or starving
        runaways, and do their best to provide aid. At times, this aid is as
        simple as sharing a nourishing handful of nuts or giving clear
        directions back to a main road, but an anhana might also offer more
        direct assistance, such as confounding a wicked pursuer. Anhanas have
        little understanding of or regard for the laws of civilization, and they
        just as eagerly aid a good-hearted fugitive as they would a
        well-intentioned sheriff.  Anhanas are fundamentally flighty and
        erratic, despite their goodly nature. They rarely carry through with
        long-term plans, no matter how sincerely agreed to, and they are prone
        to wandering away from an area for no reason at all, even if they have
        carefully tended to it for months or years.  Although most anhanas enjoy
        being helpful, they have a strong sense of personal space and don't like
        being too close to people or animals. They have an irrational phobia of
        being touched by any creature besides other anhanas, and they almost
        reflexively activate their <i>thorn body</i> ability if physical contact
        seems to be imminent.  An anhana can be summoned with a <i>summon
        nature's ally IV</i> spell.</p></h4></div></section>
    _id: CVWulgTcVIOFze0H
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!9tMNhZSvNTsdZi9y.CVWulgTcVIOFze0H'
sort: 0
_key: '!journal!9tMNhZSvNTsdZi9y'

