name: Hollow One
_id: f2q5Vqbe9B2lCOt7
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Strange_Aeons.jpg
    title:
      show: false
      level: 1
    _id: dXwmpVvHYLeLoKJj
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!f2q5Vqbe9B2lCOt7.dXwmpVvHYLeLoKJj'
  - name: Hollow One
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 112</p><section id="HollowOne"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        10</p></div><div><p><strong>XP </strong>9,600</p><p>N Medium construct
        </p><p><strong>Init </strong>+9; <strong>Senses </strong>darkvision 60
        ft., low-light vision; Perception +15</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>26, touch 16, flat-footed 20 (+5 Dex, +1 dodge, +10
        natural)</p><p><strong>hp </strong>117 (15d10+35)</p><p><strong>Fort
        </strong>+5, <strong>Ref </strong>+10, <strong>Will
        </strong>+5</p><p><strong>DR </strong>10/magic; <strong>Immune
        </strong>construct traits</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft.</p><p><strong>Melee </strong>2 slams +22
        (1d8+7/19-20)</p><p><strong>Space </strong>5 ft.; <strong>Reach
        </strong>5 ft.</p><p><strong>Special Attacks </strong>dread, rage, sneak
        attack +3d6</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>25, <strong>Dex </strong>21, <strong>Con </strong>-,
        <strong>Int </strong> 16, <strong>Wis </strong>11, <strong>Cha
        </strong>14</p><p><strong>Base Atk </strong>+15; <strong>CMB
        </strong>+22; <strong>CMD </strong>38</p><p><strong>Feats
        </strong>Combat Expertise, Combat Reflexes, Deceitful, Dodge, Improved
        Critical (slam), Improved Feint, Improved Initiative,
        Toughness</p><p><strong>Skills </strong>Bluff +21, Disguise +4,
        Intimidate +17, Perception +15, Sense Motive +15, Stealth
        +20</p><p><strong>Languages </strong>Aklo, Common, Kelish</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any</p><p><strong>Organization
        </strong>solitary</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Dread (Su)</strong> As a standard action, a hollow one
        can cause all creatures within 30 feet to become shaken for 1 minute. On
        a successful DC 19 Will save, affected creatures are instead shaken for
        1 round. This effect doesn't cause a shaken creature to become
        frightened or a frightened creature to become panicked. This is a
        mind-affecting fear effect and the save DC is Charisma-based. 
        </p><p><strong>Rage (Su)</strong> As a free action after taking damage
        in combat, a hollow one can fly into a rage. While raging, the hollow
        one gains a +2 bonus to Strength and 30 temporary hit points, but takes
        a -2 penalty to AC. The rage lasts until the end of the battle or for 1
        minute, whichever is shorter. A hollow one can voluntarily end this rage
        at any point.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>Hollow ones are constructs that are made to resemble
        living humanoids. In most conditions, it requires a successful DC 30
        Perception check to notice the supposed person is actually a construct,
        although inflicting any kind of wound on the hollow one immediately
        reveals the truth as the skin covering this creature unravels to reveal
        an empty cavity within. Hollow ones are complex  creations designed to
        fulfill a variety of purposes, most often acting as servants, guardians,
        or assistants. Each hollow one is bound by a physical similarity to a
        certain humanoid, who sacrifices a portion of himself in the formation
        process.  Unlike many constructs, however, the hollow one has a spark of
        intelligent life-the essence of its creator or the creator's victim.
        While this enlivening energy is little more than a shallow imitation of
        the original's true being, sometimes the hollow one can become more than
        an automaton and seek out its own life and motivations. These hollow
        ones are the most dangerous because they are most prone to madness
        (especially violent madness). Such a hollow one is the exception to the
        rule, however, and most are destined to become little more than slaves
        and playthings, obedient mockeries of those they were created to look
        like.  When encountered, hollow ones are often mistaken for undead
        creatures instead of constructs, mainly due to the unconventional
        building material required to create these creatures.  A hollow one is
        the same size as the humanoid it was fashion to resemble, but weighs a
        mere fraction of his weight due to being only chemically treated skin. 
        <strong></strong></p><p><strong>Ecology</strong></p><p>  As constructs
        generally created by alchemists, hollow ones are uncanny replicas of
        humanoids made from shreds of skin infused with magic and rare
        chemicals. Hollow ones have an advantage over many constructs in that
        they have intellects gifted to them by their creators during their
        construction. Even those who know the humanoids on which hollow ones are
        modeled well may still be duped by the hollow ones, as they are
        fashioned by a process similar to that used to create simulacra and
        their true nature is difficult to discern.  The fundamental intelligence
        of these creatures makes them dangerous, however. They are unstable
        beings, and 5% of hollow ones harbor a deep loathing and jealousy of
        their creator, to a point where they cannot abide continuing to serve
        them. These hollow ones turn on their owners, seeking to kill and
        sometimes replace them. Another 5% of hollow ones develop a greater
        intelligence and personality than the creatures on which they're
        modeled, becoming superior versions of the originals. Often seeking the
        immediate destruction of their would-be masters, these hollow ones have
        inflated views of their own importance and place in the world and
        proceed to try to convince others of it. These creatures gain a +4 bonus
        to both Intelligence and Wisdom and a +6 bonus to Charisma. Many of
        these hollow ones go on to take class levels, typically matching those
        of their creator (or the person being emulated).  Hollow ones begin as
        blank slates in terms of development. The energizing force within each
        evolves  differently, so that three hollow ones created at more or less
        the same time by the same master can result in three unique constructs
        depending on the context and situational factors in which they were
        created.  Some scholars make the assumption that the first hollow ones
        were created by foul creatures that made use of fleshwarping, but little
        evidence for this has been found. Records retrieved from before
        Earthfall mention hollow ones, so the practice of making these bizarre
        constructs is more than 10,000 years old, though it has only recently
        come to light again in Avistan.  <strong></strong></p><p><strong>Habitat
        &amp; Society</strong></p><p>  Many hollow ones take on specific aspects
        of their creators. Some arcane psychologists and philosophers have
        suggested that the inherited aspect is the creator's most overriding
        emotion or state of mind at the time. Thus, a happy creator fashions a
        jolly hollow one, while a psychotic person creates a deranged one, and
        so on. However, the hollow one nurtures this aspect into something
        larger as it develops. Misery becomes overwhelming sorrow or joy becomes
        completely unbearable. Each year after its creation, the construct must
        succeed at a DC 25 Will save or lapse into madness brought on by these
        emotions. The direction this madness takes varies, but is always
        dangerous in some way.  Hollow ones develop their own mannerisms similar
        to-but not always identical to-their creators'. Those that escape into
        society often act as beacons for other constructs-paragons and leaders
        that use their constructed followers on crusades of their own twisted
        devising, taking on levels in various classes, and become tyrants,
        dictators, and saints in their own design.  Several narcissistic cabals
        and cults use hollow ones as tools of vengeance and assassination,
        creating scores of copies of the cult leader to visit punishments upon
        dozens of enemies at the same time, creating the illusion of
        omnipotence. Such tactics have been employed by power-crazed tyrannical
        alchemists, who go on a frenzy of self-harm and create scores of hollow
        ones. Sadly, such personal killers often become so crazed that they turn
        on their creator en masse and exact suitable revenge, usually starting
        by flaying the creator alive.  A tale circulating through Absalom
        speaks  of a thieves' guild that once operated in that city and employed
        hollow ones as patsies and for blackmail. The guild leadership would
        capture someone, torture the captive by flaying her skin, and then
        healing her so there was no evidence of the stolen tissue. They would
        then send the hollow one to commit a visible crime in order to implicate
        their victim. To clear her name, the victim was forced to pay the
        guild's leadership a hefty price.  <br /><strong>Other Hollow
        Ones</strong><br /> The method for creating hollow ones don't restrict
        the form to be that of a humanoid. The form needs to be a creature that
        has skin, though. A catalog of constructs was recently unearthed from an
        old Azlanti ruin that described an aboleth hollow one that perplexed the
        ancient explorer who encountered the strange creature. Hollow ones made
        in the form of creatures with particularly durable skin could have a
        much higher natural armor bonus than the typical hollow one described
        here.  <br /><strong>Creating a Hollow One</strong><br /> The process
        used to fashion a hollow one requires the creator to collect a
        considerable quantity of the skin of the person being copied (referred
        to as the subject) using knives and an arcane ritual that ultimately
        deals 1d4 points of Constitution damage to the subject. The skin is
        alchemically treated and enhanced into a form not unlike the wrappings
        of a mummy. At the final moment of the ritual, the creator removes a
        piece of the subject's brain using a specially created brass and ebony
        hook with a tiny movable cutting blade attached. This hook is inserted
        into the subject's brain through the nose to remove a portion of living
        brain tissue, an act that deals an additional 1d2 points of Constitution
        damage to the subject. At this point, the creator must succeed at a DC
        30 Heal check; otherwise the subject permanently loses 1d3 points of
        Intelligence.  <br /></p><div class="heading"><p
        class="alignleft">Hollow One</p>  <strong>CL</strong> 9th;
        <strong>Price</strong> 42,000 gp  <br /><hr
        /><strong>Construction</strong><br /><hr /> 
        <strong>Requirements</strong> Craft Construct, <i>lesser
        simulacrum</i>UM; 2 square yards of skin collected over the course of 1
        month and 1 ounce of brain material; creator must be caster level 9th;
        <strong>Skill</strong>s Craft (alchemy) DC 24 and Heal DC 24;
        <strong>Cost</strong> 21,000 gp</div></h4></div></section>
    _id: AzBcNWRw0ZVy0s3m
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!f2q5Vqbe9B2lCOt7.AzBcNWRw0ZVy0s3m'
sort: 0
_key: '!journal!f2q5Vqbe9B2lCOt7'

