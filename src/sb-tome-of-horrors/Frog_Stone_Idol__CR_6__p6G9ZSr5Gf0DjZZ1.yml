name: Frog Stone Idol (CR 6)
_id: p6G9ZSr5Gf0DjZZ1
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/construct.png
    title:
      show: false
      level: 1
    _id: GYaCUIGoHjn1hDR1
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!p6G9ZSr5Gf0DjZZ1.GYaCUIGoHjn1hDR1'
  - name: Frog Stone Idol (CR 6)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors 4</p><section
        id="FrogStoneIdol"><div class="heading"><p
        class="alignright"><strong>CR</strong> 6</p></div><div><p><b>XP
        </b>2,400</p><p>N Large construct </p><p><b>Init </b>+0; <b>Senses
        </b>darkvision 60 ft., low-light vision; Perception +0</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>20, touch 9,
        flat-footed 20 (+11 natural, -1 size)</p><p><b>hp </b>79
        (9d10+30)</p><p><b>Fort </b>+3, <b>Ref </b>+3, <b>Will
        </b>+3</p><p><b>Defensive Abilities </b>rejuvenation; <b>DR
        </b>10/adamantine; <b>Immune </b>construct traits (+30 hp),
        magic</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>20 ft.</p><p><b>Melee </b>bite +13
        (2d6+7)</p><p><b>Space </b>10 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>crush (4d6+7; DC 16 Ref half)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>21, <b>Dex
        </b>10, <b>Con </b>-, <b>Int </b> -, <b>Wis </b>11, <b>Cha
        </b>1</p><p><b>Base Atk </b>+9; <b>CMB </b>+15; <b>CMD </b>25 (29 vs.
        trip)</p><p><b>Skills </b>Acrobatics +0 (+8 jumping); <b>Racial
        Modifiers </b>+8 Acrobatics when jumping</p><p><b>SQ </b>powerful
        leaper, site bound</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary or
        pair</p><p><b>Treasure </b>none</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Crush (Ex)</b> A frog stone idol
        can land on foes as a standard action, using its whole body to crush
        them. Crush attacks are effective only against opponents one or more
        size categories smaller than the frog stone idol. A crush attack affects
        as many creatures as fit in the stone idol's space. Creatures in the
        affected area must succeed on a DC 16 Reflex save or be pinned,
        automatically taking bludgeoning damage during the next round unless the
        stone idol moves off them. If the stone idol chooses to maintain the
        pin, it must succeed at a combat maneuver check as normal. Pinned foes
        take damage from the crush each round if they don't escape. The save DC
        is Constitution-based and includes a +2 racial bonus. 
        </p><p><b>Powerful Leaper (Ex)</b> A frog stone idol uses its Strength
        to modify Acrobatics checks made to jump, and it has a +8 racial bonus
        on Acrobatics checks made to jump.  </p><p><b>Immunity to Magic (Ex)</b>
        A stone idol is immune to any spell or spell-like ability that allows
        spell resistance. In addition, certain spells and effects function
        differently against the creature, as noted below.  A <i>transmute rock
        to mud</i> spell <i>slow</i>s a stone idol (as the <i>slow</i> spell)
        for 2d6 rounds, with no saving throw, while <i>transmute mud to rock</i>
        heals all of its lost hit points.  A <i>stone to flesh</i> spell does
        not actually change the stone idol's structure but negates its damage
        reduction and immunity to magic for 1 full round.  A <i>disintegrate</i>
        spell deals +50% damage to a stone idol.  A <i>chaos hammer</i>, <i>holy
        smite</i>, <i>order's wrath</i>, or <i>unholy blight</i> spell deals 1d4
        points of damage per two caster levels to a stone idol if the creator's
        alignment matches one of the aforementioned spells. 
        </p><p><b>Rejuvenation (Su)</b> Unless a stone idol's main body is
        shattered into small fragments and scattered to the winds, the creature
        reforms itself at full strength in 1d4+2 days.  </p><p><b>Site Bound
        (Ex)</b> A stone idol cannot travel more than 200 feet from a central
        point designated by its creator. Once this point is selected, it can
        never be changed. A stone idol taken outside the area ceases functioning
        until it is brought back into the area.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A stone idol
        frog is about 10 feet long and weighs 10,000 pounds. It leaps at its
        foes attempting to crush as many as it can before resorting to its
        powerful bite attack.   <br /><b>Variant Frog Stone Idol</b><br />  The
        stone idol frog detailed above is not the only known to exist. One such
        variant is detailed below.  <br /><b>Tsathogga Frog Stone Idol:</b> This
        creature is identical to the standard stone idol frog. It also gains the
        following special attack.  <br /><b>Vile Croak (Su) </b>Once every 1d4
        rounds as a standard action, a Tsathogga idol can unleash a croak that
        affects all that hear it within 60 feet. Affected creatures take 5d6
        points of negative energy damage. A DC 16 Will save reduces the damage
        by half. The save DC is Constitution-based and includes a +2 racial
        bonus.  <br /><b>Construction</b><br />  A stone idol frog is chiseled
        from a block of smooth stone that weighs at least 3,000 pounds. It must
        be of exceptional quality and have a value of not less than 5,000 gp. 
        <br /><b>FROG STONE IDOL</b><br />  <b>CL</b> 10th; <b>Price</b> 69,000
        gp  </p><p><b>Requirements </b>Craft Construct, <i>animate objects</i>,
        <i>geas/quest</i>, <i>wall of stone</i>, creator must be caster level
        10th; <b>Skill</b> Craft (sculpting) or Craft (stonemasonry) DC 16;
        <b>Cost</b> 37,000 gp.</p></h4></div></section>
    _id: hilY2kjtlDbQkUwy
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!p6G9ZSr5Gf0DjZZ1.hilY2kjtlDbQkUwy'
sort: 0
_key: '!journal!p6G9ZSr5Gf0DjZZ1'

