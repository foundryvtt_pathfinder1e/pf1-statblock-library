name: Jade Colossus (CR 16)
_id: 6Ifa23W0gXMzgWRu
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/construct.png
    title:
      show: false
      level: 1
    _id: fLUCiaejDXkZuOmC
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!6Ifa23W0gXMzgWRu.fLUCiaejDXkZuOmC'
  - name: Jade Colossus (CR 16)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="JadeColossus"><div class="heading"><p
        class="alignright"><strong>CR</strong> 16</p></div><div><p><b>XP
        </b>76,800</p><p>N Colossal construct </p><p><b>Init </b>+0; <b>Senses
        </b>darkvision 60 ft., low-light vision; Perception +0</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>34, touch 2,
        flat-footed 34 (+32 natural, -8 size)</p><p><b>hp </b>256
        (32d10+80)</p><p><b>Fort </b>+10, <b>Ref </b>+10, <b>Will
        </b>+10</p><p><b>Defensive Abilities </b>light reflection; <b>DR
        </b>15/adamantine; <b>Immune </b>construct traits, fire,
        magic</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>40 ft.</p><p><b>Melee </b>2 slams +43
        (4d8+19)</p><p><b>Space </b>30 ft.; <b>Reach </b>30 ft.</p><p><b>Special
        Attacks </b>breath weapon</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>49, <b>Dex
        </b>10, <b>Con </b>-, <b>Int </b> -, <b>Wis </b>11, <b>Cha
        </b>1</p><p><b>Base Atk </b>+32; <b>CMB </b>+59; <b>CMD
        </b>69</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization
        </b>solitary</p><p><b>Treasure </b>none</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Breath
        Weapon (Su)</b> Once every 1d4 rounds, a jade colossus can spray a blast
        of green energy in either a 60-foot cone or a 120-foot line. A creature
        in the affected area takes 15d6 points of damage, Reflex half DC 26. A
        creature slain as the result of this breath weapon transforms into jade.
        The save DC is Constitution-based. A creature turned to jade has
        Hardness 6, hp 40. This effect can be reversed by casting <i>stone to
        flesh</i>, <i>wish</i>, or <i>miracle</i>. The victim remains dead, but
        can then be <i>raised</i> or <i>resurrected</i> normally.
        </p><p><b>Immunity to Magic (Ex)</b> A jade colossus is immune to any
        spell or spell-like ability that allows spell resistance. In addition,
        certain spells and effects function differently against the creature, as
        noted below. A disintegrate spell <i>slow</i>s a jade colossus (as the
        <i>slow</i> spell) for 1d6 rounds, with no saving throw. A shatter spell
        deals 3d12 points of damage to a jade colossus, with no saving throw. A
        magical attack that deals fire damage breaks any <i>slow</i> effect on
        the jade colossus and heals 1 point of damage for each 3 points of
        damage the attack would otherwise deal. If the amount of healing would
        cause the golem to exceed its full normal hit points, it gains any
        excess as temporary hit points. A jade colossus gets no saving throw
        against fire effects. </p><p><b>Light Reflection (Ex)</b> The angles and
        makeup of the jade colossus allows it to reflect any light-based attacks
        or effects (it cannot reflect natural sunlight however). Reflected light
        acts as an emanation (lasting 1 round) that blinds all creatures within
        40 feet. A creature can attempt a Reflex save DC 26 to avoid being
        blinded If the save fails, the creature is blinded for 2d4 rounds (as
        the <i>blindness</i> spell); creatures that successfully save are
        instead dazzled for a like amount of time. Undead and similar creatures
        to whom sunlight is harmful to take 6d6 points of damage (a successful
        Reflex save DC 26 halves the damage).</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The jade
        colossus was first seen dominating the skyline over the City of Brass,
        its massive form reflecting the light from the ever-burning fires of the
        City. Since that time, rumors of these creatures moving across the
        Material Plane have been heard in taverns and inns.</p><p>Jade colossi
        are massive constructs built by powerful spellcasters to do their
        bidding; typically for protection or to wage war against an
        archrival.</p><p>A jade colossus pummels a foe with its hardened fists;
        dealing massive amounts of damage with every successful strike. It
        almost always opens combat with its breath weapon.</p><p>A typical jade
        colossus stands 35 feet tall and weighs about 60,000 pounds. (The jade
        colossus spotted in the City of Brass is thought to be over 50 feet tall
        and weigh in excess of 100,000 pounds.) <br /><b>Ruby Star of Law</b><br
        /> Sages speak of a jade colossus located in the City of Brass that has
        the <i>ruby star of law</i> (a gem of inestimable value) embedded in its
        forehead. The Jade Guardian (as it is known in the City) is a CR 18
        advanced jade colossus with the following additional ability.<br
        /><b>Energy Ray (Su):</b> Once per round as a swift action, the Jade
        Guardian can fire a ray to a range of 200 feet (requires a successful
        ranged touch attack to hit). Genies hit by this ray must make a
        successful Fortitude save (DC 26) or take 6d4 points of Constitution
        damage. On a successful save, the genie takes onehalf damage. Other
        creatures are affected as follows: Lawful creatures are
        <i><i>slow</i>ed</i> (as by the <i>slow</i> spell) for 1d4 rounds (no
        save).</p><p>Non-lawful creatures take 6d6 points of force damage (no
        save) and are stunned for 1d4 rounds (DC 26 Fortitude save
        negates).Should the <i>ruby star of law</i> ever be claimed as a magic
        item, treat it as a minor artifact that allows use of the energy ray
        ability indicated above, save that it requires a standard action to
        activate.<br /><b>Construction</b><br /> A jade colossus's body is
        constructed from 80,000 pounds of pure jade, and a mixture of rare
        stones, herbs, and chemicals totaling 8,000 gp.<br /><b>JADE
        COLOSSUS</b><br /> <b>CL</b> 15th; <b>Price </b>110,000 gp
        </p><p><b>Requirements </b>Craft Construct, <i>blindness</i>, <i>flesh
        to stone</i>, <i>geas/quest</i>, <i>polymorph any object</i>, creator
        must be caster level 15th; </p><p><b>Skill </b>Craft (sculptures) DC 30;
        </p><p><b>Cost </b>59,000 gp </p><p><b>Copyright Notice </b>Author Scott
        Greene and Casey Christofferson.</p></h4></div></section>
    _id: FdTk7WXe2KkKXJDh
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!6Ifa23W0gXMzgWRu.FdTk7WXe2KkKXJDh'
sort: 0
_key: '!journal!6Ifa23W0gXMzgWRu'

