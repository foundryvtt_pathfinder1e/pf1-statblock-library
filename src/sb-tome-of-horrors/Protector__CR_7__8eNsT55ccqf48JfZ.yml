name: Protector (CR 7)
_id: 8eNsT55ccqf48JfZ
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: qiqWnOUNuFggYiIf
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!8eNsT55ccqf48JfZ.qiqWnOUNuFggYiIf'
  - name: Protector (CR 7)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="Protector"><div class="heading"><p
        class="alignright"><strong>CR</strong> 7</p></div><div><p><b>XP
        </b>3,200</p><p>LN Medium outsider (extraplanar, lawful)</p><p><b>Init
        </b>+2; <b>Senses </b>darkvision 60 ft., know alignment 50 ft.;
        Perception +15</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
        /><div><p><b>AC </b>18, touch 12, flat-footed 16 (+2 Dex, +6
        natural)</p><p><b>hp </b>55 (7d10+14)</p><p><b>Fort </b>+9, <b>Ref
        </b>+9, <b>Will </b>+11</p><p><b>Immune </b>outsider traits</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>50
        ft.</p><p><b>Melee </b>longsword +9/+4 (1d8+2 /19-20)</p><p><b>Space
        </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Domain Spell-Like Abilities</b>
        (CL 7th)<br />8/day—<i>resistant touch</i>, <i>touch of
        law</i></p><p><b>Spells Prepared</b> (CL 7th)<br />4th—<i>chaos
        hammer</i> (DC 19), <i>divine power</i>, <i>spell immunity</i>D<br
        />3rd—<i>daylight</i>, <i>magic circle against chaos</i>, <i>protection
        from energy</i>D, <i>remove curse</i><br />2nd—<i>aid</i>, <i>align
        weapon</i>, <i>consecrate</i>, <i>resist energy</i>, <i>shatter</i> (DC
        18), <i>shield other</i>D<br />1st—<i>bless</i>, <i>bless</i> water,
        <i>command</i> (DC 17), <i>divine favor</i>, <i>endure elements</i>,
        <i>protection from chaos</i>D, <i>sanctuary</i> (DC 17)<br />0 (at
        will)—<i>create water</i>, <i>cure minor wounds</i>, <i>detect
        magic</i>, <i>read magic</i></p><p><b>D</b> domain spell; <b>Domains
        </b>Law, Protection</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>15, <b>Dex
        </b>15, <b>Con </b>15, <b>Int </b> 20, <b>Wis </b>20, <b>Cha
        </b>20</p><p><b>Base Atk </b>+7; <b>CMB </b>+9; <b>CMD </b>21 (can't be
        tripped)</p><p><b>Feats </b>Cleave, Expertise, Iron Will, Power
        Attack</p><p><b>Skills </b>Diplomacy +15, Escape Artist +9, Heal +15,
        Knowledge (any one) +12, Knowledge (local) +15, Knowledge (the planes)
        +15, Knowledge (religion) +12, Perception +15, Sense Motive +15,
        Spellcraft +12, Survival +15</p><p><b>Languages </b>Auran, Celestial,
        Common; telepathy 50 ft.</p><p><b>SQ </b><i>air
        walk</i></p><p><strong>Combat Gear</strong> longsword</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        Inner and Outer planes</p><p><b>Organization </b>solitary, pair, or
        troupe (6-11)</p><p><b>Treasure </b>standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Air Walk
        (Su)</b> Protectors can <i>air walk</i> as the spell of the same name
        (caster level 10th). This ability is always active and can be dispelled,
        but the protector can restart it as a free action on its next turn. 
        </p><p><b>Know Alignment (Su)</b> A protector automatically knows the
        alignment of any creature within 50 feet that it looks upon. 
        </p><p><b>Resistant Touch (Sp)</b> As a standard action, you can touch
        an ally to grant him your resistance bonus for 1 minute.  When you use
        this ability, you lose your resistance bonus granted by the Protection
        domain for 1 minute.  </p><p><b>Touch of Law (Sp)</b> You can touch a
        willing creature as a standard action, infusing it with the power of
        divine order and allowing it to treat all attack rolls, skill checks,
        ability checks, and saving throws for 1 round as if the natural d20 roll
        resulted in an 11.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Protectors
        are extraplanar guardians of law that appear as very noble and regal
        creatures. They are often sent to the Material Plane when the forces of
        chaos swing from the balance. Protectors move by means of their <i>air
        walk</i> ability; their feet never touch the ground, and they always
        float 6 to 10 inches above the ground. A protector stands 7 feet tall
        and weighs about 200 pounds or more.</p><p>Protectors attack neutral
        (except non-intelligent or low-intelligence creatures such as animals)
        and chaotic creatures on sight. They never knowingly and willingly
        attack lawful creatures. A protector fights with its longsword and
        spells.</p><p>Credit The Protector originally appeared in the First
        Edition module <i>B3 Palace of the Silver Princess</i> (© TSR/Wizards of
        the Coast, 1981) and is used by permission.</p><p>Copyright Notice
        Authors Scott Greene and Erica Balsley, based on original material by
        Jean Wells.</p></h4></div></section>
    _id: efAvVbcCW7igCfz4
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!8eNsT55ccqf48JfZ.efAvVbcCW7igCfz4'
sort: 0
_key: '!journal!8eNsT55ccqf48JfZ'

