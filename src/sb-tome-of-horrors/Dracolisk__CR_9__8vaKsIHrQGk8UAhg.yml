name: Dracolisk (CR 9)
_id: 8vaKsIHrQGk8UAhg
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/dragon.png
    title:
      show: false
      level: 1
    _id: C5Ll8nCnXMUvlepy
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!8vaKsIHrQGk8UAhg.C5Ll8nCnXMUvlepy'
  - name: Dracolisk (CR 9)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="Dracolisk"><div class="heading"><p
        class="alignright"><strong>CR</strong> 9</p></div><div><p><b>XP
        </b>6,400</p><p>N Large dragon </p><p><b>Init </b>+2; <b>Senses
        </b>darkvision 60 ft., low-light vision; Perception +10</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>24, touch 11,
        flat-footed 22 (+2 Dex, +13 natural, -1 size)</p><p><b>hp </b>115
        (11d12+44)</p><p><b>Fort </b>+11, <b>Ref </b>+9, <b>Will
        </b>+8</p><p><b>Immune </b>energy type, sleep, paralysis</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30 ft., fly
        60 ft. (poor)</p><p><b>Melee </b>bite +16 (2d6+6), 2 claws +16
        (1d6+6)</p><p><b>Space </b>10 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>breath weapon, petrifying gaze (Fort DC 16)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>23, <b>Dex
        </b>14, <b>Con </b>19, <b>Int </b> 6, <b>Wis </b>12, <b>Cha
        </b>13</p><p><b>Base Atk </b>+11; <b>CMB </b>+18; <b>CMD </b>30 (34 vs.
        trip)</p><p><b>Feats </b>Alertness, Blind-Fight, Cleave, Improved Vital
        Strike, Power Attack, Vital Strike</p><p><b>Skills </b>Climb +14,
        Diplomacy +8, Fly +10, Intimidate +8, Perception +10, Sense Motive +10,
        Stealth +5, Survival +8, Swim +13</p><p><b>Languages
        </b>Draconic</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> see text</p><p><b>Organization
        </b>solitary or colony (3-6)</p><p><b>Treasure </b>none</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Breath
        Weapon (Su)</b> A dracolisk's breath weapon depends on what type of
        dragon parent it had, as summarized on the table below. Regardless of
        its type, a dracolisk's breath weapon is usable once every 1d4 rounds
        (and no more than three times per day), deals 4d8 points of damage, and
        allows a DC 19 Reflex save for half damage. The save DC is
        Constitution-based. </p><table><tbody><tr><th>Dracolisk
        Variety</th><th>Breath Weapon</th></tr><tr><td>Black</td><td>60-foot
        line of acid</td></tr><tr><td>Blue</td><td>60-foot line of
        electricity</td></tr><tr><td>Green</td><td>30-foot cone of gas
        (acid)</td></tr><tr><td>Red</td><td>30-foot cone of
        fire</td></tr><tr><td>White</td><td>30-foot cone of
        cold</td></tr></tbody></table>  <p><b>Immunity to Energy (Ex)</b> A
        dracolisk is immune to one type of energy based on its dragon parent and
        variety. </p><table><tbody><tr><th>Dracolisk
        Variety</th><th>Immunity</th></tr><tr><td>Black</td><td>acid</td></tr><tr><td>Blue</td><td>electricity</td></tr><tr><td>Green</td><td>acid</td></tr><tr><td>Red</td><td>fire</td></tr><tr><td>White</td><td>cold</td></tr></tbody></table>
        <p><b>Petrifying Gaze (Su)</b> Turn to stone permanently, range 30 feet,
        Fortitude DC 16 negates. The save DC is Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The vicious
        dracolisk is a rare crossbreed of dragon and basilisk. No one is quite
        sure how the dracolisk species came to be, but all who have encountered
        it are well aware of its lethality. There is believed to be one species
        of dracolisk for every species of dragon. Thus far, however, most
        dracolisks encountered have been of the black variety. At first glance,
        a dracolisk appears to be a juvenile dragon of whatever color its dragon
        parent was-but thanks to the petrifying gaze it inherited from its
        basilisk parent, most who encounter a dracolisk never get a second
        glance. A dracolisk has a scaled body the same color as its dragon
        parent that fades to a lighter shade on its underside. A short, curved
        horn, similar to a rhino's, juts from its nose. Its dragon-like wings
        match its body color but fade to a slightly darker shade near the tips.
        A dracolisk's eyes are pale green with sparkles that match its
        dragon-parent color.</p><p>A typical dracolisk is 15 feet long and
        weighs about 3,000 pounds.</p><p>A dracolisk's environment varies based
        on its dragon heritage: black dracolisks can be found in warm marshes,
        deserts, or underground; blue dracolisks favor warm hills and mountains,
        rarely being found underground; green dracolisks favor temperate or warm
        forests and underground settings; red dracolisks favor warm mountains
        and underground settings; and white dracolisks favor cold mountains,
        cold deserts, and underground.</p><p>The dracolisk attacks first with
        its breath weapon and gaze attack. After this, it attacks with its bite
        and clawed forelegs.</p><p>Credit The Dracolisk originally appeared in
        the First Edition module <i>S4 Lost Caverns of Tsojcanth</i> (©
        TSR/Wizards of the Coast, 1982) and later in the First Edition
        <i>Monster Manual II</i> (© TSR/Wizards of the Coast, 1983) and is used
        by permission.</p><p>Copyright Notice Author Scott Greene, based on
        original material by Gary Gygax</p></h4></div></section>
    _id: J7eSb3rFs5mdpHCM
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!8vaKsIHrQGk8UAhg.J7eSb3rFs5mdpHCM'
sort: 0
_key: '!journal!8vaKsIHrQGk8UAhg'

