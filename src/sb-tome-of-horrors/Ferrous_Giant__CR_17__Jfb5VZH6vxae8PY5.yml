name: Ferrous Giant (CR 17)
_id: Jfb5VZH6vxae8PY5
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/humanoid.jpg
    title:
      show: false
      level: 1
    _id: a95I7D3HaRSPRK2y
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!Jfb5VZH6vxae8PY5.a95I7D3HaRSPRK2y'
  - name: Ferrous Giant (CR 17)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="FerrousGiant"><div class="heading"><p
        class="alignright"><strong>CR</strong> 17</p></div><div><p><b>XP
        </b>102,400</p><p>N Huge humanoid (giant)</p><p><b>Init </b>+0;
        <b>Senses </b>low-light vision; Perception +30</p><p><b>Aura
        </b>info</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
        /><div><p><b>AC </b>32, touch 8, flat-footed 32 (+6 armor, +18 natural,
        -2 size)</p><p><b>hp </b>287 (25d8+175)</p><p><b>Fort </b>+21, <b>Ref
        </b>+8, <b>Will </b>+12</p><p><b>DR </b>15/cold iron; <b>Immune
        </b>fire, mind-affecting effects</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>50
        ft.</p><p><b>Melee </b>greataxe +31/+26/+21/+16 (4d6+22, 19-20/x3) or
        greatsword +31/+26/+21/+16 (4d6+22, 17-20/x2); or 2 slams +31 (2d6+15
        plus grab)</p><p><b>Space </b>15 ft.; <b>Reach </b>15
        ft.</p><p><b>Special Attacks </b>trample (2d6+22; DC
        37)</p><p><b>Spell-Like Abilities</b> (CL 15th)<br />At
        will—<i><i>heat</i> metal</i> (DC 14), <i>levitate</i> (iron or steel
        objects or creatures only, including itself)<br />1/day—<i>wall of
        iron</i> (DC 18)</p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>40, <b>Dex </b>11, <b>Con </b>25, <b>Int </b> 14,
        <b>Wis </b>15, <b>Cha </b>14</p><p><b>Base Atk </b>+18; <b>CMB </b>+35
        (+39 bull rush, grapple, sunder); <b>CMD </b>45 (49 vs. bull rush,
        sunder)</p><p><b>Feats </b>Awesome Blow, Cleave, Critical Focus, Great
        Cleave, Greater Bull Rush, Greater Sunder, Improved Bull Rush, Improved
        Critical (greatsword or greataxe), Improved Sunder, Iron Will, Power
        Attack, Skill Focus (Craft [weaponsmithing]), Staggering
        Critical</p><p><b>Skills </b>Craft (any one) +30, Craft (weaponsmithing)
        +36, Perception +30, Spellcraft +27</p><p><b>Languages </b>Common,
        Giant</p><p><b>SQ </b>oversized weapon use, weapon and armor
        proficiency</p><p><strong>Combat Gear</strong> breastplate, Gargantuan
        greatsword or greataxe</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        temperate and warm hills and mountains</p><p><b>Organization
        </b>solitary or family (2-4 plus 35% noncombatants plus 1 cleric or
        druid of 9th-12th level)</p><p><b>Treasure </b>standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Ferrous
        Mind (Ex)</b> Ferrous giants are immune to mindinfluencing effects
        (charms, compulsions, phantasms, patterns, and morale effects).
        </p><p><b>Pound (Ex)</b> A ferrous giant that successfully grabs a foe
        two or more sizes smaller than itself can smash the opponent into the
        ground, walls, nearby trees, or other solid objects as a standard
        action. This deals 2d6+22 points of bludgeoning damage to the victim. In
        addition, the opponent must make a successful DC 37 Fortitude save or be
        stunned for one round. A ferrous giant can perform this action once per
        round as long as it maintains the grapple. The save DC is
        Strength-based. </p><p><b>Oversized Weapon Use (Ex)</b> A ferrous giant
        wields a Gargantuan greatsword or greataxe without penalty.
        </p><p><b>Weapon and Armor Proficiency</b> (Ex) Ferrous giants are
        automatically proficient with simple weapons, martial weapons, light and
        medium armor, and shields.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The great
        ferrous giant stands over 20 feet tall and appears as a humanoid
        composed of flexible and bendable iron. Though it is often mistaken for
        a construct, it is in fact a very intelligent giant. A ferrous giant
        makes its home deep within a secluded mountain range. Its lair is often
        a deep cave or cavern or a gigantic castle constructed of iron and built
        into the face of a mountain.</p><p>Ferrous giants are generally solitary
        creatures rarely interacting or dealing with other races, including
        other giants. Though not on bad terms with other giants, they simply
        have no interest in dealing with them. Ferrous giants spend most of
        their days hunting (deer, moose, boar, elk) and foraging or crafting
        exquisite weapons of iron. Some particularly sociable ferrous giants (a
        rare occurrence to be assured) set up a trading operation with a nearby
        town or city, exchanging weapons for whatever might interest the giant
        (food, iron, money, etc.).</p><p>In melee, the ferrous giant uses its
        Huge iron battleaxe or longsword to bash or cleave its opponents. If
        disarmed, or if it is feeling particularly vile, a ferrous giant can
        pummel a foe with its massive fists. When fighting smaller foes, the
        ferrous giant often opens combat by trampling its foes or grabbing them
        and flinging them aside. Particularly noisome opponents are scooped up
        and pounded into the ground. The ferrous giant uses its <i><i>heat</i>
        metal</i> ability in combat to <i>heat</i> the armor of its opponents
        and burn its foes.<br /><b>Ferrous Giants Characters</b><br /> Ferrous
        giants are extremely strong, have hardy constitutions and remain
        versatile all other facets. Ferrous giant shamans are usually adepts or
        druids. Clerics can choose two of the following domains: Earth, Fire,
        Knowledge, and Sun.<br /><b>+30 Strength, +15 Constitution, +2
        Intelligence, +2 Wisdom, +2 Charisma</b> <br /><b>Huge</b>: Ferrous
        giants are Huge and take a -2 penalty to AC, -2 penalty to attack rolls,
        +2 bonus to Combat Maneuver Bonus and Combat Maneuver Defense, and a -8
        penalty to Stealth checks. A ferrous giant's lifting and carrying limits
        quadruple those of Medium characters.<br /><b>Space</b> 15ft.,
        <b>Reach</b> 15ft.<br /><b>Speed</b>: A ferrous giant's base speed is 50
        feet.<br /><b>Low-light vision</b>: Ferrous giants can see twice as far
        as humans in conditions of dim light.<br /><b>Racial Hit Dice</b>: A
        ferrous giant starts with twenty-five levels of humanoid, which provide
        it with 25d8 HD, a base attack bonus of +18, and base save bonuses of
        Fort +14, Ref +8, Will +8.<br /><b>Racial Skills</b>: A ferrous giant's
        humanoid levels give it skill ranks equal to 25 x (2 + Int modifier).
        Its class skill are Craft (weapons), Intimidate, and Perception <br
        /><b>Racial Feats</b>: A ferrous giant's giant levels give it 12
        feats.<br /><b>AC</b>: +18 natural AC bonus.<br /><b>Natural
        Weapons</b>: Ferrous giants can fight with 2 unarmed strikes that deal
        lethal damage (2d6+15).<br /><b>Special Attacks (see above</b>): Grab,
        pound, spell-like abilities, trample.<br /><b>Special Qualities (see
        above</b>): Damage reduction 15/cold iron, fire immunity, ferrous
        mind.<br /><b>Weapon and Armor Proficiency</b>: Ferrous giants are
        automatically proficient with simple weapons, martial weapons, light and
        medium armor, and shields.<br /><b>Languages</b>: Ferrous giants begin
        play speaking Giant and Common. Ferrous giants with high intelligence
        scores can choose from the following languages: Draconic, Elven, Goblin,
        and Orc.</p><p>Copyright Notice Author Scott
        Greene.</p></h4></div></section>
    _id: Im93sxFGlNy1CUsV
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!Jfb5VZH6vxae8PY5.Im93sxFGlNy1CUsV'
sort: 0
_key: '!journal!Jfb5VZH6vxae8PY5'

