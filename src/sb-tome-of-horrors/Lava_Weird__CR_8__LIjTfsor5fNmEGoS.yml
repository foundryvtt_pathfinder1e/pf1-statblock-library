name: Lava Weird (CR 8)
_id: LIjTfsor5fNmEGoS
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: FpReEGAs6vJ5UC0W
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!LIjTfsor5fNmEGoS.FpReEGAs6vJ5UC0W'
  - name: Lava Weird (CR 8)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="LavaWeird"><div class="heading"><p
        class="alignright"><strong>CR</strong> 8</p></div><div><p><b>XP
        </b>4,800</p><p>CE Large outsider (earth, elemental, extraplanar,
        fire)</p><p><b>Init </b>+7; <b>Senses </b>darkvision 60 ft.; Perception
        +18</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC
        </b>19, touch 12, flat-footed 16 (+3 Dex, +7 natural, -1
        size)</p><p><b>hp </b>95 (10d10+40)</p><p><b>Fort </b>+10, <b>Ref
        </b>+6, <b>Will </b>+8</p><p><b>DR </b>10/bludgeoning; <b>Immune
        </b>elemental traits</p><p><b>Weaknesses </b>vulnerability to
        cold</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd
        </b>40 ft.</p><p><b>Melee </b>bite +15 (1d8+7 plus grab)</p><p><b>Space
        </b>10 ft.; <b>Reach </b>5 ft.</p><p><b>Special Attacks </b>control
        elemental, drown, fire, lava pool</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>21, <b>Dex
        </b>17, <b>Con </b>16, <b>Int </b> 11, <b>Wis </b>12, <b>Cha
        </b>16</p><p><b>Base Atk </b>+10; <b>CMB </b>+16 (+20 grapple); <b>CMD
        </b>29 (can't be tripped)</p><p><b>Feats </b>Alertness, Improved
        Initiative, Power Attack, Toughness, Weapon Focus (bite)</p><p><b>Skills
        </b>Bluff +16, Intimidate +16, Knowledge (planes) +13, Perception +18,
        Sense Motive +18, Stealth +12</p><p><b>Languages </b>Ignan, Terran,
        Weirdling</p><p><b>SQ </b>reform, transparency</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        Plane of Molten Skies, Elemental Plane of Fire</p><p><b>Organization
        </b>solitary, pack (2-4)</p><p><b>Treasure </b>standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Control
        Elemental (Ex)</b> Lava weirds can attempt to command any outsider with
        the "Earth," "Elemental," or "Fire" subtype that is within 50 feet. The
        Will save to avoid control has a DC of 21. The save DC is Charisma-based
        and includes a +4 racial bonus. This effect is similar to the
        <i>dominate monster</i> spell. The elemental receives a Will save to
        avoid being commanded. If the save succeeds, that elemental is immune to
        the control elemental ability of that lava weird for one day. If the
        save fails, the elemental falls under the control of the lava weird and
        obeys it to the best of its ability until either it or the weird dies.
        There is no limit to the number of HD of elementals a lava weird can
        control using this ability. The range is unlimited though both the lava
        weird and the elemental must be on the same plane of existence;
        otherwise, the weird loses control of the elemental. Lava weirds cannot
        control other lava weirds using this ability. </p><p><b>Fire (Ex)</b>
        Lava weirds are living creatures of elemental magma; any successful
        melee hit deals fire damage and the victim must make a successful Reflex
        save (DC 16) or catch on fire (see Catching on Fire in the <i>Pathfinder
        Core</i> Rulebook). The save DC is Constitution-based. Creatures
        attacking a lava weird unarmed or with natural weapons take fire damage
        and must make a successful Reflex save to avoid catching on fire just as
        if the lava weird had hit with its attack. </p><p><b>Drown (Ex)</b> If a
        lava weird pins a grabbed foe, it fully immerses its victim in its lava
        pool. A victim completely immersed takes fire damage (see Lava Pool
        below) and must hold its breath or drown. The victim can hold its breath
        for a number of rounds equal to twice its Constitution score. After
        that, the victim must make a successful Constitution check (DC 10, +1
        per previous check) each round to continue holding its breath. If the
        victim fails a check, it drowns. In the first round, the victim falls
        unconscious (0 hp). In the next round, the creature is dying (-1 hp),
        and in the third round the victim drowns. </p><p><b>Lava Pool</b> A lava
        weird's pool is a bubbling, churning, morass of molten rock mixed with
        elemental fire. Flammable materials that contact the lava pool
        automatically catch fire on the round contact is made. Creatures
        touching the pool take 2d8 points of fire damage per round of contact.
        Damage from the lava pool continues for 1d3 rounds after contact ceases,
        but this extra damage is only half of that dealt during contact (1d8
        points of damage). Creatures immune to fire are unaffected by the lava
        weird's pool and take no damage, though they can still drown if
        completely immersed. </p><p><b>Reform (Ex)</b> When reduced to 0 hit
        points or less, a lava weird collapses back into its pool. Four rounds
        later, it reforms at full strength minus any damage taken from coldbased
        effects or attacks. </p><p><b>Transparency (Ex)</b> A lava weird is
        effectively invisible in its lava pool until it attacks.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Lava weirds
        are serpent-like creatures formed of elemental magma. They are rarely
        encountered outside the Elemental Plane of Fire or the Plane of Molten
        Skies where they spend their time swimming in pools of liquid
        fire.</p><p>Sometimes though, a lava weird slips through a portal into
        the Material Plane and takes up residency there, always in a pool of
        lava or magma-whether man-made or naturally occurring. Lava weirds on
        the Material Plane are bound by their chosen pool and cannot leave it
        while on the Material Plane.</p><p>High-level spellcasters sometimes
        employ lava weirds as guardians.</p><p>A lava weird appears as a 10-foot
        long serpent formed of elemental fire.</p><p>Closer examination reveals
        liquid rock in scale-like patterns across its body, growing darker
        across its back. Two small flickers of white fire located on its head
        serve as eyes.</p><p>A lava weird prefers to hide in its lava pool until
        victims come within range. Once a target is close to its lava pool, the
        weird quickly lashes out and snatches the victim, biting it and coiling
        around it. It then attempts to pull the victim into its lava pool. Lava
        weirds are highly aggressive and prone to attack just about any creature
        that wanders too close to their pool.</p></h4></div></section>
    _id: WzV08LDNcX5Jzkae
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!LIjTfsor5fNmEGoS.WzV08LDNcX5Jzkae'
sort: 0
_key: '!journal!LIjTfsor5fNmEGoS'

