name: Flagstone Golem (CR 7)
_id: vPCRVn7VdSfYifA1
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/construct.png
    title:
      show: false
      level: 1
    _id: fQOXfVuq0GN52WFO
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!vPCRVn7VdSfYifA1.fQOXfVuq0GN52WFO'
  - name: Flagstone Golem (CR 7)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="FlagstoneGolem"><div class="heading"><p
        class="alignright"><strong>CR</strong> 7</p></div><div><p><b>XP
        </b>3,200</p><p>N Large construct </p><p><b>Init </b>+1; <b>Senses
        </b>darkvision 60 ft., low-light vision; Perception +0</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>20, touch 10,
        flat-footed 19 (+1 Dex, +10 natural, -1 size)</p><p><b>hp </b>74
        (8d10+30)</p><p><b>Fort </b>+2, <b>Ref </b>+3, <b>Will
        </b>+2</p><p><b>Defensive Abilities </b>energy absorption, flatten;
        <b>DR </b>10/ adamantine; <b>Immune </b>construct traits,
        magic</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>30 ft.</p><p><b>Melee </b>2 slams +13 (2d8+6 plus
        stun)</p><p><b>Space </b>10 ft.; <b>Reach </b>10 ft.</p><p><b>Special
        Attacks </b>stunning blow</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>22, <b>Dex
        </b>12, <b>Con </b>-, <b>Int </b> -, <b>Wis </b>11, <b>Cha
        </b>1</p><p><b>Base Atk </b>+8; <b>CMB </b>+15; <b>CMD
        </b>26</p><p><b>SQ </b>camouflage</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        any</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Camouflage (Ex)</b> While flattened, a flagstone golem is
        nearly indistinguishable from the surrounding floor. It takes a DC 25
        Perception check to notice a flattened flagstone golem for what it is.
        Dwarves get a +2 bonus on Perception checks to notice a flattened
        flagstone golem. </p><p><b>Energy Absorption (Su)</b> Any energy-based
        (acid, fire, cold, electricity, sonic) attack that directly targets a
        flagstone golem is absorbed into its body dealing no damage to the
        golem. A flagstone golem can use the absorbed energy to repair itself,
        healing 1 hit point for every 3 points of damage the attack would have
        otherwise dealt. Or it can release the energy in a 30-foot cone that
        deals 3d8 points of energy damage (of whatever type was absorbed) to all
        within the area. An affected opponent can attempt a DC 14 Reflex save to
        reduce the damage by half. The save DC is Constitutionbased. The
        flagstone golem can store the energy before releasing it as an energy
        blast for a number of rounds equal to half its Hit Dice (four rounds for
        the standard flagstone golem). </p><p><b>Flatten (Ex)</b> As a standard
        action, once per round, a flagstone golem can flatten its form to become
        (or appear to become) a section of floor, road, or any other stone
        surface. While flattened it cannot move or attack and its damage
        reduction increases to 20/adamantine. A flagstone golem can reform into
        its humanoid shape as a standard action. </p><p><b>Immunity to Magic
        (Ex)</b> A flagstone golem is immune to any spell or spell-like ability
        that allows spell resistance. In addition, certain spells and effects
        function differently against the creature, as noted below. A
        <i>transmute rock to mud</i> spell <i>slow</i>s it (as the <i>slow</i>
        spell) for 2d6 rounds, with no saving throw. A <i>transmute mud to
        rock</i> spell heals all its lost hit points. A <i>stone to flesh</i>
        spell does not actually change the golem's structure, but it negates the
        flagstone golem's damage reduction for 1 round. </p><p><b>Stunning Blow
        (Ex)</b> Any creature hit by a flagstone golem's slam attack must
        succeed on a DC 16 Fortitude save or be stunned for 1 round. The save DC
        is Constitution-based and includes a +2 racial bonus.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A flagstone
        golem is composed of large flat stones and bricks jointed and fitted
        together so as to allow the creature to fold itself flat. It is created
        to serve as guardians or servants by powerful spellcasters and has been
        the death of many an unsuspecting rogue.</p><p>A flagstone golem stands
        10 feet tall and weighs 1,200 pounds. Its humanoid form is composed of
        flattened stones and bricks. Two darkened sockets on its head function
        as eyes. Its arms are thick and powerful and end in stony clenched
        fists.</p><p>Though a flagstone golem cannot speak, when constructed,
        its creator can program up to 4 simple words or phrases (no more than 4
        words in length) into it that are released when certain conditions are
        met. These conditions are programmed into the golem when it is
        constructed. Conditions can be as general or specific as necessary, but
        must be fairly simple, such as "if anyone enters this room" or "if
        anyone touches this chest".</p><p>A flagstone golem tasked with guarding
        or securing a location usually flattens itself so as to go unnoticed to
        would-be-trespassers. When an interloper treads upon the golem, it
        unfurls itself into its humanoid form and attacks, crushing and pounding
        its unsuspecting victim with powerful blows from its fists.<br
        /><b>Construction</b><br /> A flagstone golem is constructed from brick,
        stone, various powders, and exotic liquids totaling 2,500 gp.<br
        /><b>FLAGSTONE GOLEM <br />CL </b>11th; <b>Price </b>30,000 gp
        </p><p><b>Requirements </b>Craft Construct, <i>bull's strength</i>,
        <i>geas/ quest</i>, <i>magic mouth</i>, <i>polymorph any object</i>,
        <i>protection from energy</i>, creator must be caster level 11th;
        </p><p><b>Skill </b>Craft (sculptures) or Craft (stonemasonry) DC 16;
        </p><p><b>Cost </b>17,500 Copyright Notice Author Scott
        Greene.</p></h4></div></section>
    _id: e9khy5GTiL7JcDZ0
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!vPCRVn7VdSfYifA1.e9khy5GTiL7JcDZ0'
sort: 0
_key: '!journal!vPCRVn7VdSfYifA1'

