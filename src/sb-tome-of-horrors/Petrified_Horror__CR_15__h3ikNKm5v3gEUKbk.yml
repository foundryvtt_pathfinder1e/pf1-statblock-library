name: Petrified Horror (CR 15)
_id: h3ikNKm5v3gEUKbk
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/construct.png
    title:
      show: false
      level: 1
    _id: UTIjQOhV5k2qPqxB
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!h3ikNKm5v3gEUKbk.UTIjQOhV5k2qPqxB'
  - name: Petrified Horror (CR 15)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors 4</p><section
        id="PetrifiedHorror"><div class="heading"><p
        class="alignright"><strong>CR</strong> 15</p></div><div><p><b>XP
        </b>51,200</p><p>N Medium construct </p><p><b>Init </b>+8; <b>Senses
        </b>blindsight 60 ft.; Perception +0</p><p><b>Aura </b>frightful
        presence (60 ft., DC 20)</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>24, touch 14,
        flat-footed 20 (+4 Dex, +10 natural)</p><p><b>hp </b>140
        (20d10+30)</p><p><b>Fort </b>+6, <b>Ref </b>+10, <b>Will
        </b>+6</p><p><b>DR </b>5/adamantine; <b>Immune </b>construct traits (+30
        hp), magic</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>30 ft.</p><p><b>Melee </b>2 slams +25
        (2d6+5)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b><i>bloodstorm</i></p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>21, <b>Dex
        </b>19, <b>Con </b>-, <b>Int </b> -, <b>Wis </b>11, <b>Cha
        </b>10</p><p><b>Base Atk </b>+20; <b>CMB </b>+25; <b>CMD
        </b>39</p><p><b>Feats </b>Improved InitiativeB</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        any</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Bloodstorm (Su)</b> A petrified horror can create a
        <i>bloodstorm</i> effect (as the spell) centered upon itself as a free
        action once every 2 minutes. The <i>bloodstorm</i> forms a whirlwind of
        blood in a column 25 feet in diameter and 40 feet high. The effect has a
        duration of 12 rounds, requiring those within to make a DC 20 Reflex
        save to avoid being blinded while they remain within the whirlwind and
        for 2d6 rounds after leaving it and a DC 20 Will save to avoid becoming
        panicked if less than 8 HD or frightened if 8 HD or above for the
        duration of the effect. Furthermore, creatures fighting within the
        <i>bloodstorm</i> or ranged attacks passing through it take -4 penalty
        on attack rolls Finally, the blood is slightly acidic and deals 1d4
        points of acid damage per round. The petrified horror is immune to the
        effects of the <i>bloodstorm</i>, including the attack penalties, and
        the whirlwind remains centered on the petrified horror even if it moves.
        The save DCs are Constitution-based.  Full details of the
        <i>bloodstorm</i> spell can be found in <i>Relics &amp; Rituals</i> by
        Sword &amp; Sorcery Studios.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>The name of
        the petrified horror belies the true nature of this hideous construct.
        For while it is true that the construct is crafted from the petrified
        remains of a Large creature, it is not until the new Medium statue is
        returned to flesh from its prison of stone that the horror is unleashed.
        Until then it just resembles an ordinary statue in whatever shape it was
        sculpted to be and is incapable of action.  This can be quite
        disconcerting when a delicate ballerina statue suddenly becomes a
        lumbering pile of bleeding meat and bone. Creators of these creatures
        often use them like time bombs, inert until freed by the spells of an
        inquisitive intruder or as part of a trap activated by the intruders.
        They instinctively recognize and avoid harming their creator but have no
        compunctions about destroying anything and anyone else they see.  In
        statue form, a typical specimen stands 5 to 8 feet tall and, being
        composed of solid stone, weighs anywhere from 1,000 to 1,800 pounds.
        Despite being a typical statue, it does give off a moderate aura of
        transmutation. When the creator or someone else casts stone to flesh or
        break enchantment the construct transforms to flesh and weighs 300 to
        500 pounds depending on its height. As a construct it is does not
        require a Fortitude save to survive the transformation. Because the
        petrified horror is constructed from a larger creature, it gains the
        bonus hit points of a size Large construct rather than a Medium. Because
        it has no actual eyes, it sees through blindsight rather than with the
        traditional darkvision and low-light vision of a construct.  Despite its
        lumbering appearance, a petrified horror's newly crafted form is quite
        quick and agile. It moves with a discernable squishing noise and leaves
        a trail of blood and ichor wherever it goes.  Petrified horrors are
        incapable of speech and do not understand or heed commands.  Petrified
        horrors are typically left in out-of-the-way places by their creators,
        so when activated the damages they cause can be minimized. Once released
        from its stony imprisonment petrified horrors immediately go into a
        rampage destroying all they see, preferring living creatures over
        inanimate objects but content to demolish even inanimate objects if that
        is all there is. As part of their programming, they rush directly into
        the center of whatever group is nearest to them (bull rushing if
        necessary) while flailing with their fists. Once they are in amongst
        their foes, they use their <i>bloodstorm</i> ability as they continue to
        fight.  <br /><b>Construction</b><br />  A petrified horror is carved
        from the petrified form of a Large creature which usually costs 10,000
        gp to procure. Special masterwork tools for this process worth 500 gp
        are also required and are ruined in the creation process. Sculpting the
        body from the base material requires a DC 25 Craft (sculptures) check.
        Failure means that the petrified creature's form has been cracked and
        ruined and a new Large petrified creature must be procured.  <br
        /><b>PETRIFIED HORROR</b><br />  <b>CL</b> 14th; <b>Price</b> 110,000
        gp  </p><p><b>CONSTRUCTION </b> </p><p><b>Requirements </b>Craft
        Construct, <i>animate dead</i>, <i>bloodstorm</i>, <i>flesh to
        stone</i>, <i>geas/quest</i>, <i>limited wish</i>, caster must be at
        least 14th level; <b>Skill</b> Craft (sculptures) DC 25; <b>Cost</b>
        65,500 gp.  Credit  Original author Greg A. Vaughan  Originally
        appearing in <i>Slumbering Tsar</i> (© Frog God Games/ Greg A. Vaughan,
        2012)</p></h4></div></section>
    _id: 1BMGFgWTBXgKwvnS
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!h3ikNKm5v3gEUKbk.1BMGFgWTBXgKwvnS'
sort: 0
_key: '!journal!h3ikNKm5v3gEUKbk'

