name: Tsathar (CR 2)
permission:
  default: 0
  IPeKQhMz2Odi77tR: 3
flags:
  core:
    sourceId: Compendium.world.tome-of-horrors.rtgoOfYKuc49kI2o
content: >-
  <p><strong>Source </strong>Tome of Horrors
  Complete</p><div><h2>Tsathar</h2><h3><i>This vile creature resembles an
  upright, humanoid frog with gray flesh and reddish-gold eyes. Its humanoid
  arms end in wicked claws.</i></h3><br /></div><p><button
  class="importStatblock">Import Tsathar with SBC</button></p><section
  id="Tsathar"><div class="heading"><p class="alignleft">Tsathar</p><p
  class="alignright">CR 2</p></div><div><p><b>XP </b>600</p><p>CE Medium
  monstrous humanoid (aquatic)</p><p><b>Init </b>+6; <b>Senses </b>darkvision 90
  ft., scent; Perception +9</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
  /><div><p><b>AC </b>18, touch 12, flat-footed 16 (+2 armor, +2 Dex, +4
  natural)</p><p><b>hp </b>13 (2d10+2)</p><p><b>Fort </b>+1, <b>Ref </b>+5,
  <b>Will </b>+4</p><p><b>Defensive Abilities </b>slimy</p><p><b>Weaknesses
  </b>light blindness</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
  /><div><p><b>Spd </b>30 ft., swim 30 ft.</p><p><b>Melee </b>shortspear +3
  (1d6+1) or kukri +3 (1d4+1/18-20) or 2 claws +3 (1d6+1), and bite +3
  (1d4+1)</p><p><b>Ranged </b>shortspear +4 (1d6+1) or net +4 touch
  (entangled)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
  Attacks </b>leap, summon hydrodaemon</p></div><hr
  /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>13, <b>Dex
  </b>14, <b>Con </b>12, <b>Int </b> 12, <b>Wis </b>12, <b>Cha
  </b>10</p><p><b>Base Atk </b>+2; <b>CMB </b>+3; <b>CMD </b>15 (27 vs.
  grapple)</p><p><b>Feats </b>Skill Focus (Perception) B, Improved
  Initiative</p><p><b>Skills </b>Acrobatics +3 (+17 long jumping, +27 high
  jumping), Climb +5, Escape Artist +15, Perception +9, Stealth +6, Swim +14;
  <b>Racial Modifiers </b>+12 Escape Artist, +14 Acrobatics when long jumping or
  +24 Acrobatics when high jumping</p><p><b>Languages </b>Abyssal,
  Tsathar</p><p><b>SQ </b>amphibious, implant</p><p><b>Combat Gear </b>leather
  armor, shortspear, kukri, net</p></div><hr
  /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
  underground and temperate marshes</p><p><b>Organization </b>solitary, gang
  (2-5), or pack (6-40 tsathar, with 1-4 tsathar scourges accompanied by 3-12
  killer monstrous frogs, led by a tsathar cleric or oracle of 3rd-6th
  level)</p><p><b>Treasure </b>standard</p></div><hr /><div><p><b>SPECIAL
  ABILITIES</b></p></div><hr /><div><p><b>Implant (Ex)</b> Tsathar are sexless,
  reproducing by injecting eggs into living hosts. An egg can be implanted only
  into a helpless host creature. The host must be of Small size or larger. Giant
  frogs, bred for this very purpose, are the most common host. Implanting an egg
  requires one minute to perform. Accompanying the egg is an anaesthetizing
  poison that causes the host to fall unconscious for the two-week gestation
  period of the egg unless the host succeeds on a DC 20 Fortitude saving throw;
  this save DC includes a +8 racial bonus. If the save succeeds, the host
  remains conscious, but is violently ill (-10 penalty on attack rolls, saving
  throws, ability checks, and skill checks) 24 hours before the eggs hatch. When
  the eggs mature, the young tsathar emerges from the host, killing it in the
  process. A <i>remove disease</i> spell rids the victim any implanted eggs. A
  DC 20 Heal check can be attempted to surgically extract an egg from a host. If
  the check fails, the healer can try again, but each attempt (successful or
  not) deals 1d6 points of damage to the patient. </p><p><b>Leap (Ex)</b>
  Tsathar are incredible jumpers, able to leap up to 30 feet horizontally or 10
  feet vertically. They have a +14 racial bonus on horizontal jumps, or +24 on
  vertical jumps, and they do not need to make a 10-foot minimum running start
  before jumping to avoid doubling the jumping DCs. Tsathar can always take 10
  when making an Acrobatics check to jump. When a tsathar begins its round by
  jumping next to an opponent it can make a full attack in the same round. A
  tsathar wearing medium or heavy armor or carrying a medium or heavy load
  cannot use this ability. </p><p><b>Summon Hydrodaemon (Sp)</b> A tsathar with
  at least five levels of cleric can, once per day, attempt to summon a
  hydrodaemon (q.v.) with a 40% chance of success. This ability is the
  equivalent of a 4th-level spell. </p><p><b>Slimy (Ex)</b> Because tsathar
  continuously cover themselves with muck and slime, they are difficult to
  grapple. Webs, magic or otherwise, do not affect tsathar, and they usually can
  wriggle free from most other forms of confinement. This grants them a +12
  racial bonus to their CMD to escape grapples, and to their Escape Artist
  checks.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
  /><div><h4><p>Tsathar (pronounced "suh-Thar") are a race of froglike humanoids
  that dwell in fetid swamps and dark caverns deep beneath the earth. They
  worship a strange and terrifying demonic being known as Tsathogga, and often
  hunt down and capture individuals of other humanoid races to serve as
  sacrifices or victims in their foul breeding pits.</p><p>Tsathar prefer to use
  short, barbed spears and kukri-like daggers in combat. They sometimes employ
  nets as well. They charge into combat with maniacal fury, and rarely use
  elaborate tactics, unless a scourge or priest is present to control them. They
  favor leather armor crafted from the hides of the frogs they breed. Priests
  favor the wicked kukri in battle.</p><p>Scourges prefer to loose their servant
  frogs on opponents, allowing common tsathar soldiers to engage opponents
  hand-to-hand. This is not to say that they are not capable fighters, for they
  certainly are. They favor barbed shortspears, twisted kukri-like daggers, and
  light armor such as leather or studded leather. They also often carry nets to
  snare their charges or foes. If their frogs are in danger, they leap in with
  their spears and attack.</p><p>A typical tsathar stands 6 feet tall and weighs
  about 300 pounds. A tsathar scourge is slightly bulkier, of equivalent height
  but weighing about 350 pounds.<b></b></p><p><b>Tsathar Society</b></p><p>
  Tsathar prefer to live in fetid lairs deep underground or in dark
  swamps.</p><p>When they dwell above ground, they are nocturnal. Some few
  surfacedwelling tsathar have joined cults of assassins.</p><p>Tsathar
  communities are led by priests-common tsathar with levels of cleric or oracle.
  They revere the demon-god Tsathogga, a foul toadlike demonic being that grants
  them access to the Chaos, Destruction, Evil, and Water domains, and the
  Catastrophe and Demon subdomains.</p><p>Tsathar are sexless and reproduce by
  implanting an egg into a host, which can be any form of living creature.
  Normally, creatures are captured or bred to serve as hosts-dire rats and giant
  frogs being common hosts.</p><p>It is said that priests must be born of an egg
  implanted into a humanoid or other creature of great intelligence.<br
  /><b>Tsathar Characters</b><br /> Tsathar favor barbarian, ranger, rogue, and
  fighter classes. Those tsathar destined for priesthood become clerics or
  oracles.<br /><b>+2 Strength,+4 Dexterity, +2 Constitution, +2 Intelligence,
  +2 Wisdom</b>.<br /><b>Darkvision:</b> Tsathar can see in the dark up to 90
  feet.<br /><b>Scent</b>.<br /><b>Racial Hit Dice:</b> A tsathar begins with
  two levels of monstrous humanoid, which provide 2d10 Hit Dice, a base attack
  bonus of +2, and base saving throw bonuses of Fort +0, Ref +3, and Will +3.<br
  /><b>Racial Skills:</b> A tsathar's monstrous humanoid levels give it skill
  points equal to 2 x (4 + Int modifier). Tsathar gain a +12 racial bonus on
  Escape Artist checks from their slimy ability, a +14 racial bonus on Acrobatic
  checks when making long jumps, and a +24 racial bonus on Acrobatic checks when
  making high jumps.<br /><b>Racial Feats:</b> A tsathar's monstrous humanoid
  levels give it one feat. In addition, all tsathar gain Skill Focus
  (Perception) as a bonus feat.<br /><b>AC:</b> +4 natural armor bonus.<br
  /><b>Weaknesses:</b> Light blindness.<br /><b>Natural Weapons:</b> Tsathar can
  fight with 2 claws that deal 1d6 base damage and a bite that deals 1d4 base
  damage.<br /><b>Weapon Proficiency:</b> Tsathar are automatically proficient
  with the kukri, net, and shortspear.<br /><b>Special Attacks:</b> Leap, summon
  hydrodaemon (see above).<br /><b>Special Qualities:</b> Amphibious, implant,
  slimy (see above).<br /><b>Languages:</b> All tsathar begin play speaking
  Abyssal and Tsathar. Bonus languages for tsathar, are Aklo, Common, Draconic,
  Gnome, Terran, and Undercommon.</p></h4></div></section>
img: systems/pf1/icons/races/creature-types/monstrous-humanoid.png
_id: qnRRm5jGX2bQYNM2
pages: []
_key: '!journal!qnRRm5jGX2bQYNM2'

