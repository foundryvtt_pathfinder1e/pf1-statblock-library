name: Grimm (CR 13)
_id: h49gb9F7WokXgvL8
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/fey.png
    title:
      show: false
      level: 1
    _id: NLBTPjaoXXdWwiXF
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!h49gb9F7WokXgvL8.NLBTPjaoXXdWwiXF'
  - name: Grimm (CR 13)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="Grimm"><div class="heading"><p
        class="alignright"><strong>CR</strong> 13</p></div><div><p><b>XP
        </b>25,600</p><p>NE Large fey </p><p><b>Init </b>+8; <b>Senses
        </b>low-light vision; Perception +27</p><p><b>Aura </b>aura of evil (5
        ft., 2d6 profane)</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
        /><div><p><b>AC </b>27, touch 13, flat-footed 23 (+4 Dex, +14 natural,
        -1 size)</p><p><b>hp </b>165 (22d6+88); fast healing 5</p><p><b>Fort
        </b>+13, <b>Ref </b>+17, <b>Will </b>+17</p><p><b>Defensive Abilities
        </b>blur, fey spell resistance; <b>DR </b>15/silver and good; <b>SR
        </b>24 (28 vs. fey magic)</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30
        ft.</p><p><b>Melee </b>bite +19 (2d8+8), 2 claws +19
        (1d8+8)</p><p><b>Space </b>10 ft.; <b>Reach </b>10 ft.</p><p><b>Special
        Attacks </b>devour</p><p><b>Spell-Like Abilities</b> (CL 22nd)<br
        />3/day—<i>call lightning</i> (DC 18), <i>darkness</i>, <i>detect
        fey</i> (functions as <i>detect animals or plants</i>, but against fey
        creatures)</p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>27, <b>Dex </b>19, <b>Con </b>19, <b>Int </b> 12,
        <b>Wis </b>14, <b>Cha </b>20</p><p><b>Base Atk </b>+11; <b>CMB </b>+20
        (+24 grapple); <b>CMD </b>34</p><p><b>Feats </b>Blind-Fight, Combat
        Reflexes, Diehard, Endurance, Great Fortitude, Improved Feint, Improved
        Initiative, Iron Will, Power Attack, Weapon Focus (bite,
        claw)</p><p><b>Skills </b>Acrobatics +18, Bluff +30, Climb +22,
        Diplomacy +17, Escape Artist +18, Heal +11, Intimidate +27, Perception
        +27, Sense Motive +20, Stealth +25</p><p><b>Languages </b>Common, Sylvan
        (can't speak)</p><p><b>SQ </b>ethereal jaunt</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        temperate mountains</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>standard</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Aura of Evil (Su)</b> A grimm
        constantly exudes an aura of evil around its form. This aura deals 2d6
        points of profane damage to all creatures in any space adjacent to the
        grimm. Evil creatures do not take this damage.  </p><p><b>Blur (Su)</b>
        As a move action, a grimm can blur its form (as the spell of the same
        name). This grants it concealment (20% miss chance).  </p><p><b>Devour
        (Su)</b> If a grimm pins a grappled creature, it deals 1d6 points of
        Strength drain each round the pin is maintained. Each time the grimm
        drains Strength, it gains a +1 bonus to its Strength score. These bonus
        points are temporary and the grimm loses them at the rate of 1 per
        hour.  </p><p><b>Ethereal Jaunt (Su)</b> A grimm can shift from the
        Ethereal Plane to the Material Plane as part of any move action and
        shift back again as a free action. It can remain on the Ethereal Plane
        for 3 rounds before returning to the Material Plane. The ability
        otherwise resembles the spell of the same name (caster level 20th). 
        </p><p><b>Fey Spell Resistance (Ex)</b> Against fey magic (spells or
        spell-like abilities used by any fey creature including elves) a grimm's
        spell resistance is 28.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A grimm is
        perhaps one of the most evil of all fey. Conceived and created by
        members of the Unseelie Court, the monster's only purpose seems to be
        the destruction of life and that which is beautiful. Legends among the
        fairy folk say powerful members of the Unseelie Court (specific legends
        say a circle of powerful quicklings sorcerers are the grimm's creators)
        created this fell beast to avenge the death of their own who were slain
        in battle when the Unseelie Court and Seelie Court did
        battle.</p><p>Grimms dwell in the deepest and darkest mountains making
        their lairs in deep recesses or hard to access caves and caverns,
        venturing forth only to kill and to eat. Grimms prefer a diet of meat,
        particularly good-aligned fey creatures, but they are not particularly
        picky and will gladly eat anything (or anyone) that crosses their path.
        Slain prey that is not immediately eaten is stored in the grimm's lair
        for later consumption.</p><p>These creatures are highly territorial and
        do not associate with other creatures, not even their own kind.</p><p>A
        grimm stands 9 feet tall and weighs in excess of 1,200 pounds. Stout and
        powerfully built, its body teems with muscle and is always covered in a
        glossy sheen.</p><p>A grimm opens combat slashing with its claws and
        gnashing with its terrible bite. If it can grab a foe, it does so,
        pinning it and draining its strength in order to increase its own. If
        fairing poorly, a grimm employs its spell-like abilities and blur
        special ability. Against overwhelming odds, a grimm shifts to the
        Ethereal Plane to avoid its foes.</p><p>Copyright Notice Author Scott
        Greene.</p></h4></div></section>
    _id: pG1yaIYSR3synB3g
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!h49gb9F7WokXgvL8.pG1yaIYSR3synB3g'
sort: 0
_key: '!journal!h49gb9F7WokXgvL8'

