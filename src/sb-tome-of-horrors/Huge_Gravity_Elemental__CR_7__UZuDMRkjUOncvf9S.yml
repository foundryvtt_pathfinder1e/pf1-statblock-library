name: Huge Gravity Elemental (CR 7)
_id: UZuDMRkjUOncvf9S
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: HThVQH7wZfhQvu1j
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!UZuDMRkjUOncvf9S.HThVQH7wZfhQvu1j'
  - name: Huge Gravity Elemental (CR 7)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="HugeGravityElemental"><div class="heading"><p
        class="alignright"><strong>CR</strong> 7</p></div><div><p><b>XP
        </b>3,200</p><p>N Huge outsider (elemental, extraplanar,
        gravity)</p><p><b>Init </b>+12; <b>Senses </b>darkvision 60 ft.;
        Perception +13</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
        /><div><p><b>AC </b>19, touch 19, flat-footed 10 (+2 deflection, +8 Dex,
        +1 dodge, -2 size)</p><p><b>hp </b>105 (10d10+50)</p><p><b>Fort </b>+8,
        <b>Ref </b>+15, <b>Will </b>+7</p><p><b>Defensive Abilities
        </b>distortion; <b>DR </b>5/-; <b>Immune </b>elemental traits, missile
        weapons, mass-dependent spells</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>fly 100 ft.
        (perfect)</p><p><b>Melee </b>2 slams +16 (1d8+5)</p><p><b>Ranged
        </b>missile +16 (damage by type)</p><p><b>Space </b>15 ft.; <b>Reach
        </b>15 ft.</p><p><b>Special Attacks </b>engulf, gravity field (crush
        5/day, 30 ft. radius, DC 20, 4d6 bludgeoning)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>20, <b>Dex
        </b>27, <b>Con </b>20, <b>Int </b> 6, <b>Wis </b>11, <b>Cha
        </b>11</p><p><b>Base Atk </b>+10; <b>CMB </b>+17; <b>CMD
        </b>38</p><p><b>Feats </b>Dodge, Flyby Attack, Improved Initiative,
        Mobility, Throw AnythingB, Weapon Finesse</p><p><b>Skills </b>Fly +25,
        Perception +13, Sense Motive +13, Stealth +13</p><p><b>SQ </b>perfect
        flier Languages Gravity Manipulation</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        Plane of Gravity</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Distortion (Su)</b> The manipulation of light waves around
        a gravity elemental grants it a +2 deflection bonus to its Armor Class.
        </p><p><b>Engulf (Ex)</b> A gravity elemental that starts its turn
        grappling a creature or holding and object up to one size smaller than
        itself can absorb the creature or object into its swirling form as a
        full-round action. An engulfed creature or object takes damage equal to
        the elemental's slam attack each round it remains engulfed and can take
        no action other than trying to break free by rolling its CMB vs. the
        gravity elementals CMD. The gravity elemental gains the grappled
        condition as long as it has a creature engulfed, and it can only engulf
        one creature at a time. A creature that dies while engulfed by a gravity
        elemental is smashed into atoms and can only be restored to life by a
        <i>miracle</i>, <i>wish</i>, or the intervention of a deity. An object
        that is destroyed while it is engulfed by a gravity elemental is
        destroyed forever and cannot be restored. There is a 1% non-cumulative
        chance each round that an engulfed creature or object is sent to a
        random plane. </p><p><b>Gravity Field (Su)</b> A gravity elemental's
        manipulation of gravity function like the <i>reverse gravity</i> and
        <i>telekinesis</i> spells (CL equal to HD). The maximum weight an Elder
        gravity elemental can lift with its <i>telekinesis</i> is not restricted
        to 375 lbs. The effects of the gravity field are centered on the gravity
        elemental and move with it. Additionally, by increasing the pull of
        gravity around itself a gravity elemental can make creatures within a
        certain radius too heavy to move, or crush them to a pulp. <i>Hold</i>:
        As a full-round action a gravity elemental can cause all corporeal
        creatures in its area of effect to become too heavy to move. Creatures
        in the gravity elemental's gravity field must succeed on a Fortitude
        save or gain the held condition until the gravity elemental releases
        them or they break free. A flying creature has a -4 penalty applied to
        the save; failure means it is unable to remain airborne and must land.
        The check DC is Strength-based. The gravity elemental can maintain this
        ability each round as a fullround action. The area of effect and save DC
        depend on the size of the elemental. If the gravity elemental maintains
        this ability, on successive rounds a held creature can break free by
        succeeding on a Strength check with a DC equal to the initial Fortitude
        save. Any land-bound creature two size categories smaller less than the
        elemental has its speed automatically reduced by half, even if the
        Strength check is successful. This ability only works on creatures with
        weight and mass; incorporeal and gaseous creatures are immune to this
        power. <i>Crush</i>: Once per day per 2HD, as a standard action, a
        gravity elemental can increase the gravity around itself to crushing
        proportions. Anything within the gravity elemental's gravity field takes
        bludgeoning damage. A successful Fortitude save reduces the damage by
        half. The area of effect and save DC depend on the elemental's size. The
        save DC is Strength-based. </p><p><b>Immunity to Missile Weapons
        (Ex)</b> A gravity elemental is immune to any attack from thrown or
        hurled weapons, arrows, bolts, stones, and similar weapons with a size
        equal to or less than its own. Any such ranged attack against a gravity
        elemental automatically fails and the weapon is pulled into the
        elemental's body. Captured missiles orbit the gravity elemental's form
        as long as it desires, and it can release one such missile each round as
        an improvised thrown weapon. The weapon deals its normal damage plus the
        gravity elemental's Strength modifier, and has a range increment of 20
        ft. A gravity elemental has the Throw Anything feat as a bonus feat.
        </p><p><b>Immunity to Spells (Ex)</b> A gravity elemental is immune to
        all spells, spell-like abilities, and effects that depend on weight or
        mass. </p><p><b>Perfect Flier (Ex)</b> A gravity elemental has perfect
        control over the forces of gravity, and never needs to make a Fly check
        when attempting a complex maneuver. It must still make Fly checks to
        resist external influences on its flight, such as strong
        winds.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>A gravity elemental embodies the very force that controls
        and holds together the fabric of the universe. The actual elemental is a
        tiny, superdense ball of matter deep within a zone of darkness and
        spiraling arms of debris that comprise the body of the elemental. A
        gravity elemental has mass and size, but no effective
        weight.</p><p>Gravity elementals originate on a weird, recently
        discovered plane that scholars affectionately call the Plane of Gravity.
        The elemental's home plane is a flat, rocky place with little life, no
        known water sources, and no plant life or foliage. The plane is always
        shrouded in darkness. Dense pockets of intense gravity and areas of
        little or no gravity dot the barren landscape. Gravity elementals seem
        to be able to converse with others of their kind by manipulating gravity
        itself.</p><p>Gravity elementals attack primarily by flailing at their
        opponents with multiple arms, each one a mixture of fluctuating fields
        of micro-gravity and debris that has been sucked into its gravity well.
        These creatures can also attack by adjusting the local gravity fields
        around themselves.</p><p>Copyright Notice Author Erica
        Balsley.</p></h4></div></section>
    _id: zhsNj348emN4muMC
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!UZuDMRkjUOncvf9S.zhsNj348emN4muMC'
sort: 0
_key: '!journal!UZuDMRkjUOncvf9S'

