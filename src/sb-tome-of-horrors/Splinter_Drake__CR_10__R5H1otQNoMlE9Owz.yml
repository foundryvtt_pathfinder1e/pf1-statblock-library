name: Splinter Drake (CR 10)
_id: R5H1otQNoMlE9Owz
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/plant.png
    title:
      show: false
      level: 1
    _id: jnbNgFhEpALqmIQE
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!R5H1otQNoMlE9Owz.jnbNgFhEpALqmIQE'
  - name: Splinter Drake (CR 10)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="SplinterDrake"><div class="heading"><p
        class="alignright"><strong>CR</strong> 10</p></div><div><p><b>XP
        </b>9,600</p><p>NE Large plant </p><p><b>Init </b>+2; <b>Senses
        </b>low-light vision; Perception +21</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>23, touch 11,
        flat-footed 21 (+2 Dex, +12 natural, -1 size)</p><p><b>hp </b>178
        (17d8+102)</p><p><b>Fort </b>+16, <b>Ref </b>+7, <b>Will
        </b>+8</p><p><b>Immune </b>plant traits; <b>Resist </b>fire
        10</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd
        </b>40 ft.</p><p><b>Melee </b>bite +20 (2d6+8/19-20), 2 claws +19
        (1d6+8)</p><p><b>Ranged </b>thorn volley +13 (3d6)</p><p><b>Space </b>10
        ft.; <b>Reach </b>5 ft. (10 ft. with bite)</p><p><b>Special Attacks
        </b>breath weapon (40 ft. cone, DC 24, 6d6 slashing and piercing),
        poison</p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>27, <b>Dex </b>15, <b>Con </b>23, <b>Int </b> 10,
        <b>Wis </b>12, <b>Cha </b>10</p><p><b>Base Atk </b>+12; <b>CMB </b>+21;
        <b>CMD </b>33 (37 vs. trip)</p><p><b>Feats </b>Cleave, Combat Reflexes,
        Critical Focus, Great Cleave, Improved Critical (bite), Improved Natural
        Attack (bite), Iron Will, Power Attack, Weapon Focus
        (bite)</p><p><b>Skills </b>Perception +21, Stealth +17 (+29 in forested
        areas); <b>Racial Modifiers </b>+12 Stealth in forested
        areas</p><p><b>Languages </b>Common, Sylvan</p><p><b>SQ </b>improved
        woodland stride</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> temperate forests</p><p><b>Organization
        </b>solitary or pair</p><p><b>Treasure </b>half standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Breath
        Weapon (Ex)</b> A splinter drake has two breath weapons, both of which
        emit razor-sharp thorns. The main breath weapon, useable every 1d4
        rounds, is a 40-ft. cone that deals 6d6 points of piercing and slashing
        damage. Its other breath weapon is a volley of thorns spit at a single
        target as a ranged attack for 3d6 points of piercing damage.
        </p><p><b>Improved Woodland Stride (Su)</b> A splinter drake can move
        through any sort of undergrowth (natural thorns, briars, overgrown
        areas, and similar terrain) at its normal speed without taking damage or
        suffering other impairment. This includes thorns, briars, and overgrown
        areas that are magically manipulated to impede motion. </p><p><b>Poison
        (Ex)</b> Three times per day as a free action, a splinter drake can
        regurgitate its poisonous stomach acids into its mouth and either use it
        in conjunction with a bite attack or release it along with either
        version of its breath weapon. The act of regurgitating the poison is a
        free action. Once it makes a successful bite attack or uses its breath
        weapon, the poison is used up (though the splinter drake can "reload"
        again on its next turn if it has any more uses of its poison attack left
        for the day). <i>Splinter Drake Venom</i>: Bite or breath weapon-injury
        or contact; save Fort DC 24; frequency 1/round for 5 rounds; effect 1d4
        Con; cure 1 save.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Splinter
        drakes are plant creatures that resemble large wingless dragons. They
        haunt dense forests and make their lairs amid the tangled underbrush,
        generally in hard-to-locate places. Much like true dragons, splinter
        drakes are extremely territorial and attack creatures that wander
        carelessly into their domain and remain for an extended amount of
        time.</p><p>A typical splinter drake's territory covers an area of
        several square miles around its lair. Unlike true dragons, splinter
        drakes do not value nor keep treasure. Any treasure found in or around
        their lairs are likely all that remains of a past meal. They generally
        keep company with evil fey, evil druids, and corrupt rangers. Such
        allies are allowed a bit of leeway when treading on a splinter drake's
        territory, but if certain boundaries are crossed or privileges abused,
        splinter drakes have no problems eating someone they once called
        friend.</p><p>Splinter drakes generally do not associate with others of
        their kind, except during mating season (early spring) when a mated pair
        is likely to be encountered. During mating, the female splinter drake
        deposits a host of eggs (or seeds) into the ground. Within several
        months, 1d4 new splinter drakes grow from the ground. Splinter drakes
        reach maturity within 1 year. A splinter drake is about 12 feet long and
        weighs around 800 pounds.</p><p>Splinter drakes typically ambush
        potential prey whenever possible, striking from surprise with their
        thorny breath weapon and poison. Once it releases its first blast, it
        charges into battle and slashes and tears at its foes with its claws and
        bite. If outnumbered or combat turns against it, a splinter drake seeks
        the quickest means of exit possible unless cornered, defending its lair,
        or starving, in which case it fights to the death.</p><p>Rarely, though
        not completely unheard of, a splinter drake takes levels in a class,
        almost always the druid or ranger class.</p></h4></div></section>
    _id: Py1GR3JSkEAIQlQ6
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!R5H1otQNoMlE9Owz.Py1GR3JSkEAIQlQ6'
sort: 0
_key: '!journal!R5H1otQNoMlE9Owz'

