name: Soul Reaper (CR 15)
_id: 0JwogW2gyAYGnwKT
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/undead.png
    title:
      show: false
      level: 1
    _id: 06mQb6dpulI64Z3e
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!0JwogW2gyAYGnwKT.06mQb6dpulI64Z3e'
  - name: Soul Reaper (CR 15)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="SoulReaper"><div class="heading"><p
        class="alignright"><strong>CR</strong> 15</p></div><div><p><b>XP
        </b>51,200</p><p>NE Large undead </p><p><b>Init </b>+8; <b>Senses
        </b>darkvision 60 ft., <i>see invisibility</i>; Perception
        +30</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC
        </b>29, touch 13, flat-footed 25 (+4 Dex, +16 natural, -1
        size)</p><p><b>hp </b>231 (22d8+132)</p><p><b>Fort </b>+14, <b>Ref
        </b>+13, <b>Will </b>+18</p><p><b>Defensive Abilities </b>channel
        resistance +4, inseparable weapon; <b>DR </b>15/silver and magic;
        <b>Immune </b>undead traits; <b>SR </b>26</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>40
        ft.</p><p><b>Melee </b><i><i>+1 scythe</i></i> +25/+20/+15/+10
        (2d6+13/19-20)</p><p><b>Space </b>10 ft.; <b>Reach </b>10
        ft.</p><p><b>Special Attacks </b>soul slash</p><p><b>Spell-Like
        Abilities</b> (CL 22nd)<br />Constant—<i>see invisibility</i><br />At
        Will—<i>desecrate</i>, <i>unholy blight</i> (DC 19)<br
        />1/day—<i><i>blindness</i>/<i>deafness</i></i> (DC 17), <i>symbol of
        pain</i> (DC 20), <i>unholy aura</i> (DC 23)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>27, <b>Dex
        </b>19, <b>Con </b>-, <b>Int </b> 18, <b>Wis </b>21, <b>Cha
        </b>20</p><p><b>Base Atk </b>+16; <b>CMB </b>+25; <b>CMD
        </b>39</p><p><b>Feats </b>Cleave, Critical Focus, Great Cleave, Great
        Fortitude, Improved Critical (scythe), Improved Initiative, Lightning
        Reflexes, Power Attack, Toughness, Vital Strike, Weapon Focus
        (scythe)</p><p><b>Skills </b>Climb +33, Diplomacy +14, Intimidate +30,
        Knowledge (arcana) +29, Knowledge (religion) +29, Perception +30, Sense
        Motive +30, Stealth +25, Survival +18</p><p><b>SQ </b>inseparable
        weapon</p><p><strong>Combat Gear</strong><i>+1 scythe</i></p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b>
        any</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Soul Slash (Su)</b> If a soul reaper scores a critical hit
        with its scythe, the target must succeed on a DC 26 Fortitude save or
        have its soul torn from its body and pulled into the soul reaper's
        scythe. If the save succeeds, the victim takes an extra 3d6 points of
        damage. A soul reaper's scythe can hold a number of souls equal to its
        Charisma bonus (four souls for a typical soul reaper). This effect is
        similar to a <i>trap the soul</i> spell. The save DC is Charisma-based. 
        A creature's soulless body collapses into a desiccated husk, and in one
        day crumbles to dust.  To reclaim a captured soul, the reaper must be
        destroyed and its scythe shattered on consecrated ground. When the
        scythe is shattered, all souls trapped are released and seek their
        original body (if it's been less than one day since the body was
        killed). Creatures without a body are left to wander in spirit form but
        can be returned to life through the successful casting of a miracle,
        wish, or true resurrection spell.  </p><p><b>Inseparable Weapon (Su)</b>
        A soul reaper has a mystic and magical connection to its scythe. If
        separated from its scythe (if the soul reaper is disarmed, for example)
        and within 100 feet of it, it may summon it to its hands as a move
        action. If an opponent is holding the weapon when the reaper summons it,
        that opponent must succeed on a DC 26 Strength check or the weapon flies
        from its hands to the soul reaper. The check DC is
        Charisma-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Soul reapers
        are vile undead with a fierce hatred for the living. They exist only to
        kill, and take great pleasure in slaying any living creature they
        encounter. They kill without guilt, without feeling or remorse, and
        without emotion; drinking the soul of their victim to insure it never
        returns to life.</p><p>Soul reapers have no ties to the land of the
        living, in that they have always existed and have always been. Their
        origins are unknown, but speculation says they stepped from the great
        void at the beginning of creation. It is thought that only six or seven
        of these creatures exist (and most living beings are thankful of
        that).</p><p>Soul reapers are 12-foot tall humanoid creatures shrouded
        in long, black hooded cloaks. Though they are corporeal creatures, their
        form is either shrouded in utter darkness or formed of the stuff of
        shadows.</p><p>No discernible facial features are visible under the
        hood. The creature's hands end in wicked claws of inky blackness that
        are always clutched tightly around its gleaming, sharp scythe. These
        creatures never speak and no one knows if they even can (and if they
        could, what would one say to a soul reaper?) Soul reapers haunt
        civilized lands searching for the next victim who will fall prey to the
        deadly scythe and its soul-draining powers. When it encounters a living
        creature, the soul reaper casts <i>unholy aura</i> on itself.</p><p>It
        then pinpoints an enemy spellcaster and uses its <i>deafness</i>
        ability, or selects the most powerful foe (its best guess) and hits it
        with <i>blindness</i>.</p><p>Invisible opponents are no match for the
        soul reaper and it often targets them first, just to show them that they
        cannot hide from its deadly blade.</p><p>When it closes to melee, the
        soul reaper uses a full attack with its scythe against its closest
        opponent.</p><p>Copyright Notice Author Scott
        Greene.</p></h4></div></section>
    _id: eafqgeZxgukBpgrM
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!0JwogW2gyAYGnwKT.eafqgeZxgukBpgrM'
sort: 0
_key: '!journal!0JwogW2gyAYGnwKT'

