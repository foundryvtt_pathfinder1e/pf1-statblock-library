name: Greater Acid Quasi-elemental (CR 7)
_id: 1bqXYLLTls6rGwnk
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: OxzBy4b8HyAEjafW
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!1bqXYLLTls6rGwnk.OxzBy4b8HyAEjafW'
  - name: Greater Acid Quasi-elemental (CR 7)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="GreaterAcidQuasielemental"><div class="heading"><p
        class="alignright"><strong>CR</strong> 7</p></div><div><p><b>XP
        </b>3,200</p><p>N Huge outsider (acid, elemental,
        extraplanar)</p><p><b>Init </b>+9; <b>Senses </b>darkvision 60 ft.;
        Perception +13</p><p><b>Aura </b>fumes (5 ft., Fort DC 19)</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>21, touch 13,
        flat-footed 16 (+5 Dex, +8 natural, -2 size)</p><p><b>hp </b>95
        (10d10+40)</p><p><b>Fort </b>+7, <b>Ref </b>+12, <b>Will
        </b>+9</p><p><b>DR </b>10/-; <b>Immune </b>acid, elemental
        traits</p><p><b>Weaknesses </b>vulnerability to water</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>20 ft., swim
        80 ft.</p><p><b>Melee </b>2 slams +14 (1d8+6 plus 1d8
        acid)</p><p><b>Space </b>15 ft.; <b>Reach </b>15 ft.</p><p><b>Special
        Attacks </b>acid, drench</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>23, <b>Dex
        </b>20, <b>Con </b>19, <b>Int </b> 6, <b>Wis </b>11, <b>Cha
        </b>11</p><p><b>Base Atk </b>+10; <b>CMB </b>+18; <b>CMD
        </b>33</p><p><b>Feats </b>Cleave, Great Cleave, Improved Initiative,
        Iron Will, Power Attack</p><p><b>Skills </b>Knowledge (planes) +11,
        Perception +13, Stealth +10, Swim +27</p><p><b>Languages
        </b>Aquan</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> Quasi-Plane of Acid</p><p><b>Organization
        </b>solitary</p><p><b>Treasure </b>none</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Acid
        (Ex)</b> Acid quasi-elementals are living creatures of acid; any melee
        hit deals acid damage. Creatures hitting an acid quasi-elemental unarmed
        or with natural attacks take acid damage as though hit by the
        quasi-elemental's slam attack. </p><p><b>Acid Swimmer (Ex)</b> Acid
        quasi-elementals cannot swim in water. The swim speed given in the
        statistics block is their movement only through acid pools or the acidic
        nature of their environment on their home plane. </p><p><b>Drench
        (Ex)</b> The quasi-elemental's touch puts out torches, campfires,
        exposed lanterns, and other open flames of nonmagical origin if these
        are of Large size or smaller. The creature can <i>dispel magic</i>al
        fire as <i>dispel magic</i> (caster level equals the quasi-elemental's
        HD). </p><p><b>Fumes (Ex)</b> The fumes from an acid quasi-elemental's
        body act as an inhaled poison. Creatures within 5 ft. of an acid
        quasi-elemental must make succeed on a Fortitude save or take 1 point of
        Constitution damage each round. This poison does not have a frequency; a
        creature is safe as soon as it moves more than 5 ft. away from the acid
        quasi-elemental. </p><p><b>Vulnerability to Water (Ex)</b> An acid
        elemental erupts in a violent chemical reaction when touched by water.
        An acid quasi-elemental takes 1d4 points of fire damage for each gallon
        of water poured into it. The reaction is so violent that all creatures
        within 5 ft. of the acid quasi-elemental when the water hits it must
        succeed on a DC 15 Reflex save or take the same damage. Pouring water
        onto an acid quasi-elemental requires a successful ranged touch attack.
        Water from other sources, such as spells or effects that create water
        but do not specify an amount in gallons, deal 1d4 points of fire damage
        per caster level to an acid quasi-elemental within its area of effect.
        Damage from water-based attacks, like the slam attack of a water
        elemental, is increased by 50% against an acid quasi-elemental. Note
        that this reaction does not occur when the acid quasi-elemental touches
        water, only when water touches the acid quasi-elemental.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Nestled among
        the various elemental there lies a plane composed entirely of acid. It
        is a place of noxious fumes and roiling, bubbling, pools. Plane jumpers
        do well to avoid this place as it is considered by many to be one of the
        deadliest, if not the deadliest, of the elemental-based
        planes.</p><p>Acid quasi-elementals rarely journey from their native
        plane, except when summoned. They do not like the Material Plane and
        when called to the place, are usually angered and always a bit
        uncomfortable. They have no trouble moving on land, but prefer the
        sanctity of their native plane to all others. At rest, an acid
        quasi-elemental is a clear puddle of liquid with a slightly green hue.
        It can rise up like a wave in a manner similar to a water elemental, or
        ooze along solid surfaces by sending out tendrils and pulling itself
        along. Wherever it travels, it leaves behind a shallow trough that emits
        wisps of smoke.</p><p>Acid quasi-elementals are almost always
        encountered in large pools of acid when on the Material Plane and rarely
        leave these pools when combating foes. A favored tactic is to grab a foe
        and pull it into the quasi-elemental's acid pool, subjecting it to
        massive amounts of acid damage (see the <i>Core Rulebook</i> for
        details).</p><p>Copyright Notice Author Scott
        Greene.</p></h4></div></section>
    _id: EWrHr0TI2KLyaUOV
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!1bqXYLLTls6rGwnk.EWrHr0TI2KLyaUOV'
sort: 0
_key: '!journal!1bqXYLLTls6rGwnk'

