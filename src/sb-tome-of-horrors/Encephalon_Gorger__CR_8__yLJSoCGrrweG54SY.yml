name: Encephalon Gorger (CR 8)
_id: yLJSoCGrrweG54SY
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/aberration.png
    title:
      show: false
      level: 1
    _id: 4WZ2oHyYO9m6rmvd
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!yLJSoCGrrweG54SY.4WZ2oHyYO9m6rmvd'
  - name: Encephalon Gorger (CR 8)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>Tome of Horrors Complete</p><section
        id="EncephalonGorger"><div class="heading"><p
        class="alignright"><strong>CR</strong> 8</p></div><div><p><b>XP
        </b>4,800</p><p>CE Medium aberration (extraplanar)</p><p><b>Init </b>+7;
        <b>Senses </b>darkvision 60 ft., mindsense; Perception +21</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>20, touch 14,
        flat-footed 16 (+3 Dex, +1 dodge, +6 natural)</p><p><b>hp </b>65
        (10d8+20); fast healing 5</p><p><b>Fort </b>+5, <b>Ref </b>+6, <b>Will
        </b>+9</p><p><b>Defensive Abilities </b>mind screen; <b>Resist </b>cold
        10</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd
        </b>30 ft.</p><p><b>Melee </b>2 claws +10 (1d6+1 plus
        grab)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>mindfeed</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>12, <b>Dex
        </b>16, <b>Con </b>14, <b>Int </b> 21, <b>Wis </b>15, <b>Cha
        </b>15</p><p><b>Base Atk </b>+7; <b>CMB </b>+8 (+12 grapple); <b>CMD
        </b>22</p><p><b>Feats </b>Dodge, Improved Initiative, Mobility, Skill
        Focus (Perception), Weapon Finesse</p><p><b>Skills </b>Bluff +15,
        Diplomacy +10, Escape Artist +11, Intimidate +15, Knowledge (local) +18,
        Knowledge (planes) +18, Knowledge (any one) +18, Perception +21, Sense
        Motive +15, Stealth +11, Survival +10; <b>Racial Modifiers </b>All
        Knowledge skills are class skills for encephalon
        gorgers</p><p><b>Languages </b>Common, Encephalon Gorger, plus  four
        additional languages</p><p><b>SQ </b>adrenal surge</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        land</p><p><b>Organization </b>solitary, crowd (2-5), or array
        (4-7)</p><p><b>Treasure </b>standard</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Adrenal Surge (Ex)</b> Twice per
        day, an encephalon gorger can create an effect on itself equivalent to a
        <i>haste</i> spell (caster level 10th).  </p><p><b>Mindfeed (Su)</b> If
        an encephalon gorger begins its turn grappling an opponent, it can
        attempt to suck its brain fluid with a successful grapple check. If it
        pins the foe, it drains cerebral fluid, dealing 1d4 points of
        Intelligence drain each round the pin is maintained. On each successful
        mindfeed attack, the encephalon gorger gains 5 temporary hit points. 
        This ability does not work against nonliving creatures or creatures
        without a central brain mass. The encephalon gorger must be able to
        reach the head or neck of the target creature to use this ability. 
        </p><p><b>Mindsense (Su)</b> As a move action, an encephalon gorger can
        gauge the relative intelligence level of any creature within 60 feet as
        follows: unintelligent, animal (Int 1-2), low (3-8), average (8-12),
        high (13-16), genius (17-20), or supra-genius (21+).  </p><p><b>Mind
        Screen (Ex)</b> The mind of an encephalon gorger is an alien and
        dangerous place. Should a creature attempt to scan the mind or read the
        thoughts of an encephalon gorger (with <i>detect thoughts</i>,
        telepathy, or the like), it must make a successful (DC 20) Will save or
        be driven permanently insane (as by an <i>insanity</i> spell, caster
        level 15th); if the save succeeds, the creature is instead
        <i>confused</i> (as the spell) for 1d4 rounds. The save DC is
        Intelligence-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Encephalon
        gorgers (sometimes known as cranial vampires) are malevolent creatures
        from another dimension or plane of existence. They are greatly feared by
        intelligent creatures for they use such beings (their brain fluid to be
        exact) to power their great cities. Many of these creatures have
        constructed strongholds or outposts on the Material Plane, though it is
        unknown when they first appeared.</p><p>Encephalon gorgers dislike for
        direct sunlight, though they are not harmed by it. When traveling
        aboveground in daylight hours, they usually cloak themselves in robes of
        gray or black. The gorger's leathery, whitish flesh is nearly
        translucent and in older encephalon gorgers one can faintly see veins
        and other organs pushing grayish-brown blood through its
        body.</p><p>This creature is completely hairless and its eyes are small
        with nictitating membranes. Its mouth is lined with short, needlelike
        teeth, with the canines being most pronounced (perhaps the reason these
        monsters are sometimes called cranial vampires).</p><p>Encephalon
        gorgers enter battle using their claws to slash and grapple their foes,
        going first for targets with the most delicious, intelligent brains.
        Once it grabs its prey it sinks its teeth into the foe's head, draining
        it of cerebral fluid. However, an encephalon gorger will not put itself
        in danger by ignoring other threats around it, so generally it only
        drinks when all of its other foes are either dead or engaged with others
        of its kind. Encephalon gorgers often make use of slave creatures and
        dumb beasts to engage an enemy's warriors while it sidles behind its
        chosen victim.</p><p>Often times, an encephalon gorger attempts to
        capture rather than kill its prey, especially in the case of intelligent
        humanoids. Captured prey is taken to one of their alien cities, where it
        is handed over to the Breeders who tend the slave pits.</p><p>Encephalon
        gorgers advance by character level; many have levels in wizard or
        psionic-oriented classes. At least one such gorger capable of
        interplanar travel is always present in any Prime Material Plane
        outpost.<b></b></p><p><b>Encephalon Gorger Society</b></p><p> Encephalon
        gorgers refer to themselves as <i>Silians</i> and they make their homes
        deep beneath the surface world or hidden far away from prying eyes
        (cloaked by natural occurrences such as fog or mist or hidden by magic).
        Underground lairs resemble great domed cities, while those on the
        surface resemble iron fortresses of exquisite craftsmanship. Each lair,
        regardless of its location, has dozens of slave pits and breeding pens
        filled with captured, intelligent humanoids (or other creatures). The
        slaves are maintained by a specialized group of <i><i>silian</i>s</i>
        called the Breeders. It is their job to tend to food supply of the city
        at all times and to gauge the relative worth of each and every humanoid
        used by the gorgers for feeding.</p><p>Encephalon gorgers sometimes
        trade with other races, usually trading for slaves, which are taken to
        the Breeders. Other slaves are kept by particular encephalon gorgers and
        assigned menial tasks. Once such a slave has exhausted its usefulness,
        it is "recycled" in the food pens so long as it isn't dead. Dead slaves
        are discarded, ground up into a bland paste that is fed to the other
        slaves.</p><p>Little is known of particulars regarding the gorger's
        society; such as reproduction, lifespan, aging patterns and so on. A few
        things that are known come from a group of adventurers that saw one of
        the iron fortresses and lived to tell about it. They spoke of large vats
        filled with cranial fluid maintained by the Breeders and of young
        <i>silian</i> being grown in these vats. They also spoke of the horrid
        squalor of the breeding pits and the slaves kept in
        them.</p><p>Copyright Notice Author Scott
        Greene.</p></h4></div></section>
    _id: bchdR7HtWOtUR5s1
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!yLJSoCGrrweG54SY.bchdR7HtWOtUR5s1'
sort: 0
_key: '!journal!yLJSoCGrrweG54SY'

