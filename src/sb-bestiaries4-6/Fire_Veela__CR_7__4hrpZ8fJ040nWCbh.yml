name: Fire Veela (CR 7)
_id: 4hrpZ8fJ040nWCbh
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: SO4CVGRFRi5iOnyC
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!4hrpZ8fJ040nWCbh.SO4CVGRFRi5iOnyC'
  - name: Fire Veela (CR 7)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 5</p><section
        id="FireVeela"><div class="heading"><p
        class="alignright"><strong>CR</strong> 7</p></div><div><p><b>XP
        </b>3,200</p><p>N Medium outsider (extraplanar, fire)</p><p><b>Init
        </b>+3; <b>Senses </b>darkvision 60 ft.; Perception +12</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>19, touch 13,
        flat-footed 16 (+3 Dex, +6 natural)</p><p><b>hp </b>85
        (9d10+36)</p><p><b>Fort </b>+10, <b>Ref </b>+9, <b>Will
        </b>+6</p><p><b>DR </b>10/magic; <b>Immune </b>fire; <b>SR
        </b>18</p><p><b>Weaknesses </b>vulnerable to cold</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>60
        ft.</p><p><b>Melee </b>mwk dagger +13/+8 (1d4+5/19-20 plus 2d6 fire),
        mwk dagger +13/+8 (1d4+5/19-20 plus 2d6 fire) or <br /> 2 slams +14
        (1d4+5 plus 2d6 fire)</p><p><b>Space </b>5 ft.; <b>Reach </b>5
        ft.</p><p><b>Special Attacks </b>beckoning dance, elemental
        veil</p><p><b>Spell-Like Abilities</b> (CL 9th; concentration +14) <br
        />At Will—<i>resist energy</i> (fire only), <i>scorching ray</i> <br
        />3/day—<i>cure serious wounds</i>, <i>suggestion</i> (DC 18) <br
        />1/day—<i>dispel magic</i>, <i>fire snake</i> (DC 20)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>20, <b>Dex
        </b>17, <b>Con </b>19, <b>Int </b> 14, <b>Wis </b>11, <b>Cha
        </b>20</p><p><b>Base Atk </b>+9; <b>CMB </b>+14; <b>CMD
        </b>27</p><p><b>Feats </b>Ability Focus (beckoning dance), Combat
        Reflexes, Double Slice, Improved Two-Weapon Fighting, Two- Weapon
        Fighting</p><p><b>Skills </b>Acrobatics +15, Bluff +17, Diplomacy +17,
        Knowledge (any one) +14, Perception +12, Perform (dance) +21, Sense
        Motive +12, Stealth +15; <b>Racial Modifiers </b>+4 Perform
        (dance)</p><p><b>Languages </b>Common, Ignan</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        warm land (Plane of Fire)</p><p><b>Organization </b>solitary, pair, or
        troupe (3-6)</p><p><b>Treasure </b>standard (2 mwk daggers)</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Beckoning
        Dance (Su)</b> As a standard action, a veela can compel a target that it
        can see to join it in dancing. The target must succeed at a DC 21 Will
        save or find herself forced to dance with the veela for up to 1 minute.
        At the end of each of the target's turns, she must attempt a Perform
        (dance) check opposed by the veela's Perform (dance) check. If the
        target doesn't meet or exceed the veela's result, she takes 1d4 points
        of Constitution damage and becomes fatigued. For every point of
        Constitution damage a veela deals in this way, it heals 5 hit points.
        Hit points healed in excess of its maximum become temporary hit points
        that last up to 1 hour before dissipating. While engaged in a beckoning
        dance, both a veela and its target are protected from being attacked as
        if by a <i>sanctuary</i> spell (DC 18). Any target of a veela's
        beckoning dance that exceeds the veela's result on the opposed Perform
        check ends the beckoning dance and gains the benefits of the veela's
        <i>cure serious wounds</i> spell-like ability (if any uses of that
        ability remain), which consumes one of the veela's daily uses. Targets
        that save against a veela's beckoning dance can't be affected by that
        veela's beckoning dance again for 24 hours. The save DC is
        Charisma-based. </p><p><b>Elemental Veil (Su)</b> A veela's link to a
        particular element manifests as an overflow of energy that infuses its
        natural attacks and any melee weapons it holds, causing it to deal an
        extra 1d6 points of damage on any successful melee attack. This is
        electricity damage for an air veela, bludgeoning damage for an earth
        veela, fire damage for a fire veela, and cold damage for a water veela.
        In addition, as a standard action a veela can wreathe itself in a
        luminescent halo of energy. This duplicates the effect of the spell
        <i>fire shield</i> (caster level 9th) but deals damage of the same
        damage type as the extra melee damage. Ending or resuming this effect is
        a standard action.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Capricious
        yet alluring, veelas are elemental spirits given shape. On the Elemental
        Planes, veelas revel in the unbridled energy of the elements from which
        they draw power. On the other planes, however, they lose some measure of
        the elemental energy saturating their forms. To compensate for this,
        they can siphon vitality from other living beings by engaging them in
        their ancient dances. Veelas typically do this with a creature's
        permission, leaving partners exhilarated but exhausted. Only in the most
        dire circumstances does a veela use its dance as a weapon. Those few who
        can match these elemental spirits' movements might earn a veela's
        respect and compel it to share some of its beneficial magical powers. A
        veela typically stands just under 6 feet tall and weighs approximately
        140 pounds.</p></h4></div></section>
    _id: 9KD6lzCG8GoODWKf
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!4hrpZ8fJ040nWCbh.9KD6lzCG8GoODWKf'
sort: 0
_key: '!journal!4hrpZ8fJ040nWCbh'

