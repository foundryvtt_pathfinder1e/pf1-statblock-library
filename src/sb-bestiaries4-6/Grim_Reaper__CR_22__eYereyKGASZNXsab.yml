name: Grim Reaper (CR 22)
_id: eYereyKGASZNXsab
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/undead.png
    title:
      show: false
      level: 1
    _id: 8qRfzG5kN7RDHOq5
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!eYereyKGASZNXsab.8qRfzG5kN7RDHOq5'
  - name: Grim Reaper (CR 22)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 5</p><section
        id="GrimReaper"><div class="heading"><p
        class="alignright"><strong>CR</strong> 22</p></div><div><p><b>XP
        </b>614,400</p><p>NE Medium undead (extraplanar)</p><p><b>Init </b>+14;
        <b>Senses </b>darkvision 60 ft., <i>see invisibility</i>, <i>status</i>
        sight, <i>true seeing</i>; Perception +6</p><p><b>Aura </b>misfortune
        (20 ft.), unnatural aura</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>39, touch 29,
        flat-footed 28 (+10 Dex, +1 dodge, +10 natural, +8 profane)</p><p><b>hp
        </b>400 (32d8+256)</p><p><b>Fort </b>+26, <b>Ref </b>+29, <b>Will
        </b>+32</p><p><b>Defensive Abilities </b>channel resistance +4, death's
        grace; <b>DR </b>10/-; <b>Immune </b>undead traits; <b>SR
        </b>33</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>60 ft., fly 90 ft. (perfect)</p><p><b>Melee
        </b><i><i>+5 keen adamantine scythe</i></i> +40/+40/+35/+30/+25
        (2d4+18/19-20/x4 plus death strike and energy drain)</p><p><b>Space
        </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special Attacks </b>death strike
        (DC 34), energy drain (2 levels, DC 34), final death</p><p><b>Spell-Like
        Abilities</b> (CL 20th; concentration +27) <br />Constant—<i>haste</i>,
        <i>see invisibility</i>, <i>true seeing</i> <br />At Will—<i>call
        spirit</i> (DC 23) <br />3/day—<i>finger of death</i> (DC 26), <i>power
        word kill</i> <br />1/day—<i>plane shift</i> (DC 23)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>29, <b>Dex
        </b>30, <b>Con </b>-, <b>Int </b> 15, <b>Wis </b>23, <b>Cha
        </b>26</p><p><b>Base Atk </b>+24; <b>CMB </b>+34; <b>CMD
        </b>62</p><p><b>Feats </b>Cleave, Cleaving Finish, Combat Casting,
        Combat Reflexes, Critical Focus, Dazing Assault, Furious Focus, Great
        Cleave, Improved Cleaving Finish, Improved Initiative, Power Attack,
        Staggering Critical, Step Up, Stunning Critical, Tiring Critical, Weapon
        Focus (scythe)</p><p><b>Skills </b>Acrobatics +42, Disguise +43, Fly
        +18, Intimidate +43, Knowledge (planes) +18, Knowledge (religion) +21,
        Ride +42, Stealth +45</p><p><b>Languages </b>Celestial, Common,
        Infernal</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization
        </b>solitary</p><p><b>Treasure </b>standard (<i>+5 keen adamantine
        scythe</i>, other treasure)</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Death Strike (Su)</b> A grim
        reaper automatically confirms any critical hit. A creature damaged by a
        critical hit from a grim reaper must succeed at a DC 34 Fortitude saving
        throw or be instantly killed. The save DC is Charisma-based.
        </p><p><b>Death's Grace (Su)</b> The dark power stolen from countless
        souls protects a grim reaper, granting it a profane bonus on all saving
        throws and to AC equal to its Charisma modifier. </p><p><b>Final Death
        (Su)</b> A creature killed by a grim reaper can't be brought back to
        life by any means short of divine intervention. </p><p><b>Misfortune
        Aura (Su)</b> When a living creature attempts an ability check, attack
        roll, caster level check, skill check, or saving throw within 20 feet of
        a grim reaper, it must roll two d20s and take the lowest roll before
        applying any modifiers. </p><p><b>Status Sight (Su)</b> When a grim
        reaper gazes on a creature, it can see that creature's emotion aura
        (<i>Pathfinder RPG Occult Adventures</i> 198) and that creature's
        current health and overall well-being. This acts as the <i>status</i>
        spell, as well as the emotion aura aspect of the <i>analyze aura</i>
        spell.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>As silent as the grave and as inevitable as time, grim
        reapers are more akin to forces of nature than individual beings, being
        nothing less than personifications of grim, violent death. Unlike their
        lesser kin, grim reapers never work together or with other creatures,
        save for select ancient dragons and dragon-like undead that sometimes
        serve these dreaded soul hunters as steeds. This lack of cooperation has
        led some to speculate that there is only one grim reaper who plagues the
        multiverse. Others claim that least nine of these creatures stalk the
        worlds and planes, culling the living as inexplicable servants of true
        entropy. According to the teaching of some death cults, the final goal
        of a grim reaper is to end the entire cycle of life and death and to
        serve as a silent lord of an empty universe. Grim reapers are so feared
        that even most outsiders give them a wide berth. This suits the reapers
        well, as that means less interference with the harvest of doomed souls.
        A grim reaper stands approximately 6-1/2 feet tall and weighs about 40
        pounds.</p></h4></div></section>
    _id: CX0I5U53sjt3vbwf
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!eYereyKGASZNXsab.CX0I5U53sjt3vbwf'
sort: 0
_key: '!journal!eYereyKGASZNXsab'

