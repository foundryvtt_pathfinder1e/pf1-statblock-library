name: Cambion (CR 2)
_id: oRw1wW5pGnVkNRit
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/outsider.png
    title:
      show: false
      level: 1
    _id: yfuIfSSLS5W7haiM
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!oRw1wW5pGnVkNRit.yfuIfSSLS5W7haiM'
  - name: Cambion (CR 2)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 5</p><section
        id="Cambion"><div class="heading"><p
        class="alignright"><strong>CR</strong> 2</p></div><div><p><b>XP
        </b>600</p><p>CE Medium outsider (chaotic, demon, evil,
        extraplanar)</p><p><b>Init </b>+1; <b>Senses </b>darkvision 60 ft.;
        Perception +7</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
        /><div><p><b>AC </b>15, touch 11, flat-footed 14 (+3 armor, +1 Dex, +1
        natural)</p><p><b>hp </b>22 (3d10+6)</p><p><b>Fort </b>+5, <b>Ref
        </b>+2, <b>Will </b>+4</p><p><b>Immune </b>electricity, poison;
        <b>Resist </b>acid 10, cold 10, fire 10; <b>SR </b>13</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30
        ft.</p><p><b>Melee </b>mwk scimitar +6 (1d6+2/18-20), claw +0
        (1d4+1/19-20) or <br /> 2 claws +5 (1d4+2/19-20)</p><p><b>Ranged </b>mwk
        composite longbow +5 (1d8+2/x3)</p><p><b>Space </b>5 ft.; <b>Reach </b>5
        ft.</p><p><b>Special Attacks </b>sadistic strike,
        sinfrenzy</p><p><b>Spell-Like Abilities</b> (CL 3rd; concentration +5)
        <br />3/day—<i>command</i> (DC 13) <br />1/day—<i>charm person</i> (DC
        13), <i>death knell</i> (DC 14), <i>enthrall</i> (DC 14)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>15, <b>Dex
        </b>13, <b>Con </b>14, <b>Int </b> 13, <b>Wis </b>12, <b>Cha
        </b>14</p><p><b>Base Atk </b>+3; <b>CMB </b>+5; <b>CMD
        </b>16</p><p><b>Feats </b>Deceitful, Power Attack</p><p><b>Skills
        </b>Acrobatics +7, Bluff +10, Disguise +10, Intimidate +8, Perception
        +7, Sense Motive +7, Stealth +7</p><p><b>Languages </b>Abyssal, Common;
        telepathy 30 ft.</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any (Abyss)</p><p><b>Organization
        </b>solitary, pair, gang (3-7), or cult (8-13)</p><p><b>Treasure </b>NPC
        gear (studded leather, mwk scimitar, mwk composite longbow [+2 Str] with
        20 arrows, other treasure)</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Sadistic Strike (Su)</b>
        Cambions excel at causing pain and anguish. Cambions always treat any
        weapons with which they attack (including natural weapons and spells
        with attack rolls) as if they had the Improved Critical feat for the
        weapons. </p><p><b>Sinfrenzy (Su)</b> Every cambion carries an excessive
        capacity for one of the seven classical sins, determined at the moment
        of the cambion's birth and depending on the nature of his humanoid
        parent's greatest sin. Once per day for a number of rounds equal to his
        Hit Dice, a cambion can embrace his sin and enter a frenzied state as a
        free action. While a cambion is in this frenzy, his land speed increases
        by 10 feet and he gains a +1 bonus on Reflex saves and attack rolls.
        These bonuses do not stack with those granted by <i>haste</i> or similar
        effects, but if the cambion has at least 1 level in the class listed for
        his specific sin, the sinfrenzy grants an additional effect. <i>Envy
        (cleric)</i>: Cambion clerics covet the gods' power. When they are in a
        sinfrenzy, their channel energy effect increases by 1d6. <i>Gluttony
        (alchemist)</i>: Cambion alchemists enjoy drinking extracts, potions,
        mutagens, and even poisons. When they are in a sinfrenzy, their bombs
        deal an additional die of damage. <i>Greed (rogue)</i>: Cambion rogues
        are obsessed with gathering wealth and power. When they are in a
        sinfrenzy, cambion rogues deal an additional die of sneak attack damage.
        <i>Lust (bard)</i>: Cambion bards seek to dominate and control others,
        turning them into slaves. When cambion bards are in a sinfrenzy, the
        bonuses granted by their inspire courage, inspire competence, inspire
        greatness, and inspire heroics abilities increase by 1. The save DCs of
        their bardic performances also increase by 1. <i>Pride (wizard)</i>:
        Cambion wizards believe their magic is the greatest. When they are in a
        sinfrenzy, their spell save DCs increase by 1. <i>Sloth (fighter)</i>:
        Cambion fighters bleed slowly. When entering a sinfrenzy, they gain 2
        temporary hit points per Hit Die that disappear when the sinfrenzy ends.
        <i>Wrath (ranger)</i>: Cambion rangers attack with savage hatred. When
        they are in a sinfrenzy, their favored enemy bonuses increase by
        2.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>When an incubus procreates with a humanoid in the Abyss,
        and the mortal parent resides on that plane throughout the pregnancy,
        the gestating child absorbs enough Abyssal energy to be born not as a
        half-demon, but as a type of humanoid demon known as a cambion. Cambions
        are true outsiders. The majority of cambions have red skin, horns on
        their heads, and cloven hooves for feet-the fiendish aspects of
        cambions' appearance don't range as widely as those of tief lings. A
        typical cambion stands 6 feet tall and weighs 190
        pounds.</p></h4></div></section>
    _id: xYEVmWWwOb3jjZw2
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!oRw1wW5pGnVkNRit.xYEVmWWwOb3jjZw2'
sort: 0
_key: '!journal!oRw1wW5pGnVkNRit'

