name: Flying Polyp (CR 14)
_id: 8KYjPdjKAzBVyNMB
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/aberration.png
    title:
      show: false
      level: 1
    _id: z4j1mgR6N8JODEIu
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!8KYjPdjKAzBVyNMB.z4j1mgR6N8JODEIu'
  - name: Flying Polyp (CR 14)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 4</p><section
        id="FlyingPolyp"><div class="heading"><p
        class="alignright"><strong>CR</strong> 14</p></div><div><p><b>XP
        </b>38,400</p><p>CE Huge aberration (air)</p><p><b>Init </b>+6;
        <b>Senses </b>darkvision 60 ft.; Perception +26</p><p><b>Aura
        </b>frightful presence (90 ft., DC 24)</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>29, touch 16,
        flat-footed 26 (+5 deflection, +2 Dex, +1 dodge, +13 natural, -2
        size)</p><p><b>hp </b>207 (18d8+126)</p><p><b>Fort </b>+13, <b>Ref
        </b>+12, <b>Will </b>+16</p><p><b>Defensive Abilities </b>amorphous,
        deflecting winds, partial invisibility; <b>DR </b>10/magic and slashing;
        <b>Immune </b>acid, cold, sonic; <b>SR </b>25</p><p><b>Weaknesses
        </b>vulnerable to electricity</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30 ft., fly
        60 ft. (perfect)</p><p><b>Melee </b>4 tentacles +21 (1d8+9/19-20 plus
        grab)</p><p><b>Space </b>15 ft.; <b>Reach </b>15 ft.</p><p><b>Special
        Attacks </b>constrict (1d8+9), sucking wind, wind
        blast</p><p><b>Spell-Like Abilities</b> (CL 14th; concentration +19) <br
        />At Will—<i>alter winds</i>, <i>gust of wind</i> (DC 17), <i>whispering
        wind</i>, <i>wind walk</i> <br />3/day—<i>control winds</i> (DC 20),
        <i>river of wind</i> (DC 19), <i>wind wall</i> <br />1/day—<i>control
        weather</i>, <i>whirlwind</i> (DC 23)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>28, <b>Dex
        </b>15, <b>Con </b>24, <b>Int </b> 19, <b>Wis </b>20, <b>Cha
        </b>21</p><p><b>Base Atk </b>+13; <b>CMB </b>+24 (+28 grapple); <b>CMD
        </b>42 (can't be tripped)</p><p><b>Feats </b>Combat Reflexes, Dodge,
        Flyby Attack, Improved Critical (tentacle), Improved Initiative,
        Mobility, Power Attack, Vital Strike, Weapon Focus
        (tentacle)</p><p><b>Skills </b>Fly +27, Knowledge (engineering) +22,
        Knowledge (history) +22, Knowledge (nature) +22, Perception +26,
        Spellcraft +25, Stealth +15, Use Magic Device +23</p><p><b>Languages
        </b>Aklo</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary,
        pair, or storm (3-10)</p><p><b>Treasure </b>standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Deflecting
        Winds (Su)</b> A flying polyp's mastery over air and wind allows it to
        surround itself with blasts of precisely aimed gusts, granting the
        creature a +5 deflection bonus to its Armor Class and a +4 resistance
        bonus on Reflex saving throws. </p><p><b>Partial Invisibility (Su)</b> A
        flying polyp's body constantly flickers and shifts, passing from
        visibility to invisibility in a seemingly random pattern and often not
        wholly at once, leaving the creature's body in what appear to be
        multiple sections. This ability, combined with the flying polyp's
        amorphous, elastic form, makes it difficult to target the creature,
        granting it a 20% miss chance against all attacks. By concentrating, a
        flying polyp can become fully invisible. </p><p><b>Sucking Wind (Su)</b>
        This attack allows the flying polyp to send an eerie wind out to
        <i>slow</i> and eventually stop a creature's escape. The wind itself
        isn't particularly strong, but it creates a peculiar sucking sensation
        as if it were attempting to pull creatures back toward the flying polyp.
        Activating this ability is a full-round action, and it must concentrate
        each round to maintain the effect. The sucking wind manifests as a
        100-foot-radius spread, with the flying polyp at the center. Each round
        the polyp maintains concentration, the sucking wind's radius increases
        by 100 feet, to a maximum radius of a mile. A flying polyp can detect
        creatures within this area via tremorsense. As a free action, it can
        increase the effects of the sucking wind on up to five different
        creatures within the area at one time. Each targeted creature must
        succeed at a DC 26 Fortitude save each round it remains in the area of
        the sucking wind or it is <i>slow</i>ed until it leaves the area. A
        creature already under the effects of any <i>slow</i>ing effect (such as
        from this sucking wind or a <i>slow</i> spell) that fails this save is
        held in place for 1 round-it is not helpless, but cannot move via any
        means. <i>Freedom of movement</i> protects against the effects of the
        sucking wind, and <i>control winds</i> negates its effects in the area
        of effect of the <i>control winds</i> spell. Natural windstorms or other
        powerful winds have no effect on a sucking wind. A flying polyp can
        activate a sucking wind once per day, and can maintain concentration on
        the effect for up to an hour. The save DC is Constitution-based.
        </p><p><b>Wind Blast (Su)</b> Once every 1d4 rounds as a standard
        action, a flying polyp can create a powerful blast of wind at a range of
        up to 120 feet. This blast of wind creates a sudden explosion of
        flesh-scouring wind in a 30-foot-radius burst. All creatures within this
        area take 14d6 points of bludgeoning damage, with a successful DC 26
        Reflex save halving the damage. In addition, these winds can check or
        blow away creatures as if they were tornado-strength winds
        (<i>Pathfinder RPG Core Rulebook</i> 439). The save DC is
        Constitution-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A flying
        polyp is a nauseating mass of flesh, eyes, tentacles, and mouths. A
        typical flying polyp measures 30 feet in length but is unusually light
        for its size, weighing no more than 2,000 pounds. These creatures seem
        to have no maximum lifespan, but their violent, warlike nature ensures
        that death eventually occurs-even if it takes eons for the polyp to
        encounter something capable of defeating it. A flying polyp is a
        physical being, but one composed of material strangely unlike the flesh
        that garbs most living creatures. While the stuff that makes up the
        exterior of a flying polyp's body might seem similar to ordinary flesh,
        it often behaves in ways that should be impossible. The material seems
        to fade in and out of visibility, almost at random, at points becoming
        transparent enough that the nauseating inner workings of the thing's
        body are laid bare. Although the polyp feels moist and damp to the
        touch, what might serve as blood in other creatures behaves more like
        strange vortices of wind within a flying polyp's body. When wounded, its
        damaged flesh does not bleed so much as whistle and gust. A flying
        polyp's association with wind goes far beyond the strange storms that
        surge through what pass as veins and arteries in its massive body. These
        creatures have a remarkable ability to control the air around them, both
        via a wide array of spell-like abilities and through the use of potent
        supernatural powers. They do not wield tools or weapons as a rule,
        instead using their mastery of the winds themselves to wage war and
        build their grim cities, scouring towers and chambers out of basalt with
        precise blasts of sand-laden wind. Although flying polyps display some
        of the features of other sentient races, particularly in their habit of
        building cities (although these towering settlements usually incorporate
        architectural features that most other races find awkward and
        unsettling), in other areas they seem strangely primitive or
        uninterested. They are as aberrant in mind and philosophy as they are in
        physical form. For example, they seem to have neither a name for their
        own race, nor a language to call their own. Their cities, while
        bewildering in their vast scale, seem to serve little purpose other than
        to unnerve, for flying polyps do not engage in trade or politics or
        other social constructs. The primary exception to this, to the detriment
        of other creatures unfortunate enough to dwell in regions claimed by
        flying polyps, is war. Flying polyps excel at genocide, using their
        mastery over wind to scour clean entire cities and civilizations when
        they come upon them. Some among their kind can even travel to other
        planets by bringing with them a sizable sphere of purloined wind to
        carry them aloft and sustain them, and with this power they lead armies
        from planet to planet as necessary, relentlessly tracking their chosen
        enemies across worlds. Every so often, flying polyps encounter a race
        that is their equal in war, and on some worlds, they still endure the
        humiliation of these ancient defeats after being imprisoned in extensive
        underground chambers where they are cut off from the outside world. Yet
        flying polyps are long-lived, and should unforeseen tectonic events
        creates new exit to their prison chambers, lost in the forgotten corners
        of those planets' depths, they emerge with unabated fury to seek revenge
        against the enemies who dared humiliate them
        so.</p></h4></div></section>
    _id: vUlQEsRggegrCkqs
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!8KYjPdjKAzBVyNMB.vUlQEsRggegrCkqs'
sort: 0
_key: '!journal!8KYjPdjKAzBVyNMB'

