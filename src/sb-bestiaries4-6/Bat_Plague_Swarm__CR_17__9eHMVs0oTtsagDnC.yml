name: Bat Plague Swarm (CR 17)
_id: 9eHMVs0oTtsagDnC
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/magical-beast.png
    title:
      show: false
      level: 1
    _id: qygNMYL0uts9vx7Q
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!9eHMVs0oTtsagDnC.qygNMYL0uts9vx7Q'
  - name: Bat Plague Swarm (CR 17)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 5</p><section
        id="BatPlagueSwarm"><div class="heading"><p
        class="alignright"><strong>CR</strong> 17/MR 8</p></div><div><p><b>XP
        </b>102,400</p><p>NE Diminutive magical beast (mythic,
        swarm)</p><p><b>Init </b>+19M/-1, dual initiative; <b>Senses
        </b>blindsense 100 ft., darkvision 60 ft., low-light vision; Perception
        +34</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC
        </b>32, touch 24, flat-footed 24 (+7 Dex, +1 dodge, +8 natural, +2
        profane, +4 size)</p><p><b>hp </b>279 (21d10+164); fast healing
        10</p><p><b>Fort </b>+18, <b>Ref </b>+21, <b>Will </b>+13; second
        save</p><p><b>Defensive Abilities </b>profane protection, swarm traits,
        unstoppableMA; <b>DR </b>10/epic; <b>Immune </b>weapon
        damage</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>5 ft., fly 40 ft. (good)</p><p><b>Melee </b>swarm
        (5d6 plus 5d6 blight, distraction, and soul bleed)</p><p><b>Space </b>20
        ft.; <b>Reach </b>0 ft.</p><p><b>Special Attacks </b>create spawn,
        distraction (DC 26), mythic power (8/day, surge +1d10), soul
        bleed</p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>6, <b>Dex </b>25, <b>Con </b>18, <b>Int </b> 11,
        <b>Wis </b>18, <b>Cha </b>9</p><p><b>Base Atk </b>+21; <b>CMB </b>-;
        <b>CMD </b>-</p><p><b>Feats </b>Ability Focus (distraction), Ability
        Focus (soul bleed), DodgeM, Extra Mythic PowerM, Great Fortitude,
        Improved InitiativeM, Iron Will, Lightning ReflexesM, Lightning Stance,
        Skill Focus (Fly), Skill Focus (Perception), Wind Stance</p><p><b>Skills
        </b>Fly +37, Perception +34, Stealth +32</p><p><b>Languages
        </b>Common</p><p><b>SQ </b>multiply (bat swarm with soul bleed [Fort DC
        11])</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary or
        brood (plague swarm plus 1-100 spawn)</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Blight (Su)</b> Plague swarms deal additional untyped
        damage equal to their standard swarm damage. This damage originates from
        divine power, similar to the spell <i>flame strike</i>. A plague swarm's
        damage counts as magic and epic for the purposes of bypassing damage
        reduction. </p><p><b>Multiply (Ex)</b> Plague swarms spawn at an
        incredible rate. Once per minute, a plague swarm can produce a new swarm
        of the type listed in its stat block as full-round action. When the
        plague swarm is not in combat, it typically uses this ability once per
        minute. If the plague swarm is destroyed, all of the swarms that it
        spawned also disperse. </p><p><b>Reform (Su)</b> When a plague swarm is
        dispersed by damage, it is not destroyed. A dispersed plague swarm
        reforms in 24 hours. Each plague swarm has a unique destruction
        condition. In addition, because plague swarms are manifestations of
        divine retribution, placating the deity's rage destroys the swarm
        instantly. </p><p><b>Create Spawn (Su)</b> A humanoid creature who dies
        from a bat plague swarm's soul bleed ability rises from death as a
        vampire in 1d4 days. This vampire is under the command of the swarm, and
        remains enslaved until the swarm is destroyed. The swarm can have
        enslaved spawn whose Hit Dice total no more than twice its own; any
        spawn it creates that would exceed this limit become free-willed undead.
        </p><p><b>Profane Protection (Su)</b> A bat plague swarm gains a +2
        profane bonus to its Armor Class from its divine creator.
        </p><p><b>Reform (Su)</b> A bat plague swarm is truly destroyed only if
        it is dispersed in the area of a <i>hallow</i> spell. </p><p><b>Soul
        Bleed (Su)</b> A creature that takes damage from a bat plague swarm
        begins to bleed from its body and its soul. Each round, a creature under
        the effects of soul bleed takes 1d4 points of Constitution bleed damage
        and gains 1 negative level. A creature affected by soul bleed can't be
        healed by magical healing from a non-mythic source. A mythic caster can
        attempt a DC 24 caster level check to heal a creature under the effects
        of soul bleed. If the caster succeeds, the soul bleed ends. Otherwise,
        any creature can end a soul bleed effect with a successful DC 35 Heal
        check. Twenty-four hours after a creature takes negative levels from
        soul bleed, it must attempt a separate DC 26 Fortitude save for each
        negative level. If it succeeds at a save, that negative level is
        removed. If it fails, that negative level becomes permanent. The save DC
        is Constitution-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Sent down by
        vengeful deities to punish communities that have offended them, plague
        swarms scour crops, terrorize populations, and lay waste to civilized
        lands. Normal means of dealing with vermin and pestilence don't drive
        them off, for these vicious and intelligent mythic swarms carry the
        destructive blessings of their gods, and breed at an exceptional rate
        until their spawn consume entire cities. They are single-minded in their
        devotion to carrying out the furious will of their
        deities.</p></h4></div></section>
    _id: kKQ7N64YFU1WDebX
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!9eHMVs0oTtsagDnC.kKQ7N64YFU1WDebX'
sort: 0
_key: '!journal!9eHMVs0oTtsagDnC'

