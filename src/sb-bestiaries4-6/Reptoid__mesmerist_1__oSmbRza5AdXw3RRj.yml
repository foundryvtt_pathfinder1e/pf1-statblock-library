name: Reptoid (mesmerist 1)
_id: oSmbRza5AdXw3RRj
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/humanoid.jpg
    title:
      show: false
      level: 1
    _id: MTdRCs8xSELEuXjF
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!oSmbRza5AdXw3RRj.MTdRCs8xSELEuXjF'
  - name: Reptoid (mesmerist 1)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 5</p><section
        id="Reptoid"><div class="heading"><p
        class="alignright"><strong>CR</strong> 1/2</p></div><div><p><b>XP
        </b>200</p><p>Reptoid mesmerist 1 (<i>Pathfinder RPG Occult
        Adventures</i> 38)</p><p>LE Medium humanoid (reptilian,
        shapechanger)</p><p><b>Init </b>+0; <b>Senses </b>low-light vision;
        Perception +5</p></div><hr /><div><p><b>DEFENSE</b></p></div><hr
        /><div><p><b>AC </b>11, touch 10, flat-footed 11 (+1
        natural)</p><p><b>hp </b>7 (1d8-1)</p><p><b>Fort </b>+1, <b>Ref </b>+2,
        <b>Will </b>+3; +2 vs. mind-affecting effects and poison</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30
        ft.</p><p><b>Melee </b>bite +1 (1d3+1), 2 claws +1
        (1d3+1)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>hypnotic stare, mesmerist tricks (4/day, psychosomatic
        surge), painful stare</p><p><b>Mesmerist Spells Known</b> (CL 1st;
        concentration +4) <br />1st (2/day)—<i>hypnotism</i> (DC 14),
        <i>innocence</i> <br />0 (at will)—<i>daze</i> (DC 13), <i>detect
        magic</i>, <i>message</i>, <i>prestidigitation</i></p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>12, <b>Dex
        </b>10, <b>Con </b>8, <b>Int </b> 14, <b>Wis </b>13, <b>Cha
        </b>17</p><p><b>Base Atk </b>+0; <b>CMB </b>+1; <b>CMD
        </b>11</p><p><b>Feats </b>Great Fortitude</p><p><b>Skills </b>Bluff +8,
        Diplomacy +7, Knowledge (local, nobility) +6, Linguistics +6, Perception
        +5, Sense Motive +5, Use Magic Device +7</p><p><b>Languages </b>Common,
        Elven, Reptoid</p><p><b>SQ </b>change shape, consummate liar, mental
        potency</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any land</p><p><b>Organization
        </b>solitary, pair, or cabal (3-8)</p><p><b>Treasure </b>NPC
        gear</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Change Shape (Su)</b> A reptoid can assume the appearance
        of a specific single </p><p><b>Medium</b> humanoid. The reptoid always
        takes this specific form when it uses this ability. The reptoid gains a
        +10 racial bonus on Disguise checks to appear as that type of humanoid.
        This ability otherwise functions as <i>alter self</i>, except the
        reptoid does not adjust its ability scores. A reptoid can select a new
        humanoid form by spending 1 week preparing itself for the change, but
        can then no longer assume its previous humanoid form. </p><p><b>Mental
        Potency (Ex)</b> A reptoid's mental effects can affect more powerful
        creatures or a greater number of creatures than normal. Both the Hit Die
        limit and the total number of Hit Dice affected by each enchantment or
        illusion spell it casts increase by 1. For enchantment and illusion
        spells it casts that target a number of creatures greater than one, the
        number of creatures affected also increases by one (so a spell that
        targets one creature per level would be affected, but a spell that
        targets only one creature would not be). This ability stacks with the
        mesmerist's ability of the same name.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Reptoids are
        bipedal reptiles from another planet, or perhaps even another plane-they
        refuse to reveal their origins to humanoids, even under duress.
        Disguised as members of others races, the reptoids seek positions of
        power in order to secretly prepare for an invasion from their homeworld.
        Even those who are aware of reptoids in their midst can never be truly
        sure who is real and who is an alien, as these creatures are often adept
        at enchantment and psychic magic, which they use to cover the tracks of
        their deceptions and to ensure the complacency or even collusion of
        their soon-to-be thralls. <br /><b>Reptoid Characters</b><br /> Reptoids
        are defined by their class levels-they don't have racial Hit Dice. They
        have the following racial traits. <br /><b>+2 Strength, +2 Charisma, -2
        Dexterity:</b> Reptoids are both manipulative and strong, but they're
        deliberate in their movements. <br /><b>Medium</b>: Reptoids are Medium
        creatures and have no bonuses or penalties due to their size. <br
        /><b>Normal Speed</b>: Reptoids have a base speed of 30 feet. <br
        /><b>Low-Light Vision (Ex)</b>: Reptoids can see twice as far as humans
        in dim light. <br /><b>Scales</b>: When in its natural form, a reptoid
        has a +1 natural armor bonus. <br /><b>Cold-Blooded (Ex)</b>: Reptoids
        receive a +2 racial saving throw bonus against mind-affecting effects
        and poison. <br /><b>Natural Weapons (Ex)</b>: When in its natural form,
        a reptoid has a bite attack and two claw attacks that deal 1d3 points of
        damage each. <br /><b>Change Shape (Su)</b>: See above. <br /><b>Mental
        Potency (Ex)</b>: See above. <br /><b>Languages</b>: All reptoids begin
        play speaking Common and Reptoid. Reptoids with high Intelligence scores
        can choose any languages they want (except for secret languages, such as
        Druidic).</p></h4></div></section>
    _id: KAMeHnicGSvSpKB4
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!oSmbRza5AdXw3RRj.KAMeHnicGSvSpKB4'
sort: 0
_key: '!journal!oSmbRza5AdXw3RRj'

