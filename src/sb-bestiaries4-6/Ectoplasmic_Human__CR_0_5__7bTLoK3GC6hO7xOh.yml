name: Ectoplasmic Human (CR 0.5)
_id: 7bTLoK3GC6hO7xOh
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/undead.png
    title:
      show: false
      level: 1
    _id: 0RgK30K4d3Pmcgbr
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!7bTLoK3GC6hO7xOh.0RgK30K4d3Pmcgbr'
  - name: Ectoplasmic Human (CR 0.5)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 4</p><section
        id="EctoplasmicHuman"><div class="heading"><p
        class="alignright"><strong>CR</strong> 1/2</p></div><div><p><b>XP
        </b>200</p><p>N Medium undead </p><p><b>Init </b>+0; <b>Senses
        </b>darkvision 60 ft.; Perception +0</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>12, touch 10,
        flat-footed 12 (+2 natural)</p><p><b>hp </b>7 (1d8+3)</p><p><b>Fort
        </b>+0, <b>Ref </b>+0, <b>Will </b>+2</p><p><b>DR </b>5/slashing;
        <b>Immune </b>undead traits</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>30 ft.;
        <i>air walk</i></p><p><b>Melee </b>slam +3 (1d4+3 plus horrifying
        ooze)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>horrifying ooze</p><p><b>Spell-Like Abilities</b> (CL 1st;
        concentration +1) <br />Constant—<i>air walk</i></p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>16, <b>Dex
        </b>11, <b>Con </b>-, <b>Int </b> -, <b>Wis </b>10, <b>Cha
        </b>12</p><p><b>Base Atk </b>+0; <b>CMB </b>+3; <b>CMD
        </b>13</p><p><b>Feats </b>ToughnessB</p><p><b>SQ </b>phase
        lurch</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary,
        pair, or haunt (3-6)</p><p><b>Treasure </b>none</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Horrifying
        Ooze (Su)</b> Any creature struck by an ectoplasmic creature's slam
        attack must succeed at a DC 11 Will save or be shaken for 1d4 rounds.
        The save DC is Charisma-based. </p><p><b>Phase Lurch (Su)</b> An
        ectoplasmic creature has the ability to pass through walls or material
        obstacles. To use this ability, the ectoplasmic creature must begin and
        end its turn outside of whatever wall or obstacle it's moving through.
        An ectoplasmic creature cannot move through corporeal creatures with
        this ability, and its movement speed is halved while moving through a
        wall or obstacle. Any surface it moves through is coated with a thin,
        silvery mucus that lingers for 1 minute.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Drawn from
        energies of the Ethereal Plane, ectoplasm is a vile substance resembling
        thick tangles of slimy linen or dripping goo. It shapes itself into the
        form of an undead creature, creating a host for a soul unfortunate
        enough to be confined within it. The existence of an ectoplasmic being
        is a cruel one, and few souls willingly choose this painful form of
        undeath. An ectoplasmic creature is approximately the same size as the
        body it inhabited in life, though it weighs nearly twice as much, as the
        ropes of undead matter that compose its body are signif icantly heavier
        than most living flesh. Even more so than most undead beings, creatures
        born of ectoplasm live hateful existences, filled with nothing but a
        lust for destruction and suffering. They have no bodily needs and
        require no sustenance; the only thing an ectoplasmic creature feeds upon
        is its own hatred of the living. Once a spirit has passed to the
        afterlife, it seldom wishes to return at all, let alone in a disfigured
        ectoplasmic body. Spirits that aren't powerful enough to come back as
        ghosts or spectres sometimes return as ectoplasmic monsters,
        particularly when there are no remains of the creature's original body
        for its soul to inhabit in the form of a skeleton or zombie. Sometimes,
        ghosts and other strong undead purposefully draw upon ectoplasm from the
        ethereal realm, yearning for even more power in their ectoplasmic hosts.
        Those who suffer this sorrowful fate, by misfortune or choice, are
        usually stuck in their ectoplasmic prisons until death grants them sweet
        release from this unlife. The transition from death to ectoplasmic
        undeath is a torturous ordeal, as is retaining the horrid form into
        which the creature is reborn. Often, this persistent agony drives these
        beings beyond mad, creating within an insatiable rage akin to that
        experienced by frustrated ghosts and other haunted souls. An ectoplasmic
        creature's burning desperation and embitterment often pushes it toward
        violence: most such beings fling themselves into battle willingly,
        killing to satiate their natural hunger for the suffering of others,
        while simultaneously hoping to be killed and thus freed of their own
        suffering own. Whenever in contact with surfaces (including walls they
        pass through), ectoplasmic creatures leave a trail of a silvery
        substance that resembles a slug's mucus-a trait almost exclusive to
        these undead. This slippery secretion dries within moments, so if its
        encountered, there is surely such a creature lurking nearby. Ectoplasmic
        beings can inhabit any location, regardless of environment or climate.
        The horrors tend to prowl the areas in which they died, and rarely
        venture outside these areas, as though they were anchored there. Though
        these entities rarely coordinate complicated actions with others of
        their kind, they seem to do so unintentionally at times. Their unnatural
        strength makes ectoplasmic creatures formidable combatants, which those
        not familiar with fighting ectoplasmic creatures would expect by looking
        at them. Fortunately for the wary, the sticky ectoplasm that trails
        behind these undead monsters is a clear indicator of their presence, and
        most experienced clerics can identify the substance at a glance. <br
        /><b>CREATING AN ECTOPLASMIC CREATURE</b><br /> "Ectoplasmic" is an
        acquired template that can be added to any corporeal creature (other
        than an undead), referred to hereafter as the base creature. <br
        /><b>Challenge Rating:</b> Same as the base creature +1. <br
        /><b>Alignment:</b> Usually chaotic evil. <br /><b>Type:</b> The
        creature's type changes to undead. It retains any subtype except for
        alignment subtypes (such as evil) and subtypes that indicate kind (such
        as giant). It does not gain the augmented subtype. It uses all the base
        creature's statistics and special abilities except as noted in the
        following sections. <br /><b>Armor Class:</b> The creature's natural
        armor bonus changes as follows: </p><table><tbody><tr><th>Ectoplasm
        Size</th><th>Natural Armor Bonus</th></tr><tr><td>Tiny or
        smaller</td><td>+0</td></tr><tr><td>Small</td><td>+1</td></tr><tr><td>Medium</td><td>+2</td></tr><tr><td>Large</td><td>+3</td></tr><tr><td>Huge</td><td>+4</td></tr><tr><td>Gargantuan</td><td>+6</td></tr><tr><td>Colossal</td><td>+8</td></tr></tbody></table>
        <br /><b>Hit Dice:</b> Drop HD gained from class levels (to a minimum of
        1 HD) and change racial Hit Dice to d8s. Ectoplasmic creatures use their
        Charisma modifiers to determine bonus hit points (instead of
        Constitution). <br /><b>Saves:</b> Base save bonuses for racial Hit Dice
        are Fort +1/3 HD, Ref +1/3 HD, and Will +1/2 HD + 2. <br /><b>Defensive
        Abilities:</b> An ectoplasmic creature loses the base creature's
        defensive abilities, and gains DR 5/ slashing as well as all of the
        standard immunities and traits possessed by undead creatures. <br
        /><b>Speed:</b> Winged ectoplasmic creatures can still fly, but their
        maneuverability drops to poor if it was initially any better. If the
        base creature flew magically, so can the ectoplasmic creature. Retain
        all other movement types. An ectoplasmic creature gains the ability to
        traverse the air (as the <i>air walk</i> spell) as a constant effect.
        <br /><b>Attacks:</b> An ectoplasmic creature retains all natural
        weapons of the base creature. It gains a slam attack that deals damage
        based on the ectoplasmic creature's size. <br /><b>Special Attacks:</b>
        An ectoplasmic creature retains all of the special attacks of the base
        creature. In addition, an ectoplasmic creature gains the following
        special attack. <br /><i>Horrifying Ooze (Su)</i>: Any creature struck
        by an ectoplasmic creature's slam attack must succeed at a Will save (DC
        = 10 + 1/2 the ectoplasmic creature's Hit Dice + the ectoplasmic
        creature's Charisma modifier) or be shaken for 1d4 rounds. <br
        /><b>Abilities:</b> An ectoplasmic creature receives a +2 bonus to
        Strength and a +2 bonus to Charisma. An ectoplasmic creature has no
        Constitution or Intelligence score, and its Wisdom score becomes 10. <br
        /><b>BAB:</b> An ectoplasmic creature's base attack bonus is equal to
        3/4 its Hit Dice. <br /><b>Feats:</b> An ectoplasmic creature loses all
        feats possessed by the base creature, and gains Toughness as a bonus
        feat. <br /><b>Special Abilities:</b> An ectoplasmic creature loses most
        special qualities of the base creature. It retains any extraordinary
        special qualities that improve its melee or ranged attacks. An
        ectoplasmic creature gains the following special ability: <br /><i>Phase
        Lurch (Su)</i>: An ectoplasmic creature has the ability to pass through
        walls or material obstacles. In order to use this ability, the
        ectoplasmic creature must begin and end its turn outside of whatever
        wall or obstacle it's moving through. An ectoplasmic creature cannot
        move through corporeal creatures with this ability, and its movement
        speed is halved while moving through a wall or obstacle. Any surface it
        moves through is coated with a thin, silvery mucus that lingers for 1
        minute.</h4></div></section>
    _id: zxKeOb5YYa0Zw9LM
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!7bTLoK3GC6hO7xOh.zxKeOb5YYa0Zw9LM'
sort: 0
_key: '!journal!7bTLoK3GC6hO7xOh'

