name: Graeae (CR 5)
_id: mhWPSqCXNdu8pD5L
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/monstrous-humanoid.png
    title:
      show: false
      level: 1
    _id: N6cWZdZavvkYsNDs
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!mhWPSqCXNdu8pD5L.N6cWZdZavvkYsNDs'
  - name: Graeae (CR 5)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 4</p><section id="Graeae"><div
        class="heading"><p class="alignright"><strong>CR</strong> 5/MR
        2</p></div><div><p><b>XP </b>1,600</p><p>NE Medium monstrous humanoid
        (evil, mythic)</p><p><b>Init </b>+3; <b>Senses </b>blindsense 30 ft.,
        darkvision 60 ft.; Perception +10</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>19, touch 13,
        flat-footed 16 (+3 Dex, +6 natural)</p><p><b>hp </b>65
        (6d10+32)</p><p><b>Fort </b>+3, <b>Ref </b>+8, <b>Will
        </b>+8</p><p><b>DR </b>5/epic; <b>SR </b>16</p><p><b>Weaknesses </b>eye
        of the graeae</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>30 ft., fly 60 ft. (good)</p><p><b>Melee </b>2 claw
        +8 (1d6+2)</p><p><b>Space </b>5 ft.; <b>Reach </b>5 ft.</p><p><b>Special
        Attacks </b>luck ripple, mythic power (2/day, surge
        +1d6)</p><p><b>Spell-Like Abilities</b> (CL 6th; concentration +12) <br
        />At Will—<i>arcane sight</i>, <i>fly</i>, <i>undetectable alignment</i>
        <br />5/day—<i>ill omen</i> (DC 17) <br />3/day—<i>augury</i>,
        <i>enthrall</i> (DC 18), <i>feast of ashes</i> (DC 18) <br
        />1/day—<i>ray of exhaustion</i> (DC 19)</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>14, <b>Dex
        </b>17, <b>Con </b>12, <b>Int </b> 23, <b>Wis </b>13, <b>Cha
        </b>8</p><p><b>Base Atk </b>+6; <b>CMB </b>+8; <b>CMD
        </b>21</p><p><b>Feats </b>Combat Casting, Iron WillM,
        Toughness</p><p><b>Skills </b>Bluff +5, Craft (alchemy) +15, Fly +15,
        Intimidate +8, Knowledge (arcana) +12, Perception +10, Spellcraft +12,
        Stealth +12, Survival +10, Use Magic Device +5</p><p><b>Languages
        </b>Aklo, Common, Giant, Goblin, Sylvan</p><p><b>SQ </b>coven, fate
        casting</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary, a
        pair, or coven (3-12)</p><p><b>Treasure </b>standard</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Coven
        (Ex)</b> Like hags, graeaes also form covens. A graeae coven can be
        composed entirely of graeaes, or could or include hags or witches with
        the coven hex. A hag or witch with the coven hex counts as a graeae for
        purposes of joining a graeae's coven. Likewise, a graeae counts as a hag
        for purposes of joining a hag's coven. </p><p><b>Eye of the Graeae
        (Su)</b> Each graeae possesses a mystic eyeball. A graeae can sense the
        location of her eyeball from anywhere on the same plane. She must remain
        within 30 feet of her eyeball or she becomes completely blind and cannot
        use any of her spell-like or supernatural abilities. The eyeball only
        works for its graeae. If a graeae is slain, her mystic eye instantly
        turns to dust. </p><p><b>Fate Casting (Su)</b> A graeae has the ability
        to predict future events. On a creature's request, a graeae can expend a
        use of mythic power as a full-round action to answer a single question
        as if by the <i>divination</i> spell. </p><p><b>Luck Ripple (Su)</b> A
        graeae can use her mystic eye to alter the circumstances of any creature
        within 30 feet. As a swift action, she can cast her eye on a single
        creature, causing the target to take a -2 penalty or gain a +2 bonus to
        one of the following (graeae's choice): AC, ability checks, attack
        rolls, saving throws, or skill checks. A successful DC 19 Will save
        negates the effect, which otherwise lasts for 1d6 rounds. This is a
        mind-affecting gaze effect. The DC of the save is
        Intelligence-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A graeae is a
        haglike creature, though more human in appearance. Some describe them as
        emissaries of the gods, while others believe graeaes to be physical
        manifestations of fate. <br /><b>GRAEAE COVEN </b><br /> Whenever three
        or more graeaes of the same coven are within 10 feet of one another,
        they can work together to use any of the following spell-like abilities:
        <i>clairaudience/clairvoyance</i>, <i>commune</i>, <i>contact other
        plane</i>, <i>speak with dead</i>, and <i>tongues</i>. All three graeaes
        must spend a full-round action to take part in this form of cooperative
        magic. All coven spell-like abilities are CL 9th (or at the highest
        caster level available to the most powerful graeaes in the
        coven).</p></h4></div></section>
    _id: N9jaX9gqmchvuOWU
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!mhWPSqCXNdu8pD5L.N9jaX9gqmchvuOWU'
sort: 0
_key: '!journal!mhWPSqCXNdu8pD5L'

