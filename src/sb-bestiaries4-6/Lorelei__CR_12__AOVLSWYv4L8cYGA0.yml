name: Lorelei (CR 12)
_id: AOVLSWYv4L8cYGA0
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/aberration.png
    title:
      show: false
      level: 1
    _id: SIUjZFR52YuFQ33B
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!AOVLSWYv4L8cYGA0.SIUjZFR52YuFQ33B'
  - name: Lorelei (CR 12)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 4</p><section
        id="Lorelei"><div class="heading"><p
        class="alignright"><strong>CR</strong> 12</p></div><div><p><b>XP
        </b>19,200</p><p>NE Large aberration (aquatic)</p><p><b>Init </b>+5;
        <b>Senses </b>darkvision 60 ft.; Perception +24</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>27, touch 10,
        flat-footed 26 (+1 Dex, +17 natural, -1 size)</p><p><b>hp </b>162
        (12d8+108)</p><p><b>Fort </b>+13, <b>Ref </b>+5, <b>Will
        </b>+13</p><p><b>Immune </b>sonic; <b>Resist </b>cold 10</p></div><hr
        /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd </b>20 ft., climb
        20 ft., swim 20 ft.</p><p><b>Melee </b>4 tentacles +18 (1d8+9 plus
        poison)</p><p><b>Space </b>10 ft.; <b>Reach </b>15 ft.</p><p><b>Special
        Attacks </b>murmur, poison, vortex</p><p><b>Spell-Like Abilities</b> (CL
        12th; concentration +17) <br />At Will—<i>ghost sound</i> (DC 15),
        <i>speak with dead</i> (DC 18), <i>ventriloquism</i> (DC 16),
        <i>whispering wind</i> <br />3/day—<i>command undead</i> (DC 17),
        <i>control water</i>, <i>fog cloud</i></p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>28, <b>Dex
        </b>13, <b>Con </b>29, <b>Int </b> 11, <b>Wis </b>16, <b>Cha
        </b>20</p><p><b>Base Atk </b>+9; <b>CMB </b>+19; <b>CMD </b>30 (can't be
        tripped)</p><p><b>Feats </b>Improved Initiative, Iron Will, Power
        Attack, Skill Focus (Perception), Skill Focus (Stealth), Weapon Focus
        (tentacle)</p><p><b>Skills </b>Bluff +15, Climb +21, Perception +24,
        Sense Motive +15, Stealth +18 (+26 in rocky areas), Swim +21; <b>Racial
        Modifiers </b>+8 Stealth in rocky areas</p><p><b>Languages </b>Aquan,
        Common</p><p><b>SQ </b>freeze, water dependency</p></div><hr
        /><div><p><b>ECOLOGY</b></p></div><hr /><div><p><b>Environment </b> any
        coastlines</p><p><b>Organization </b>solitary</p><p><b>Treasure
        </b>none</p></div><hr /><div><p><b>SPECIAL ABILITIES</b></p></div><hr
        /><div><p><b>Murmur (Su)</b> A lorelei's murmur has the power to infect
        the minds of those that hear it. This effect even influences undead
        creatures despite their usual immunity to mind-affecting effects. When a
        lorelei murmurs, all creatures aside from other lorelei within a
        300-foot spread must succeed on a DC 20 Will saving throw or become
        captivated. A creature that successfully saves is not subject to the
        same lorelei's song for 24 hours. A victim under the effects of the
        murmur moves toward the lorelei using the most direct means available.
        If the path leads them into a dangerous area such as through fire or off
        a cliff, that creature receives a second saving throw to end the effect
        before moving into peril. Affected creatures can take no actions other
        than to defend themselves. A victim within 5 feet of the lorelei simply
        stands and offers no resistance to its attacks. This effect continues
        for as long as the lorelei murmurs and for 1 round thereafter. This is a
        sonic mind-affecting charm effect. The save DC is Charisma-based.
        </p><p><b>Poison (Ex)</b> Tentacle-injury; <i>save</i> Fort DC 25;
        <i>frequency</i> 1/ round for 4 rounds; <i>effect</i> 1d4 Str;
        <i>cure</i> 2 consecutive <i>save</i>s. </p><p><b>Vortex (Su)</b> A
        lorelei can create a whirlpool as a standard action, at will. This
        ability functions identically to the whirlwind special attack
        (<i>Pathfinder RPG Bestiary</i> 306), but can form only underwater and
        cannot leave the water. A creature must succeed at a DC 25 Reflex save
        or be snared by the churning waters. The vortex is 20 feet across and 80
        feet deep, and deals 2d8+9 points of damage. The save DC is
        Constitution-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>A lorelei
        resembles an enormous, stony sea anemone with a humanlike face covering
        much of its body. Noted for their magical murmurs that entrance sailors,
        the creatures lurk near rocky shoals barely concealed by crashing waves
        or rushing rivers, eager to lure humanoids to their deaths. Also known
        as a "murmur stone" for its rocklike natural camouf lage, a lorelei is a
        solitary creature that shuns peaceful contact with other living things.
        It broods in the shadows of seaside cliffs and ocean trenches, emerging
        only to torment the living. When not pursuing complicated schemes, a
        lorelei is fond of wrecking ships on rocks and luring sailors beneath
        the surface to drown. Some scholars claim that these creatures were once
        a species of beautiful fey cursed by foul forces. This claim is backed
        by the fact that they behave much like nereids, nixies, and sirens. A
        lorelei stands 9 feet tall, not counting the mess of tentacles atop its
        body, and weighs around 2,000 pounds.</p></h4></div></section>
    _id: sxtAgq9NheANSWkj
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!AOVLSWYv4L8cYGA0.sxtAgq9NheANSWkj'
sort: 0
_key: '!journal!AOVLSWYv4L8cYGA0'

