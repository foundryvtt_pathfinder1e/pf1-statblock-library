name: Yarthoon (CR 25)
_id: 55zo1DbvThDhtff0
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/magical-beast.png
    title:
      show: false
      level: 1
    _id: btRnEt4tgY3kTlCG
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!55zo1DbvThDhtff0.btRnEt4tgY3kTlCG'
  - name: Yarthoon (CR 25)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 6</p><section
        id="Yarthoon"><div class="heading"><p
        class="alignright"><strong>CR</strong> 25</p></div><div><p><b>XP
        </b>1,638,400</p><p>CN Colossal magical beast (air, kaiju)</p><p><b>Init
        </b>+13; <b>Senses </b>low-light vision, mistsight, see in darkness,
        tremorsense 300 ft.; Perception +28</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>44, touch 12,
        flat-footed 34 (+9 Dex, +1 dodge, +32 natural, -8 size)</p><p><b>hp
        </b>565 (29d10+406); fast healing 30</p><p><b>Fort </b>+30, <b>Ref
        </b>+25, <b>Will </b>+19</p><p><b>Defensive Abilities </b>ferocity,
        recovery; <b>DR </b>20/epic; <b>Immune </b>ability damage, ability
        drain, cold, death effects, disease, energy drain, fear; <b>Resist
        </b>acid 30, electricity 30, fire 30, negative energy 30, sonic
        30</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr /><div><p><b>Spd
        </b>60 ft., burrow 100 ft., fly 100 ft. (average), swim 100
        ft.</p><p><b>Melee </b>2 bites +39 (4d6+27/19-20 plus 4d6 cold and
        grab), slam +39 (4d8+27/19-20 plus 4d6 cold and staggering
        strike)</p><p><b>Space </b>30 ft.; <b>Reach </b>30 ft.</p><p><b>Special
        Attacks </b>clinging frost, eye beams, fast swallow, freezing mist, hurl
        foe, penetrating cold, swallow whole (6d6 bludgeoning and 6d6 cold
        damage, AC 26, 56 hp), swift bite</p></div><hr
        /><div><p><b>STATISTICS</b></p></div><hr /><div><p><b>Str </b>46, <b>Dex
        </b>29, <b>Con </b>38, <b>Int </b> 3, <b>Wis </b>26, <b>Cha
        </b>21</p><p><b>Base Atk </b>+29; <b>CMB </b>+55 (+59 overrun); <b>CMD
        </b>75 (77 vs. overrun, can't be tripped)</p><p><b>Feats </b>Critical
        Focus, Dodge, Flyby Attack, Greater Overrun, Improved Critical (bite,
        slam), Improved Initiative, Improved Iron Will, Improved Overrun,
        Improved Vital Strike, Iron Will, Power Attack, Staggering Critical,
        Vital Strike, Wingover</p><p><b>Skills </b>Fly +32, Perception +28, Swim
        +26; <b>Racial Modifiers </b>+16 Perception</p><p><b>Languages </b>Auran
        (can't speak)</p><p><b>SQ </b>massive, no breath, powerful blows (bite,
        slam), starflight</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any cold (the world's moon or outer
        space)</p><p><b>Organization </b>solitary (unique)</p><p><b>Treasure
        </b>incidental</p></div><hr /><div><p><b>SPECIAL
        ABILITIES</b></p></div><hr /><div><p><b>Clinging Frost (Su)</b> Whenever
        a creature takes cold damage from Yarthoon (including from her freezing
        mist), the creature becomes encrusted with a layer of clinging frost for
        1 round. The duration of this clinging frost stacks with multiple
        instances of cold damage from Yarthoon. As long as a creature has at
        least 1 round of clinging frost remaining, it takes a -2 penalty on all
        Reflex saving throws and Dexterity-based skill checks. As long as a
        creature has at least 2 rounds of clinging frost remaining, it is
        staggered. <i>Freedom of movement</i> negates the effects of clinging
        frost, and if a creature activates a fire-based supernatural ability on
        its turn, it reduces the number of rounds remaining by 1d4. Creatures
        with the fire subtype, the heat ability, or any similar ability to shed
        significant warmth are immune to clinging frost. </p><p><b>Eye Beams
        (Su)</b> Once every 4 rounds as a standard action, Yarthoon can emit
        several beams of freezing energy from her eyes. When Yarthoon uses this
        attack, she can choose to fire all of the beams in one direction to
        create a single line 1,200 feet long, or she can instead fire eight
        separate beams as ranged touch attacks with a range of 1,200 feet. If
        she chooses to fire them in a line, all creatures within the area of
        effect take 20d6 points of cold damage (Reflex DC 38 half). If she fires
        the beams as ranged touch attacks, she has a +30 attack bonus but can
        target a single creature with no more than two eye beams at a time
        (though she can fire the beams in any direction to attack multiple
        targets in range). A single eye beam deals 8d6 points of cold damage on
        a hit (no save). The save DC for the line-based attack is
        Constitution-based. </p><p><b>Freezing Mist (Su)</b> Once per day as a
        swift action, Yarthoon can exhale a cloud of freezing mist, filling a
        200-foot-radius sphere surrounding her. This mist obscures vision as per
        <i>obscuring mist</i> and persists for 10 rounds, and is not dispersed
        by moderate wind. A strong wind (21+ mph) disperses the mist in 2d4
        rounds, while <i>fireball</i>, <i>flame strike</i>, or similar spells
        burn away the mist in the spell's area. Any creature within the area of
        the mist when Yarthoon creates it takes 8d6 points of cold damage
        (Reflex DC 38 half). A creature that begins its turn within the mist
        takes 4d6 points of cold damage (no save) at the start of its turn. The
        save DC is Constitution-based. </p><p><b>Penetrating Cold (Su)</b> When
        Yarthoon deals cold damage, the damage ignores the first 30 points of
        cold resistance the target has. </p><p><b>Staggering Strike (Ex)</b> If
        Yarthoon hits a Gargantuan or smaller foe that's standing on the ground
        with her slam attack, the target struck must succeed at a DC 42 Reflex
        save or be knocked prone and become staggered for 1d6 rounds. If she
        hits a Gargantuan or smaller flying foe, the creature is staggered for
        1d6 rounds and must succeed at a DC 42 Fly check or lose 30 feet of
        altitude (a winged flying creature loses 60 feet of altitude instead;
        this replaces the normal rule for being attacked while flying for winged
        creatures). A Colossal creature struck by Yarthoon's slam must succeed
        at a DC 42 Reflex save to resist being staggered for 1 round, but is not
        knocked prone or forced to lose altitude. The save DC is Strength-based.
        </p><p><b>Starflight (Su)</b> Yarthoon can survive in the void of outer
        space. She flies through space at incredible speeds, but generally only
        does so to travel between the moon and the world below when her
        attention is caught or her curiosity piqued. It takes Yarthoon only 2d4
        hours to travel from the moon to the world below. If she were to travel
        to a more distant point in the same solar system, she requires only 3d6
        days to make the journey, while trips beyond this range generally take
        her 3d6 weeks. </p><p><b>Swift Bite (Ex)</b> Yarthoon strikes with
        astonishing speed when she attacks with her bite. Whenever Yarthoon
        makes a bite attack on her turn, she can attack two times, either as a
        standard action to bite twice, or as part of a full attack to bite two
        times in addition to making one slam attack. She can make any number of
        attacks of opportunity with her bite attack, but does not get to bite
        more than once when she does so.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Yarthoon is
        among the least powerful of the kaiju, yet even she is of staggering
        size and capable of unleashing devastation on an apocalyptic scale.
        Known as the Moon Grub to kaiju scholars, Yarthoon does indeed dwell
        upon the moon itself, where she spends much of her time in a frozen
        reach of ice and snow, created, in part, by her presence and the effects
        of her freezing breath over the course of centuries. (Note that although
        Yarthoon's exhalations fill the region with freezing mist, the Moon Grub
        herself has no need to breathe and can exist comfortably in the vacuum
        of space.) Were Yarthoon content to remain upon the moon, there would be
        little known about the frozen kaiju; unfortunately, the larger world her
        home orbits is a constant fascination for her. Indeed, certain events
        have been known to specifically attract Yarthoon's attention, whether
        they're intentional calls such as powerful rituals led by apocalyptic
        cultists, or accidental lures that occur when powerful effects of
        magical cold occur. The exact nature of these attractions varies, and
        even the most learned scholars argue over what exactly it is that draws
        Yarthoon to visit the world. None, however, dispute the devastation
        Yarthoon unleashes, if unintentionally, when she visits the world.
        Unlike many kaiju, Yarthoon seems not to purposefully seek out
        civilizations to destroy, but her immense size and her freezing breath
        wreak havoc nonetheless. Fortunately, Yarthoon's visits to the world
        typically last for only a few days before she slithers off into the sky
        like an eel swimming through water to return to her den on the
        moon.</p><p>Yarthoon has a complex relationship with Mogaru, the Final
        King (<i>Pathfinder RPG Bestiary 4</i> 170). Often, Mogaru's devastation
        on a region is enough to lure Yarthoon down from the moon, in which case
        she often clashes with the more powerful kaiju. Likewise, Mogaru's
        ability to sense kaiju often results in him coming to investigate a
        region that Yarthoon has decided to visit. Yet the two kaiju never seem
        willing to finish a fight against the other, and rather than deliver a
        death blow when the chance rises, each seems content to let the other
        flee. The one thing that seems to unite the two kaiju is their shared
        hatred of Lord Varklops, and the two kaiju have teamed up several times
        to drive off the three-headed fiend more than once.</p><p>Although
        Yarthoon is the least powerful of the known kaiju, the Moon Grub has
        been remarkably resilient. Time and time again, Yarthoon has returned
        after suffering what seemed to be a complete defeat, at either the hands
        of powerful heroes or the claws and fangs of fellow kaiju. Scholars of
        these immense monsters have theorized that Yarthoon may not be a unique
        creature, and that in its hidden lair on the moon, multiple Moon Grubs
        writhe and dream. Others posit that Yarthoon is but the larval stage of
        a kaiju, and that while only one may live at any time, numerous other
        eggs lie in a hidden crèche on the moon, waiting to hatch and release
        replacement Moon Grubs as needed.</p><p>Yarthoon is 250 feet long from
        head to tail and weighs 14,000 tons.</p></h4></div></section>
    _id: U09pfbS3KXXJBxFy
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!55zo1DbvThDhtff0.U09pfbS3KXXJBxFy'
sort: 0
_key: '!journal!55zo1DbvThDhtff0'

