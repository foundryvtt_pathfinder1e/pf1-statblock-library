name: Giant Ant Spore Zombie (CR 3)
_id: cb2rSaW6u2g5EO01
pages:
  - name: Cover
    type: image
    src: systems/pf1/icons/races/creature-types/undead.png
    title:
      show: false
      level: 1
    _id: tIm6VcCb99YNJe4t
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!cb2rSaW6u2g5EO01.tIm6VcCb99YNJe4t'
  - name: Giant Ant Spore Zombie (CR 3)
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>PFRPG Bestiary 6</p><section
        id="GiantAntSporeZombie"><div class="heading"><p
        class="alignright"><strong>CR</strong> 3</p></div><div><p><b>XP
        </b>800</p><p>CE Medium undead (augmented vermin)</p><p><b>Init </b>+4;
        <b>Senses </b>darkvision 60 ft., scent; Perception +12</p></div><hr
        /><div><p><b>DEFENSE</b></p></div><hr /><div><p><b>AC </b>17, touch 10,
        flat-footed 17 (+7 natural)</p><p><b>hp </b>34 (4d8+16)</p><p><b>Fort
        </b>+4, <b>Ref </b>+3, <b>Will </b>+5</p><p><b>Immune </b>undead
        traits</p></div><hr /><div><p><b>OFFENSE</b></p></div><hr
        /><div><p><b>Spd </b>50 ft., climb 20 ft.</p><p><b>Melee </b>bite +7
        (1d6+4 plus grab), sting +7 (1d4+4 plus poison)</p><p><b>Space </b>5
        ft.; <b>Reach </b>5 ft.</p><p><b>Special Attacks </b>spore burst (DC
        15)</p></div><hr /><div><p><b>STATISTICS</b></p></div><hr
        /><div><p><b>Str </b>18, <b>Dex </b>10, <b>Con </b>-, <b>Int </b> 10,
        <b>Wis </b>13, <b>Cha </b>17</p><p><b>Base Atk </b>+3; <b>CMB </b>+7
        (+11 grapple); <b>CMD </b>18 (26 vs. trip)</p><p><b>Feats </b>Improved
        Initiative, Lightning Reflexes, ToughnessB</p><p><b>Skills </b>Climb
        +19, Perception +12, Stealth +7, Survival +9; <b>Racial Modifiers </b>+4
        Perception, +4 Survival</p><p><b>Languages </b>Abyssal (can't
        speak)</p></div><hr /><div><p><b>ECOLOGY</b></p></div><hr
        /><div><p><b>Environment </b> any</p><p><b>Organization </b>solitary or
        bloom (2-12)</p><p><b>Treasure </b>none</p></div><hr
        /><div><p><b>SPECIAL ABILITIES</b></p></div><hr /><div><p><b>Poison
        (Ex)</b> Sting-injury; <i>save</i> Fort DC 15; <i>frequency</i> 1/round
        for 4 rounds; <i>effect</i> 1d2 Str damage; <i>cure</i> 1
        <i>save</i>.</p></div><hr /><div><strong>DESCRIPTION</strong></div><hr
        /><div><h4><p>There are certain evil fungal creatures (such as fungus
        queens [see page 130], but also rare fungal growths or extraplanar
        blights upon the wild) that can infest vermin with spores that have been
        infused with sinister power and negative energy. These foul spores grow
        quickly in the body of a dead vermin, eventually bursting from its head
        to form disturbing, antler-like growths. At the same time, the spores
        animate the vermin as an intelligent undead creature. These are then
        known as spore zombies. <br /><b>CREATING A SPORE ZOMBIE </b><br />
        "Spore zombie" is an acquired template that can be added to any vermin,
        which is referred to hereafter as the base creature. <br /><b>Challenge
        Rating:</b> The base creature's CR + 1. <br /><b>Alignment:</b> Always
        chaotic evil. <br /><b>Type:</b> The creature's type changes to undead.
        It retains any subtypes and gains the augmented subtype. <br /><b>Armor
        Class:</b> A spore zombie gains a +2 bonus to the base creature's
        natural armor. <br /><b>Hit Dice:</b> The base creature's Hit Dice + 2.
        <br /><b>Saves:</b> The creature's base save bonuses are Fort +1/3 Hit
        Dice, Ref +1/3 Hit Dice, and Will +1/2 Hit Dice + 2. <br /><b>Defensive
        Abilities:</b> Spore zombies gain all of the qualities and immunities
        granted by the undead type, and retain all defensive abilities that the
        base creature had. Attacks: A spore zombie retains all of the base
        creature's natural attacks. <br /><b>Special Attacks:</b> A spore zombie
        retains all of the base creature's special attacks, plus the following
        (any special attack save DCs that are Constitution-based are now
        Charisma-based). <br /><i>Spore Burst (Ex)</i>: Once per day as a swift
        action, a spore zombie can spray a cloud of spores through the area.
        This deals 2d6 points of damage to the spore zombie and creates a cloud
        of spores that fills an area equal to the spore zombie's reach. Any
        creature in this area must succeed at a Fortitude save or be nauseated
        by the spores for 1d6 rounds. Vermin that fail this save become infested
        for 24 hours. If an infested vermin dies during this time, it rises as a
        spore zombie 1d6 rounds after its death. <br /><b>Abilities:</b>
        Strength +4. A spore zombie gains an Intelligence score of 10 and a
        Charisma score equal to the base creature's Constitution score. They do
        not have a Con score. <br /><b>Feats:</b> A spore zombie gains feats as
        appropriate for its Hit Dice, and gains Toughness as a bonus feat. <br
        /><b>Skills:</b> A spore zombie gains skill points equal to 4 + Int
        modifier per Hit Die (4 points per HD for most). Climb, Fly, Perception,
        and Stealth are class skills. <br /><b>Languages:</b> A spore zombie can
        understand Abyssal but can't speak.</p></h4></div></section>
    _id: CnUSnkrj7uF0MmbR
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!cb2rSaW6u2g5EO01.CnUSnkrj7uF0MmbR'
sort: 0
_key: '!journal!cb2rSaW6u2g5EO01'

