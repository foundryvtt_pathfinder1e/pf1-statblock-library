name: Blood Lily
_id: z0M3DQHxlsRlnopp
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Ruins_of_Azlant.jpg
    title:
      show: false
      level: 1
    _id: 86hQFkX9sn1sz4Hp
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!z0M3DQHxlsRlnopp.86hQFkX9sn1sz4Hp'
  - name: Blood Lily
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 124</p><section id="BloodLily"><div
        class="heading"><p class="alignright"><strong>CR</strong>
        10</p></div><div><p><strong>XP </strong>9,600</p><p>N Large plant
        (aquatic)</p><p><strong>Init </strong>+4; <strong>Senses
        </strong>low-light vision, tremorsense 60 ft.; Perception
        +8</p></div><hr /><div><p><strong>DEFENSE</strong></p></div><hr
        /><div><p><strong>AC </strong>25, touch 13, flat-footed 21 (+4 Dex, +12
        natural, -1 size)</p><p><strong>hp </strong>120
        (16d8+48)</p><p><strong>Fort </strong>+13, <strong>Ref </strong>+9,
        <strong>Will </strong>+3</p><p><strong>Defensive Abilities
        </strong>camouflage; <strong>Immune </strong>plant traits</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>30 ft., swim 30 ft.</p><p><strong>Melee </strong>2 slams +16
        (2d6+4 plus poison and trip)</p><p><strong>Ranged </strong>4 petal
        spikes +16 (1d8/19-20 plus bleed)</p><p><strong>Space </strong>10 ft.;
        <strong>Reach </strong>10 ft. (20 ft. with slam)</p><p><strong>Special
        Attacks </strong>bleed (1d8), petal spikes, poison, trip</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>18, <strong>Dex </strong>19, <strong>Con </strong>16,
        <strong>Int </strong> 2, <strong>Wis </strong>7, <strong>Cha
        </strong>9</p><p><strong>Base Atk </strong>+12; <strong>CMB
        </strong>+17; <strong>CMD </strong>31</p><p><strong>Feats
        </strong>Ability Focus (poison), Combat Reflexes, Improved Critical
        (petal spike), Point-Blank Shot, Power Attack, Precise Shot, Weapon
        Focus (petal spike), Weapon Focus (slam)</p><p><strong>Skills
        </strong>Perception +8, Stealth +12 (+20 when among vegetation), Swim
        +12; <strong>Racial Modifiers </strong>+8 Stealth when among
        vegetation</p><p><strong>SQ </strong>amphibious</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> warm coastlines or
        underwater</p><p><strong>Organization </strong>solitary or pod
        (2-5)</p><p><strong>Treasure </strong>incidental</p></div><hr
        /><div><p><strong>SPECIAL ABILITIES</strong></p></div><hr
        /><div><p><strong>Camouflage (Ex)</strong> A blood lily can contract its
        distinctive scarlet spikes into a small bundle. By hiding the bundle
        within its vines, the blood lily can easily pass as an ordinary plant.
        This ability grants the blood lily a +8 racial bonus on Stealth checks
        when among vegetation.  </p><p><strong>Petal Spikes (Ex)</strong> A
        blood lily's petals are razor sharp, and as a standard action, the plant
        can launch up to four petals as a ranged attack (with a separate attack
        roll for each petal). This attack has a range of 180 feet with no range
        increment; the range is halved underwater. All targets must be within 30
        feet of each other. The blood lily can launch only 16 spikes in any
        24-hour period.  </p><p><strong>Poison (Ex)</strong> Slam-injury;
        <i>save</i> Fort DC 23; <i>frequency</i> 1/round for 6 rounds;
        <i>effect</i> 1d4 Con damage and sickened; <i>cure</i> 2 consecutive
        <i>save</i>s. A blood lily's poison causes the victim to bleed copiously
        from its pores. Each time the victim fails its save against the blood 
        lily's poison, it becomes sickened as blood coats its entire body and
        trickles into its mouth. The save DC is Constitution-based.</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Many an
        unsuspecting victim has been lured to a blood lily by its large radiant
        blossom-a ball of scarlet spikes reminiscent of a sea urchin. Those who
        live near blood lilies know to be wary of the plants, but as nonanimate
        blood lilies also grow in such areas, it can be difficult to tell the
        dangerous plants from the inert ones. To complicate matters, blood
        lilies have learned to hide their scarlet petals when hunting. A
        favorite trick of these plants is to disguise themselves next to
        ordinary blood lilies, using their harmless kin as a distraction. When
        travelers pass by, the predatory blood lilies then attack from
        concealment.  Blood lilies commonly grow in tropical areas near
        coastlines, where they hunt both in and out of the water. Fish and other
        aquatic creatures are drawn to the blood lily's brilliant petals,
        sometimes mistaking them for colorful tropical fish. Blood lilies can
        slaughter and consume dozens of small fish in under a minute if a school
        draws close enough. Though blood lilies devour such prey whole, the
        masticating action of the edges of a blood lily's digestive orifice
        leaves clouds of blood in the water. Their blossoms also attract giant
        insects, which can be dangerous and damaging to the plant creatures.
        Blood lilies can easily defend against most of these vermin, but they
        almost never eat insects, preferring warm-blooded prey above all
        others.  Blood lilies have an animalistic cunning and can use
        surprisingly sophisticated tactics for plants. Blood lilies that grow
        near the settlements of intelligent creatures (such as humans on land or
        merfolk underwater) keep their petals hidden and hide among other plants
        to better ambush their prey. When they grow in remote areas far from any
        settlements, they keep their petals exposed to draw in animals. Blood
        lilies remember the location of fertile hunting grounds and adapt their
        tactics based on the prey they seek. Surprisingly mobile, they sometimes
        travel long distances to hunt and feed at these fertile locations. A few
        naturalists have recorded rare blood lilies that never remain in the
        same area for long, roaming far and wide until they mature enough to
        enter a transformative hibernation (see page 85).  A blood lily is
        composed of a thick trunk plated with extremely tough but flexible green
        bark. An orifice lined with large thorns splits the trunk about halfway
        up its length. The blood lily uses this opening to devour its prey.
        Potent acids inside the creature's trunk dissolve fleshy organic matter
        quickly, allowing the lily to eat much more than would seem possible
        given its size.  A lily's vines are covered in the same hard bark as its
        trunk, and the vines are lined with tiny thorns that seep poison. The
        blood lily gets its name not only from their vivid coloring but also
        from the disturbing effects of their toxin. Creatures affected by a
        blood lily's poison bleed from their pores, coating their skin in a
        light sheen of crimson. The blood lily focuses its attacks on bleeding
        victims, assuming they have been weakened by its toxin. However, a blood
        lily cannot distinguish between a creature bleeding from the blood
        lily's poison and one bleeding from a wound. Individuals traveling in
        areas where blood lilies are common sometimes carry bladders of animal
        blood to splash nearby during combat, confusing any blood lilies as they
        attack.  Though most blood lilies live and hunt alone, they occasionally
        come together to form small pods. In particularly rich hunting grounds
        where the lilies don't have to compete for prey, they work together in
        pods to bring down victims. Pods are adept at felling large creatures or
        groups of smaller creatures by working in tandem. Though a pod's tactics
        are simple, it can take on even dangerous creatures. The blood lilies in
        a pod spread out and conceal their petals until their target is in
        range, and then they all unleash their petal barrages at once. The
        nearest lily then moves to attack with its vine lash while the other
        lilies continue to fire petal spikes until they deplete their reserves.
        All lilies then move in to finish off their victim with vine lashes. 
        Blood lilies reproduce asexually. Once a lily matures, it begins to grow
        a small bulb-like structure called a bulbil near the base of its stem,
        protected by its foliage. The bulbil takes 12 to 18 months to mature, at
        which point it drops off the parent plant and grows into a mature blood
        lily within a year. If released on land, a bulbil takes root in the
        soil, from which it draws sustenance; if released underwater, it drifts
        with the currents, drawing sustenance from the water as it grows. Blood
        lilies don't protect their released bulbils; many are consumed by
        predators, and in areas where hunting is poor, adult blood lilies might
        even consume their own immature sprouts. If a sprout reaches maturity,
        it gains the ability to move under its own power and begins to hunt. 
        Mature blood lilies can live for decades if hunting remains good and
        they aren't killed by other predators. A blood lily that survives for at
        least 10 years sometimes enters a hibernation phase during which its
        petals fall off and its vines wrap around its trunk, forming a
        cocoon-like structure.  After a season of dormancy, the blood lily
        sprouts larger, darker petals that carry the same toxin as the thorns in
        its vines. The blood lily's vines undergo a growth spurt, gaining an
        extra 5 feet in length. These blood lilies are hardier than their lesser
        kin and can challenge even the deadliest predators. They have the
        advanced creature simple template, they add their poison to attacks with
        their petal spikes, and their reach with slam attacks increases by 5
        feet.</p></h4></div></section>
    _id: QQfBWK8bDLRm7SUT
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!z0M3DQHxlsRlnopp.QQfBWK8bDLRm7SUT'
sort: 0
_key: '!journal!z0M3DQHxlsRlnopp'

