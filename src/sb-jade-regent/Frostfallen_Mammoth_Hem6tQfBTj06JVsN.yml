name: Frostfallen Mammoth
_id: Hem6tQfBTj06JVsN
pages:
  - name: Cover
    type: image
    src: modules/statblock-library/assets/Covers/Jade_Regent.jpg
    title:
      show: false
      level: 1
    _id: Sn4Vhuu63GS2WUsb
    image: {}
    text:
      format: 1
    video:
      controls: true
      volume: 0.5
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!Hem6tQfBTj06JVsN.Sn4Vhuu63GS2WUsb'
  - name: Frostfallen Mammoth
    type: statblock-library.statblock
    title:
      show: true
      level: 1
    text:
      format: 1
      content: >-
        <p><strong>Source </strong>AP 51</p><section
        id="FrostfallenMammoth"><div class="heading"><p
        class="alignright"><strong>CR</strong> 10</p></div><div><p><strong>XP
        </strong>9,600</p><p>NE Huge undead (cold)</p><p><strong>Init
        </strong>+1; <strong>Senses </strong>darkvision 60 ft., lifesense;
        Perception +0</p></div><hr
        /><div><p><strong>DEFENSE</strong></p></div><hr /><div><p><strong>AC
        </strong>25, touch 9, flat-footed 26 (+1 Dex, +16 natural, -2
        size)</p><p><strong>hp </strong>91 (14d8+28)</p><p><strong>Fort
        </strong>+5, <strong>Ref </strong>+5, <strong>Will
        </strong>+9</p><p><strong>DR </strong>10/bludgeoning; <strong>Immune
        </strong>cold, undead traits</p><p><strong>Weaknesses
        </strong>vulnerable to fire</p></div><hr
        /><div><p><strong>OFFENSE</strong></p></div><hr /><div><p><strong>Spd
        </strong>40 ft.</p><p><strong>Melee </strong>gore +21 (2d8+13 plus 3d6
        cold), slam +21 (2d6+13 plus 3d6 cold)</p><p><strong>Space </strong>15
        ft.; <strong>Reach </strong>15 ft.</p><p><strong>Special Attacks
        </strong>cold</p></div><hr
        /><div><p><strong>STATISTICS</strong></p></div><hr /><div><p><strong>Str
        </strong>36, <strong>Dex </strong>12, <strong>Con </strong>-,
        <strong>Int </strong> -, <strong>Wis </strong>10, <strong>Cha
        </strong>12</p><p><strong>Base Atk </strong>+10; <strong>CMB
        </strong>+25; <strong>CMD </strong>36 (40 vs. trip)</p><p><strong>Feats
        </strong>ToughnessB</p><p><strong>SQ </strong>lifesense</p></div><hr
        /><div><p><strong>ECOLOGY</strong></p></div><hr
        /><div><p><strong>Environment </strong> any cold
        land</p><p><strong>Organization </strong>solitary, pair, or drove
        (3-12)</p><p><strong>Treasure </strong>none</p></div><hr
        /><div><strong>DESCRIPTION</strong></div><hr /><div><h4><p>Frostfallen
        creatures are mindless undead infused with icy cold and animated by a
        hatred for all living things. Their bodies radiate a devastating chill
        that cloaks them in patches of ice that act as armor. Frostfallen
        creatures appear otherwise as they did at the time of their reanimation,
        except for a cold gleam in the eyes. 
        <strong></strong></p><p><strong>Ecology</strong></p><p>  In the harshest
        reaches of the world, cold is an ever-present enemy. When a creature
        dies from exposure to such harsh conditions, bitter anger and the
        searing cold sometimes combine to reanimate the dead as one of the
        frostfallen. Once reanimated, frostfallen creatures prowl the cold lands
        in which they fell, wreaking indiscriminate vengeance on living
        creatures out of spiteful rage.  Although frostfallen creatures can be
        created through necromantic magic like other undead, arcane scholars are
        fascinated by the necromancy that brings frostfallen creatures to
        unlife, for the animating force is a synthesis of negative energy and a
        bone-numbing cold. This cold empowers the creature's attacks, wreathing
        its natural weapons in a deathly chill. Those who have fought these
        creatures describe great wounds instantly numbed by the lethal touch of
        frostbite. In addition, a frostfallen creature's body emanates an aura
        of cold that flash-freezes moisture in the air around it, coating the
        creature in a patchwork of ice capable of def lecting blows from even
        the heaviest weapons. The creature's affinity for cold also allows it to
        locate nearby warm-blooded creatures, even in driving blizzards and on
        moonless nights.  Frostfallen creatures move with the same gaits they
        had in life, while gaining a savage surge in physical power.
        Additionally, unlike normal zombies, the frozen condition of the
        creature fortifies its unlife, making it extremely durable. Indeed,
        throughout the northern reaches of Avistan, legends persist of specific
        frostfallen mammoths, dire bears, and even giants that have hunted the
        Crown of the World for centuries.  
        <strong></strong></p><p><strong>Habitat &amp; Society</strong></p><p> 
        The corrupt animus that powers frostfallen creatures imbues them with a
        hatred of all living things, and most spend their time wandering wintry
        terrains near where they died, looking for life to snuff out. Sometimes,
        vestiges of behavior from their original forms lead to groups banding
        together, like frostfallen wolves that hunt in packs. Even those
        frostfallen that have never encountered others of their kind seem to
        instinctively recognize their own, and band together if prey is nearby. 
        In Irrisen, winter witches and other ice casters prefer creating
        frostfallen creatures, as such creatures' bodies more readily withstand
        the decay and brittleness often faced by skeletons and zombies in harsh
        northern climes. Casters who create frostfallen creatures often use
        minor magic to emblazon a creature's ice armor with carvings or specific
        colors, effectively branding the creature as a servant.  On rare
        occasions, casters will create frostfallen creatures in warmer regions
        like the Mwangi Expanse or the deserts of Thuvia. Despite the scorching
        heat of such places, frostfallen creatures retain all of their powers.
        In such cases, the undead's creator normally employs her minions against
        creatures that are susceptible to the frostfallen creature's freezing
        attacks. Some frostfallen creatures have even survived independent of
        their creators. When such a rarity comes to pass, the frostfallen
        creature simply begins hunting the region for fresh life to end, though
        it will often lie in wait in cooler places like deep caves, or under the
        water of an oasis so as to better ambush creatures and travelers who
        would never expect to see a frostfallen creature so far south.  <br
        /><strong>Animating a Frostfallen Creature</strong><br />  A magic-user
        can create any form of frostfallen creature by casting <i>animate
        dead</i> upon the corpse to be animated and providing an amount of ice
        of equal weight, plus two blue topazes or turquoises worth at least 100
        gp each. The creator can only create a number of Hit Dice of frostfallen
        creatures equal to the amount allowed by <i>animate dead</i>.
        Frostfallen creatures count against the number of Hit Dice of skeletons
        and zombies that can be created using <i>animate dead</i>.  <br
        /><strong>Creating a Frostfallen Creature</strong><br />  "Frostfallen
        creature" is an acquired template that can be added to any corporeal
        creature (other than an undead), referred to hereafter as the base
        creature. A frostfallen creature uses the base creature's statistics and
        special abilities except as noted here.  <br /><strong>CR:</strong> As
        base creature + 1.  <br /><strong>Alignment:</strong> Always neutral
        evil.  <br /><strong>Type:</strong> The creature's type changes to
        undead and it gains the cold subtype. It retains any subtypes except for
        alignment subtypes (such as good) and subtypes that  indicate kind (such
        as giant). It does not gain the augmented subtype. It uses all the base
        creature's statistics and special abilities except as noted here.  <br
        /><strong>Armor Class:</strong> The natural armor bonus improves by +4. 
        Hit Dice: A frostfallen creature drops any HD gained from class levels
        and changes racial HD to d8s. Creatures without racial HD are treated as
        if they had 1 racial HD. A skeleton uses its Charisma modifier (instead
        of its Constitution modifier) to determine bonus hit points.  <br
        /><strong>Defensive Abilities:</strong> Frostfallen creatures gain DR 5/
        bludgeoning (or DR 10/bludgeoning if it has 11 HD or more). They are
        immune to cold.  <br /><strong>Weaknesses:</strong> Frostfallen
        creatures gain vulnerability to fire.  <br /><strong>Speed:</strong> A
        winged frostfallen creature's maneuverability drops to clumsy. If the
        base creature flew magically, its fly speed is unchanged. Retain all
        other movement types.  <br /><strong>Attacks:</strong> A frostfallen
        creature retains all the natural weapons, manufactured weapon attacks,
        and weapon proficiencies of the base creature. It also gains a slam
        attack that deals damage based on the frostfallen creature's size, but
        as if it were one size category larger than its actual size.  <br
        /><strong>Special Attacks:</strong> A frostfallen creature retains all
        of the base creature's special attacks and also gains the following. 
        <br /><i>Cold</i> (Su): A frostfallen creature's body generates intense
        cold, dealing an amount of cold damage with its touch determined by its
        Hit Dice. Creatures attacking a frostfallen creature with unarmed
        strikes or natural weapons take this same cold damage each time one of
        their attacks hits.  </p><table><tbody><tr><th>Frostfallen
        Creature</th></tr><tr><td>Hit Dice</td><td>Cold
        Damage</td></tr><tr><td>1-5</td><td>1d6</td></tr><tr><td>6-10</td><td>2d6</td></tr><tr><td>11-15</td><td>3d6</td></tr><tr><td>16+</td><td>4d6</td></tr></tbody></table>
        <br /><strong>Abilities:</strong> A frostfallen creature's Strength
        increases by +2 and its Charisma by +6. A frostfallen creature has no
        Constitution or Intelligence score, and its Wisdom becomes 10.  <br
        /><strong>BAB:</strong> A frostfallen creature's base attack bonus is
        equal to 3/4 of its Hit Dice.  <br /><strong>Skills:</strong> A
        frostfallen creature loses all skill ranks possessed by the base
        creature and gains none of its own.  <br /><strong>Feats:</strong> A
        frostfallen creature loses all feats possessed by the base creature and
        gains Toughness as a bonus feat.  <br /><strong>Special
        Qualities:</strong> A frostfallen creature loses most special qualities
        of the base creature. It retains any extraordinary special qualities
        that improve its melee or ranged attacks. It also gains the following
        special quality. <br /> <i>Lifesense (Su)</i>: The frostfallen creature
        notices and locates living creatures within 60 feet, just as if it
        possessed the blindsight ability.</h4></div></section>
    _id: IB4ajX24nbuwUZmf
    image: {}
    video:
      controls: true
      volume: 0.5
    src: null
    system: {}
    sort: 0
    ownership:
      default: -1
    flags: {}
    _stats:
      systemId: null
      systemVersion: null
      coreVersion: null
      createdTime: null
      modifiedTime: null
      lastModifiedBy: null
    _key: '!journal.pages!Hem6tQfBTj06JVsN.IB4ajX24nbuwUZmf'
sort: 0
_key: '!journal!Hem6tQfBTj06JVsN'

